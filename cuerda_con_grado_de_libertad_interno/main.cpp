// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "qew_minimal_solucion.h"

//#define Deltatrun	560000
//#define Deltateq	40000

#define Deltatrun	100000
#define Deltateq	5000

int main()
{
	Cuerda C;
	C.init(); 

	std::ofstream velout("velforce.dat");
	thrust::tuple<REAL,REAL> v;

	C.set_T0(0.03);

	C.set_f0(0.5); //walker field

	float cmu0,cmphi0;
	cmu0=cmphi0=0.0;

	for(int i=0;i<500;i++)
	{
		C.dynamics(Deltateq); // equilibration

		v=C.dynamics(Deltatrun);

		// u-properties
		C.set_whichone(0);
		float cmushifted = C.get_center_of_mass();  
		float cmu=cmushifted-(REAL)C.get_ushift();
		float widthu=C.get_roughness(cmushifted);

		// phi-properties
		C.set_whichone(1); 
		float cmphishifted = C.get_center_of_mass();  
		float cmphi=cmphishifted+(REAL)C.get_phishift();
		float widthphi=C.get_roughness(cmphishifted);

		velout << 
		C.get_f0() << " " << C.get_T0() << " " << get<0>(v) << " " << get<1>(v)*M_PI << " "
		<< cmu-cmu0 << " " << (cmphi-cmphi0)*M_PI << " "
		<< widthu << " " << widthphi*(M_PI*M_PI) 
		<< std::endl;

		cmu0=cmu; cmphi0=cmphi;

		//C.increment_f0(0.02);
		C.increment_T0(-0.0001);
	}
	C.print_timmings();
	return 0;
}
