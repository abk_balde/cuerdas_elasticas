/////////////////////////////////////////////////////////////////////////////
// FUNCTORS usados en los algoritmos TRANSFORM

/*
		transform(
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(0),
			u_it0-1,u_it0,u_it0+1,
			phi_it0-1,phi_it0,phi_it0+1
			)),
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(L),
			u_it1-1,u_it1,u_it1+1,
			phi_it1-1,phi_it1,phi_it1+1
			)),
			make_zip_iterator(make_tuple(
			Ftot_it0,Ftotphi_it0
			))
			fuerza_uphi(n,f0,T0,lambda,kperp,alpha) 
		);
*/

struct fuerza_uphi
{
    RNG rng;       // random number generator
    long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL f0;	
    REAL T0;
	REAL lambda;
	REAL kperp;	
	REAL alpha;
	unsigned ushift;
	unsigned phishift;
	REAL cmphi, cmu;
		
    fuerza_uphi(long _t, float _f0, float _T0, float _lambda, float _kperp, float _alpha, unsigned _ushift, unsigned _phishift)
	:tiempo(_t),f0(_f0),T0(_T0),lambda(_lambda),kperp(_kperp),alpha(_alpha),ushift(_ushift),phishift(_phishift) 
    {
	noiseamp=sqrt(T0/Dt);
    }; 

// overloaded constructor for mean field dynamics...
    fuerza_uphi(long _t, float _f0, float _T0, float _lambda, float _kperp, float _alpha, unsigned _ushift, unsigned _phishift, float _cmu, float _cmphi)
	:tiempo(_t),f0(_f0),T0(_T0),lambda(_lambda),kperp(_kperp),alpha(_alpha),ushift(_ushift),phishift(_phishift), cmu(_cmu), cmphi(_cmphi) 
    {
	noiseamp=sqrt(T0/Dt);
    }; 


    __device__
    thrust::tuple<REAL, REAL> operator()(tuple<long,REAL,REAL,REAL,REAL,REAL,REAL> tt)
    {	
	// thread/particle id
	uint32_t tid=get<0>(tt); 

	// keys and counters 
    RNG::ctr_type c={{}};
    RNG::key_type k={{}};
	RNG::ctr_type r;

	c[1]=SEED; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}

	REAL um1=get<1>(tt);
	REAL u=get<2>(tt);
	REAL up1=get<3>(tt);

	REAL phim1=get<4>(tt);
	REAL phi=get<5>(tt);
	REAL phip1=get<6>(tt);

	// LAPLACIAN
	REAL laplaciano,laplaciano_phi;
	laplaciano = laplaciano_phi = 0.0;
	#ifndef NOELASTICITYU
	laplaciano = um1 + up1 - 2.*u;
	#endif
	#ifndef NOELASTICITYPHI
	laplaciano_phi = (phim1 + phip1 - 2.*phi)*M_PI;
	#endif


	// DISORDER
	REAL quenched_noise =0.0;
#ifndef NODISORDER
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	REAL firstRN, secondRN;
	int U=int(u);
	c[0]=uint32_t(U)+ushift; // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	firstRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	c[0]=uint32_t(U+1)+ushift; // COUNTER={U+1,GLOBAL SEED}
	r = rng(c, k);
	secondRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	quenched_noise = D0*(firstRN - (firstRN-secondRN)*(u-U));// linearly interpolated force
#endif

	REAL quenched_noise_phi = 0.5*sin(2.0*M_PI*phi)*kperp;

	// THERMAL NOISE
	REAL thermal_noise, thermal_noise_phi;

// TODO: agregar -DFINITETEMPERATURE en el Makefile para agregar 
// fluctuaciones termicas pero antes corregir el FIXME siguiente! 
#ifdef FINITETEMPERATURE
	// FIXME: Lo de abajo tiene un error grave!!. Corrijalo.
	// c[0]=tid; // COUNTER={tid, GLOBAL SEED #2} 
	// SOLUCION: lo correcto es usar el indice del tiempo como counter! (el ruido termico NO se repite).

	k[0]=tid; 	  //  KEY = {threadID} 
	c[0]=uint32_t(tiempo); // COUNTER={tiempo, GLOBAL SEED #2}
	
	c[1]=SEED2; // para evitar correlaciones entre el ruido térmico y el desorden congelado...
	r = rng(c, k);
	thermal_noise = noiseamp*box_muller(r);    			

	c[1]=SEED2+1; // para evitar correlaciones entre el ruido térmico y el desorden congelado...
	r = rng(c, k);
	thermal_noise_phi = noiseamp*box_muller(r)/M_PI;    			
#else
	thermal_noise=0.0;
	thermal_noise_phi=0.0;
#endif
	// Fuerza total en el monómero tid

#ifndef MM
	//return (laplaciano+quenched_noise+thermal_noise+f0);	

	thrust::tuple<REAL, REAL> velocity_tuple(
	(lambda/(1.+alpha*alpha))*(alpha*(laplaciano+quenched_noise+f0)+(laplaciano_phi+quenched_noise_phi))+lambda*thermal_noise,
	(1./(M_PI*(1.+alpha*alpha)))*(-(laplaciano+quenched_noise+f0)+alpha*(laplaciano_phi+quenched_noise_phi))+thermal_noise_phi
	);

#else
	thrust::tuple<REAL, REAL> velocity_tuple(
	(lambda/(1.+alpha*alpha))*(alpha*(laplaciano+quenched_noise+MM*(f0*tiempo*Dt-u))+(laplaciano_phi+quenched_noise_phi))+lambda*thermal_noise,
	(1./(M_PI*(1.+alpha*alpha)))*(-(laplaciano+quenched_noise+MM*(f0*tiempo*Dt-u))+alpha*(laplaciano_phi+quenched_noise_phi))+thermal_noise_phi
	);
	//return (laplaciano+quenched_noise+thermal_noise+MM*(f0*tiempo*Dt-u));
#endif
	return velocity_tuple;

  }
};




// Explicit forward Euler step: lo mas simple que hay 
// (pero ojo con el paso de tiempo que no sea muy grande!)
struct euler_uphi
{
    __device__
    thrust::tuple<REAL, REAL> operator()(tuple<REAL,REAL,REAL,REAL> tt)
	//(REAL u_old, REAL forceu, REAL phi_old, REAL forcephi)
    {
	 float u_old=get<0>(tt);
	 float forceu=get<1>(tt);
	 float phi_old=get<2>(tt);
	 float forcephi=get<3>(tt);

	 float unew=(u_old + forceu*Dt);
	 float phinew=(phi_old + forcephi*Dt);	
	 thrust::tuple<REAL, REAL> tupla(unew,phinew);
	 return tupla;
    }
};	
/*
		transform(
			make_zip_iterator(make_tuple(
			u_it0,Ftot_it0,phi_it0,Ftotphi_it0
			)),
			make_zip_iterator(make_tuple(
			u_it1,Ftot_it1,phi_it1,Ftotphi_it1
			)),
			make_zip_iterator(make_tuple(
			u_it0,phi_it0
			)),
			euler_uphi()  // functor not declared/defined yet
*/

