/*
=============================================================================================
A. B. Kolton

Segundo Encuentro Nacional de Computación de Alto Rendimiento para Aplicaciones Científicas
7 al 10 de Mayo de 2013.
=============================================================================================

Este programita simula la dinámica de una cuerdita elastica en un medio desordenado en la GPU, 
usando la librería Thrust, y la libreria Random123. Es una versión reducida de la que 
usamos con E. Ferrero y S. Bustingorry para la publicación:

Phys. Rev. E 87, 032122 (2013)
Nonsteady relaxation and critical exponents at the depinning transition
http://pre.aps.org/abstract/PRE/v87/i3/e032122
http://arxiv.org/abs/1211.7275

Y la explicación resumida del código esta en el material suplementario de la revista:
http://pre.aps.org/supplemental/PRE/v87/i3/e032122


Sin resolver los TODO ya se puede compilar con "make", y larga algunos timmings.
Genera dos ejecutables a la vez, uno de CUDA (corre en GPU) y otro de openMP (corre en CPU multicore).

OBJETIVOS:
- Practicar el manejo básico de la librería Thrust.
- Practicar el manejo básico de la librería Random123.
- Comparar performances CPU vs GPU.
- Aprender a combinar herramientas para resolver una ecuación diferencial 
parcial estocástica con desorden congelado.

EJERCICIOS:	
- Levantar los TODO.
- Para los mas expertos: Como mejoraría la performance del codigo?
*/



#include "simple_timer.h"
#include<cmath>
#include<fstream>
#include <iostream>

/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG


/* algunos headers de la libreria thrust */
// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
// https://github.com/thrust/thrust/wiki/Documentation
#include<thrust/transform.h>
#include<thrust/copy.h>
#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/tuple.h>
#include<thrust/functional.h>
#include<thrust/sequence.h>
#include <thrust/fill.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/reduce.h>
#include <thrust/extrema.h>

/* parámetros del problema */
#ifndef TAMANIO
#define L	4096   // numero de partículas/monomeros
#else
#define L	TAMANIO   // numero de partículas/monomeros
#endif
#ifndef TIEMPORUN
#define TRUN	100000       // numero de iteraciones temporales 
#else
#define TRUN	TIEMPORUN    // numero de iteraciones temporales 
#endif
#ifndef TPROP
#define TPROP	100 // intervalo entre mediciones
#endif

#ifndef F0
#define F0	0.12       // (0.12) fuerza uniforme sobre la interface/polímero 
#endif

#ifndef TEMP
#define TEMP	0.00     // temperatura	
#endif

#define LAMBDA 2.0;
#define KPERP 1.0;
#define ALPHA 1.0;

#define Dt	0.01       // paso de tiempo
#define D0	1.0	  // intensidad del desorden
#define SEED 	12345678 // global seed RNG (quenched noise)
#define SEED2 	12312313 // global seed#2 RNG (thermal noise)

#define MAXPOINTSDISPLAY	512

// to tackle too large floats problem...
#define NSHIFT	100     // check frequency
#define UMAX	10000   // maximum float allowed for reset
#define OFFSET	100     // minimum displacement after reset


// para evitar poner "thrust::" a cada rato all llamar sus funciones
using namespace thrust;

// precisón elegida para los números reales
typedef float 	REAL;

// para generar números aleatorios gausianos a partir de dos uniformes
// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
__device__
REAL box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	REAL u1 = u01_open_closed_32_53(r_philox[0]);
  	REAL u2 = u01_open_closed_32_53(r_philox[1]);

  	REAL r = sqrt( -2.0*log(u1) );
  	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);    			
}

#ifdef PRINTCONFS
// para imprimir configuraciones cada tanto...
std::ofstream configs_out("configuraciones.dat");
void print_configuration(device_vector<REAL> &u, device_vector<REAL> &Ftot, float velocity, int size, int time)
{
	long i=0;
	int every = int(L*1.0/size);
	for(i=1;i<u.size()-1;i+=every)
	{
		configs_out << u[i] << " " << Ftot[i] << " " << velocity << " " << time << std::endl;
	}
	configs_out << "\n\n";
}
#endif

// genera un path gaussiano periodico con rugosidad dada
/*struct gaussian_path{
	RNG rng;       // random number generator
	unsigned ll;
	gaussian_path(ll_):ll(ll_){

	}
	__device__
    	REAL operator()(tuple<unsigned long,unsigned long,REAL,REAL> tt)
    	{	
		// thread/particle id
		uint32_t tid=get<0>(tt); 
    	} 	
	//IN CONSTRUCTION
}*/


#include "mt.h"
void fourier_gen_path(REAL roughness, int ll, device_vector<REAL>::iterator X, unsigned long seed)
{
	init_genrand(seed);
	REAL sigma;
	int Nmodes=ll;
	thrust::host_vector<REAL> A(ll);
	thrust::host_vector<REAL> B(ll);
	thrust::host_vector<REAL> H(ll);

	thrust::fill(H.begin(),H.end(),REAL(0.0));

	std::cout << "generating modes\n";
	for(int n=0;n<Nmodes;n++){
		sigma = pow((double)ll/(2.0*M_PI*(n+1)),roughness)/sqrt(M_PI*(n+1));
		A[n] = sigma*gasdev_real3();
		B[n] = sigma*gasdev_real3();
		//std::cout << n << "\n";
	}
	std::cout << "generating vector\n";
	double sinx,cosx;
	for(int t=0;t<ll;t++){
		for(int n=0;n<Nmodes;n++){
			sincos(2.*M_PI*(n+1)*t*1.0/ll, &sinx, &cosx);
			H[t] = H[t] + A[n]*cosx + B[n]*sinx;			
		}
		if(t%100==0) std::cout << t << "\n";
	}
	REAL minimo=thrust::reduce(H.begin(),H.end(),REAL(0.0),thrust::minimum<REAL>());
	for(int t=0;t<ll;t++) H[t]+=fabs(minimo);
	thrust::copy(H.begin(),H.end(),X);
	std::cout << "done\n";
};



// functor usado para calcular la rugosidad 
struct roughtor: public thrust::unary_function<REAL,REAL>
{
    REAL u0; // un "estado interno" del functor	
    roughtor(REAL _u0):u0(_u0){};	
    __device__
    REAL operator()(REAL u)
    {	
	return (u-u0)*(u-u0);
    }
};	



/////////////////////////////////////////////////////////////////////////////
// FUNCTORS usados en los algoritmos TRANSFORM

struct fuerza
{
    RNG rng;       // random number generator
    long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL f0;	
    REAL T0;	
		
    fuerza(long _t, float _f0, float _T0):tiempo(_t),f0(_f0),T0(_T0)
    {
	noiseamp=sqrt(T0/Dt);
    }; 

    __device__
    REAL operator()(tuple<long,REAL,REAL,REAL> tt)
    {	
	// thread/particle id
	uint32_t tid=get<0>(tt); 

	// keys and counters 
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	c[1]=SEED; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}

	// LAPLACIAN
	REAL um1=get<1>(tt);
	REAL u=get<2>(tt);
	REAL up1=get<3>(tt);
	REAL laplaciano = um1 + up1 - 2.*u;

	// DISORDER
	REAL quenched_noise=0.0;
#ifndef NODISORDER
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	REAL firstRN, secondRN;
	int U=int(u);
	c[0]=uint32_t(U); // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	firstRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	c[0]=uint32_t(U+1); // COUNTER={U+1,GLOBAL SEED}
	r = rng(c, k);
	secondRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	quenched_noise = D0*(firstRN - (firstRN-secondRN)*(u-U));// linearly interpolated force
#endif
	// THERMAL NOISE
	REAL thermal_noise;

// TODO: agregar -DFINITETEMPERATURE en el Makefile para agregar 
// fluctuaciones termicas pero antes corregir el FIXME siguiente! 
#ifdef FINITETEMPERATURE
	// FIXME: Lo de abajo tiene un error grave!!. Corrijalo.
	// c[0]=tid; // COUNTER={tid, GLOBAL SEED #2} 
	// SOLUCION: lo correcto es usar el indice del tiempo como counter! (el ruido termico NO se repite).
	k[0]=tid; 	  //  KEY = {threadID} 
	c[0]=uint32_t(tiempo); // COUNTER={tiempo, GLOBAL SEED #2}

	c[1]=SEED2; // para evitar correlaciones entre el ruido térmico y el desorden congelado...
	r = rng(c, k);
	thermal_noise = noiseamp*box_muller(r);    			
#else
	thermal_noise=0.0;
#endif
	// Fuerza total en el monómero tid

#ifndef MM
	return (laplaciano+quenched_noise+thermal_noise+f0);
#else
	return (laplaciano+quenched_noise+thermal_noise+MM*(f0*tiempo*Dt-u));
#endif
  }
};




// Explicit forward Euler step: lo mas simple que hay 
// (pero ojo con el paso de tiempo que no sea muy grande!)
struct euler
{
    __device__
    REAL operator()(REAL u_old, REAL force)
    {	
	return (u_old + force*Dt);
    }
};	


#ifdef OMP
#include <omp.h>
#endif

/////////////////////////////////////////////////////////////////////////////

#include "uphi.h"

/////////////////////////////////////////////////////////////////////////////

class Cuerda
{
	public:
	#ifdef OMP
	omp_timer t;
	#else
	gpu_timer t;
	#endif

	double timer_fuerzas_elapsed;
	double timer_euler_elapsed;
	double timer_props_elapsed;

	Cuerda(){};

	void init(){
		#ifdef OMP
		std::cout << "#conociendo el host, OMP threads = " << omp_get_max_threads() << std::endl;
		#endif		
		
		/////////////
		// STRING "U"
		u.resize(L+2);
		u_it0 = u.begin()+1;
		u_it1 = u.end()-1;
		u_raw_ptr = raw_pointer_cast(&u[1]);

		// container de fuerza total: 
		Ftot.resize(L); 
		// el rango de interés definido por los iteradores 
		Ftot_it0 = Ftot.begin();
		Ftot_it1 = Ftot.end();

		// alguna condición inicial chata (arbitraria)
		fill(u_it0,u_it1,100.0); 

		// o una condicion inicial rugosa (random)
		//fourier_gen_path(1.5, L, u_it0, 123456);
		//////////////


		/////////////
		// STRING "Phi"
		phi.resize(L+2);
		phi_it0 = phi.begin()+1;
		phi_it1 = phi.end()-1;
		phi_raw_ptr = raw_pointer_cast(&u[1]);

		// container de fuerza total: 
		Ftotphi.resize(L); 
		// el rango de interés definido por los iteradores 
		Ftotphi_it0 = Ftotphi.begin();
		Ftotphi_it1 = Ftotphi.end();	

		// alguna condición inicial chata (arbitraria)
		fill(phi_it0,phi_it1,1.0); 

		//////////////



		timer_fuerzas_elapsed=0.0;
		timer_euler_elapsed=0.0;
		timer_props_elapsed=0.0;
	
		tiempo=0;

		#ifndef OMP
		propsout.open("someprops.dat");
		#else
		propsout.open("someprops_omp.dat");
		#endif
		
#ifndef PARAMETERSFILE
		f0=F0;
		T0=TEMP;
		//fuerza_uphi(n,f0,T0,lambda,kperp,alpha) 
		lambda=LAMBDA;
		kperp=KPERP;
		alpha=ALPHA;
#else
		std::ifstream paramsin("parameters.inp");
		paramsin >> f0;
		paramsin >> T0;
		paramsin >> lambda;
		paramsin >> kperp;
		paramsin >> alpha;
#endif

		whichone=0;
		ushift=phishift=0;

		std::ofstream log("logfile.dat");
		log << "f0= " << f0 << std::endl;			
		log << "T0= " << T0 << std::endl;			
		log << "lambda= " << lambda << std::endl;			
		log << "kperp= " << kperp << std::endl;			
		log << "alpha= " << alpha << std::endl;			
		log << "L= " << L << std::endl;			
		log << "D0= " << D0 << std::endl;			
		log << "Dt= " << Dt << std::endl;			
		log << "SEED= " << SEED << std::endl;			
		log << "SEED2= " << SEED2 << std::endl;			
	};
	

///////////////////////////////////////////
	void step()
	{
		static unsigned long mtprop=1;
		static unsigned long count=0;

		// Impone PBC en el "halo"		
		u[0]=u[L];u[L+1]=u[1];
#ifdef VIVIENSTRING
		phi[0]=phi[L];phi[L+1]=phi[1];	
#endif

		int n=tiempo; tiempo++;
		t.tic(); // para cronometrar el tiempo de la siguiente transformación
		
		// Fuerza en cada monómero calculada concurrentemente en la GPU: 
		// Ftot(X)= laplacian + disorder + thermal_noise + F0, X=0,..,L-1
		// mirar el functor "fuerza" mas arriba... 
		// Notar: los iteradores de interés estan agrupado con un "fancy" zip_iterator, 
		// ya que transform no soporta mas de dos secuencias como input.
		// Notar: make_counting_iterator es otro "fancy" iterator, 
		// que simula una secuencia que en realidad no existe en memoria (implicit sequences).
		// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
		// http://thrust.github.io/doc/group__fancyiterator.html
		//fuerza.set_time(n);

#ifndef VIVIENSTRING
		// Force on String "u"
		transform(
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(0),u_it0-1,u_it0,u_it0+1
			)),
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(L),u_it1-1,u_it1,u_it1+1
			)),
			Ftot_it0,
			fuerza(n,f0,T0)
		);
#else
		// Here we should instead calculate both forces, on "u" and "phi"
		// Model?
	#ifndef MEANFIELD
		transform(
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(0),
			u_it0-1,u_it0,u_it0+1,
			phi_it0-1,phi_it0,phi_it0+1
			)),
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(L),
			u_it1-1,u_it1,u_it1+1,
			phi_it1-1,phi_it1,phi_it1+1
			)),
			make_zip_iterator(make_tuple(
			Ftot_it0,Ftotphi_it0
			)),
			fuerza_uphi(n,f0,T0,lambda,kperp,alpha,ushift,phishift) 
		);
	#else
		int whichone_orig=whichone;// needed if interactive
		set_whichone(0); float cmu=get_center_of_mass();
		set_whichone(1); float cmphi=get_center_of_mass();
		transform(
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(0),
			u_it0-1,u_it0,u_it0+1,
			phi_it0-1,phi_it0,phi_it0+1
			)),
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(L),
			u_it1-1,u_it1,u_it1+1,
			phi_it1-1,phi_it1,phi_it1+1
			)),
			make_zip_iterator(make_tuple(
			Ftot_it0,Ftotphi_it0
			)),
			fuerza_uphi(n,f0,T0,lambda,kperp,alpha,ushift,phishift,cmu, cmphi) 
		);
		set_whichone(whichone_orig);
	#endif
#endif
		timer_fuerzas_elapsed+=t.tac();



		// Explicit forward Euler step, implementado en paralelo en la GPU: 
		// u(X,n) += Ftot(X,n) Dt, X=0,...,L-1
		// Mirar el functor "euler" mas arriba...
		// Notar: no hace falta zip_iterator, ya que transform si soporta hasta dos secuencias de input
		t.tic();
#ifndef VIVIENSTRING
		// String "u"
		transform(
			u_it0,u_it1,Ftot_it0,u_it0, 
			euler()
		);
#else
		// Euler step for both Strings
		// A model for it would be...
		transform(
			make_zip_iterator(make_tuple(
			u_it0,Ftot_it0,phi_it0,Ftotphi_it0
			)),
			make_zip_iterator(make_tuple(
			u_it1,Ftot_it1,phi_it1,Ftotphi_it1
			)),
			make_zip_iterator(make_tuple(
			u_it0,phi_it0
			)),
			euler_uphi()
		);
#endif
		timer_euler_elapsed+=t.tac();
	}

	thrust::tuple<REAL,REAL> some_properties()
	{
		// algunas propiedades de interés, calculadas cada TPROP
		//#ifndef LOGPRINT
		//if(n%TPROP==0){	
		//#else
		//if(n%mtprop==0)
		//{
			//count++;if(count==10){count=0;mtprop*=10;}
		//#endif
			t.tic();
			
			/* TODO:
			   usando algoritmos REDUCE 
			   [ http://thrust.github.io/doc/group__reductions.html#gacf5a4b246454d2aa0d91cda1bb93d0c2 ]
			   calcule la velocidad media de la interface
			   y la posición del centro de masa de la interface 
			   HINT:
			   REAL velocity = reduce(....)/L; //center of mass velocity
			   REAL center_of_mass = reduce(....)/L; // center of mass position
			*/
			// SOLUCION:			
			REAL velocity = reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L); //center of mass velocity
			REAL center_of_mass = reduce(u_it0,u_it1,REAL(0.0))/REAL(L); // center of mass position

			REAL velocity_phi = reduce(Ftotphi_it0,Ftotphi_it1,REAL(0.0))/REAL(L); //center of mass velocity
			REAL center_of_mass_phi = reduce(phi_it0,phi_it1,REAL(0.0))/REAL(L); // center of mass position

			/* TODO: 
			   usando el algoritmo TRANSFORM_REDUCE, 
	[ http://thrust.github.io/doc/group__transformed__reductions.html#ga087a5af8cb83647590c75ee5e990ef66 ]
			   el functor "roughtor" arriba definido, 
			   y la posición del centro de masa "ucm" calculada en el TODO anterior, 
			   calcule la rugosidad (mean squared width) de la interface:
			   roughness := Sum_X [u(X)-ucm]^2 /L
			   HINT:
			   REAL roughness = transform_reduce(...,...,roughtor(center_of_mass),0.0,thrust::plus<REAL>());
			*/
			// SOLUCION:	
			REAL roughness = transform_reduce
			(
				u_it0, u_it1,
                                roughtor(center_of_mass),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);

			REAL roughness_phi = transform_reduce
			(
				phi_it0, phi_it1,
                                roughtor(center_of_mass),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);

			// solución alternativa
  			/*REAL roughness = reduce
			(
			make_transform_iterator(u_it0, roughtor(center_of_mass)),
			make_transform_iterator(u_it1, roughtor(center_of_mass)),
			REAL(0.0)
			);
			*/
			timer_props_elapsed+=t.tac();
	
			/* TODO: calcule la posicion de la particula con maxima velocidad */
			//SOLUCION:
			//device_vector<REAL>::iterator result = 
			//int posmax=int(thrust::max_element(Ftot_it0,Ftot_it1)-Ftot_it0);			

			/* TODO: descomentar para que imprima la velocidad media, centro de masa, y rugosidad 
			   calculadas en los otros "TODOes", en el file "someprops.dat" */	
			// SOLUCION:
			propsout << tiempo << " " << f0 << " " << T0 
			<< " " << velocity << " " << center_of_mass << " " << roughness << " " 
			<< " " << velocity_phi << " " << center_of_mass_phi << " " << roughness_phi << " " 
			<< std::endl;
			propsout.flush();
			
			/* TODO:
			 Descomente -DPRINTCONFS en el Makefile, y recompile, para que imprima la posición 
			 y velocidad de 128 (o lo que quiera) particulas de la interface de tamanio L (una de cada L/128) 
			 en pantalla (descomente con cuidado, que imprime mucho!. Solo para hacer un "intuitive debugging")
			*/
			#ifdef PRINTCONFS
			print_configuration(u,Ftot,velocity,MAXPOINTSDISPLAY,n);
			#endif
			
			return make_tuple(velocity,velocity_phi);
		//}
	};




	void print_timmings(){
		// resultados del timming
		double total_time = (timer_fuerzas_elapsed+timer_euler_elapsed+timer_props_elapsed); 

		std::cout << "L= " << L << " TRUN= " << TRUN << std::endl; 

		std::cout << "Forces calculation -> " << 1e3 * timer_fuerzas_elapsed << " miliseconds (" 
	  	<< int(timer_fuerzas_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Euler step -> " << 1e3 * timer_euler_elapsed << " miliseconds (" 
		  << int(timer_euler_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Properties -> " << 1e3 * timer_props_elapsed << " miliseconds (" 
		  << int(timer_props_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Total -> " << 1e3 * total_time << " miliseconds (100%)" << std::endl;
	};

	////////////////////////////////////////////
	thrust::tuple<REAL,REAL> dynamics(long trun)
	{
		int whichone_orig=whichone;// needed if interactive

		set_whichone(0); float u0=get_center_of_mass()+ushift;
		set_whichone(1); float phi0=get_center_of_mass()-phishift;

		tuple<REAL,REAL> maxmin;
		for(unsigned long n=0;n<trun;n++)
		{
			step();

			// reseting positions to avoid floating point problems...
			if(n%NSHIFT==0)
			{
				set_whichone(0);
				maxmin=get_posmax();
				REAL max=get<0>(maxmin);
				REAL min=get<1>(maxmin);
				if(min<0){std::cout << "error umin<0" << std::endl;exit(1);};
				if(max>UMAX){ 
					ushift+=uint32_t(min)-OFFSET;
					REAL minshift=uint32_t(min)-OFFSET;
					thrust::transform(u_it0,u_it1,make_constant_iterator(minshift),u_it0, minus<REAL>());
					std::cout<< "ushift= " << ushift << std::endl;
				} // TODO: RNG

				set_whichone(1);
				maxmin=get_posmax();
				max=get<0>(maxmin);
				min=get<1>(maxmin);
				if(-max>UMAX){ 
					phishift+=uint32_t(-min)-OFFSET;
					REAL minshift=uint32_t(-min)-OFFSET;
					thrust::transform(phi_it0,phi_it1,make_constant_iterator(minshift),phi_it0, plus<REAL>());
					std::cout<< "phishift= " << phishift << std::endl;
				}

			}
		}
		set_whichone(0); float u1=get_center_of_mass()+ushift;
		set_whichone(1); float phi1=get_center_of_mass()-phishift;

		float mean_velocity=(u1-u0)/(trun*Dt);
		float mean_velocity_phi=(phi1-phi0)/(trun*Dt);

		set_whichone(whichone_orig);// needed if interactive

		return thrust::make_tuple(mean_velocity,mean_velocity_phi);
	};
	////////////////////////////////////////////

	void whichonetoplot(){
		whichone++;
		whichone=whichone%2;
	}
	void set_whichone(int w){
		whichone=w%2;
	}

	device_vector<REAL>::iterator posicion_begin(){
		if(whichone==0)
		return u_it0;
		else
		return phi_it0;
	};	
	device_vector<REAL>::iterator posicion_end(){
		if(whichone==0)
		return u_it1;
		else
		return phi_it1;
	};	
	
	REAL get_center_of_mass(){ 
		if(whichone==0)
		return reduce(u_it0,u_it1,REAL(0.0))/REAL(L); 
		else
		return reduce(phi_it0,phi_it1,REAL(0.0))/REAL(L); 
	};

	REAL get_roughness(REAL cm){
		if(whichone==0)
		return  transform_reduce
			(
				u_it0, u_it1,
                roughtor(cm),
                REAL(0.0),
                thrust::plus<REAL>()
			)/REAL(L);
		else
		return  transform_reduce
			(
				phi_it0, phi_it1,
                roughtor(cm),
                REAL(0.0),
                thrust::plus<REAL>()
			)/REAL(L);
	};

	tuple<REAL, REAL> get_posmax(){
		REAL min, max; // in absolute value
		if(whichone==0){
			REAL ref=u[1];
			max = thrust::reduce(u_it0, u_it1,ref,thrust::maximum<REAL>());
			min = thrust::reduce(u_it0, u_it1,ref,thrust::minimum<REAL>());
		}
		else{
			REAL ref=phi[1];
			max = thrust::reduce(phi_it0, phi_it1,ref,thrust::minimum<REAL>());
			min = thrust::reduce(phi_it0, phi_it1,ref,thrust::maximum<REAL>());
		}
		return make_tuple(max,min);
	}

	void increment_f0(float df){
		f0+=(f0+df>=0.0)?(df):(0.0);
	};
	void increment_T0(float dT0){
		T0+=(T0+dT0>=0.0)?(dT0):(0.0);
	};
	float get_f0(){
		return f0;
	}
	float get_T0(){
		return T0;
	}	
	void set_f0(float x){
		f0=x;
	}
	void set_T0(float x){
		T0=x;
	}	


	REAL get_velocity(){ 
		if(whichone==0)
		return reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L); //center of mass velocity
		else
		return reduce(Ftotphi_it0,Ftotphi_it1,REAL(0.0))/REAL(L); //center of mass velocity
	}

	unsigned int get_phishift(){return phishift;}
	unsigned int get_ushift(){return ushift;}

	private:
	long tiempo;
	float f0;	
	float T0;	
	float lambda;
	float kperp;
	float alpha;
	int whichone;
	unsigned int ushift;
	unsigned int phishift;

	//////////////////////////////////////
	// STRING "u"
	// posiciones de los monómeros: 
	// Notar que alocamos dos elementos de mas para usarlos como "halo"
	// esto nos permite fijar las condiciones de borde facilmente, por ejemplo periódicas: u[0]=u[L]; u[L+1]=u[1];
	device_vector<REAL> u;
	device_vector<REAL>::iterator u_it0;
	device_vector<REAL>::iterator u_it1;
	REAL * u_raw_ptr;

	// container de fuerza total: 
	device_vector<REAL> Ftot; 
	// el rango de interés definido por los iteradores 
	device_vector<REAL>::iterator Ftot_it0;
	device_vector<REAL>::iterator Ftot_it1;

#ifdef VIVIENSTRING
	//////////////////////////////////////
	// STRING "phi"
	device_vector<REAL> phi;
	device_vector<REAL>::iterator phi_it0;
	device_vector<REAL>::iterator phi_it1;
	REAL * phi_raw_ptr;

	// container de fuerza total: 
	device_vector<REAL> Ftotphi; 
	// el rango de interés definido por los iteradores 
	device_vector<REAL>::iterator Ftotphi_it0;
	device_vector<REAL>::iterator Ftotphi_it1;
#endif

	// file para guardar algunas propiedades dependientes del tiempo
	#ifndef OMP
	std::ofstream propsout;
	#else
	std::ofstream propsout;
	#endif

};



/////////////////////////////////////////////////////////////////////////////
/*#include "qew_minimal_solucion.h"
int main(){
	Cuerda C;
	C.dynamics(TRUN);
	C.print_timmings();
	return 0;
}*/

