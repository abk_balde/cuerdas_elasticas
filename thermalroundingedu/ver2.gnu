#QEW
beta=0.245
nu=4./3.
z=1.433
zeta=1.25

nmax=15
fc=1.621

#file="pru"

#L=65536
set key left bot
fuerza(x)=1.618+x*0.001

indicef(x)=int((x-1.618)/0.001)

set multi lay 2, 2

set logs 
###################
set tit sprintf("L=%d, fth=Gauss[1,1], dx=Exp[1]/2",L); 

set xlabel "t"; set ylabel "activity"; \
plot for[i=0:nmax:1] file u 1:($2/L) index i w l t sprintf("f=%f",fuerza(i)),\
0.35/x**(beta/(nu*z)) lc 0 lt -1 lw 2, file u 1:(($1>50)?($2/L):(1/0.)) index indicef(fc) w p lw 1 lc 0 t 'f~fc'
###################

xif(x)=abs(fc-fuerza(x))**(-nu)

set tit sprintf("fc~%f, beta=%f, nu=%f, z=%f",fc,beta, nu, z); 

set xlabel "t/|fc-f|^{-nu.z}"; set ylabel "v.|fc-f|^{-beta}"; \
plot [][:3] for[i=0:nmax] file u ($1/xif(i)**z):(($1>50)?($2*xif(i)**(beta/nu)/L):(1./0)) index i w p t '',\
0.29/x**(beta/(nu*z)) lc 0 lt -1 lw 2
###################
set tit sprintf("fc~%f, beta=%f, nu=%f, z=%f",fc,beta, nu, z); 

set xlabel "t"; set ylabel "w"; \
plot [][] for[i=0:nmax] file u ($1):($5) index i w l t '', 1e5*x**(2.0*0.5/2.0), x**(2*zeta/z)
###################

set tit sprintf("fc~%f, beta=%f, nu=%f, z=%f",fc,beta, nu, z); 

set xlabel "t"; set ylabel "w"; \
plot [][] for[i=0:nmax] file u ($1):($5/($1)**(2*zeta/z)) index i w l t ''
###################

unset multi

#pause 10
#reread

