/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG

// generates a gaussian RN from philox number
__device__
FLOAT box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	FLOAT u1x = u01_open_closed_32_53(r_philox[0]);
  	FLOAT u2x = u01_open_closed_32_53(r_philox[1]);

  	FLOAT rx = sqrt( -2.0*log(u1x) );
  	FLOAT thetax = 2.0*M_PI*u2x;

	return rx*sin(thetax);    			
}


struct gaussian_functor{
	int semi;
	FLOAT average;
	FLOAT sigma;

	gaussian_functor(FLOAT _average, FLOAT _sigma, int _semi):
	semi(_semi),average(_average),sigma(_sigma){};

	__device__
	FLOAT operator()(int tid){
	    	// keys and counters 
	    	RNG philox; 	
		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;

		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=tid; 
		c[1]=semi;
		c[0]=0;
		
		r = philox(c, k);
		return FLOAT(sigma*box_muller(r)+average); 
	}	
};

struct quenched_gaussian_functor{
	FLOAT *u;
	FLOAT average;
	FLOAT sigma;

	quenched_gaussian_functor(FLOAT _average, FLOAT _sigma, FLOAT * _u):
	u(_u),average(_average),sigma(_sigma){};

	__device__
	FLOAT operator()(int tid){
	    	// keys and counters 
	    	RNG philox; 	
		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;

		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=tid; 
		c[1]=uint32_t(u[tid]*10);
		c[0]=SEED;
		
		r = philox(c, k);
		return FLOAT(sigma*box_muller(r)+average); 
	}	
};


struct poisson_functor{
	int semi;
	FLOAT average;

	poisson_functor(FLOAT _average, int _semi):
	semi(_semi),average(_average){};

	__device__
	FLOAT operator()(int tid){
	    	// keys and counters 
	    	RNG philox; 	
		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;

		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=tid; 
		c[1]=semi;
		c[0]=SEED;
		
		r = philox(c, k);
		FLOAT u1x = u01_open_closed_32_53(r[0]);
		return -log(u1x)/average; 
	}	
};

#define SMALLDU 0.001
struct quenched_poisson_functor{
	FLOAT average;
	FLOAT *u;

	quenched_poisson_functor(FLOAT _average, FLOAT *_u):
	u(_u),average(_average){};

	__device__
	FLOAT operator()(int tid){
	    	// keys and counters 
	    	RNG philox; 	
		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;

		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=tid; 
		c[1]=uint32_t(u[tid]*10);
		c[0]=SEED;
		
		r = philox(c, k);
		FLOAT u1x = u01_open_closed_32_53(r[0]);
		return -log(u1x)/average+SMALLDU; // experimental 
	}	
};


struct uniform_functor{
	int semi;
	uniform_functor(int _semi):
	semi(_semi){};

	__device__
	FLOAT operator()(int tid){
	    	// keys and counters 
	    	RNG philox; 	
		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;

		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=tid; 
		c[1]=semi;
		c[0]=SEED;
		
		r = philox(c, k);
		FLOAT u1x = u01_open_closed_32_53(r[0]);
		return u1x; 
	}	
};


void logsequence(thrust::host_vector<int> &monitor)
{
	int n=monitor.size();
	int j=0;
	int step=1;
	for(int i=0;i<n;i++){
		monitor[i]=j;
		if(j==step*10) step*=10;	
		j+=step;
	}

// otra forma:
// tn = int(x^n) -> 0,x,x^2,x^3,... 

/*	thrust::sequence(monitor.begin(),monitor.begin()+10,0,1);
	thrust::sequence(monitor.begin()+10,monitor.begin()+20,10,10);
	thrust::sequence(monitor.begin()+20,monitor.begin()+30,100,100);
	thrust::sequence(monitor.begin()+30,monitor.begin()+40,1000,1000);
	thrust::sequence(monitor.begin()+40,monitor.begin()+50,10000,10000);

	for(int i=0;i<50;i++) std::cout << monitor[i] << std::endl;
	exit(1);
*/
}

#include <set>
void logsequence2(thrust::host_vector<int> &monitor, float base)
{
	int n=monitor.size();
	std::set<int> myset;

	for(int i=0;i<n;i++){
		int next = int(powf(base,i));
		myset.insert(next);
	}

	int i=0;
	std::set<int>::iterator it;
	for (it=myset.begin(); it!=myset.end(); ++it)
	{
		monitor[i]=(*it);
		std::cout << monitor[i] << std::endl;
		i++;		
	}	

// otra forma:
// tn = int(x^n) -> 0,x,x^2,x^3,... 

/*	thrust::sequence(monitor.begin(),monitor.begin()+10,0,1);
	thrust::sequence(monitor.begin()+10,monitor.begin()+20,10,10);
	thrust::sequence(monitor.begin()+20,monitor.begin()+30,100,100);
	thrust::sequence(monitor.begin()+30,monitor.begin()+40,1000,1000);
	thrust::sequence(monitor.begin()+40,monitor.begin()+50,10000,10000);

	for(int i=0;i<50;i++) std::cout << monitor[i] << std::endl;
	exit(1);
*/
}

