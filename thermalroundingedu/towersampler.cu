#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <fstream>
#include <thrust/scan.h>
#include <thrust/binary_search.h>
#include <thrust/device_vector.h>

using namespace std;

int main(int argc, char **argv)
{
	srand (time(NULL));
	int samples=atoi(argv[1]);

	float data[6] = {0.1, 0, 0.15, 0.25, 0.1, 0.4};

	cout << "initial data\n";
	for(int i=0;i<6;i++){
		cout << data[i] << endl;
	}

	thrust::inclusive_scan(data, data + 6, data); // in-place scan
	cout << "inclusive scan data\n";
	for(int i=0;i<6;i++){
		cout << data[i] << endl;
	}
	cout << "\n\n";

	float histo[6]= {0, 0, 0, 0, 0, 0};

	float Q=data[6-1];
	float t=0.f;
	for(int n=0;n<samples;n++)
	{
		float u=rand()*Q/RAND_MAX;
		//cout << "random number = " << u << endl;
		int i= int(thrust::lower_bound(data, data+6, u)-data); 
		//cout << "la transicion seleccionada es la " << i << endl;

		float uu=rand()/RAND_MAX;
		t+=log(1./uu)/Q;
		
		histo[i]++;
	}

	ofstream histoout("histo.dat");
	for(int i=0;i<6;i++){
		histoout << i << " " << histo[i]/samples << endl;
	}
}
