## promedia Sofq para una fuerza dada f=$1 y temperatura T=$2

f=$1
T=$2

rm Sofq*.dat
rm vel*.dat
for((i=1;i<=10;i++)); 
do 
	nvcc qew+m.cu -o cuda.out -I/home/rcortes/vortex/common/ -DFOURIER -lcufft -lgsl -lgslcblas -lm -DLOGMONITOR -DTEQUIL=1 -DSEED=$i

	./cuda.out $f $T 100000 > "vel"$i".dat" ; 

	mv Sofq_vs_time.dat "Sofq"$i".dat"; 

	# va promediando sobre la marcha...
	paste Sofq*.dat | gawk '{cum=0;for(i=2;i<=NF;i+=2) cum=cum+$i; if(NF>1) printf("%f %f\n",$1,cum/NF); else print;}' > "avsofq_f"$f"T"$T".dat"

	#paste vel*.dat > "vel_f"$f"T"$T".dat"
done

#plot for[fac=8:8] 'avsofq_f2T0.005.dat' u (fac*int($0/fac)*2*pi/8192.):2 index 40:100 w l smooth unique t '2',(300./x)**2, for[fac=8:8] 'avsofq_f5_T0.005.dat' u (fac*int($0/fac)*2*pi/8192.):2 index 40:100 w l smooth unique t '5'
