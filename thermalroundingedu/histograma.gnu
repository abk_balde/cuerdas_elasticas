binwidth=binw
bin(x,width)=width*floor(x/width)

set print "histo.dat"
plot datafile using (bin($1,binwidth)):(1.0) smooth freq with boxes
