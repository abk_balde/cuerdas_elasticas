#include <iostream>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/transform.h>
#include <cmath>
#include <thrust/copy.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/extrema.h>
#include <thrust/sequence.h>
#include <thrust/scan.h>
#include <thrust/binary_search.h>
#include <thrust/device_vector.h>

#include <fstream>
#include "simple_timer.h"

#define N	16777216
#define SEED	1
#define MEAN	1.0
#define SIGMA	1.0
#define TEMP	(0.1)

typedef float FLOAT;
//typedef double FLOAT;

#include "deviates.h"

struct above{

	FLOAT *fel;
	FLOAT *fth;
	int seed;
	FLOAT T;
	above(FLOAT *_fel, FLOAT *_fth, FLOAT _T, int _seed):fel(_fel), fth(_fth), seed(_seed), T(_T){};

	__device__
	bool operator()(int i){
		if(T>0){
	    	// keys and counters 
	    	RNG philox; 	
			RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;
			// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=i; 
			c[1]=seed;
			c[0]=0;
			r = philox(c, k);
		
			// metropolis: si fth<fel -> acepted, si fth>fel acepted con prob exp(-Delta/T)
			return u01_open_closed_32_53(r[0]) < exp(-(fth[i]-fel[i])/T);
		}
		else return bool(fel[i]>fth[i]);				
	}
};

struct givemeyourrate{

	FLOAT *fel;
	FLOAT *fth;
	int seed;
	FLOAT T;
	givemeyourrate(FLOAT *_fel, FLOAT *_fth, FLOAT _T, int _seed):fel(_fel), fth(_fth), seed(_seed), T(_T){};

	__device__
	FLOAT operator()(int i){
		if(T>0){
	    	// keys and counters 
	    	/*RNG philox; 	
			RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;
			// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=i; 
			c[1]=seed;
			c[0]=0;
			r = philox(c, k);
			*/
		// metropolis: si fth<fel -> acepted, si fth>fel acepted con prob exp(-Delta/T)
			return exp(-(fth[i]-fel[i])/T);
		}
		else return FLOAT(fel[i]>fth[i]);				
	}
};




// inicialize with gaussians
void initialize_random(thrust::device_vector<FLOAT> &v, FLOAT mean, FLOAT sigma, int seed)
{
	int size=v.size();
	thrust::transform(
		thrust::make_counting_iterator(0),thrust::make_counting_iterator(size), 
		v.begin(),gaussian_functor(mean,sigma,seed)
	);

	FLOAT promedio=thrust::reduce(v.begin(),v.end(),FLOAT(0.0),thrust::plus<FLOAT>())/v.size();

	using namespace thrust::placeholders;
	thrust::transform(v.begin(),v.end(),v.begin(),_1-promedio+mean);	
}

/*void adjust(thrust::device_vector<FLOAT> &v, FLOAT mean, FLOAT sigma, int seed)
{
	FLOAT promedio=thrust::reduce(v.begin(),v.end(),FLOAT(0.0),thrust::plus<FLOAT>())/v.size();
}*/


// detect points with fel>fth among N points, and put them in a reduced list of m indices
int detect_inestables(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth, thrust::device_vector<int> &indices, float Temp, int t)
{
	// detect unstable indices
	FLOAT *fthraw=thrust::raw_pointer_cast(fth.data());
	FLOAT *felraw=thrust::raw_pointer_cast(fel.data());
	thrust::device_vector<int> unstable_indices(N); //INEFICIENTE: reemplazar por alocacion estatica

	thrust::device_vector<int>::iterator end=
	thrust::copy_if(
		thrust::make_counting_iterator(0),thrust::make_counting_iterator(N),
		unstable_indices.begin(),above(felraw,fthraw,Temp,t)
	);
	int m=(end-unstable_indices.begin());

	indices.resize(m);
	thrust::copy(unstable_indices.begin(),unstable_indices.begin()+m,indices.begin());

	return m;
}

// detect points with fel>fth among N points, and put them in a reduced list of m indices
int kinetic_montecarlo(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth, thrust::device_vector<int> &indices, float Temp, int &t,
	thrust::device_vector<FLOAT> &rates)
{
	// detect unstable indices
	FLOAT *fthraw=thrust::raw_pointer_cast(fth.data());
	FLOAT *felraw=thrust::raw_pointer_cast(fel.data());
	FLOAT *ratesraw=thrust::raw_pointer_cast(rates.data());
	thrust::device_vector<int> unstable_indices(N); //INEFICIENTE: reemplazar por alocacion estatica

	thrust::transform(
		thrust::make_counting_iterator(0),thrust::make_counting_iterator(N),
		rates.begin(),givemeyourrate(felraw,fthraw,Temp,t)
	);

	thrust::inclusive_scan(rates.begin(), rates.end(), rates.begin()); // in-place scan
	float Q=rates[N-1];

	// primer numero random para seleccionar evento
	float u=rand()*Q/RAND_MAX;
	int evento= int(thrust::lower_bound(rates.begin(), rates.end(), u)-rates.begin());

	unstable_indices[0]=evento;
	int m=1;
	indices.resize(m);
	thrust::copy(unstable_indices.begin(),unstable_indices.begin()+m,indices.begin());
	
	return m;
}


// given the list of unstables, updates its thresholds and discharge on left/right neighbors
void update_inestables(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth,
thrust::device_vector<float> &u, thrust::device_vector<int> &indices, int t)
{
	/*std::cout << "\n#originales\n";
	std::cout << "#fth fel\n";
	for(int i=0;i<N;i++)
	std::cout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	std::cout << std::endl;
	*/

	// number of unstable sites
	int ninest=indices.size();

	FLOAT *fthraw=thrust::raw_pointer_cast(fth.data());
	FLOAT *felraw=thrust::raw_pointer_cast(fel.data());

	typedef thrust::device_vector<FLOAT>::iterator ElementIterator;
	typedef thrust::device_vector<int>::iterator   IndexIterator;

	// update of thresholds
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterth(fth.begin(), indices.begin());
	thrust::transform(
		indices.begin(), indices.end(), iterth,
		gaussian_functor(MEAN,SIGMA,t)
	);
/*
	std::cout << "\n#luego de updatear los thresholds\n";
	std::cout << "#fth fel\n";
	for(int i=0;i<N;i++)
	std::cout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	std::cout << std::endl;
*/
	// random displacement
	thrust::device_vector<FLOAT> dx(ninest);
	thrust::transform(indices.begin(),indices.end(),dx.begin(),poisson_functor(MEAN,t));

	using namespace thrust::placeholders; 

	// update total displacement
	thrust::permutation_iterator<ElementIterator,IndexIterator> iteru(u.begin(), indices.begin());
	thrust::transform(
		iteru, iteru+ninest, dx.begin(), iteru, _1+_2
	);


	// descarga del central
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterel(fel.begin(), indices.begin());
	thrust::transform(
		iterel, iterel+ninest, dx.begin(), iterel, _1-1.0*_2
	);

/*
	std::cout << "\n#luego de updatear las fuerzas elasticas locales\n";
	std::cout << "#fth fel\n";
	for(int i=0;i<N;i++)
	std::cout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	std::cout << std::endl;
*/

	// carga sobre los izquierdos
	thrust::device_vector<int> indices_left(ninest);
	thrust::transform(indices.begin(),indices.end(),indices_left.begin(),(_1-1+N)%N);
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterleft(fel.begin(), indices_left.begin());
	thrust::transform(iterleft, iterleft+ninest, dx.begin(), iterleft, _1+0.5*_2);
/*
	std::cout << "\n#luego de updatear las fuerzas elasticas de la izquierda\n";
	std::cout << "#fth fel\n";
	for(int i=0;i<N;i++)
	std::cout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	std::cout << std::endl;
*/
	// carga sobre los derechos
	thrust::device_vector<int> indices_right(ninest);
	thrust::transform(indices.begin(),indices.end(),indices_right.begin(),(_1+1)%N);
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterright(fel.begin(), indices_right.begin());
	thrust::transform(iterright, iterright+ninest, dx.begin(), iterright, _1+0.5*_2);

	
	//fuerzasout << "\n#luego de updatear las fuerzas elasticas de la derecha (todo listo)\n";
	//fuerzasout << "#fth fel\n";
	//for(int i=0;i<N;i++)
	//fuerzasout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	//fuerzasout << std::endl;
	
}
//================================================

thrust::tuple<int,FLOAT> whichisnext(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth)
{
	using namespace thrust::placeholders;
	thrust::device_vector<FLOAT> dif(N);
	thrust::transform(fel.begin(),fel.end(),fth.begin(),dif.begin(),_2-_1);

	//FLOAT x=thrust::reduce(dif.begin(),dif.begin(),FLOAT(dif[0]),thrust::minimum<FLOAT>());
	thrust::device_vector<FLOAT>::iterator este = thrust::min_element(dif.begin(), dif.end());
	std::cout << "#next one is " << int(este-dif.begin()) << " with Delta=" << *este << std::endl;

	return  thrust::make_tuple(int(este-dif.begin()),FLOAT(*este));
}

#include "extra.h"
int main(int argc, char **argv)
{
	//int seed=0;


	thrust::device_vector<FLOAT> fth(N);
	thrust::device_vector<FLOAT> fel(N);
	thrust::device_vector<FLOAT> rates(N);
	thrust::device_vector<FLOAT> u(N,float(0.0));

	thrust::device_vector<int> inest;

	initialize_random(fth,MEAN,SIGMA,0);

	float drive=(argc>1)?(atof(argv[1])):(1.6);
	// gaussian 
	initialize_random(fel,drive,SIGMA,1);
	// constant
	thrust::fill(fel.begin(),fel.end(),drive);

	float Temp=(argc>2)?(atof(argv[2])):(-1.0);

	int n;
	int t=0;

	std::ofstream inestout("inestables.dat"); 
	std::ofstream fuerzasout("fuerzas.dat");

	thrust::host_vector<int> monitor(50);

	//logsequence2(monitor,1.5);
	//exit(1);

	logsequence(monitor);
	int next=0;

	int tmax=500000;
	if(argc>3) tmax=atoi(argv[3]);

	using namespace thrust::placeholders; 

	gpu_timer crono;

	float ms_updates=0.0;
	float ms_detect=0.0;

	//for(int ava=0;ava<100;ava++){
		n=detect_inestables(fel,fth,inest,Temp,t);
		while(n > 0 && t<tmax)
		{
			if(t==monitor[next] || (t+1==tmax)){ 
				float ucm = thrust::reduce(u.begin(), u.end())/float(N);
				float wcm = thrust::transform_reduce(u.begin(), u.end(),(_1-ucm)*(_1-ucm),float(0.0),thrust::plus<float>())/float(N);

				std::cout << t << " " <<  n << 
				" " << thrust::reduce(fel.begin(), fel.end())/N << 
				" " << thrust::reduce(fth.begin(), fth.end())/N << 
				" " << wcm << std::endl;
				next++;
			}

			//gnuplot> plot for[i=0:100] 'inestables.dat' index i u 2:1 t '' pt 5
			//for(int i=0;i<inest.size();i++) inestout << t << " " << inest[i] << " " << ava << std::endl;
			//inestout << "\n\n";
			
			crono.tic();
			update_inestables(fel,fth,u,inest,t);
			ms_updates+=crono.tac();
			//update_inestables_dp(fel,fth,u,inest,t);

			//for(int i=0;i<N;i++)
			//fuerzasout << t << " " << i*1.0 << " " << fth[i] << " " << fel[i] << " " << std::endl;
			//fuerzasout << "\n\n";

			crono.tic();	
			n=detect_inestables(fel,fth,inest,Temp,t);
			if(n==0) ;//kinetic_montecarlo(fel,fth,inest,Temp,t,rates);
			else t++;
			ms_detect+=crono.tac();
		}
		std::cout << "ms updates = " << ms_updates << std::endl;
		std::cout << "ms detect = " << ms_detect << std::endl;
		//inestout << "\n\n";
		//thrust::tuple<int,FLOAT> next=whichisnext(fel,fth);
		//fel[thrust::get<0>(next)]++;
	//}
	return 0;
}

/*
g++ -O2 -x c++ qew+m.cu -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp -I /usr/local/cuda-7.0/include -o omp.out
nvcc qew+m.cu -o cuda.out
*/ 

