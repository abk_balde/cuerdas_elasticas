

// given the list of unstables, updates its thresholds and discharge on left/right neighbors
void update_inestables_dp(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth,
thrust::device_vector<float> &u, thrust::device_vector<int> &indices, int t)
{
	// number of unstable sites
	int ninest=indices.size();

	FLOAT *fthraw=thrust::raw_pointer_cast(fth.data());
	//FLOAT *felraw=thrust::raw_pointer_cast(fel.data());

	typedef thrust::device_vector<FLOAT>::iterator ElementIterator;
	typedef thrust::device_vector<int>::iterator   IndexIterator;

	// update of thresholds central: lo saturo en 1
	using namespace thrust::placeholders;
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterth(fth.begin(), indices.begin());
	thrust::transform(thrust::make_constant_iterator(1.0),thrust::make_constant_iterator(1.0)+ninest, iterth, _1);

	// update los izquierdos: random uniforme[0,1]
	thrust::device_vector<int> indices_left(ninest);
	thrust::transform(indices.begin(),indices.end(),indices_left.begin(),(_1-1+N)%N);
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterleft(fth.begin(), indices_left.begin());
	thrust::transform(indices_left.begin(), indices_left.end(), iterleft, uniform_functor(t));

	// update los derechos: random uniforme[0,1]
	thrust::device_vector<int> indices_right(ninest);
	thrust::transform(indices.begin(),indices.end(),indices_right.begin(),(_1+1+N)%N);
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterright(fth.begin(), indices_right.begin());
	thrust::transform(indices_right.begin(), indices_right.end(), iterright, uniform_functor(t));
}
//================================================


#include <thrust/device_ptr.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_histogram.h>

	// to compute <sum_i[delta(fi-fthi-f)]>
/*	int Nhisto=1000;
    gsl_histogram * histo_th = gsl_histogram_alloc (Nhisto);
    gsl_histogram_set_ranges_uniform (histo_th, 0, drive);
    thrust::host_vector<int> fthminusfel(N);
*/

// note: functor inherits from unary_function

struct resta_floats_in_tuple : public thrust::unary_function<thrust::tuple<float,float>,float>
{
  __host__ __device__
  float operator()(thrust::tuple<float,float> tup) const
  {
    return (thrust::get<1>(tup)-thrust::get<0>(tup));
  }
};

#ifdef HISTOGRAMA
class histograma_gsl
{
	public:		
    histograma_gsl(int _n, float a, float b,thrust::device_vector<float> &v1, thrust::device_vector<float> &v2)
    {
		n=_n;
		nvec=v1.size();

    	h = gsl_histogram_alloc (n);
    	gsl_histogram_set_ranges_uniform (h, a, b);

    	hmin = gsl_histogram_alloc (n);
    	gsl_histogram_set_ranges_uniform (hmin, a, b);
 
 		data1=&v1[0];
 		data2=&v2[0];

    }		
    ~histograma_gsl(){
		gsl_histogram_free (h);
    }
  
    void increment()
    {
    	float xmin=10000000;
    	for(int i=0;i<nvec;i++){
	    	float x=data2[i]-data1[i];
    		gsl_histogram_increment (h, x);
    		if(x<xmin) xmin=x;
    	}
    	gsl_histogram_increment (hmin, xmin);
    }

    void print(){
    	FILE *fout=fopen("histo.dat","w");
    	gsl_histogram_fprintf (fout, h, "%g", "%g");
    	fclose(fout);

    	FILE *fout2=fopen("histomin.dat","w");
    	gsl_histogram_fprintf (fout2, hmin, "%g", "%g");
    	fclose(fout2);
    }

	private:
	int n; // number of bins 	
	int nvec;
	gsl_histogram * h; //histogram handle
	gsl_histogram * hmin; //histogram handle
	thrust::device_ptr<float> data1;
	thrust::device_ptr<float> data2;
};
#endif

#include <thrust/transform_reduce.h>
#include <thrust/functional.h>

float minimum_diference(thrust::device_vector<float> &v1, thrust::device_vector<float> &v2)
{
	float result = thrust::transform_reduce(
		thrust::make_zip_iterator(thrust::make_tuple(v1.begin(),v2.begin())),
		thrust::make_zip_iterator(thrust::make_tuple(v1.end(),v2.end())),
        resta_floats_in_tuple(), float(1000000.0), thrust::minimum<float>());

	return result;
}


#ifdef FOURIER

#include "cutil.h"
#include <cufft.h>
typedef cufftReal REAL;
typedef cufftComplex COMPLEX;

struct modulosquared
{
	__device__
	REAL operator()(COMPLEX c){
		return c.x*c.x+c.y*c.y;
	}	
};


// una clase para hacer factores de estructura
class structure_factor
{
	private:
	int l;	
	int Ncomp;	// componentes complejas

	int counts;

	REAL *u_input_raw; // puntero raw al input para input de cufft

	thrust::device_vector<COMPLEX> u_output; // transformada instantanea
	COMPLEX *u_output_raw; // puntero raw para output de cufft

	thrust::device_vector<REAL> cumu; // factor de estructura acumulado
	thrust::host_vector<REAL> cumu_h; // factor de estructura acumulado

	thrust::device_vector<REAL> inst; // factor de estructura acumulado
	thrust::host_vector<REAL> inst_h; // factor de estructura acumulado

	cufftHandle plan_r2c; // plan real to complex

	public:
	structure_factor(thrust::device_vector<REAL> &u){

		l=u.size();
		Ncomp=l/2+1;

		u_input_raw=thrust::raw_pointer_cast(&u[0]);

		// para guardar el factor de estructura instantaneo
		u_output.resize(Ncomp);
		u_output_raw=thrust::raw_pointer_cast(&u_output[0]);

		// para guardar el factor de estructura acumulado
		cumu.resize(Ncomp);
		cumu_h.resize(Ncomp);


		inst.resize(Ncomp);
		inst_h.resize(Ncomp);

		// inicializa factor de estructura acumulado
		thrust::fill(cumu.begin(),cumu.end(),REAL(0.0));

		// forward transform R2C
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_r2c,l,CUFFT_R2C,1));

		counts=0;
	}	

	~structure_factor(){
		cufftDestroy(plan_r2c);
	}

	void transform_fourier(){
		//std::cout << "transforming FOURIER" << std::endl;
		CUFFT_SAFE_CALL(cufftExecR2C(plan_r2c, u_input_raw, u_output_raw));
		//thrust::transform(u_output.begin(),u_output.end(),cumu.begin(),cumu.begin(),modulosquared());
		thrust::transform(u_output.begin(),u_output.end(),inst.begin(),modulosquared());

		using namespace thrust::placeholders;
		thrust::transform(inst.begin(),inst.end(),cumu.begin(),cumu.begin(),_1+_2);
		counts++;
	};


	void print_cumu(){
		// file para guardar el factor de estructura
		std::ofstream fou("averagesofq.dat");
		thrust::copy(cumu.begin(),cumu.end(),cumu_h.begin());
		for(int i=0;i<Ncomp;i++){
			REAL c=cumu_h[i];
			fou << (2.f*M_PI*i/l) << " " << c/counts <<  std::endl;
		}
	}
		
	void print_inst(std::ofstream &fout){
		// file para guardar el factor de estructura
		//std::ofstream fou("averagesofq.dat");
		thrust::copy(inst.begin(),inst.end(),inst_h.begin());
		for(int i=0;i<Ncomp;i++){
			REAL d=inst_h[i];
			fout << (2.f*M_PI*i/l) << " " << d << std::endl;
		}
		fout << "\n\n";
	}
};
#endif


void print_inestables(std::ofstream &fout, thrust::device_vector<int> &indices, int n){
	static int counts=0;
	for(int i=0;i<n;i++) fout << counts << " " << indices[i] << std::endl;
	counts++; 	
}

void print_config(thrust::device_vector<FLOAT> &u, std::ofstream &fout)
{
	for(int i=0;i<N;i++) fout << u[i] << "\n";
	fout << "\n\n" << std::endl;
}




