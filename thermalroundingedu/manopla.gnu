if(first==1) {sigma=0.16; psi=0.24; first=0; res; set logs;}

set title sprintf('u ($1*T**(%f/%f)):($2/T**(%f))',psi,sigma,psi)
plot for[T in "0.0001 0.0002 0.0005 0.001 0.002"] sprintf('zzz%s',T) u \
($1*T**(psi/sigma)):($2/T**(psi)) t sprintf('zzz%s',T) w lp lc 0,\
1.2e7/x**(sigma)

pause mouse keypress

if(MOUSE_KEY==1008) sigma=sigma+0.001;
if(MOUSE_KEY==1010) sigma=sigma-0.001;
if(MOUSE_KEY==1009) psi=psi+0.001;
if(MOUSE_KEY==1011) psi=psi-0.001;
if(MOUSE_KEY==27) first=1;

print MOUSE_KEY, " sigma=",sigma, " psi=",psi

reread 
