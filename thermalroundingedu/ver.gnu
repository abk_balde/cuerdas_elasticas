set multi lay 1,2

set title sprintf("tiempo=%d",i)

# index i es el tiempo
plot [0:1024][2:7] 'fuerzas.dat' index i u 2:4 w l t 'fuerzas elasticas', \
'' index i u 2:3 w l t 'thresholds locales',\
'' index i u 2:(($3<$4)?($4):(1./0)) ps 4 t 'inestables'

plot 'inestables.dat' index 0:i u 2:1:3 t '' pt 5 ps .1 lc variable

unset multi

load 'wait_for_tab'

if(i<iend) reread
