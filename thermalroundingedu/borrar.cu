#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/device_vector.h>

// note: functor inherits from unary_function
struct square_root : public thrust::unary_function<float,float>
{
  __host__ __device__
  float operator()(float x) const
  {
    return sqrtf(x);
  }
};


// note: functor inherits from unary_function
struct suma_y_square_root : public thrust::unary_function<thrust::tuple<float,float>,float>
{
  __host__ __device__
  float operator()(thrust::tuple<float,float> tup) const
  {
    return sqrtf(tup.get<0>()+tup.get<1>());
  }
};

int main(void)
{
  thrust::device_vector<float> v(4);
  v[0] = 1.0f;
  v[1] = 4.0f;
  v[2] = 9.0f;
  v[3] = 16.0f;

  typedef thrust::device_vector<float>::iterator FloatIterator;
                                                                                                                                                           
  // iter[4] is an out-of-bounds error

  // segundo test                                                                                         

  // typedef a tuple of these iterators
  typedef thrust::tuple<FloatIterator, FloatIterator> IteratorTuple;

  // typedef the zip_iterator of this tuple
  typedef thrust::zip_iterator<IteratorTuple> ZipIterator;

  ZipIterator iterzip( thrust::make_tuple(v.begin(), v.begin()) );

  using namespace thrust::placeholders;

  thrust::transform_iterator<suma_y_square_root, ZipIterator> itertuple(iterzip, suma_y_square_root());

  thrust::device_vector<float> vv(4);
   

  // primer test                                                                                         
  thrust::transform_iterator<square_root, FloatIterator> iter(v.begin(), square_root());                                                                                         
  std::cout << *iter << std::endl;   // returns 1.0f
  for(int i=0;i<4;i++){
  	std::cout << iter[i] << std::endl; 
  }                       

  for(int i=0;i<4;i++){
  	std::cout << itertuple[i] << " vs " << sqrt(2*v[i]) << std::endl;	  
  }                       

}





