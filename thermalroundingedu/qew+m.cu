/* PARA COMPILAR CON OMP o CUDA
g++ -O2 -x c++ qew+m.cu -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp -I /usr/local/cuda-7.0/include -o omp.out
nvcc qew+m.cu -o cuda.out
*/ 

/*

*/

#include <iostream>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/transform.h>
#include <cmath>
#include <thrust/copy.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/extrema.h>
#include <thrust/sequence.h>
#include <thrust/scan.h>
#include <thrust/binary_search.h>
#include <thrust/device_vector.h>

#include <fstream>
#include "simple_timer.h"

#define N	1024 //16777216

#ifndef SEED
#define SEED	1
#endif

#define MEAN	1.0
#define SIGMA	1.0
#define TEMP	(0.1)

#ifndef TEQUIL
#define TEQUIL	0
#endif

#ifndef TPROP
#define TPROP	1000
#endif


typedef float FLOAT;
//typedef double FLOAT;

#include "deviates.h"

struct periodicwrap
{
	int p;
	periodicwrap(int _p):p(_p){};
	__device__
	int operator()(int i){
		return (i+p+N)%N;
	}	
};

struct above{

	FLOAT *fel;
	FLOAT *fth;
	int seed;
	FLOAT T;
	above(FLOAT *_fel, FLOAT *_fth, FLOAT _T, int _seed):fel(_fel), fth(_fth), seed(_seed), T(_T){};

	__device__
	bool operator()(int i){
		if(T>0){
	    		// keys and counters 
	    		RNG philox; 	
			RNG::ctr_type c={{}};
    			RNG::key_type k={{}};
    			RNG::ctr_type r;
			// Garantiza una secuencia random "unica" para cada thread	
    			k[0]=i; 
			c[1]=seed;
			c[0]=SEED;
			r = philox(c, k);
		
			// metropolis: si fth<fel -> acepted, si fth>fel acepted con prob exp(-Delta/T)
			return bool(u01_open_closed_32_53(r[0]) < exp(-(fth[i]-fel[i])/T));
		}
		else return bool(fel[i]>fth[i]);				
	}
};

struct givemeyourrate{

	FLOAT *fel;
	FLOAT *fth;
	int seed;
	FLOAT T;
	givemeyourrate(FLOAT *_fel, FLOAT *_fth, FLOAT _T, int _seed):fel(_fel), fth(_fth), seed(_seed), T(_T){};

	__device__
	FLOAT operator()(int i){
		if(T>0){
	    	// keys and counters 
	    	/*RNG philox; 	
			RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;
			// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=i; 
			c[1]=seed;
			c[0]=0;
			r = philox(c, k);
			*/
		// metropolis: si fth<fel -> acepted, si fth>fel acepted con prob exp(-Delta/T)
			return exp(-(fth[i]-fel[i])/T);
		}
		else return FLOAT(fel[i]>fth[i]);				
	}
};

struct givemeyourbarrier{

	FLOAT *fel;
	FLOAT *fth;
	givemeyourbarrier(FLOAT *_fel, FLOAT *_fth):fel(_fel), fth(_fth){};
	__device__
	FLOAT operator()(int i){
		return FLOAT(fth[i]-fel[i]);				
	}
};



// inicialize with gaussians
void initialize_random(thrust::device_vector<FLOAT> &v, FLOAT mean, FLOAT sigma, int seed)
{
	int size=v.size();
	thrust::transform(
		thrust::make_counting_iterator(0),thrust::make_counting_iterator(size), 
		v.begin(),gaussian_functor(mean,sigma,seed)
	);

	FLOAT promedio=thrust::reduce(v.begin(),v.end(),FLOAT(0.0),thrust::plus<FLOAT>())/v.size();

	using namespace thrust::placeholders;
	thrust::transform(v.begin(),v.end(),v.begin(),_1-promedio+mean);	
}

/*void adjust(thrust::device_vector<FLOAT> &v, FLOAT mean, FLOAT sigma, int seed)
{
	FLOAT promedio=thrust::reduce(v.begin(),v.end(),FLOAT(0.0),thrust::plus<FLOAT>())/v.size();
}*/


// detect points with fel>fth among N points, and put them in a reduced list of m indices (there are m unstable)
int detect_inestables(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth, thrust::device_vector<int> &indices, float Temp, int t)
{
	// detect unstable indices
	FLOAT *fthraw=thrust::raw_pointer_cast(fth.data());
	FLOAT *felraw=thrust::raw_pointer_cast(fel.data());
	//thrust::device_vector<int> unstable_indices(N); //INEFICIENTE: reemplazar por alocacion estatica

	thrust::device_vector<int>::iterator end=
	thrust::copy_if(
		thrust::make_counting_iterator(0),thrust::make_counting_iterator(N),
		indices.begin(),above(felraw,fthraw,Temp,t)
	);
	int m=(end-indices.begin());

	//indices.resize(m);
	//thrust::copy(unstable_indices.begin(),unstable_indices.begin()+m,indices.begin());

	return m;
}


// EMPROLIJAR ESTO!!!
int kinetic_montecarlo(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth, thrust::device_vector<int> &indices, float Temp, int &t,
	thrust::device_vector<FLOAT> &rates)
{
	// detect unstable indices
	FLOAT *fthraw=thrust::raw_pointer_cast(fth.data());
	FLOAT *felraw=thrust::raw_pointer_cast(fel.data());
	FLOAT *ratesraw=thrust::raw_pointer_cast(rates.data());
	//thrust::device_vector<int> unstable_indices(N); //INEFICIENTE: reemplazar por alocacion estatica

	#ifndef EXTREME
	thrust::transform(
		thrust::make_counting_iterator(0),thrust::make_counting_iterator(N),
		rates.begin(),givemeyourrate(felraw,fthraw,Temp,t)
	); // rates <= exp[-(fth-fel)/T]

	thrust::inclusive_scan(rates.begin(), rates.end(), rates.begin()); // in-place scan
	float Q=rates[N-1];

	// primer numero random para seleccionar evento
	float u=rand()*Q/RAND_MAX;
	int evento= int(thrust::lower_bound(rates.begin(), rates.end(), u)-rates.begin());

	float uu=rand()*1.0/RAND_MAX;
    	t+=logf(1./uu)/Q;

	indices[0]=evento;
	#else
	//ineficiente, no hace falta calcular una exponencial...
	thrust::transform(
		thrust::make_counting_iterator(0),thrust::make_counting_iterator(N),
		rates.begin(),givemeyourbarrier(felraw,fthraw)
	); // rates <= (fth-fel)

	// choose minimum barrier min_i (fth-fel)_i
	thrust::device_vector<FLOAT>::iterator iter = thrust::min_element(rates.begin(), rates.end()); 
	int evento = int(iter - rates.begin()); 
	indices[0]=evento;
	if(evento < 0 || evento>=N) std::cout << "error min_element !" << std::endl;
	#endif

	int m=1; //only one is activated, with index=evento
	
	return m;
}



// arreglos auxiliares: apuntan a buffers
thrust::device_vector<int>::iterator indices_left_begin;
thrust::device_vector<int>::iterator indices_right_begin;
thrust::device_vector<FLOAT>::iterator dx_begin;
// given the list of unstables, updates its thresholds and discharge on left/right neighbors
void update_inestables(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth,
thrust::device_vector<float> &u, thrust::device_vector<int> &indices, int ninest, int t)
{
	/*std::cout << "\n#originales\n";
	std::cout << "#fth fel\n";
	for(int i=0;i<N;i++)
	std::cout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	std::cout << std::endl;
	*/

	// number of unstable sites: ninest

	FLOAT *fthraw=thrust::raw_pointer_cast(fth.data());
	FLOAT *felraw=thrust::raw_pointer_cast(fel.data());
	FLOAT *uraw=thrust::raw_pointer_cast(u.data());

	typedef thrust::device_vector<FLOAT>::iterator ElementIterator;
	typedef thrust::device_vector<int>::iterator   IndexIterator;

	// UPDATE OF THRESHOLDS FORCES
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterth(fth.begin(), indices.begin());
	#ifndef QUENCHED
	thrust::transform(
		indices.begin(), indices.begin()+ninest, iterth,
		gaussian_functor(MEAN,SIGMA,t)
	);
	#else
	thrust::transform(
		indices.begin(), indices.begin()+ninest, iterth,
		quenched_gaussian_functor(MEAN,SIGMA,uraw)
	);
	#endif

/*
	std::cout << "\n#luego de updatear los thresholds\n";
	std::cout << "#fth fel\n";
	for(int i=0;i<N;i++)
	std::cout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	std::cout << std::endl;
*/
	// random displacement
	//thrust::device_vector<FLOAT> dx(ninest);//INEFICIENTE

	// UPDATE ALL DISPLACEMENTS
	#ifndef QUENCHED
	thrust::transform(indices.begin(),indices.begin()+ninest,dx_begin,poisson_functor(MEAN,t)); 
	#else
	thrust::transform(indices.begin(),indices.begin()+ninest,dx_begin,quenched_poisson_functor(MEAN,uraw)); 
	#endif

	using namespace thrust::placeholders; 

	thrust::permutation_iterator<ElementIterator,IndexIterator> iteru(u.begin(), indices.begin());
	#ifndef LATTICESTEPS
	thrust::transform(
		iteru, iteru+ninest, dx_begin, iteru, _1+_2
	);
	#else
	thrust::transform(
		iteru, iteru+ninest, iteru, _1+LATTICESTEPS
	);
	#endif

	#ifndef FREEPARTICLES
	// descarga del central
	thrust::permutation_iterator<ElementIterator,IndexIterator> iterel(fel.begin(), indices.begin());
	thrust::transform(
		iterel, iterel+ninest, dx_begin, iterel, _1-1.0*_2
	);

/*
	std::cout << "\n#luego de updatear las fuerzas elasticas locales\n";
	std::cout << "#fth fel\n";
	for(int i=0;i<N;i++)
	std::cout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	std::cout << std::endl;
*/

	// carga sobre los izquierdos
	//thrust::device_vector<int> indices_left(ninest);//INEFICIENTE

	//thrust::transform(indices.begin(),indices.begin()+ninest,indices_left_begin,(_1-1+N)%N); // sospechoso de error...
	thrust::transform(indices.begin(),indices.begin()+ninest,indices_left_begin,periodicwrap(-1));


	thrust::permutation_iterator<ElementIterator,IndexIterator> iterleft(fel.begin(), indices_left_begin);
	thrust::transform(iterleft, iterleft+ninest, dx_begin, iterleft, _1+0.5*_2);
/*
	std::cout << "\n#luego de updatear las fuerzas elasticas de la izquierda\n";
	std::cout << "#fth fel\n";
	for(int i=0;i<N;i++)
	std::cout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	std::cout << std::endl;
*/
	// carga sobre los derechos
	//thrust::device_vector<int> indices_right(ninest);//INEFICIENTE

	//thrust::transform(indices.begin(),indices.begin()+ninest,indices_right_begin,(_1+1)%N); // sospechoso de error...
	thrust::transform(indices.begin(),indices.begin()+ninest,indices_right_begin,periodicwrap(1));


	thrust::permutation_iterator<ElementIterator,IndexIterator> iterright(fel.begin(), indices_right_begin);
	thrust::transform(iterright, iterright+ninest, dx_begin, iterright, _1+0.5*_2);
	#endif
	
	//fuerzasout << "\n#luego de updatear las fuerzas elasticas de la derecha (todo listo)\n";
	//fuerzasout << "#fth fel\n";
	//for(int i=0;i<N;i++)
	//fuerzasout << i*1.0 << " " << fth[i] << " " << fel[i] << std::endl;
	//fuerzasout << std::endl;
	
}
//================================================

thrust::tuple<int,FLOAT> whichisnext(thrust::device_vector<FLOAT> &fel,thrust::device_vector<FLOAT> &fth)
{
	using namespace thrust::placeholders;
	thrust::device_vector<FLOAT> dif(N);
	thrust::transform(fel.begin(),fel.end(),fth.begin(),dif.begin(),_2-_1);

	//FLOAT x=thrust::reduce(dif.begin(),dif.begin(),FLOAT(dif[0]),thrust::minimum<FLOAT>());
	thrust::device_vector<FLOAT>::iterator este = thrust::min_element(dif.begin(), dif.end());
	std::cout << "#next one is " << int(este-dif.begin()) << " with Delta=" << *este << std::endl;

	return  thrust::make_tuple(int(este-dif.begin()),FLOAT(*este));
}


#include "extra.h"

#ifndef STARTPRINTINGCONFIGS
#define STARTPRINTINGCONFIGS 10
#endif

#ifndef DRIVECHANGE
#define DRIVECHANGE 0.5
#endif

int main(int argc, char **argv)
{
	std::cout << "#global seed=" << SEED << std::endl;
	std::cout << "#usage:" << argv[0] << "f T" << std::endl;

	thrust::device_vector<FLOAT> fth(N); //pinning threshold forces
	thrust::device_vector<FLOAT> fel(N); //elastic interaction forces
	thrust::device_vector<FLOAT> rates(N); // exp(-(fth-fel)/T)
	thrust::device_vector<FLOAT> u(N,float(0.0)); // desplazamientos 

	thrust::device_vector<int> inest(N); // indices de todos los sitios inestables

	// auxiliares
	thrust::device_vector<int> buffer_int(N);
	thrust::device_vector<float> buffer_float(N);
	indices_left_begin=buffer_int.begin();
	indices_right_begin=buffer_int.begin();
	dx_begin=buffer_float.begin();;

	// inicializatio of thresholds
	initialize_random(fth,MEAN,SIGMA,0);

	// driving force from the command line or defaultvalue
	float drive=(argc>1)?(atof(argv[1])):(1.6);

	// gaussian 
	//initialize_random(fel,drive,SIGMA,1);
	// constant 
	thrust::fill(fel.begin(),fel.end(),drive);

	// temperature from the command line or default value	
	float Temp=(argc>2)?(atof(argv[2])):(-1.0);

	int n;
	int t=0;

	std::ofstream inestout("inestables.dat"); 
	std::ofstream fuerzasout("fuerzas.dat");
	std::ofstream configsout("configs.dat");

	thrust::host_vector<int> monitor(50);

	//logsequence2(monitor,1.5);
	//exit(1);

	logsequence(monitor);
	int next=0;

	int tmax=50000;
	if(argc>3) tmax=atoi(argv[3]);

	using namespace thrust::placeholders; 

	//gpu_timer crono;
	omp_timer crono;

	float ms_updates=0.0;
	float ms_detect=0.0;

	#ifdef FOURIER
	structure_factor Sofq(u);
	#endif

	#ifdef HISTOGRAMA
	histograma_gsl Hgsl(1000, 0, drive,fel,fth);
	#endif

	std::ofstream fsofq("Sofq_vs_time.dat");
	std::ofstream events("kmcevents.dat");

	int number_of_events=0;
	int number_of_metastables=0;

	bool is_metastable=0;
	bool apply_new_force=1;
/*	bool apply_new_force2=1;
	bool apply_new_force3=1;
	bool apply_new_force4=1;
	bool apply_new_force5=1;
*/
	//for(int ava=0;ava<100;ava++){
	//while(number_of_metastables<tmax)
	while(t<tmax)
	{
		crono.tic();

		// MC detection	
		n=detect_inestables(fel,fth,inest,-1,t); 

		// if Temp very small, and metastable state, it is likely that n=0, so we use KMC
		#ifdef KMC
		if(n==0) {
			// imprimir config
 			if(number_of_metastables>STARTPRINTINGCONFIGS) print_config(u, configsout);
			n=kinetic_montecarlo(fel,fth,inest,Temp,t,rates);
			number_of_metastables++;
			events << t << " " << fth[inest[0]]-fel[inest[0]] << " kmc" << std::endl;
			std::cout << "metastable, activating " << inest[0] << " -> " << number_of_metastables << std::endl;
			is_metastable=1;			
		}
		else is_metastable=0;
		#endif
		ms_detect+=crono.tac();

		#ifdef PRINTACTIVITYPATTERN
  		print_inestables(inestout, inest, n);
  		#endif

  		//if(n>0)
		//events << t << " " << fth[inest[0]]-fel[inest[0]] << std::endl;



		#ifdef LOGMONITOR
		if(t==monitor[next] || (t+1==tmax))
		#else
		if(t%TPROP==0 && t>TEQUIL)
		#endif	
		{ 
			#ifdef PRINTPROPS
			float ucm = thrust::reduce(u.begin(), u.end())/float(N);
			float wcm = thrust::transform_reduce(u.begin(), u.end(),(_1-ucm)*(_1-ucm),float(0.0),thrust::plus<float>())/float(N);

			std::cout << t << " " << n << 
			" " << thrust::reduce(fel.begin(), fel.end())/N << 
			" " << thrust::reduce(fth.begin(), fth.end())/N << 
			" " << wcm << " " << ucm  << 
			" " << minimum_diference(fel, fth) << 
			std::endl;
			#endif

			next++;

			#ifdef FOURIER
			Sofq.transform_fourier();
			Sofq.print_cumu();
			Sofq.print_inst(fsofq);
			#endif

			// ojo: es lento ...
			#ifdef HISTOGRAMA
			Hgsl.increment();
			Hgsl.print();
			#endif
		}

		crono.tic();


		//update_inestables_dp(fel,fth,u,inest,t); // directed percolation update
		update_inestables(fel,fth,u,inest,n,t); // parallel update		
		number_of_events+=n;

		if(is_metastable){ 
			#ifdef EXTREME
			t++; // KMC EXTREME no se puede calcular el salto de tiempo real, seria infinito, asi que avanzamos en 1
			#endif
			// KMC actualiza el tiempo t el solo
		}
		else{ 
			// parallel update of several unstable sites takes one step
			t++;	
		}

		ms_updates+=crono.tac();


		#ifdef MIDDLETON
		if(number_of_metastables==STARTPRINTINGCONFIGS && apply_new_force && is_metastable){ 
			thrust::transform(fel.begin(),fel.end(),fel.begin(),_1-DRIVECHANGE);
			std::cout << "new drive" << std::endl;
			apply_new_force=0;
		}	
		/*if(number_of_metastables==1000 && apply_new_force2 && is_metastable){ 
			thrust::transform(fel.begin(),fel.end(),fel.begin(),_1-0.1);
			std::cout << "new drive" << std::endl;
			apply_new_force2=0;
		}	
		if(number_of_metastables==1500 && apply_new_force3 && is_metastable){ 
			thrust::transform(fel.begin(),fel.end(),fel.begin(),_1-0.1);
			std::cout << "new drive" << std::endl;
			apply_new_force3=0;
		}	
		if(number_of_metastables==2000 && apply_new_force4 && is_metastable){ 
			thrust::transform(fel.begin(),fel.end(),fel.begin(),_1-0.1);
			std::cout << "new drive" << std::endl;
			apply_new_force4=0;
		}	
		if(number_of_metastables==2500 && apply_new_force5 && is_metastable){ 
			thrust::transform(fel.begin(),fel.end(),fel.begin(),_1-0.1);
			std::cout << "new drive" << std::endl;
			apply_new_force5=0;
		}*/	
		#endif

		//gnuplot> plot for[i=0:100] 'inestables.dat' index i u 2:1 t '' pt 5
		//for(int i=0;i<inest.size();i++) inestout << t << " " << inest[i] << " " << ava << std::endl;
		//inestout << "\n\n";
		
		//for(int i=0;i<N;i++)
		//fuerzasout << t << " " << i*1.0 << " " << fth[i] << " " << fel[i] << " " << std::endl;
		//fuerzasout << "\n\n";
	}
	std::cout << "#ms updates = " << ms_updates << std::endl;
	std::cout << "#ms detect = " << ms_detect << std::endl;
	return 0;
}

