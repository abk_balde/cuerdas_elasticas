# version middleton, cambia la fuerza despues de un numero dado de estados metastables
echo "compiling MIDDLETON"
nvcc qew+m.cu -o cudamid.out -DKMC -DEXTREME -DQUENCHED -DMIDDLETON -DPRINTPROPS -DSTARTPRINTINGCONFIGS=10 -DDRIVECHANGE=0.1 #-arch=sm_20 #-Wno-deprecated-gpu-targets  #-DLATTICESTEPS=2.0 ;  

# version normal no cambia la fuerza
echo "compiling NORMAL"
nvcc qew+m.cu -o cudaconst.out -DKMC -DEXTREME -DQUENCHED -DPRINTPROPS -DSTARTPRINTINGCONFIGS=10 -DDRIVECHANGE=0.1 #-arch=sm_20 #-Wno-deprecated-gpu-targets #-DLATTICESTEPS=2.0 ;

echo "running MIDDLETON"
#optirun ./cudamid.out 1.0 -1 5000;
./cudamid.out 1.5 -1 5000;
mv configs.dat configsmid.dat

echo "runnning NORMAL"
#optirun ./cudaconst.out 1.0 -1 5000;
./cudaconst.out 1.5 -1 5000;
mv configs.dat configsconst.dat

