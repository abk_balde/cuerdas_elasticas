#include "cutil.h"	// CUDA_SAFE_CALL, CUT_CHECK_ERROR


struct modulo_cuadrado
{
	__device__
	REAL operator()(COMPLEX d)
	{
		return d.x*d.x+d.y*d.y;
	}
};

struct modulo_cuadrado_for_each
{
	int W;
	modulo_cuadrado_for_each(int W_):W(W_){};

	template <typename Tuple>
    	__host__ __device__
	void operator()(Tuple tup)
	{
		COMPLEX fou=thrust::get<0>(tup);
		REAL fou2 = fou.x*fou.x+fou.y*fou.y;

		thrust::get<2>(tup)=fou2; 

		// du/dt = -u/tau + f/tau -> u=int exp[-(t-t')/tau] (f(t')/tau) dt' 
		// u(t+dt) = u(t)(1-dt/tau) + f*dt/tau -> u = sum exp[-(t-t')/tau] f(t') dt/tau  

		thrust::get<1>(tup) = thrust::get<1>(tup)*(1.0-1.0/W) + fou2/W; 
	}
};


/*
Factor_de_estructura S(); 
S.init(N); // inicializa para transformar N elementos
S.fft(D_real); // transforma Fourier  
S.acum(D); // acumula la nueva FFT D en un acumulador de |FFT|^2   

*/
class Factor_de_estructura
{
	public:

	// container para acumular el factor de estructura
	thrust::device_vector<REAL> Sofq_inst;
	thrust::device_vector<REAL> Sofq_acum;

	//typedef thrust::device_ptr<REAL> dptr;
	//std::vector<dptr> Sofq_inst_aging();

	thrust::device_vector<COMPLEX> D_fourier;
	COMPLEX *d_fourier; 
	cufftHandle plan;

	REAL *d_real;
	
	int Ncomp;
	int L;
	
	//contador
	int cont;

	void fft(){
		//REAL *d_real = thrust::raw_pointer_cast(&D_real[0]); 
		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftExecD2Z(plan, d_real,d_fourier));
		#else
		CUFFT_SAFE_CALL(cufftExecR2C(plan, d_real,d_fourier));
		#endif	
		acum(D_fourier);
	}

	void resetacum(){
		thrust::fill(Sofq_acum.begin(), Sofq_acum.end(),0.0f);	
	}

	void init(thrust::device_vector<REAL> &D_real)
	{
		d_real = thrust::raw_pointer_cast(&D_real[0]);

		L=D_real.size();
		Ncomp=L/2+1;


		Sofq_acum.resize(Ncomp);
		Sofq_inst.resize(Ncomp);			
		thrust::fill(Sofq_acum.begin(), Sofq_acum.end(),0.0f);
		thrust::fill(Sofq_inst.begin(), Sofq_inst.end(),0.0f);
		cont=0;
		
		D_fourier.resize(Ncomp);
		d_fourier = thrust::raw_pointer_cast(&D_fourier[0]); 

		COMPLEX zero; zero.x=zero.y=0.0;
		thrust::fill(D_fourier.begin(), D_fourier.end(),zero);

		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftPlan1d(&plan,L,CUFFT_D2Z,1));
		#else
		CUFFT_SAFE_CALL(cufftPlan1d(&plan,L,CUFFT_R2C,1));
		#endif
	};

	void acum(thrust::device_vector<COMPLEX> &D)
	{
		cont++;
		thrust::for_each(
			thrust::make_zip_iterator(thrust::make_tuple(D.begin(),Sofq_acum.begin(),Sofq_inst.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(D.end(),Sofq_acum.end(),Sofq_inst.end())),
			modulo_cuadrado_for_each(20)
		);

		//thrust::transform(D.begin(),D.end(),Sofq_inst.begin(),modulo_cuadrado());
		//thrust::transform(Sofq_inst.begin(),Sofq_inst.end(),Sofq.begin(), Sofq.begin(),_1+_2); // Sofq+=Sofq_inst;
		//thrust::transform(Sofq.begin(),Sofq.end(),Sofq_inst.begin(), Sofq.begin(),thrust::plus<REAL>()); // Sofq+=Sofq_inst;
	}		

	void print_inst(std::ofstream &fout)
	{
		thrust::host_vector<REAL> H_Sofq_inst=Sofq_inst;
	
		for(int i=0;i < Ncomp;i++)
		fout << H_Sofq_inst[i]/L << "\n"; 	
		fout << "\n\n";
	}		

	//void print(const char *nombre)
	void print(std::ofstream &fout)
	{
		//ofstream fout(nombre);
		thrust::host_vector<REAL> H_Sofq_acum=Sofq_acum;
		thrust::host_vector<REAL> H_Sofq_inst=Sofq_inst;

		for(int i=0;i < Ncomp;i++)
		fout << H_Sofq_acum[i]/(cont*L) << " " << H_Sofq_inst[i]/(L) << " " << cont << "\n";	
		fout << "\n\n";
	}		
};

