/*
=============================================================================================
A. B. Kolton

5/01/2015.

* Langevin dynamics simulation of a vortex line
* Random point or columnar disorder is dynamically generated (using random123)
* force driven or velocity driven ensemble (moving isotropic parabola)
=============================================================================================
*/

#include<cmath>
#include<fstream>
#include <iostream>
#include "simple_timer.h"

/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG
typedef r123::Philox2x32 RNG2; // particular counter-based RNG
typedef r123::Philox4x32 RNG4; // particular counter-based RNG


/* algunos headers de la libreria thrust */
// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
// https://github.com/thrust/thrust/wiki/Documentation
#include<thrust/transform.h>
#include<thrust/copy.h>
#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include<thrust/functional.h>
#include<thrust/sequence.h>
#include <thrust/fill.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/reduce.h>
#include <thrust/extrema.h>

// CUFFT include http://docs.nvidia.com/cuda/cufft/index.html

#include <cufft.h>
#define STRUCTUREFACTORUPDATES 1000
#if defined STRUCTUREFACTORUPDATES
	#ifdef DOUBLE_PRECISION
	typedef cufftDoubleReal REAL;
	typedef cufftDoubleComplex COMPLEX;
	#else
	typedef cufftReal REAL;
	typedef cufftComplex COMPLEX;
	#endif
#else
	// precisón elegida para los números reales
	typedef float 	REAL;
#endif

#include "structurefactor.h"

#include "defines.h"

// para evitar poner "thrust::" a cada rato all llamar sus funciones
using namespace thrust;


// para generar números aleatorios gausianos a partir de dos uniformes
// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
__device__
REAL box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	REAL u1 = u01_open_closed_32_53(r_philox[0]);
  	REAL u2 = u01_open_closed_32_53(r_philox[1]);

  	REAL r = sqrt( -2.0*log(u1) );
  	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);    			
}

// define a 2d float vector
typedef thrust::tuple<float,float> vec2;

// generates two gaussian random numbers
__device__
vec2 box_muller(RNG4::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	float u1x = u01_open_closed_32_53(r_philox[0]);
  	float u2x = u01_open_closed_32_53(r_philox[1]);
	float u1y = u01_open_closed_32_53(r_philox[2]);
  	float u2y = u01_open_closed_32_53(r_philox[3]);

  	float rx = sqrtf( -2.0*logf(u1x) );
  	float thetax = 2.0*M_PI*u2x;

  	float ry = sqrtf( -2.0*logf(u1y) );
  	float thetay = 2.0*M_PI*u2y;

	return vec2(rx*sinf(thetax),ry*sinf(thetay));    			
}

#ifdef PRINTCONFS
// used for printing configuration snapshots
std::ofstream configs_out("configurations.dat");
void print_configuration(device_vector<REAL> &u, device_vector<REAL> &v,device_vector<REAL> &fu, device_vector<REAL> &fv, int time)
{
	long i=0;
	int every = int(L*1.0/PRINTCONFS);
	for(i=1;i<u.size()-1;i+=every)
	{
		configs_out 
		<< u[i] << " " <<  v[i] << " " 
		<< fu[i] << " " << fv[i] << " " 
		<< time << std::endl;
	}
	configs_out << "\n\n";
}
#endif

// functor usado para calcular la rugosidad 
struct roughtor: public thrust::unary_function<REAL,REAL>
{
    REAL u0; // un "estado interno" del functor	
    roughtor(REAL _u0):u0(_u0){};	
    __device__
    REAL operator()(REAL u)
    {	
	return (u-u0)*(u-u0);
    }
};	


/////////////////////////////////////////////////////////////////////////////
// FUNCTORS usados en los algoritmos TRANSFORM

__device__
void pinning_field(float X,float Y, unsigned tid, vec2 &f)
{
	RNG rng;
	RNG::ctr_type c={{}};
	RNG::key_type k={{}};
	RNG::ctr_type r;
	k[0]=tid;

	float x0=int(X);
	float y0=int(Y);
	float x1=x0+1;
	float y1=y0+1;
	float x=X;
	float y=Y;

	/*
	// x component
	k[1]=1;

	c[0]=uint32_t(x0); c[1]=uint32_t(y0);r = rng(c, k);
	float f00 = u01_open_closed_32_53(r[0])-0.5;
	
	c[0]=uint32_t(x1); c[1]=uint32_t(y0);r = rng(c, k);
	float f10 = u01_open_closed_32_53(r[0])-0.5;

	c[0]=uint32_t(x0); c[1]=uint32_t(y1);r = rng(c, k);
	float f01 = u01_open_closed_32_53(r[0])-0.5;

	c[0]=uint32_t(x1); c[1]=uint32_t(y1);r = rng(c, k);
	float f11 = u01_open_closed_32_53(r[0])-0.5;

	// bilinearly interpolated potential
	float fx = f00*(x1-x)*(y1-y) + f10*(x-x0)*(y1-y) 
		   + f01*(y-y0)*(x1-x) + f11*(y-y0)*(x-x0);
		
	// y component
	k[1]=2;

	c[0]=uint32_t(x0); c[1]=uint32_t(y0);r = rng(c, k);
	f00 = u01_open_closed_32_53(r[0])-0.5;
	
	c[0]=uint32_t(x1); c[1]=uint32_t(y0);r = rng(c, k);
	f10 = u01_open_closed_32_53(r[0])-0.5;

	c[0]=uint32_t(x0); c[1]=uint32_t(y1);r = rng(c, k);
	f01 = u01_open_closed_32_53(r[0])-0.5;

	c[0]=uint32_t(x1); c[1]=uint32_t(y1);r = rng(c, k);
	f11 = u01_open_closed_32_53(r[0])-0.5;

	// bilinearly interpolated potential
	float fy = f00*(x1-x)*(y1-y) + f10*(x-x0)*(y1-y) 
		   + f01*(y-y0)*(x1-x) + f11*(y-y0)*(x-x0);

	*/
	
	k[1]=1;

	c[0]=uint32_t(x0); c[1]=uint32_t(y0);r = rng(c, k);
	float f00 = u01_open_closed_32_53(r[0])-0.5;
	
	c[0]=uint32_t(x1); c[1]=uint32_t(y0);r = rng(c, k);
	float f10 = u01_open_closed_32_53(r[0])-0.5;

	c[0]=uint32_t(x0); c[1]=uint32_t(y1);r = rng(c, k);
	float f01 = u01_open_closed_32_53(r[0])-0.5;

	c[0]=uint32_t(x1); c[1]=uint32_t(y1);r = rng(c, k);
	float f11 = u01_open_closed_32_53(r[0])-0.5;

	// bilinearly interpolated potential
	/*float fxy = f00*(x1-x)*(y1-y) + f10*(x-x0)*(y1-y) 
		   + f01*(y-y0)*(x1-x) + f11*(y-y0)*(x-x0);*/

	float fx= -f00*(y1-y) + f10*(y1-y) 
		   - f01*(y-y0) + f11*(y-y0);

	float fy = -f00*(x1-x) - f10*(x-x0) 
		   + f01*(x1-x) + f11*(x-x0);

	thrust::get<0>(f)=fx;
	thrust::get<1>(f)=fy;
	//return thrust::make_tuple(fx, fy);		
}

//pinning_centers_forces(u,v, tid, RINTPIN, RCUTPIN, fpin);	
__device__
void pinning_centers_forces(float X,float Y, unsigned Z, const float rint, const float rcut, vec2 &f)
{
	RNG2 rng;
	RNG2::ctr_type c={{}};
	RNG2::key_type k={{}};
	RNG2::ctr_type r;

	// positions inside the computational box
	X=X-floor(X/LX)*LX;
	Y=Y-floor(Y/LY)*LY;

	// 2D cell coordinates
	unsigned int ic=(X/rcut); // fac ~ 1.0/rf
	unsigned int jc=(Y/rcut);

	// neighbor cells coordinates
	unsigned int icnei[9];
	unsigned int jcnei[9];

	// centro
	icnei[0]=ic; jcnei[0]=jc;

	// NX = LX/rcut, NY = LY/rcut

	// derecha
	icnei[1]=(ic+1)%WPIN; jcnei[1]=jc;

	//fila arriba: derecha, centro, izquierda
	icnei[2]=(ic+1)%WPIN; jcnei[2]=(jc+1)%HPIN;
	icnei[3]=ic; jcnei[3]=(jc+1)%HPIN;
	icnei[4]=(ic-1+WPIN)%WPIN; jcnei[4]=(jc+1)%HPIN;

	// izquierda
	icnei[5]=(ic-1+WPIN)%WPIN; jcnei[5]=jc;

	// izquierda abajo
	icnei[6]=(ic-1+WPIN)%WPIN; jcnei[6]=(jc-1+HPIN)%HPIN;

	// abajo
	icnei[7]=ic; jcnei[7]=(jc-1+HPIN)%HPIN;

	// abajo derecha
	icnei[8]=(ic+1)%WPIN; jcnei[8]=(jc-1+HPIN)%HPIN;

	for(int n=0;n<9;n++){
		// counter based on the 2d pinning neighbor cell "coordinates" 
		c[0]=uint32_t(icnei[n]+PISO); c[1]=uint32_t(jcnei[n]+PISO);

		#ifdef COLUMNAR
		k[0]=0;
		#else
		k[0]=Z*NPINSPERCELL*2;
		//k[0]=Z;
		#endif		

		// first random number is used for the number of pins
		// r = rng(c, k);
		// it is not uniform...:
	 	// unsigned int npins = (u01_open_closed_32_53(r[0])*NPINSPERCELL);
		// nor exponential...:
	 	// unsigned int npins = -logf(u01_open_closed_32_53(r[0]))*NPINSPERCELL;
		// but poisson (two alternatives shown):
		#ifdef POISSON1
		REAL ll,uu, pp; unsigned int kk;
		ll=expf(-NPINSPERCELL);kk=0; pp=1.f;
		do{
			kk++;
			r = rng(c, k);
			uu=u01_open_closed_32_53(r[0]); pp=pp*uu;
			k[0]++;
		}while(pp>ll);
		unsigned int npins=kk-1;	
		#else
		float uu,xx,pp,ss;
		xx=0.f; pp=ss=expf(-NPINSPERCELL);
		r = rng(c, k);k[0]++;
		uu=u01_open_closed_32_53(r[0]); 
		while(uu>ss){
			xx++;
			pp=pp*NPINSPERCELL/xx;
			ss=ss+pp;
		}
		unsigned int npins=xx;
		#endif

		float xpin, ypin, fpinx, fpiny;
		float dx,dy, dr2;	
		for(int p=0;p<npins;p++)
		{
			// extra random numbers (moving the key k[0]) are used for pin coordinates in the neighbor cell
			r = rng(c, k);	k[0]++;

	 		xpin = (icnei[n] + u01_open_closed_32_53(r[0]))*rcut;
	 		ypin = (jcnei[n] + u01_open_closed_32_53(r[1]))*rcut;

			dx = (X-xpin); if(dx>LX*0.5) dx+=-LX; if(dx<-LX*0.5) dx+=LX;
			dy = (Y-ypin); if(dy>LY*0.5) dy+=-LY; if(dy<-LY*0.5) dy+=LY;

			dr2=dx*dx+dy*dy;

			#ifndef MACLAS
			if(dr2<rcut*rcut){
				// a gaussian attractive potential
				/*vpin=-expf(-(dx*dx+dy*dy)/(rint*rint))*0.05/(rint*rint);
				thrust::get<0>(f)+=vpin*dx; // /(rint*rint);
				thrust::get<1>(f)+=vpin*dy; // /(rint*rint);
				*/

				single_pin_force(dx,dy, rint, rcut, fpinx, fpiny);
				thrust::get<0>(f)+=fpinx; 
				thrust::get<1>(f)+=fpiny; 
			}
			#else
			if(dr2<rcut*rcut){
				// a gaussian attractive potential
				/*vpin=-expf(-(dx*dx)/(rint*rint))*0.05/(rint*rint);
				thrust::get<0>(f)+=vpin*dx; // /(rint*rint);
				thrust::get<1>(f)+=vpin*dy; // /(rint*rint);
				*/

				single_pin_force(dx,0.0, rint, rcut, fpinx, fpiny);
				thrust::get<0>(f)+=fpinx; 
				//thrust::get<1>(f)+=0.0; 
			}
			#endif
		}
	
	}	
}

struct fuerza
{
    RNG rng;       // random number generator
    RNG4 rng4;       // random number generator
    long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL f0;	
    REAL T0;
    REAL Dt;
    REAL W; // parabola's current position		

    fuerza(long _t, float _f0, float _T0, REAL _Dt, float _W):tiempo(_t),f0(_f0),T0(_T0),Dt(_Dt), W(_W)
    {
	noiseamp=sqrt(T0/Dt);
    }; 

    __device__
    thrust::tuple<REAL,REAL> operator()(thrust::tuple<long,REAL,REAL,REAL,REAL,REAL,REAL> tt)
    {	
	// thread/particle id
	uint32_t tid=get<0>(tt); 

	// LAPLACIAN
	REAL um1=get<1>(tt);
	REAL u=get<2>(tt);
	REAL up1=get<3>(tt);
	REAL laplacianou = um1 + up1 - 2.*u;

	REAL vm1=get<4>(tt);
	REAL v=get<5>(tt);
	REAL vp1=get<6>(tt);
	REAL laplacianov = vm1 + vp1 - 2.*v;

	// DISORDER
	REAL quenched_noiseu=0.0; 
	REAL quenched_noisev=0.0; 
#ifndef NODISORDER
	vec2 fpin=vec2(0.0f,0.0f);
	
	#ifndef PINNINGFIELD	
	pinning_centers_forces(u,v, tid, RINTPIN, RCUTPIN, fpin);
	#else 
	pinning_field(u,v, tid, fpin);
	#endif	
	
	quenched_noiseu=thrust::get<0>(fpin)*D0;
	quenched_noisev=thrust::get<1>(fpin)*D0;

	#ifdef FLIM
	REAL totalpinforce = (quenched_noiseu*quenched_noiseu+quenched_noisev*quenched_noisev)/FLIM;
	REAL correction = (totalpinforce<0.1)?(1.0-totalpinforce*totalpinforce/3.0):(tanh(totalpinforce)/(totalpinforce));
	quenched_noiseu*= correction;	
	quenched_noisev*= correction;	
	#endif

#endif
	// THERMAL NOISE
	REAL thermal_noiseu, thermal_noisev;

#ifdef TEMP
	// keys and counters 
    	RNG4::ctr_type C={{}};
    	RNG4::key_type K={{}};
	RNG4::ctr_type R;

	C[1]=SEED2; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}
	K[0]=tid; 	  //  KEY = {threadID} 
	C[0]=uint32_t(tiempo); // COUNTER={tiempo, GLOBAL SEED #2}

	R = rng4(C, K);
	vec2 noise = box_muller(R);	
	thermal_noiseu = noiseamp*thrust::get<0>(noise);    			
	thermal_noisev = noiseamp*thrust::get<1>(noise);    			
#else
	thermal_noiseu=thermal_noisev=0.0;
#endif
	// Fuerza total en el monómero tid

#ifndef MM
	return thrust::make_tuple(laplacianou+quenched_noiseu+thermal_noiseu+f0,laplacianov+quenched_noisev+thermal_noisev);
#else
//	parabola affects the transverse direction
//	return thrust::make_tuple(laplacianou+quenched_noiseu+thermal_noiseu+MM*(W-u),laplacianov+quenched_noisev+thermal_noisev+MM*(-v));

//	parabola doest not affect the transverse direction
	return thrust::make_tuple(laplacianou+quenched_noiseu+thermal_noiseu+MM*(W-u),laplacianov+quenched_noisev+thermal_noisev);
#endif
  }
};


// Explicit forward Euler step: simplest choice.. 
// (control the time step!)
struct euler
{
    REAL Dt;	
    euler(REAL _Dt):Dt(_Dt){};	
    __device__
    REAL operator()(REAL u_old, REAL force)
    {	
	return (u_old + force*Dt);
    }
};	


#ifdef OMP
#include <omp.h>
#endif
/////////////////////////////////////////////////////////////////////////////

#define TAUTRANS	100000
class Cuerda
{
	public:
	#ifdef OMP
	omp_timer t;
	#else
	gpu_timer t;
	#endif

	double timer_fuerzas_elapsed;
	double timer_euler_elapsed;
	double timer_props_elapsed;
	
	#ifdef TRANSDIF
	thrust::host_vector<float> transverse_quadratic_displacement;
	#endif

	#ifdef STRUCTUREFACTOR
	Factor_de_estructura Su,Sv;
	#endif

	Cuerda(){
	#ifdef TRANSDIF
	transverse_quadratic_displacement.resize(TAUTRANS);
	thrust::fill(transverse_quadratic_displacement.begin(),transverse_quadratic_displacement.end(),0.0);
	#endif
	};

	void init(){
		#ifndef OMP
		logout.open("logfile.dat");
		propsout.open("someprops.dat");
		zerosout.open("zeros.dat");
		nzerosout.open("nzeros.dat");
		#else
		logout.open("logfile_omp.dat");
		propsout.open("someprops_omp.dat");
		zerosout.open("zeros_omp.dat");
		nzerosout.open("nzeros_omp.dat");
		#endif
		
		logout << "initialization..."; std::cout.flush();

		#ifdef OMP
		std::cout << "#conociendo el host, OMP threads = " << omp_get_max_threads() << std::endl;
		#endif		

		u.resize(L+2);
		u_it0 = u.begin()+1;
		u_it1 = u.end()-1;
		u_raw_ptr = raw_pointer_cast(&u[1]);

		v.resize(L+2);
		v_it0 = v.begin()+1;
		v_it1 = v.end()-1;
		v_raw_ptr = raw_pointer_cast(&v[1]);

		// container de fuerza total: 
		Ftotu.resize(L); 
		Ftotu_it0 = Ftotu.begin();
		Ftotu_it1 = Ftotu.end();

		Ftotv.resize(L); 
		Ftotv_it0 = Ftotv.begin();
		Ftotv_it1 = Ftotv.end();

		// alguna condición inicial chata (arbitraria)
		fill(u_it0,u_it1,10.0); 
		fill(v_it0,v_it1,10.0); 

		// o una condicion inicial rugosa (random)
		//fourier_gen_path(1.5, L, u_it0, 123456);


		#ifdef STRUCTUREFACTOR
		Su.init(u); Sv.init(v);
		#endif

		timer_fuerzas_elapsed=0.0;
		timer_euler_elapsed=0.0;
		timer_props_elapsed=0.0;
	
		tiempo=0;

		f0=F0;
		#ifdef TEMP
		T0=TEMP;
		#endif
		Dt=TIMESTEP;
		W=L*1.0f;
	
		propsout << "tiempo f0 velocityu center_of_massu roughnessu velocityv center_of_massv roughnessv" << std::endl;
		propsout.flush();

		logout << "done!" << std::endl;

		#ifdef MM
		logout << "driving speed = " << f0 << std::endl;
		logout << "parabola mass = " << MM << std::endl;
		#else
		logout << "driving force = " << f0 << std::endl;
		#endif
		logout << "temperature = " << T0 << std::endl;
		logout << "time step = " << Dt << std::endl;
		logout << "string length = " << L << std::endl;
		logout << "pinning interaction cutoff = " << RCUTPIN << std::endl;
		logout << "pinning density = " << NPINSPERCELL/RCUTPIN << std::endl;		

		logout << "defined MACROS:\n";
		#ifdef TEMP
		logout << "finite temperature\n";
		#endif
		#ifdef NODISORDER
		logout << "no disorder\n";
		#endif
		#ifdef FLIM			
		logout << "pinning force with saturation, maxvalue = " << FLIM << std::endl;
		#endif
		#ifdef COLUMNAR			
		logout << "columnar disorder\n";
		#endif
		#ifdef STRUCTUREFACTOR			
		logout << "structure factor is calculated\n";
		logout << "\t each \n" << STRUCTUREFACTOR << "steps" << std::endl;
		#endif
		#ifdef PINNINGFIELD		
		logout << "pinning field\n";
		#endif
		#ifdef GAUSSIANWELLSPINS		
		logout << "gaussian wells pinning centers\n";
		#endif
	};
	
	
	void step()
	{
		static unsigned long mtprop=1;
		static unsigned long count=0;

		#ifdef TRANSDIF
		static REAL trans_disp=0.;
		static REAL trans_disp0=0.;
		#endif

		t.tic(); // para cronometrar el tiempo de la siguiente transformación

		// Impone PBC en el "halo"		
		u[0]=u[L];u[L+1]=u[1];
		v[0]=v[L];v[L+1]=v[1];

		int n=tiempo; tiempo++;
		if(n%CHECK==0) (checkdt());	

		
		// Ftot(Z)= laplacian + disorder + thermal_noise + F0, Z=0,..,L-1
		// or: Ftot(Z)= laplacian + disorder + thermal_noise + MM*(W-u), Z=0,..,L-1
		transform(
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(0),
			u_it0-1,u_it0,u_it0+1,
			v_it0-1,v_it0,v_it0+1
			)),
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(L),
			u_it1-1,u_it1,u_it1+1,
			v_it1-1,v_it1,v_it1+1
			)),
			make_zip_iterator(make_tuple(
			Ftotu_it0,Ftotv_it0
			)),
			fuerza(n,f0,T0,Dt,W)
		);

		timer_fuerzas_elapsed+=t.tac();

		// Explicit forward Euler step, implementado en paralelo en la GPU: 
		// u(X,n) += Ftot(X,n) Dt, X=0,...,L-1
		// see "euler" functor above
		// Notar: no hace falta zip_iterator, ya que transform si soporta hasta dos secuencias de input
		t.tic();
		transform(
			u_it0,u_it1,Ftotu_it0,u_it0, 
			euler(Dt)
		);
		transform(
			v_it0,v_it1,Ftotv_it0,v_it0, 
			euler(Dt)
		);
		W+=f0*Dt;
		timer_euler_elapsed+=t.tac();
		

		#ifdef TRANSDIF
		trans_disp = reduce(v_it0,v_it1,REAL(0.0))/REAL(L); // center of mass position
		int index=n%TAUTRANS;
		if(index==0){
			trans_disp0=trans_disp;
		}
		//std::cout << index << " " << trans_disp << " " << trans_disp0 << std::endl;
		transverse_quadratic_displacement[index] +=
		(trans_disp-trans_disp0)*(trans_disp-trans_disp0);
		#endif

		// properties of interest, each TPROP
		//#ifndef LOGPRINT
		//if(n%TPROP==0){	
		//#else
		if(n%mtprop==0){
			#ifdef LOG10PRINT
			count++;if(count==10){count=0;mtprop*=10;}
			#elif defined(LOG2PRINT)
			count++;if(count==10){count=0;mtprop*=2;}
			#else 
			count++; mtprop=TPROP;
			#endif
		//#endif
			t.tic();

	
			#ifdef TRANSDIF
			std::ofstream fout("transdisp.dat");
			for(int i=0;i<TAUTRANS;i+=10){
				fout << transverse_quadratic_displacement[i]/(n*1.0/TAUTRANS) << std::endl;
			}
			fout.close();
			#endif

			
			/* TODO:
			   usando algoritmos REDUCE 
			   [ http://thrust.github.io/doc/group__reductions.html#gacf5a4b246454d2aa0d91cda1bb93d0c2 ]
			   calcule la velocidad media de la interface
			   y la posición del centro de masa de la interface 
			   HINT:
			   REAL velocity = reduce(....)/L; //center of mass velocity
			   REAL center_of_mass = reduce(....)/L; // center of mass position
			*/
			// SOLUCION:			
			REAL velocityu = reduce(Ftotu_it0,Ftotu_it1,REAL(0.0))/REAL(L); //center of mass velocity
			REAL center_of_massu = reduce(u_it0,u_it1,REAL(0.0))/REAL(L); // center of mass position
			REAL velocityv = reduce(Ftotv_it0,Ftotv_it1,REAL(0.0))/REAL(L); //center of mass velocity
			REAL center_of_massv = reduce(v_it0,v_it1,REAL(0.0))/REAL(L); // center of mass position

			/* TODO: 
			   usando el algoritmo TRANSFORM_REDUCE, 
	[ http://thrust.github.io/doc/group__transformed__reductions.html#ga087a5af8cb83647590c75ee5e990ef66 ]
			   el functor "roughtor" arriba definido, 
			   y la posición del centro de masa "ucm" calculada en el TODO anterior, 
			   calcule la rugosidad (mean squared width) de la interface:
			   roughness := Sum_X [u(X)-ucm]^2 /L
			   HINT:
			   REAL roughness = transform_reduce(...,...,roughtor(center_of_mass),0.0,thrust::plus<REAL>());
			*/
			// SOLUCION:	
			REAL roughnessu = transform_reduce
			(
				u_it0, u_it1,
                                roughtor(center_of_massu),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);
			REAL roughnessv = transform_reduce
			(
				v_it0, v_it1,
                                roughtor(center_of_massv),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);
			// solución alternativa
  			/*REAL roughness = reduce
			(
			make_transform_iterator(u_it0, roughtor(center_of_mass)),
			make_transform_iterator(u_it1, roughtor(center_of_mass)),
			REAL(0.0)
			);
			*/
			timer_props_elapsed+=t.tac();
	
			propsout << tiempo << " " << f0 << " " << velocityu << " " << center_of_massu << " " << roughnessu << " " ;
			propsout << velocityv << " " << center_of_massv << " " << roughnessv << std::endl;
			propsout.flush();
			
			#ifdef PRINTCONFS
			print_configuration(u,v,Ftotu,Ftotv,n);
			#endif
		} // propiedades para monitorear frecuentemente 
				
	};

	void print_timmings(){
		// resultados del timming
		double total_time = (timer_fuerzas_elapsed+timer_euler_elapsed+timer_props_elapsed); 

		std::cout << "L= " << L << " TRUN= " << TRUN << std::endl; 

		std::cout << "Forces calculation -> " << 1e3 * timer_fuerzas_elapsed << " miliseconds (" 
	  	<< int(timer_fuerzas_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Euler step -> " << 1e3 * timer_euler_elapsed << " miliseconds (" 
		  << int(timer_euler_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Properties -> " << 1e3 * timer_props_elapsed << " miliseconds (" 
		  << int(timer_props_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Total -> " << 1e3 * total_time << " miliseconds (100%)" << std::endl;
	};

	void checkdt(){
		REAL maxvel=thrust::reduce(Ftotu_it0,Ftotu_it1,0.0,thrust::maximum<REAL>());	
		//logout << "maxvel=" << maxvel << std::endl;
		if(maxvel*Dt>RINTPIN*0.1){
			logout << "time-step too big" << std::endl;
			Dt=RINTPIN*0.1/maxvel;
			logout << "readjusting to Dt=" << Dt << std::endl;
			//exit(1);
		};
	};

	void dynamics(long trun)
	{
		for(unsigned long n=0;n<trun;n++)
		{
			step();	

			/*#ifdef STRUCTUREFACTOR
			if((n+1)%STRUCTUREFACTOR==0){
				Su.fft(u);
				Sv.fft(v);
			}
			#endif
			*/
		}
		//print_timmings();
	};

	device_vector<REAL>::iterator posicionu_begin(){
		return u_it0;
	};	
	device_vector<REAL>::iterator posicionu_end(){
		return u_it1;
	};		
	REAL get_center_of_massu(){ 
		return reduce(u_it0,u_it1,REAL(0.0))/REAL(L); 
	};
	device_vector<REAL>::iterator posicionv_begin(){
		return v_it0;
	};	
	device_vector<REAL>::iterator posicionv_end(){
		return v_it1;
	};		
	REAL get_center_of_massv(){ 
		return reduce(v_it0,v_it1,REAL(0.0))/REAL(L); 
	};

	REAL get_roughness(REAL cm){
		return  transform_reduce
			(
				u_it0, u_it1,
                                roughtor(cm),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);
	};


	void increment_f0(float df){
		f0=f0+df;
		if(f0<0.0f) f0=0.0f;
	};
	void increment_T0(float dT0){
		T0=T0+dT0;
		if(T0<0.0f) T0=0.0f;
	};
	float get_f0(){
		return f0;
	}
	float get_T0(){
		return T0;
	}	
	REAL get_velocityu(){ 
		return reduce(Ftotu_it0,Ftotu_it1,REAL(0.0))/REAL(L); //center of mass velocity
	}
	REAL get_velocityv(){ 
		return reduce(Ftotv_it0,Ftotv_it1,REAL(0.0))/REAL(L); //center of mass velocity
	}

	private:
	long tiempo;
	float f0;	
	float T0;
	REAL Dt;	
	REAL W;

	// posiciones de los monómeros: 
	// Notar que alocamos dos elementos de mas para usarlos como "halo"
	// esto nos permite fijar las condiciones de borde facilmente, por ejemplo periódicas: u[0]=u[L]; u[L+1]=u[1];
	device_vector<REAL> u;
	device_vector<REAL>::iterator u_it0;
	device_vector<REAL>::iterator u_it1;
	REAL * u_raw_ptr;
	device_vector<REAL> v;
	device_vector<REAL>::iterator v_it0;
	device_vector<REAL>::iterator v_it1;
	REAL * v_raw_ptr;

	// container de fuerza total: 
	device_vector<REAL> Ftotu; 
	// el rango de interés definido por los iteradores 
	device_vector<REAL>::iterator Ftotu_it0;
	device_vector<REAL>::iterator Ftotu_it1;
	device_vector<REAL> Ftotv; 
	// el rango de interés definido por los iteradores 
	device_vector<REAL>::iterator Ftotv_it0;
	device_vector<REAL>::iterator Ftotv_it1;

	// file para guardar algunas propiedades dependientes del tiempo
	std::ofstream logout;
	std::ofstream propsout;
	std::ofstream zerosout;
	std::ofstream nzerosout;
};



/////////////////////////////////////////////////////////////////////////////
/*#include "qew_minimal_solucion.h"
int main(){
	Cuerda C;
	C.dynamics(TRUN);
	C.print_timmings();
	return 0;
}*/

