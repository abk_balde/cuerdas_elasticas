/* PARAMETERS */

////////////////////////////////////////
// comment the following to disactivate/activate  
////////////////////////////////////////
#define TEMP	0.000     // activates langevin noise with temperature TEMP

//#define NODISORDER 	// desactivate any disorder

//#define FLIM	2.0  	// activates saturation of pinning forces up to FLIM 

//#define COLUMNAR 	// activates columnar disorder

//#define PRINTCONFS  	512 // print snaphots with PRINTCONFS points of the configurations  

#define MM	0.1 //#0.00001  // activates velocity driven case with parabola's mass MM [should be ~(10/L)**2]

#define STRUCTUREFACTOR	100 // activates and updates structure factor each STRUCTUREFACTOR steps

////////////////////////////////////////
// simulation parameters 
////////////////////////////////////////
#define L	1024    // number of particles

#define TRUN	100000  // total number of iterations 

#define TPROP	100 	// iterations between measurements (if LOG10PRINT not defined)

#define LOG10PRINT  	// print time dependent properties for t=1,2,...10, 20,...100, 200...

#define F0	0.1     // driving force or driving velocity (if MM defined) 

#define TIMESTEP	0.1       // initial time step (is adaptative)

#define CHECK	10000 // number of iterations for checking the current timestep

#define D0	1.0	  // quenched disorder intensity (irrelevant if NODISORDER defined)

// some global seeds
#define SEED 	12345678 // global seed RNG (quenched noise)
#define SEED2 	12312313 // global seed#2 RNG (thermal noise)
#define PISO	100	 // extra seed


////////////////////////////////////
// pinning cell list parameters
#define RINTPIN		1.0   // pinning potential characteristic length 	
#define RCUTPIN		5.0   // cutoff pinning potential RCUTPIN > RINTPIN
#define	WPIN		1000000 // number of pinning cells in X directon RMAXPIN*WPIN=LX
#define HPIN		1000000 // number of pinning cells in Y directon
#define NPINSPERCELL	2.0	// number of pinning centers per cell (float)
#define LX		1000000 // periodicity pinning potential
#define LY		1000000 // periodicity pinning potential

// choose one single pin potential
//#define PINNINGFIELD
#define GAUSSIANWELLSPINS 
//#define NANOPARTICLE 

__device__
void single_pin_force(float dx,float dy, const float rint, const float rcut, float &fx, float &fy)
{
	fx=fy=0.f;

	// gaussian attractive well
	#ifdef GAUSSIANWELLSPINS
	float vpin=-expf(-(dx*dx+dy*dy)/(rint*rint))*0.05/(rint*rint);
	fx = vpin*dx;
	fy = vpin*dy;
	#endif

	#ifdef NANOPARTICLE
	float dr2=dx*dx + dy*dy;
	float rcut2=rcut*rcut;
	float fpin;
	if(dr2<rcut2){
		fpin = (2.*(-rcut2 + dr2)*(2. + rcut2 + dr2))/
   		(rcut2*rcut2*(1.0f + dr2)*(1.0f + dr2));
		fx=fpin*dx;
		fy=fpin*dy;
	}
	#endif
}

