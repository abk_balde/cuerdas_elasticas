#include "qew3d_minimal_solucion.h"

/*
Interface de alto nivel.
programar aqui lo que uno quiere hacer con la cuerda.
*/


//////////////////////////////////////////////////
int basico(){
	Cuerda C;
	C.init();
	C.dynamics(TRUN);
	C.print_timmings();
	return 0;
}


///////////////////////////////////////////////////
#ifdef STRUCTUREFACTOR
#define STRUCTUREFACTORPRINT (STRUCTUREFACTOR*100)
std::ofstream struout("Stru.dat");
std::ofstream strvout("Strv.dat");
#endif
int struc(){
	Cuerda C;
	C.init();
	
	
	// Esta equilibracion funciona si MM esta definido (parabola)
	// Y si la condicion inicial tiene a la cuerda retrasada respecto a la parabola.
	// En el estado estacionario la velocidad MEDIA deberia ser la impuesta.
	// Entonces se considera al sistema aproximadamente equilibrado cuando 
	// la velocidad instantanea del centro de masa supera por primera vez a la impuesta.
	std::cout << "equilibration...";
	std::ofstream eqout("eq.out");
	unsigned long n=0;
	REAL imposed_velocity=C.get_f0();
	REAL vel=C.get_velocityu();	
	do{
		C.step();
		n++;
		if(n%1000==0) eqout << vel << std::endl;
	}
	//while(n<1000000);
	while((vel=C.get_velocityu())<0.01*imposed_velocity);
	std::cout << "done" << std::endl;

	
	// Se supone que aqui ya estamos en el estacionario, 
	// asi que podemos empezar a medir el factor de estructura
	// se calcula cada STRUCTUREFACTOR pasos (ver #defines.h) 
	// y se imprime cada STRUCTUREFACTORPRINT pasos
	std::cout << "acumulating structure factor statistics...";
	for(n=0;n<TRUN;n++)
	{
		C.step();	

		#ifdef STRUCTUREFACTOR
		if((n+1)%STRUCTUREFACTOR==0){
			C.Su.fft();
			C.Sv.fft();
		}
		if((n+1)%(STRUCTUREFACTORPRINT)==0){
			C.Su.print(struout);
			C.Sv.print(strvout);
		}
		#endif
	}
	std::cout << "done" << std::endl;

	C.print_timmings();
	return 0;
}


/////////////////////////////////////////////
int main(){
	struc();
	return 0;
}
