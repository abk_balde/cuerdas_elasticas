res


############
set log y
set term qt 1

f(x)=(k/lambda)*(x/lambda)**(k-1)*exp(-(x/lambda)**k)

set multi lay 2,1
set tit "T=0.00000001, d=1"
set xla '{/Symbol t}'
set yla 'P({/Symbol t};T)'
set key top
plot for [L in "64 128 256 512" ] \
sprintf("histoL%sT0.00000001.dat",L) u 1:2 t 'L='.L w his

#fit [0:] f(x) "histoL132768T0.00000001.dat" via k, lambda

plot for [L in "2048 4096 8192 32768 65536 132768" ] \
sprintf("histoL%sT0.00000001.dat",L) u 1:2 t 'L='.L w his, f(x)



#expo=0.4
#set tit "P({/Symbol t};T)=  T^{2/(6-d)} f(t T^{2/(6-d)},L) ? ---> NO"
#set xla sprintf('{/Symbol t} T^{%1.2f}',expo)
#set yla sprintf('P({/Symbol t};T)/T^{%1.2f}',expo)
#set key bot

#plot for [T in "0.00000000125 0.0000000025 0.000000005 0.00000001 0.00000002 0.00000004 0.00000008" ] \
#sprintf("histoL65536T%s.dat",T) u ($1*T**expo):($2/T**expo) t 'T='.T w p
unset multi

#-------------------------------------------------------------------
res
set term qt 2
set multi lay 2,2

############
set log y 
set tit "L=65536, d=1"
set xla '{/Symbol t}'
set yla 'P({/Symbol t};T)'
set key top
plot for [T in "0.00000000125 0.0000000025 0.000000005 0.00000001 0.00000002 0.00000004 0.00000008" ] \
sprintf("histoL65536T%s.dat",T) u 1:2 t 'T='.T w his

expo=0.4
set tit "P({/Symbol t};T)=  T^{2/(6-d)} f(t T^{2/(6-d)},L) ? ---> NO"
set xla sprintf('{/Symbol t} T^{%1.2f}',expo)
set yla sprintf('P({/Symbol t};T)/T^{%1.2f}',expo)
set key bot

plot for [T in "0.00000000125 0.0000000025 0.000000005 0.00000001 0.00000002 0.00000004 0.00000008" ] \
sprintf("histoL65536T%s.dat",T) u ($1*T**expo):($2/T**expo) t 'T='.T w p


############
expo=0
set tit "d=1, L T^{1/(6-d)}=cte= 1086" #cte~1086
set xla '{/Symbol t}'
set yla 'P({/Symbol t};T)'
plot \
"histoL65536T0.00000000125.dat" u ($1*(0.00000000125)**expo):($2/(0.00000000125)**expo) t 'L=65536, T=0.00000000125' w his,\
"histoL57016T0.00000000250.dat" u ($1*(0.00000000250)**expo):($2/(0.00000000250)**expo) t 'L=57016, T=0.00000000250' w his,\
"histoL49604T0.00000000500.dat" u ($1*(0.00000000500)**expo):($2/(0.00000000500)**expo) t 'L=49604, T=0.00000000500' w his,\
"histoL43155T0.00000001000.dat" u ($1*(0.00000001000)**expo):($2/(0.00000001000)**expo) t 'L=43155, T=0.00000001000' w his

expo=0.4
set tit "P({/Symbol t};T)=  T^{2/(6-d)} f(t T^{2/(6-d)},L T^{1/(6-d)}=cte) ? ---> YES" #cte~1086
set xla sprintf('{/Symbol t} T^{%1.2f}',expo)
set yla sprintf('P({/Symbol t};T)/T^{%1.2f}',expo)
plot \
"histoL65536T0.00000000125.dat" u ($1*(0.00000000125)**expo):($2/(0.00000000125)**expo) t 'L=65536, T=0.00000000125' w p,\
"histoL57016T0.00000000250.dat" u ($1*(0.00000000250)**expo):($2/(0.00000000250)**expo) t 'L=57016, T=0.00000000250' w p,\
"histoL49604T0.00000000500.dat" u ($1*(0.00000000500)**expo):($2/(0.00000000500)**expo) t 'L=49604, T=0.00000000500' w p,\
"histoL43155T0.00000001000.dat" u ($1*(0.00000001000)**expo):($2/(0.00000001000)**expo) t 'L=43155, T=0.00000001000' w p


unset multi

