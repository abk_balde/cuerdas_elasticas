for i in fbig_*.dat
do 
	#echo $i
	#echo $(gawk -v str=$i 'BEGIN{print substr(str,6,6)}') \
	echo $(echo $i | cut -d'_' -f 2 |  cut -d'.' -f 1,2) \
	$(tail -n $1 $i | gawk 'BEGIN{v=0; v2=0; w=0;}{v+=$2;v2+=$2*$2;w+=$4;w2+=$4*$4;n++;}\
	END{print v/n, sqrt(v2/n-(v*v)/(n*n)), w/n, sqrt(w2/n-(w*w)/(n*n));}'); 
done
