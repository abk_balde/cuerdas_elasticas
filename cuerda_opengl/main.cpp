// non interactive main
#include "qew_minimal_solucion.h"

void single_force(char **);
void force_ramp(char **);

int main(int argc, char **argv){

	if(argc==5)
	{	
		single_force(argv);
	}
	else if(argc==7)
	{	
		force_ramp(argv);
	} 	
	else{
		std::cout << "4 arguments: f T trun drun" << std::endl;
		std::cout << "6 arguments: f1 f2 df T trun drun" << std::endl;
		exit(1);
	}

	return 0;
}

//////////////////////////////////////////
void single_force(char **argv)
{
	Cuerda C;
	C.init();

	float force,temp, drun;
	int trun;

	{	
		force=atof(argv[1]);
		temp=atof(argv[2]);
		trun=atoi(argv[3]);
		drun=atof(argv[4]);
		std::cout << "force=" << force << ", T=" << temp << std::endl;
		std::cout << "trun=" << trun << ", drun=" << drun << std::endl;
		C.set_T0(temp);
		C.set_f0(force);
		//C.dynamics(trun);
		C.dynamics(trun,drun);
		C.print_timmings();

		C.restart();
	}
}

//////////////////////////////////////////
void force_ramp(char **argv)
{
	Cuerda C;
	C.init();

	float force, force2, df, temp, drun;
	int trun;

	{	
		force=atof(argv[1]);
		force2=atof(argv[2]);
		df=atof(argv[3]);
		temp=atof(argv[4]);
		trun=atoi(argv[5]);
		drun=atof(argv[6]);
		std::cout << "force1=" << force << ", force2=" << force2 << ", df=" << df<< ", T=" << temp << std::endl;
		std::cout << "trun=" << trun << ", drun=" << drun << std::endl;

		std::ofstream vvsfout("vvsf.dat");

		C.set_T0(temp);

		for(float f=force;(df>0.)?(f<force2):(f>force2);f+=df){
			C.set_f0(f);
			//C.dynamics(trun);
			C.reset_log_monitoring();

			REAL v1,v0;
			C.dynamics(1,drun);
			//do
			{
				v0=C.get_velocity();
				C.dynamics(trun,drun);
				v1=C.get_velocity();
			}
			//while((df<0.0)?(v1<v0):(v1>v0));
			//while(fabs(v1-v0)/fabs(v1)>0.1);
			vvsfout << f << " " << v1 << " " << v0 << std::endl;
				
			char nom[50];sprintf(nom,"restart_%08lf.dat",f);
			C.restart(nom);
		}

		C.print_timmings();
	} 	
}
