// non interactive main
#include "qew_minimal_solucion.h"

void single_force(char **);
void force_ramp(char **);
void temp_ramp(char **);
void force_acdc(int, char **);
void first_soliton(char **argv);
void solitons(char **argv);

int main(int argc, char **argv){

/*	if(argc==5)
	{	
		single_force(argv);
	}
	else if(argc==7)
	{	
		force_ramp(argv);
	} 	
	else{
		std::cout << "4 arguments: f T trun drun" << std::endl;
		std::cout << "6 arguments: f1 f2 df T trun drun" << std::endl;
		exit(1);
	}
*/

	//force_acdc(argc,argv);

	//force_ramp(argv);
	//temp_ramp(argv);
	single_force(argv);
	//first_soliton(argv);

	//force_acdc(argc,argv);

	//solitons(argv);
	return 0;
}

//////////////////////////////////////////
void single_force(char **argv)
{
	Cuerda C;
	C.init();

	float force,temp, drun;
	int trun;

	{	
		force=atof(argv[1]);
		temp=atof(argv[2]);
		trun=atoi(argv[3]);
		drun=atof(argv[4]);
		std::cout << "#force=" << force << ", T=" << temp << std::endl;
		std::cout << "#trun=" << trun << ", drun=" << drun << std::endl;
		C.set_T0(temp);
		C.set_f0(force);
		//C.dynamics(trun);
		C.dynamics(trun,drun);
		C.print_timmings();

		float v1=C.get_velocity();
		float vfluc=C.get_velocity_fluctuations();	

		//std::ofstream vvsfout("vvsf.dat");
		std::cout << force << " " << temp  << " " << v1 << " " << vfluc << std::endl;
		C.restart();
	}
}

//////////////////////////////////////////
void force_ramp(char **argv)
{
	Cuerda C;
	C.init();

	float force, force2, df, temp, drun;
	int trun;

	{	
		force=atof(argv[1]);
		force2=atof(argv[2]);
		df=atof(argv[3]);
		temp=atof(argv[4]);
		trun=atoi(argv[5]);
		drun=atof(argv[6]);
		std::cout << "force1=" << force << ", force2=" << force2 << ", df=" << df<< ", T=" << temp << std::endl;
		std::cout << "trun=" << trun << ", drun=" << drun << ", L="  << L <<  std::endl;


		std::ofstream vvsfout("vvsf.dat");

		C.set_T0(temp);


		REAL v1=1.0,v0=1.0, vfluc=0.0;
		for(float f=force;(df>0.)?(f<=force2):(f>=force2) && (v1 > 0.001);f+=df){
			C.set_f0(f);
			//C.dynamics(trun);
			C.reset_log_monitoring();

			C.dynamics(1,drun);
			//do
			{
				v0=C.get_velocity();
				C.dynamics(trun,drun);
				v1=C.get_velocity();
				vfluc=C.get_velocity_fluctuations();				
			}
			//while((df<0.0)?(v1<v0):(v1>v0));
			//while(fabs(v1-v0)/fabs(v1)>0.1);
			vvsfout << f << " " << v1 << " " << v0 << " " << temp << " " << vfluc << std::endl;
				
			char nom[50];sprintf(nom,"restart_%08lf.dat",f);
			C.restart(nom);
		}

		C.print_timmings();
	} 	
}


//////////////////////////////////////////
void force_acdc(int argc, char **argv)
{
	Cuerda C;
	C.init();

	float amplitud, temp;
	int trun, taupulse;

	amplitud=atof(argv[1]);
	temp=atof(argv[2]);
	taupulse=atoi(argv[3]);
	trun=atoi(argv[4]);

	printf("amplitud=%f, temp=%f, taupulse=%d, trun=%d\n", amplitud,temp,taupulse,trun);

	C.set_temp(temp);

	int n=0;
	while(n<trun){
		C.set_f0(amplitud);
		C.dynamics(taupulse);
		C.set_f0(-amplitud);
		C.dynamics(taupulse);
		n+=taupulse;
	}
	n=0;
	C.set_f0(amplitud);
	while(n<trun){
		C.dynamics(2.0*taupulse);
		n+=taupulse;
	}
}


//////////////////////////////////////////
void temp_ramp(char **argv)
{
	Cuerda C;
	C.init();

	float temp, temp2, dtemp, force, drun;
	int trun;

	{	
		temp=atof(argv[1]);
		temp2=atof(argv[2]);
		dtemp=atof(argv[3]);
		force=atof(argv[4]);
		trun=atoi(argv[5]);
		drun=atof(argv[6]);
		std::cout << "temp1=" << temp << ", temp2=" << temp2 << ", dtemp=" << dtemp<< ", force=" << force << std::endl;
		std::cout << "trun=" << trun << ", drun=" << drun << ", L="  << L <<  std::endl;


		std::ofstream vvsfout("vvsf.dat");

		C.set_f0(force);


		REAL v1=1.0,v0=1.0, vfluc=0.0;
		for(float T=temp;(T>=temp2) && (v1 > 0.000001);T*=dtemp){
			C.set_temp(T);
			//C.dynamics(trun);
			C.reset_log_monitoring();

			C.dynamics(1,drun);
			//do
			{
				v0=C.get_velocity();
				C.dynamics(trun,drun);
				v1=C.get_velocity();
				vfluc=C.get_velocity_fluctuations();				
			}
			//while((df<0.0)?(v1<v0):(v1>v0));
			//while(fabs(v1-v0)/fabs(v1)>0.1);
			vvsfout << force << " " << v1 << " " << v0 << " " << T << " " << vfluc << std::endl;
				
			//char nom[50];sprintf(nom,"restart_%08lf.dat",f);
			//C.restart(nom);
		}

		C.print_timmings();
	} 	
}


//////////////////////////////////////////
std::ofstream first_soliton_out("first_soliton.dat");
void first_soliton(char **argv)
{
	Cuerda C;
	C.init();

	float force,temp, drun;
	int trun;
	int nsamples;

	{	
		force=atof(argv[1]);
		temp=atof(argv[2]);
		trun=atoi(argv[3]);
		drun=atof(argv[4]);
		nsamples=atoi(argv[5]);
		first_soliton_out << "#force=" << force << ", T=" << temp << std::endl;
		first_soliton_out << "#trun=" << trun << ", drun=" << drun << std::endl;
		first_soliton_out << "#samples=" << nsamples << std::endl;
		first_soliton_out << "#L=" << L << std::endl;
		C.set_T0(temp);
		C.set_f0(force);
		//C.dynamics(trun);
		
		for(int i=0;i<nsamples;i++){
			int j=0;
			while((C.count_solitons())<2){
				C.step();
				j++;
			}
			first_soliton_out << j << std::endl;			
			C.restart_flat_at(10.0);
		}
	}
}

//////////////////////////////////////////
std::ofstream soliton_out("solitons.dat");
std::ofstream configs_out("configs.dat");
void solitons(char **argv)
{
	Cuerda C;
	C.init();

	float force,temp, drun;
	int trun;

	force=atof(argv[1]);
	temp=atof(argv[2]);
	trun=atoi(argv[3]);
	drun=atof(argv[4]);

	std::cout << "#force=" << force << ", T=" << temp << std::endl;
	std::cout << "#trun=" << trun << ", drun=" << drun << std::endl;
	C.set_T0(temp);
	C.set_f0(force);

	for(int i=0;i<trun;i++)
	{	
		C.dynamics(5,drun);
		C.print_solitons(i, soliton_out);
		C.print_displacements(i, configs_out);
	}
}



