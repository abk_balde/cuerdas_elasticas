res

zeta=0.39
nu=1./(1-zeta)
fc=0.14785

set multi lay 2,1

#set log y 
set ylab "v"; set xlab "f"; 
set tit "sigma=1, gaussian wells, fc=0.14785"; 
plot [0:0.5] '<  ./extraervvsfstationary.sh' u ($1):($2):(($3)) w errorl t '',x

set logs
set ylab "v/fc"; set xlab "(f-fc)/fc"; 
set tit ""; 
set key left
plot [2e-3:10] '<  ./extraervvsfstationary.sh' u ($1/fc-1):($2/fc):(($3)/fc) w error t '', x**0.65*0.47, x

#plot [2e-4:1] '<  ./extraervvsfstationary.sh' u ($1-0.14785):4:5 w error t '', x**(-2*nu*zeta)

unset multi

