[T,K,V]=textread("snap_solitons.dat");

SV0=abs(fft(V(end-512:end-1))).^2/512;
X0=[1:512]/512.;

SV1=abs(fft(V(end-1024:end-1))).^2/1024;
X1=[1:1024]/1024.;

SV2=abs(fft(V(end-2048:end-1))).^2/2048;
X2=[1:2048]/2048.;

SV3=abs(fft(V(end-4096:end-1))).^2/4096;
X3=[1:4096]/4096. ;

SV4=abs(fft(V(end-8192:end-1))).^2/8192;
X4=[1:8192]/8192. ;

#SV5=abs(fft(V(end-16384:end-1))).^2;
#X4=[1:16384]/16384. ;

loglog(X0, SV0, X1,SV1,X2,SV2, X3, SV3, X4, SV4);


A0=[transpose(X0(1:end)) SV0(1:end)]
A1=[transpose(X1(1:end)) SV1(1:end)]
A2=[transpose(X2(1:end)) SV2(1:end)]
A3=[transpose(X3(1:end)) SV3(1:end)]
A4=[transpose(X4(1:end)) SV4(1:end)]

save "spectras.dat" A0 A1 A2 A3 A4 
#X1 SV1 X2 SV2 X3 SV3 X4 SV4
