rm test.dat
for F in 0.01 0.005 0.001 0.0005 0.0001 0.00005 0.00001
do
echo $F
gnuplot << EOF
	set print "test.dat" append
	G="${F}";
	a=0;b=0;
        f(x)=a*x+b;
	file=sprintf("someprops_T%s.dat",G);
        fit [1e6:1e7] f(x) file using 1:3 via a,b
	print sprintf("%s %f %f",G,a,b)
EOF
done
