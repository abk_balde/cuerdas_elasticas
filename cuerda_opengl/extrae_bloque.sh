#extrae el bloque n del file file
n=$1
blocksize=$2
file=$3

gawk -v n=$n -v b=$blocksize 'BEGIN{i=0;}{if(NR>b*n && NR<=(n+1)*b) print i, NR-1, $1; i++;}' $file
