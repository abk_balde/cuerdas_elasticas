/*
=============================================================================================
A. B. Kolton

Segundo Encuentro Nacional de Computación de Alto Rendimiento para Aplicaciones Científicas
7 al 10 de Mayo de 2013.
=============================================================================================

Este programita simula la dinámica de una cuerdita elastica en un medio desordenado en la GPU, 
usando la librería Thrust, y la libreria Random123. Es una versión reducida de la que 
usamos con E. Ferrero y S. Bustingorry para la publicación:

Phys. Rev. E 87, 032122 (2013)
Nonsteady relaxation and critical exponents at the depinning transition
http://pre.aps.org/abstract/PRE/v87/i3/e032122
http://arxiv.org/abs/1211.7275

Y la explicación resumida del código esta en el material suplementario de la revista:
http://pre.aps.org/supplemental/PRE/v87/i3/e032122


Sin resolver los TODO ya se puede compilar con "make", y larga algunos timmings.
Genera dos ejecutables a la vez, uno de CUDA (corre en GPU) y otro de openMP (corre en CPU multicore).

OBJETIVOS:
- Practicar el manejo básico de la librería Thrust.
- Practicar el manejo básico de la librería Random123.
- Comparar performances CPU vs GPU.
- Aprender a combinar herramientas para resolver una ecuación diferencial 
parcial estocástica con desorden congelado.

EJERCICIOS:	
- Levantar los TODO.
- Para los mas expertos: Como mejoraría la performance del codigo?
*/



#ifdef FOURIER
#include "cutil.h"
#include <cufft.h>
#endif

#include "simple_timer.h"
#include<cmath>
#include<fstream>
#include <iostream>


/* algunos headers de la libreria thrust */
// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
// https://github.com/thrust/thrust/wiki/Documentation
#include<thrust/transform.h>
#include<thrust/copy.h>
#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include<thrust/functional.h>
#include<thrust/sequence.h>
#include <thrust/fill.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/reduce.h>
#include <thrust/extrema.h>

/* parámetros del problema */
#ifndef TAMANIO
#define L	4096   // numero de partículas/monomeros
#else
#define L	TAMANIO   // numero de partículas/monomeros
#endif
#ifndef TIEMPORUN
#define TRUN	100000       // numero de iteraciones temporales 
#else
#define TRUN	TIEMPORUN    // numero de iteraciones temporales 
#endif
#ifndef TPROP
#define TPROP	100 // intervalo entre mediciones
#endif

#ifndef F0
#define F0	0.12       // (0.12) fuerza uniforme sobre la interface/polímero 
#endif

#ifndef Dt
#define Dt	0.05       // paso de tiempo (hacer una funcion para que se adapte)
#endif

#ifndef TEMP
#define TEMP	0.001     // temperatura	
#endif

#ifndef TZEROS
#define TZEROS	100000000000000     // cada cuanto determina los zeros
#endif

#define D0	1.0	  // intensidad del desorden

#ifndef SEED
#define SEED 	12345678 // global seed RNG (quenched noise)
#endif

#ifndef SEED2
#define SEED2 	12312313 // global seed#2 RNG (thermal noise)
#endif

#define MAXPOINTSDISPLAY	512

#ifndef TCHECKBIG
#define TCHECKBIG	1000000
#endif

#ifndef UMAX
#define UMAX		1000000
#endif

// para evitar poner "thrust::" a cada rato all llamar sus funciones
using namespace thrust;

// precisón elegida para los números reales
#ifdef FOURIER
typedef cufftDoubleReal REAL;
typedef cufftDoubleComplex COMPLEX;
#else
typedef double 	REAL;
#endif


/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG


// para generar números aleatorios gausianos a partir de dos uniformes
// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
__device__
REAL box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	REAL u1 = u01_open_closed_32_53(r_philox[0]);
  	REAL u2 = u01_open_closed_32_53(r_philox[1]);

  	REAL r = sqrt( -2.0*log(u1) );
  	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);    			
}

#ifdef PRINTCONFS
// para imprimir configuraciones cada tanto...
std::ofstream configs_out("configuraciones.dat");
void print_configuration(device_vector<REAL> &u, device_vector<REAL> &Ftot, float velocity, int size, int time)
{
	long i=0;
	int every = int(L*1.0/size);
	for(i=1;i<u.size()-1;i+=every)
	{
		configs_out << u[i] << " " << Ftot[i] << " " << velocity << " " << time << std::endl;
	}
	configs_out << "\n\n";
}
#endif

// genera un path gaussiano periodico con rugosidad dada
/*struct gaussian_path{
	RNG rng;       // random number generator
	unsigned ll;
	gaussian_path(ll_):ll(ll_){

	}
	__device__
    	REAL operator()(tuple<unsigned long,unsigned long,REAL,REAL> tt)
    	{	
		// thread/particle id
		uint32_t tid=get<0>(tt); 
    	} 	
	//IN CONSTRUCTION
}*/


// no es efifiente generar asi un path, rehacer usando cufft...
/*#include "mt.h"
void fourier_gen_path(REAL roughness, int ll, device_vector<REAL>::iterator X, unsigned long seed)
{
	init_genrand(seed);
	REAL sigma;
	int Nmodes=ll;
	thrust::host_vector<REAL> A(ll);
	thrust::host_vector<REAL> B(ll);
	thrust::host_vector<REAL> H(ll);

	thrust::fill(H.begin(),H.end(),REAL(0.0));

	std::cout << "generating modes\n";
	for(int n=0;n<Nmodes;n++){
		sigma = pow((double)ll/(2.0*M_PI*(n+1)),roughness)/sqrt(M_PI*(n+1));
		A[n] = sigma*gasdev_real3();
		B[n] = sigma*gasdev_real3();
		//std::cout << n << "\n";
	}
	std::cout << "generating vector\n";
	double sinx,cosx;
	for(int t=0;t<ll;t++){
		for(int n=0;n<Nmodes;n++){
			sincos(2.*M_PI*(n+1)*t*1.0/ll, &sinx, &cosx);
			H[t] = H[t] + A[n]*cosx + B[n]*sinx;			
		}
		if(t%100==0) std::cout << t << "\n";
	}
	REAL minimo=thrust::reduce(H.begin(),H.end(),REAL(0.0),thrust::minimum<REAL>());
	for(int t=0;t<ll;t++) H[t]+=fabs(minimo);
	thrust::copy(H.begin(),H.end(),X);
	std::cout << "done\n";
};
*/


// functor usado para calcular la rugosidad 
struct roughtor: public thrust::unary_function<REAL,REAL>
{
    REAL u0; // un "estado interno" del functor	
    roughtor(REAL _u0):u0(_u0){};	
    __device__
    REAL operator()(REAL u)
    {	
	return (u-u0)*(u-u0);
    }
};	


// predicate para detectar zeros en la cuerda wrt center of mass...
struct is_zeros
{
	REAL cm0;
	REAL *u_ptr;
	is_zeros(REAL _cm0, REAL * _ptr):cm0(_cm0),u_ptr(_ptr){};
	__host__ __device__
	bool operator()(const int i)
	{
		REAL first_point= u_ptr[i]-cm0;
		REAL second_point= u_ptr[i+1]-cm0;
		bool positive_zero=(first_point < 0.0)&&(second_point > 0.0);
		bool negative_zero=(first_point > 0.0)&&(second_point < 0.0);
		return (positive_zero || negative_zero);
	}
}; 


/////////////////////////////////////////////////////////////////////////////
// FUNCTORS usados en los algoritmos TRANSFORM

// pinning cell list parameters
#define RINTPIN		1.0   // pinning potential characteristic length 	
#define RCUTPIN		5.0   // cutoff pinning potential RCUTPIN > RINTPIN
#define	WPIN		10000000 // number of pinning cells in X directon RMAXPIN*WPIN=LX
#define NPINSPERCELL	1	// number of pinning centers per cell
#define	LX		10000000 
#define FLIM		1.0

__device__ 
REAL pinning_force(int tid,REAL u)
{
	// random number generator
	RNG rng;       
	// keys and counters 
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	c[1]=uint32_t(SEED);
#ifdef PIECEWISELINEAR
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	REAL firstRN, secondRN;
	int U=int(u);
	c[0]=uint32_t(U); // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	firstRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	c[0]=uint32_t(U+1); // COUNTER={U+1,GLOBAL SEED}
	r = rng(c, k);
	secondRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	return D0*(firstRN - (firstRN-secondRN)*(u-U));// linearly interpolated force
#else
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif

	u=u-floor(u/LX)*LX;

	//neighbor cells
	int cell[3];
	cell[0]=int(u/RCUTPIN);
	cell[1]=(int(u/RCUTPIN)-1+WPIN)%WPIN;
	cell[2]=(int(u/RCUTPIN)+1)%WPIN;

	unsigned int npins;
	#ifdef POISSON
	float uu,xx,pp,ss,xpin,fpin, apin;
	#else
	float xpin,fpin, apin;
	#endif

	float dx,dr2;
	fpin=0.0f;
	for(int ic=0;ic<3;ic++)
	{	
		c[0]=cell[ic]; c[1]=uint32_t(SEED);

		// number of pins in the cell: poisson dist.
		#ifdef POISSON
		xx=0.f; pp=ss=expf(-NPINSPERCELL);
		r = rng(c, k); c[1]++;
		uu=u01_open_closed_32_53(r[0]); 
		while(uu>ss){
			xx++;
			pp=pp*NPINSPERCELL/xx;
			ss=ss+pp;
		}
		npins=xx;
		#else
		npins=NPINSPERCELL;
		#endif

		// random positions for the pins in the cell
		for(int n=0;n<npins;n++){
			//c[0] makes pins in different cells uncorrelated
			//c[1] makes pins in the same cell uncorrelated
			r = rng(c, k); c[1]++; 

			#ifdef RANDOMPOS
	 		xpin = (cell[ic] + u01_open_closed_32_53(r[0]))*RCUTPIN;
			#else 
			xpin = (cell[ic] + 0.5)*RCUTPIN;
			#endif

			#ifdef RANDOMAMP
			apin = u01_open_closed_32_53(r[0]);
			#else
			apin = 1.0;
			#endif

			dx = (u-xpin); if(dx>LX*0.5) dx+=-LX; if(dx<-LX*0.5) dx+=LX;
			dr2=dx*dx;

			if(dr2<RCUTPIN*RCUTPIN){
				// a gaussian attractive potential
				fpin+=-apin*expf(-dr2/(RINTPIN*RINTPIN))*dx*2.0/(RINTPIN*RINTPIN);
			}
		}
	}
	#ifdef PINSATURATION
	return tanh(fpin/FLIM)*FLIM;
	#else
	return fpin;
	#endif
#endif
}

#ifdef FOURIER
struct suma
{
	__device__
	COMPLEX operator()(const COMPLEX a, const COMPLEX b)
	{
		COMPLEX c;
		c.x=a.x+b.x;
		c.y=a.y+b.y;
		return c;
	}
}; 
#endif

#include "util.h"
#define a0 	3.0 // for elastic chains
struct fuerza
{
    RNG rng;       // random number generator
    unsigned long long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL f0;	
    REAL T0;
    REAL dt;
    REAL *u_ptr;	
		
    fuerza(unsigned long long _t, REAL _f0, REAL _T0, REAL dt_, REAL *ptr_):tiempo(_t),f0(_f0),T0(_T0),u_ptr(ptr_),dt(dt_)
    {
	noiseamp=sqrt(T0/dt);
    }; 

    __device__
    REAL operator()(tuple<long,REAL,REAL,REAL> tt)
    {	
	// thread/particle id
	uint32_t tid=get<0>(tt); 

	// keys and counters 
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	c[1]=SEED; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}

	// LAPLACIAN
	REAL u=get<2>(tt);
	#ifndef FINITERANGEELASTICITY
	REAL um1=get<1>(tt);
	REAL up1=get<3>(tt);
	REAL laplaciano = um1 + up1 - 2.*u;
	#else
	/* // igual que antes pero usando u_ptr
	REAL um1 =*(&u_ptr[tid]-1);
	REAL up1 =*(&u_ptr[tid]+1);
	REAL laplaciano = up1 + um1 - 2.*u;
	*/
	/* 
	REAL laplaciano = 0.0f;
	REAL um, up;	
	for(int i=1;i<2;i++){ //funciona solo hasta 2
		um =*(&u_ptr[tid]-i);		
		up =*(&u_ptr[tid]+i);		
		laplaciano += ((up-u)+(um-u))/i;
	}*/
	REAL laplaciano = 0.0f;
	REAL um, up;
	for(int i=1;i<int(5./a0);i++){
		um =u_ptr[(tid-i+L)%L];		
		up =u_ptr[(tid+i)%L];		
		//laplaciano += ((up-u)+(um-u))/i; //funca
		laplaciano += -k1(fabs(up+i*a0-u))+k1(fabs(-i*a0+um-u)); 
		//laplaciano += k2(i)*(up+um-2.f*u);
	}
	#endif

	// DISORDER
	REAL quenched_noise=0.0; 
#ifndef NODISORDER
	quenched_noise = pinning_force(tid,u);
#endif
#ifdef WASHBOARD
	quenched_noise = sinf(2.0f*M_PI*u);
#endif	

	// THERMAL NOISE
	REAL thermal_noise;

#ifdef FINITETEMPERATURE
	k[0]=tid; 	  //  KEY = {threadID} 
	c[0]=uint32_t(tiempo); // COUNTER={tiempo, GLOBAL SEED #2}

	c[1]=SEED2; // para evitar correlaciones entre el ruido térmico y el desorden congelado...
	r = rng(c, k);
	thermal_noise = noiseamp*box_muller(r);    			
#else
	thermal_noise=0.0;
#endif

#ifndef MM // force driven
	return (laplaciano+quenched_noise+thermal_noise+f0);
#else // velocity driven
	return (laplaciano+quenched_noise+thermal_noise+MM*(f0*tiempo*Dt-u));
#endif
  }
};


// Explicit forward Euler step: lo mas simple que hay 
// (pero ojo con el paso de tiempo que no sea muy grande!)
struct euler
{
    REAL dt;
    euler(REAL dt_):dt(dt_){};
		
    __device__
    REAL operator()(REAL u_old, REAL force)
    {	
	return (u_old + force*dt);
    }
};	


#ifdef OMP
#include <omp.h>
#endif
/////////////////////////////////////////////////////////////////////////////

class Cuerda
{
	public:
	#ifdef OMP
	omp_timer t;
	#else
	gpu_timer t;
	#endif

	double timer_fuerzas_elapsed;
	double timer_euler_elapsed;
	double timer_props_elapsed;
	
	std::ofstream fou;

	Cuerda(){};
	~Cuerda(){
	#ifdef FOURIER
	cufftDestroy(plan_d2z);
	#endif
	};

	void init(){
		#ifdef OMP
		std::cout << "#conociendo el host, OMP threads = " << omp_get_max_threads() << std::endl;
		#endif		

		u.resize(L+2);
		u_it0 = u.begin()+1;
		u_it1 = u.end()-1;
		u_raw_ptr = raw_pointer_cast(&u[1]);

		// container de fuerza total: 
		Ftot.resize(L); 
		// el rango de interés definido por los iteradores 
		Ftot_it0 = Ftot.begin();
		Ftot_it1 = Ftot.end();

		// alguna condición inicial chata (arbitraria)
		fill(u_it0,u_it1,10.0); 

		// o una condicion inicial rugosa (random)
		//fourier_gen_path(1.5, L, u_it0, 123456);


		timer_fuerzas_elapsed=0.0;
		timer_euler_elapsed=0.0;
		timer_props_elapsed=0.0;
	
		reset_log_monitoring();

		#ifndef OMP
		propsout.open("someprops.dat");
		propsout << "tiempo velocity center_of_mass roughness posmax T0 f0" << std::endl;
		zerosout.open("zeros.dat");
		nzerosout.open("nzeros.dat");
		#else
		propsout.open("someprops_omp.dat");

		zerosout.open("zeros_omp.dat");
		nzerosout.open("nzeros_omp.dat");
		#endif
		
		log.open("logfile.dat");

		f0=F0;
		T0=TEMP;
		dt=Dt;

		#ifdef FOURIER
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_d2z,L,CUFFT_D2Z,1));
		Ncomp=L/2+1;
		u_fourier.resize(Ncomp);
		cumu_fourier.resize(Ncomp);
		COMPLEX zero; zero.x=zero.y=0.0;
		thrust::fill(cumu_fourier.begin(),cumu_fourier.end(),zero);
		h_u_fourier.resize(Ncomp);
		u_fourier_raw = thrust::raw_pointer_cast(&u_fourier[0]); 
		fou.open("fourier.dat");
		//std::cout << u_fourier.size() << std::endl;
		#endif

		log << "L=" << L << std::endl;
		log << "f=" << f0 << std::endl;
		log << "T=" << T0 << std::endl;

		log << "TPROP =" << TPROP << std::endl;
		#ifdef FOURIER
		log << "fourier transforms activated"<< std:: endl;
		#endif	

		#ifdef FINITETEMPERATURE
			log << "FINITE TEMPERATURE" << std:: endl;
			log << "THERMAL SEED =" << SEED2 << std:: endl;
		#else
			log << "ZERO TEMPERATURE" << std:: endl;
		#endif	

		#ifdef NODISORDER
			log << "DISORDER desactivated" << std:: endl;
		#else	
			log << "DISORDER SEED =" << SEED << std:: endl;
			#ifdef PIECEWISELINEAR
			log << "PIECEWISELINEAR DISORDER" << std:: endl;
			#else
			log << "GAUSSIAN WELLS DISORDER:" << std:: endl;		
			log << "RINITPIN, RCUTPIN = " << RINTPIN << ", " << RCUTPIN << std:: endl;				
			log << "NPINSPERCELL = " << NPINSPERCELL << std:: endl;		
			#ifdef PINSATURATION
			log << "PINSATURATION, FLIM = " << FLIM << std:: endl;				
			#endif
			#endif
		#endif

		#ifdef FINITERANGEELASTICITY
			log << "FINITERANGEELASTICITY" << std:: endl;	
			log << "a0 = " << a0 << std:: endl;		
		#endif

		#ifndef OMP
		log << "CUDA" << std:: endl;				
		#else
		log << "OMP" << std:: endl;				
		#endif

		#ifdef MM 
		log << "VELOCITY DRIVEN" << std:: endl;				
		log << "MM = " << MM << std:: endl;				
		#else

		#endif
	};

	void reset_log_monitoring()
	{
		mtprop=1;
		tiempo=1;
		count=0;
	};
	
	void step()
	{
		//static unsigned long mtprop=1;
		//static unsigned long count=0;

		// Impone PBC en el "halo"		
		u[0]=u[L];u[L+1]=u[1];

		unsigned long long n=tiempo; tiempo++;
		t.tic(); // para cronometrar el tiempo de la siguiente transformación
		
		// Fuerza en cada monómero calculada concurrentemente en la GPU: 
		// Ftot(X)= laplacian + disorder + thermal_noise + F0, X=0,..,L-1
		// mirar el functor "fuerza" mas arriba... 
		// Notar: los iteradores de interés estan agrupado con un "fancy" zip_iterator, 
		// ya que transform no soporta mas de dos secuencias como input.
		// Notar: make_counting_iterator es otro "fancy" iterator, 
		// que simula una secuencia que en realidad no existe en memoria (implicit sequences).
		// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
		// http://thrust.github.io/doc/group__fancyiterator.html
		//fuerza.set_time(n);
		#ifndef VMCDYNAMICS
		transform(
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(0),u_it0-1,u_it0,u_it0+1
			)),
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(L),u_it1-1,u_it1,u_it1+1
			)),
			Ftot_it0,
			fuerza(n,f0,T0,dt,u_raw_ptr)
		);
		timer_fuerzas_elapsed+=t.tac();

		// Explicit forward Euler step, implementado en paralelo en la GPU: 
		// u(X,n) += Ftot(X,n) Dt, X=0,...,L-1
		// Mirar el functor "euler" mas arriba...
		// Notar: no hace falta zip_iterator, ya que transform si soporta hasta dos secuencias de input
		t.tic();
		transform(
			u_it0,u_it1,Ftot_it0,u_it0, 
			euler(dt)
		);
		timer_euler_elapsed+=t.tac();
		#else
		// plan: avanzar pares, impares
		// para eso: declarar permutation-iterators pares e impares
/*		typedef thrust::device_vector<float>::iterator ElementIterator;
  		typedef parimpIterator IndexIterator;
	    thrust::permutation_iterator<ElementIterator,IndexIterator> upar0_left(u.begin(), genPAR(0));
	    thrust::permutation_iterator<ElementIterator,IndexIterator> upar0(u.begin(), genPAR(1));
	    thrust::permutation_iterator<ElementIterator,IndexIterator> upar0_right(u.begin(), genPAR(2));

	    thrust::permutation_iterator<ElementIterator,IndexIterator> uimpar0_left(u.begin(), genIMPAR(0));
	    thrust::permutation_iterator<ElementIterator,IndexIterator> uimpar0(u.begin(), genIMPAR(1));
	    thrust::permutation_iterator<ElementIterator,IndexIterator> uimpar0_right(u.begin(), genIMPAR(2));

		transform(
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(0),uimpar0_left,upar0,uimpar0
			)),
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(L),uimpar0_left+L,upar0+L,uimpar1
			)),
			upar0,
			vmctor(n,f0,T0,dt,u_raw_ptr)
		);
		timer_euler_elapsed+=t.tac();
*/
		#endif

		// algunas propiedades de interés, calculadas cada TPROP
		//#ifndef LOGPRINT
		//if(n%TPROP==0){	
		//#else

		if(n%TCHECKBIG==0){	
			//thrust::pair<REAL *, REAL *> result = thrust::minmax_element(u_it0,u_it1);
			REAL init = u[1];
			REAL min = thrust::reduce(u_it0,u_it1,init,thrust::minimum<REAL>());
			std::cout << "min displacement= " << min << std::endl;
			if(min>UMAX){
				using namespace thrust::placeholders;
				thrust::transform(u_it0,u_it1,u_it0,_1-UMAX);
				std::cout << "shifting u" << std::endl;
			};
		}

		if(n%mtprop==0){
			//adapt_time_step();
			count++;if(count==10){count=0;mtprop*=10;}
		//#endif
			t.tic();
			
			/* TODO:
			   usando algoritmos REDUCE 
			   [ http://thrust.github.io/doc/group__reductions.html#gacf5a4b246454d2aa0d91cda1bb93d0c2 ]
			   calcule la velocidad media de la interface
			   y la posición del centro de masa de la interface 
			   HINT:
			   REAL velocity = reduce(....)/L; //center of mass velocity
			   REAL center_of_mass = reduce(....)/L; // center of mass position
			*/
			// SOLUCION:			
			REAL velocity = reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L); //center of mass velocity
			REAL center_of_mass = reduce(u_it0,u_it1,REAL(0.0))/REAL(L); // center of mass position

			/* TODO: 
			   usando el algoritmo TRANSFORM_REDUCE, 
	[ http://thrust.github.io/doc/group__transformed__reductions.html#ga087a5af8cb83647590c75ee5e990ef66 ]
			   el functor "roughtor" arriba definido, 
			   y la posición del centro de masa "ucm" calculada en el TODO anterior, 
			   calcule la rugosidad (mean squared width) de la interface:
			   roughness := Sum_X [u(X)-ucm]^2 /L
			   HINT:
			   REAL roughness = transform_reduce(...,...,roughtor(center_of_mass),0.0,thrust::plus<REAL>());
			*/
			// SOLUCION:	
			REAL roughness = transform_reduce
			(
				u_it0, u_it1,
                                roughtor(center_of_mass),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);
			// solución alternativa
  			/*REAL roughness = reduce
			(
			make_transform_iterator(u_it0, roughtor(center_of_mass)),
			make_transform_iterator(u_it1, roughtor(center_of_mass)),
			REAL(0.0)
			);
			*/
			timer_props_elapsed+=t.tac();
	
			/* TODO: calcule la posicion de la particula con maxima velocidad */
			//SOLUCION:
			//device_vector<REAL>::iterator result = 
			int posmax=int(thrust::max_element(Ftot_it0,Ftot_it1)-Ftot_it0);			

			/* TODO: descomentar para que imprima la velocidad media, centro de masa, y rugosidad 
			   calculadas en los otros "TODOes", en el file "someprops.dat" */	
			// SOLUCION:
			propsout << n << " " << velocity << " " << center_of_mass << " " 
					 << roughness << " " << posmax << " " << T0 << " " << f0 << std::endl;
			propsout.flush();
			
			/* TODO:
			 Descomente -DPRINTCONFS en el Makefile, y recompile, para que imprima la posición 
			 y velocidad de 128 (o lo que quiera) particulas de la interface de tamanio L (una de cada L/128) 
			 en pantalla (descomente con cuidado, que imprime mucho!. Solo para hacer un "intuitive debugging")
			*/
			#ifdef PRINTCONFS
			print_configuration(u,Ftot,velocity,MAXPOINTSDISPLAY,n);
			#endif

			#ifdef FOURIER
			transform_fourier();
			print_fourier(n);
			#endif
		}
		
	};

	void print_timmings(){
		// resultados del timming
		double total_time = (timer_fuerzas_elapsed+timer_euler_elapsed+timer_props_elapsed); 

		std::cout << "L= " << L << " TRUN= " << TRUN << std::endl; 

		std::cout << "Forces calculation -> " << 1e3 * timer_fuerzas_elapsed << " miliseconds (" 
	  	<< int(timer_fuerzas_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Euler step -> " << 1e3 * timer_euler_elapsed << " miliseconds (" 
		  << int(timer_euler_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Properties -> " << 1e3 * timer_props_elapsed << " miliseconds (" 
		  << int(timer_props_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Total -> " << 1e3 * total_time << " miliseconds (100%)" << std::endl;
	};

	void dynamics(long trun)
	{
		for(unsigned long n=0;n<trun;n++)
		{
			if(n%TZEROS==0) nzerosout << get_zeros() << std::endl;
			step();		
		}
		//print_timmings();
	};

	device_vector<REAL>::iterator posicion_begin(){
		return u_it0;
	};	
	device_vector<REAL>::iterator posicion_end(){
		return u_it1;
	};	
	
	REAL get_center_of_mass(){ 
		return reduce(u_it0,u_it1,REAL(0.0))/REAL(L); 
	};

	int get_zeros(){
		REAL cm=get_center_of_mass();
		thrust::device_vector<int> zeros(L);	
		thrust::device_vector<int>::iterator result= thrust::copy_if(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(L),zeros.begin(),is_zeros(cm,u_raw_ptr)
		);
		int n=int(result-zeros.begin());
		int signoinicial=(u[1+zeros[0]]>0)?(1):(-1);

		/*
		thrust::host_vector<int> densityzeros(129,0);	
		for(int i=0;i<n;i++){
			int j=int(zeros[i]*128.0/L);
			densityzeros[j]++; 
		}
		for(int i=0;i<128;i++)
		zerosout << densityzeros[i] << std::endl;
		if(n>0) zerosout << std::endl << std::endl;
		*/

		for(int i=0;i<n;i++){
			zerosout << zeros[i] << " " << signoinicial << std::endl;
			signoinicial*=-1;
		}
		if(n>0) zerosout << std::endl << std::endl;

		//return number_of_zeros;*/
		return n;
	};


	REAL get_roughness(REAL cm){
		return  transform_reduce
			(
				u_it0, u_it1,
                                roughtor(cm),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);
	};
	REAL get_maximum_velocity(){
		return  reduce
			(
				Ftot_it0, Ftot_it1,
                                REAL(0.0),
                                thrust::maximum<REAL>()
			)/REAL(L);
	};
	/*void adapt_time_step(){
		dt=0.1/get_maximum_velocity();
		if(dt>Dt) dt=Dt;
		std::cout << "dt=" << dt << std::endl;
	};*/

	void increment_f0(float df){
		f0+=df;
		//if(f0<0) f0=0.0;
	};
	void increment_T0(float dT0){
		T0+=dT0;
		if(T0<0.0f) T0=0.0f;
	};
	float get_f0(){
		return f0;
	}
	float get_T0(){
		return T0;
	}	
	void set_T0(float T1){
		T0=T1;
	}	
	void set_f0(float f1){
		f0=f1;
	}	
	REAL get_velocity(){ 
		return reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L); //center of mass velocity
	}
	
	#ifdef FOURIER
	void transform_fourier(){
		REAL *u_input=thrust::raw_pointer_cast(&u[0]);
		COMPLEX *u_output=thrust::raw_pointer_cast(&u_fourier[0]);
		CUFFT_SAFE_CALL(cufftExecD2Z(plan_d2z, u_input, u_output));
		//cufftExecD2Z(plan_d2z, u_input, u_output);
		//thrust::transform(u_fourier.begin(),u_fourier.end(),cumu_fourier.begin(),cumu_fourier.begin(),suma());
	};
	void print_fourier(int tt){
		thrust::copy(u_fourier.begin(),u_fourier.end(),h_u_fourier.begin());
		for(int i=0;i<Ncomp;i++){
			COMPLEX c=h_u_fourier[i];
			fou << (2.f*M_PI*i/L) << " " << c.x*c.x+c.y*c.y << " " << tt << std::endl;
		}
		fou << "\n\n";
	};
	#endif

	private:
	unsigned long long tiempo, mtprop, count;
	float f0;	
	float T0;
	float dt;	

	// posiciones de los monómeros: 
	// Notar que alocamos dos elementos de mas para usarlos como "halo"
	// esto nos permite fijar las condiciones de borde facilmente, por ejemplo periódicas: u[0]=u[L]; u[L+1]=u[1];
	device_vector<REAL> u;
	device_vector<REAL>::iterator u_it0;
	device_vector<REAL>::iterator u_it1;
	REAL * u_raw_ptr;

	// container de fuerza total: 
	device_vector<REAL> Ftot; 
	// el rango de interés definido por los iteradores 
	device_vector<REAL>::iterator Ftot_it0;
	device_vector<REAL>::iterator Ftot_it1;

	// file para guardar algunas propiedades dependientes del tiempo
	#ifndef OMP
	std::ofstream propsout;
	std::ofstream zerosout;
	std::ofstream nzerosout;
	#else
	std::ofstream propsout;
	std::ofstream zerosout;
	std::ofstream nzerosout;
	#endif
	std::ofstream log; // logfile


	#ifdef FOURIER
	int Ncomp;	
	thrust::device_vector<COMPLEX> u_fourier;
	thrust::device_vector<COMPLEX> cumu_fourier;
	thrust::host_vector<COMPLEX> h_u_fourier;
	COMPLEX *u_fourier_raw; 
	cufftHandle plan_d2z;
	#endif
};



/////////////////////////////////////////////////////////////////////////////
// sin opengl el main seria simplemente algo asi...

/*#include "qew_minimal_solucion.h"
int main(){
	Cuerda C;
	C.init();
	C.dynamics(TRUN);
	C.print_timmings();
	return 0;
}*/

