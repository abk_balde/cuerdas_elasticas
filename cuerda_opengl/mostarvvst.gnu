set multi lay 2,2; 

#0.1480 
#
# 0.1482 1483 0.1487 0.1488 0.1492 0.1493 0.1497

set xla 'f-fc'; set yla 'v(t)/(f-fc)^0.65'
plot [][0.1:] for[f in "0.1482 0.1483 0.1487 0.1488 0.1492 0.1493 0.1497 0.1485 0.1490 0.1495 0.1500 0.1505 0.1510 0.1515 0.1520 0.1525 0.1530 0.1535 0.1540 0.1545 0.1550 0.1560 0.1570 0.1580"] \
sprintf("fbig_%s.dat",f) u 1:($2/(f-0.14785)**0.65) t sprintf("f=%s",f) w lp; 

set xla 'f-fc'; set yla 'v(t)/(f-fc)^0.8'
plot [][0.1:] for[f in "0.1680 0.1700 0.1720 0.1740 0.1760 0.1780 0.1800"] sprintf("fbig_%s.dat",f) u 1:($2/(f-0.14785)**0.8) t sprintf("f=%s",f) w lp


set xla 'f-fc'; set yla 'v(t)/(f-fc)^0.9'
plot [][0.1:] for[f in "0.1850 0.1900 0.1950 0.2000 0.2050 0.2100 0.2150 0.2200 0.2250 0.2300 0.2350 0.2400"] sprintf("fbig_%s.dat",f) u 1:($2/(f-0.14785)**0.9) t sprintf("f=%s",f) w lp


set xla 'f-fc'; set yla 'v(t)/(f-fc)^1'
plot [][0.1:] for[f in "0.2400 0.2500 0.2600 0.2700 0.2800 0.2900 0.3000"] sprintf("fbig_%s.dat",f) u 1:($2/(f-0.14785)**1) t sprintf("f=%s",f) w lp


unset multi
