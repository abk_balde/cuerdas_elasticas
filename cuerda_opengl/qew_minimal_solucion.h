/*
=============================================================================================
A. B. Kolton

Segundo Encuentro Nacional de Computación de Alto Rendimiento para Aplicaciones Científicas
7 al 10 de Mayo de 2013.
=============================================================================================

Este programita simula la dinámica de una cuerdita elastica en un medio desordenado en la GPU, 
usando la librería Thrust, y la libreria Random123. Es una versión reducida de la que 
usamos con E. Ferrero y S. Bustingorry para la publicación:

Phys. Rev. E 87, 032122 (2013)
Nonsteady relaxation and critical exponents at the depinning transition
http://pre.aps.org/abstract/PRE/v87/i3/e032122
http://arxiv.org/abs/1211.7275

Y la explicación resumida del código esta en el material suplementario de la revista:
http://pre.aps.org/supplemental/PRE/v87/i3/e032122


Sin resolver los TODO ya se puede compilar con "make", y larga algunos timmings.
Genera dos ejecutables a la vez, uno de CUDA (corre en GPU) y otro de openMP (corre en CPU multicore).

OBJETIVOS:
- Practicar el manejo básico de la librería Thrust.
- Practicar el manejo básico de la librería Random123.
- Comparar performances CPU vs GPU.
- Aprender a combinar herramientas para resolver una ecuación diferencial 
parcial estocástica con desorden congelado.

EJERCICIOS:	
- Levantar los TODO.
- Para los mas expertos: Como mejoraría la performance del codigo?
*/

/*
notas: potencial suave default setting, fc=0.144 con sigma=1

*/

#ifdef FOURIER
#include "cutil.h"
#include <cufft.h>
#endif

#include "simple_timer.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <cstdlib>


/* algunos headers de la libreria thrust */
// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
// https://github.com/thrust/thrust/wiki/Documentation
#include <thrust/transform.h>
#include <thrust/copy.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <thrust/functional.h>
#include <thrust/sequence.h>
#include <thrust/fill.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/reduce.h>
#include <thrust/extrema.h>
#include <thrust/transform_reduce.h>
#include <thrust/sort.h>


/* parámetros del problema */
#ifndef TAMANIO
#define L	4096   // numero de partículas/monomeros
#else
#define L	TAMANIO   // numero de partículas/monomeros
#endif
#ifndef TIEMPORUN
#define TRUN	100000       // numero de iteraciones temporales 
#else
#define TRUN	TIEMPORUN    // numero de iteraciones temporales 
#endif
//#ifndef TPROP
//#define TPROP	100 // intervalo entre mediciones
//#endif

// fc ~ 0.368 unconmensurate pinning
#ifndef F0
#define F0	0.00       // (0.12) fuerza uniforme sobre la interface/polímero 
#endif

#ifndef F0AC
#define F0AC	0.00       // (0.12) fuerza uniforme sobre la interface/polímero 
#endif


#ifndef TAU0
#define TAU0	10000       // (0.12) fuerza uniforme sobre la interface/polímero 
#endif


#ifndef Dt
#define Dt	0.01       // paso de tiempo (hacer una funcion para que se adapte)
#endif

#ifndef TEMP
#define TEMP	0.001     // temperatura	
#endif

#ifndef TZEROS
#define TZEROS	100000000000000     // cada cuanto determina los zeros
#endif

#ifndef D0
#define D0	1.0	  // intensidad del desorden
#endif

//#ifndef SEED
//#define SEED 	12345678 // global seed RNG (quenched noise)
//#endif

#ifndef SEED2
#define SEED2 	12312313 // global seed#2 RNG (thermal noise)
#endif

#ifndef SEED
#define SEED 	12312313 // global seed#2 RNG (thermal noise)
#endif


#define MAXPOINTSDISPLAY	512

#ifndef TCHECKBIG
#define TCHECKBIG	1000000
#endif

#ifndef UMAX
#define UMAX		1000000
#endif

#ifndef SIGMA
#define SIGMA		2.0
#endif

#ifndef CELASTIC
#define CELASTIC	1.0
#endif

// para evitar poner "thrust::" a cada rato all llamar sus funciones
using namespace thrust;

// precisón elegida para los números reales
#ifdef FOURIER
	#ifdef DOUBLEPRECISION
	typedef cufftDoubleReal REAL;
	typedef cufftDoubleComplex COMPLEX;
	#else
	typedef cufftReal REAL;
	typedef cufftComplex COMPLEX;
	#endif
#else
	#ifdef DOUBLEPRECISION
	typedef double 	REAL;
	#else
	typedef float 	REAL;
	#endif
#endif


/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG


template<typename T>
struct absolute_value : public unary_function<T,T>
{
  T offset;
  absolute_value(T offset_):offset(offset_){};	
  __host__ __device__ T operator()(const T &x) const
  {
  	T y=x-offset;
    return y < T(0) ? -y : y;
  }
};

// para generar números aleatorios gausianos a partir de dos uniformes
// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
__device__
REAL box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	REAL u1 = u01_open_closed_32_53(r_philox[0]);
  	REAL u2 = u01_open_closed_32_53(r_philox[1]);

  	REAL r = sqrt( -2.0*log(u1) );
  	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);    			
}

#ifdef PRINTCONFS
// para imprimir configuraciones cada tanto...
std::ofstream configs_out("configuraciones.dat");
void print_configuration(device_vector<REAL> &u, device_vector<REAL> &Ftot, REAL velocity, int size, int time)
{
	//std::cout << "printing configuration" << std::endl;
	long i=0;
	int every = int(L*1.0/size);
	for(i=1;i<u.size()-2;i+=every)
	{
		std::cout << i << std::endl;
		configs_out << u[i] << " " << Ftot[i] << " " << velocity << " " << time << std::endl;
	}
	configs_out << "\n\n";
	//std::cout << "done" << std::endl;
}
#endif



// genera un path gaussiano periodico con rugosidad dada
/*struct gaussian_path{
	RNG rng;       // random number generator
	unsigned ll;
	gaussian_path(ll_):ll(ll_){

	}
	__device__
    	REAL operator()(tuple<unsigned long,unsigned long,REAL,REAL> tt)
    	{	
		// thread/particle id
		uint32_t tid=get<0>(tt); 
    	} 	
	//IN CONSTRUCTION
}*/


// no es efifiente generar asi un path, rehacer usando cufft...
/*#include "mt.h"
void fourier_gen_path(REAL roughness, int ll, device_vector<REAL>::iterator X, unsigned long seed)
{
	init_genrand(seed);
	REAL sigma;
	int Nmodes=ll;
	thrust::host_vector<REAL> A(ll);
	thrust::host_vector<REAL> B(ll);
	thrust::host_vector<REAL> H(ll);

	thrust::fill(H.begin(),H.end(),REAL(0.0));

	std::cout << "generating modes\n";
	for(int n=0;n<Nmodes;n++){
		sigma = pow((double)ll/(2.0*M_PI*(n+1)),roughness)/sqrt(M_PI*(n+1));
		A[n] = sigma*gasdev_real3();
		B[n] = sigma*gasdev_real3();
		//std::cout << n << "\n";
	}
	std::cout << "generating vector\n";
	double sinx,cosx;
	for(int t=0;t<ll;t++){
		for(int n=0;n<Nmodes;n++){
			sincos(2.*M_PI*(n+1)*t*1.0/ll, &sinx, &cosx);
			H[t] = H[t] + A[n]*cosx + B[n]*sinx;			
		}
		if(t%100==0) std::cout << t << "\n";
	}
	REAL minimo=thrust::reduce(H.begin(),H.end(),REAL(0.0),thrust::minimum<REAL>());
	for(int t=0;t<ll;t++) H[t]+=fabs(minimo);
	thrust::copy(H.begin(),H.end(),X);
	std::cout << "done\n";
};
*/

// functor usado para calcular la rugosidad 
#ifdef FOURIER
struct longrange_functor
{
    __device__
    COMPLEX operator()(COMPLEX c, REAL r)
    {	
    	COMPLEX res;
    	REAL f=-powf(fabs(r),SIGMA);
    	res.x = c.x*f;
    	res.y = c.y*f;
		return res;
    }
};	
#endif

// functor usado para calcular la rugosidad 
struct roughtor: public thrust::unary_function<REAL,REAL>
{
    REAL u0; // un "estado interno" del functor	
    roughtor(REAL _u0):u0(_u0){};	
    __device__
    REAL operator()(REAL u)
    {	
	return (u-u0)*(u-u0);
    }
};	


// predicate para detectar zeros en la cuerda wrt center of mass...
struct is_zeros
{
	REAL cm0;
	REAL *u_ptr;
	is_zeros(REAL _cm0, REAL * _ptr):cm0(_cm0),u_ptr(_ptr){};
	__host__ __device__
	bool operator()(const int i)
	{
		REAL first_point= u_ptr[i]-cm0;
		REAL second_point= u_ptr[i+1]-cm0;
		bool positive_zero=(first_point < 0.0)&&(second_point > 0.0);
		bool negative_zero=(first_point > 0.0)&&(second_point < 0.0);
		return (positive_zero || negative_zero);
	}
}; 

struct is_soliton
{
    REAL *ptr;	
    is_soliton(REAL *ptr_):ptr(ptr_){};	
    __device__
    bool operator()(const int p)
    {	
    	// para ser soliton
    	// u es tal que
    	//|cosf(2.0f*M_PI*u)|<epsilon (local extrema)
    	//-d cosf(2.0f*M_PI*u)/ du = sinf(2.0f*M_PI*u) > 0
	// o mejor:
	// -cosf(2.0f*M_PI*u_1)<0, -cosf(2.0f*M_PI*u_2)>0 ==> el soliton esta en ~(1+2)/2

	REAL u1=ptr[p];
	REAL u2=ptr[(p+1)%L];

	if(-cosf(2.0f*M_PI*u1)<0 && -cosf(2.0f*M_PI*u2)>0) return 1; // es soliton
	else return 0; // no es soliton
    }
};



/////////////////////////////////////////////////////////////////////////////
// FUNCTORS usados en los algoritmos TRANSFORM

// pinning cell list parameters
#define RINTPIN		1.0   // pinning potential characteristic length 	
#define RCUTPIN		5.0   // cutoff pinning potential RCUTPIN > RINTPIN
#define	WPIN		10000000 // number of pinning cells in X directon RMAXPIN*WPIN=LX
#define NPINSPERCELL	2	// number of pinning centers per cell
#define	LX		10000000 
#define FLIM		1.0

__device__ 
REAL pinning_force(int tid,REAL u,unsigned long *countpin,REAL *upin);

struct pinning_force_functor
{
	int tid;
	unsigned long *countpin;
	REAL *upin;
	pinning_force_functor(int tid_, unsigned long *countpin_, REAL *upin_):tid(tid_),countpin(countpin_),upin(upin_){};
	
	__device__ 
	REAL operator()(REAL u)
	{
		return pinning_force(tid,u,countpin,upin);
	}

};


__device__ 
REAL pinning_force(int tid,REAL u,unsigned long *countpin,REAL *upin)
{
	// random number generator
	RNG rng;       
	// keys and counters 
    RNG::ctr_type c={{}};
    RNG::key_type k={{}};
	RNG::ctr_type r;

	c[1]=uint32_t(SEED);
#ifdef PIECEWISELINEAR
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	REAL firstRN, secondRN;
	int U=int(u);
	c[0]=uint32_t(U); // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	firstRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
//	#ifdef DISORDERINX
//	firstRNx=(u01_closed_closed_32_53(r[1])-0.5); // alternativa: box_muller(r);
//	#endif
	c[0]=uint32_t(U+1); // COUNTER={U+1,GLOBAL SEED}
	r = rng(c, k);
	secondRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
//	#ifdef DISORDERINX
//	secondRNx=(u01_closed_closed_32_53(r[1])-0.5); // alternativa: box_muller(r);
//	#endif
//	#ifndef DISORDERINX
	return D0*(firstRN - (firstRN-secondRN)*(u-U));// linearly interpolated force
//	#else
//	return D0*(firstRN - (firstRN-secondRN)*(u-U))
//	+up1minusum1*D0*(firstRN - (firstRNx-secondRNx)*(u-U));// linearly interpolated force
//	#endif

#elif defined(VARIABLEPIECEWISECOSINES)	
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	REAL firstRN;
	REAL u1,u2;

	c[0]=uint32_t(countpin[tid]); // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	firstRN=(u01_closed_closed_32_53(r[0]))-0.5; // alternativa: box_muller(r);
	u1=upin[tid];
	u2=u1+(1.0+VARIABLEPIECEWISECOSINES*firstRN);

	while(u>u2){
		u1=u2;
		upin[tid]=u1;
		countpin[tid]++;
		c[0]=uint32_t(countpin[tid]); 
		r = rng(c, k);
		firstRN=(u01_closed_closed_32_53(r[0]))-0.5; 
		u2=u1+(1.0+VARIABLEPIECEWISECOSINES*firstRN);
	}

	//potencial ~ (cos(2*M_PI*(u-u1)/(u2-u1))-1);
	return D0*sin(2.0*M_PI*(u-u1)/(u2-u1))/(2.0*M_PI/(u2-u1));


#elif defined(PIECEWISEPARABOLIC)
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	REAL firstRN, secondRN;
	int U=int(u);
	c[0]=uint32_t(U); // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	firstRN=(u01_closed_closed_32_53(r[0])); // alternativa: box_muller(r);
	c[0]=uint32_t(U+1); // COUNTER={U+1,GLOBAL SEED}
	//r = rng(c, k);
	//secondRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	return -D0*(2*(u-U)-1)*firstRN;

#elif defined(WASHBOARDUNCONMENSURATE)
	//(Abs[Mod[x,1]-0.5]^g-0.5^g/(1+g))/((1. 0.5^g)/(1. +g))
	/*#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	c[0]=0; // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	REAL firstRN=(u01_closed_closed_32_53(r[0])); // alternativa: box_muller(r);
	#endif
	*/
	REAL g=2;
	REAL uu=u;
	//return (powf(fabs(u-firstRN-floor(u-firstRN)-0.5),g)-powf(0.5,g)/(1.+g))/(powf(0.5,g)/(1. +g));
	return (powf(fabs(uu-floor(uu)-0.5),g)-powf(0.5,g)/(1.+g))/(powf(0.5,g)/(1. +g));

#elif defined(RANDOMPERIODICSIN)
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	r = rng(c, k);
	REAL firstRN=(u01_closed_closed_32_53(r[0])); 
	return D0*sinf(2*M_PI*(u+firstRN));
#elif defined(PERIODICSINWITHLARKINFORCE)
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	r = rng(c, k);
	REAL firstRN=(u01_closed_closed_32_53(r[0])); 
	return D0*sinf(2*M_PI*u)+firstRN;
	//return D0*sinf(2*M_PI*(u+firstRN));
	//return D0*sinf(u+firstRN*2*M_PI);

#elif defined(RANDOMPERIODICSINFREQ)
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	r = rng(c, k);
	REAL firstRN=(u01_closed_closed_32_53(r[0])); 
	REAL secondRN=(u01_closed_closed_32_53(r[1])); 
	return D0*sinf( 2*M_PI*(u*(1+RANDOMPERIODICSINFREQ*firstRN)+secondRN) );
#elif defined(RANDOMPERIODICPARABOLAS)
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	r = rng(c, k);
	REAL firstRN=(u01_closed_closed_32_53(r[0])); 
	return -D0*(u+firstRN-floor(u+firstRN)-0.5);
#elif defined(RANDOMFORCE)
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif
	r = rng(c, k);
	REAL firstRN=(u01_closed_closed_32_53(r[0])-0.5); 
	return D0*firstRN;
#else
	#ifdef COLUMNAR
	k[0]=0;
	#else
	k[0]=tid; 	  //  KEY = {threadID} 
	#endif

	u=u-floor(u/LX)*LX;

	//neighbor cells
	int cell[3];
	cell[0]=int(u/RCUTPIN);
	cell[1]=(int(u/RCUTPIN)-1+WPIN)%WPIN;
	cell[2]=(int(u/RCUTPIN)+1)%WPIN;

	unsigned int npins;
	REAL uu,xx,pp,ss,xpin,fpin;
	REAL dx,dr2;
	fpin=0.0f;
	for(int ic=0;ic<3;ic++)
	{	
		//https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables
		// number of pins in the cell: poisson dist.
		xx=0.f; pp=ss=expf(-NPINSPERCELL);
		c[0]=cell[ic]; c[1]=uint32_t(SEED);
		r = rng(c, k); c[1]++;
		uu=u01_open_closed_32_53(r[0]); 
		#ifndef FIXPINSPERCELL
		while(uu>ss){
			xx++;
			pp=pp*NPINSPERCELL/xx;
			ss=ss+pp;
		}
		npins=xx;
		#else
		npins=NPINSPERCELL;
		#endif				
//    algorithm Poisson generator based upon the inversion by sequential search:[37]
//    init:
//         Let x ← 0, p ← e−λ, s ← p.
//         Generate uniform random number u in [0,1].
//    while u > s do:
//         x ← x + 1.
//         p ← p * λ / x.
//         s ← s + p.
//    return x.
		
		// random positions for the pins in the cell
		for(int n=0;n<npins;n++){
			//c[0] makes pins in different cells uncorrelated
			//c[1] makes pins in the same cell uncorrelated
			r = rng(c, k); c[1]++; 
			#ifndef REGULARLYSPACEDWELLS
	 		xpin = (cell[ic] + u01_open_closed_32_53(r[0]))*RCUTPIN;
	 		#else
	 		xpin = (cell[ic] + n*1.0/npins)*RCUTPIN;
	 		#endif
			dx = (u-xpin); if(dx>LX*0.5) dx+=-LX; if(dx<-LX*0.5) dx+=LX;
			dr2=dx*dx;

			if(dr2<RCUTPIN*RCUTPIN){
				// a gaussian attractive potential
				fpin+=-expf(-dr2/(RINTPIN*RINTPIN))*dx*2.0/(RINTPIN*RINTPIN);
			}
		}
	}
	#ifdef PINSATURATION
	return tanh(fpin/FLIM)*FLIM;
	#else
	return D0*fpin;
	#endif
#endif
}

#ifdef FOURIER
struct suma
{
	__device__
	COMPLEX operator()(const COMPLEX a, const COMPLEX b)
	{
		COMPLEX c;
		c.x=a.x+b.x;
		c.y=a.y+b.y;
		return c;
	}
}; 
#endif

#include "util.h"
#define a0 	3.0 // for elastic chains
struct fuerza
{
    RNG rng;       // random number generator
    unsigned long long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL f0;	
    REAL T0;
    REAL dt;
    REAL *u_ptr;
    REAL uparabola;	
    REAL *upin;
    unsigned long *countpin;

		
    fuerza(unsigned long long _t, REAL _f0, REAL _T0, REAL dt_, REAL *ptr_,REAL uparabola_,unsigned long *countpin_raw_ptr_,REAL *upin_raw_ptr_):
    tiempo(_t),f0(_f0),T0(_T0),u_ptr(ptr_),dt(dt_),uparabola(uparabola_),upin(upin_raw_ptr_),countpin(countpin_raw_ptr_)
    {
	noiseamp=sqrt(T0/dt);
    }; 

    __device__
    REAL operator()(tuple<long,REAL,REAL,REAL> tt)
    {	
	// thread/particle id
	uint32_t tid=get<0>(tt); 

	//#ifdef FINITETEMPERATURE
		// keys and counters 
	    RNG::ctr_type c={{}};
	    RNG::key_type k={{}};
		RNG::ctr_type r;
		c[1]=SEED+1; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}
	//#endif


	// LAPLACIAN
	REAL u=get<2>(tt);
	REAL laplaciano = 0.0;

	#ifndef FREEPARTICLES
	#ifndef MEANFIELDINTERACTION
		#ifndef FINITERANGEELASTICITY
			REAL um1=get<1>(tt);
			REAL up1=get<3>(tt);
			#ifndef PLASTICITY
			#ifndef ONLYANHARMONICCORRECTIONS
			laplaciano = um1 + up1 - 2.*u;
			#endif
			#else
			laplaciano = sinf(um1-u)+sinf(up1-u);
			#endif
		#else
			/* // igual que antes pero usando u_ptr
			REAL um1 =*(&u_ptr[tid]-1);
			REAL up1 =*(&u_ptr[tid]+1);
			REAL laplaciano = up1 + um1 - 2.*u;
			*/
			/* 
			REAL laplaciano = 0.0f;
			REAL um, up;	
			for(int i=1;i<2;i++){ //funciona solo hasta 2
				um =*(&u_ptr[tid]-i);		
				up =*(&u_ptr[tid]+i);		
				laplaciano += ((up-u)+(um-u))/i;
			}*/
			laplaciano = 0.0f;
			REAL um, up;
			//experimentos con distintas interacciones de largo alcance
			for(int i=1;i<int(5./a0);i++){
				um =u_ptr[(tid-i+L)%L];		
				up =u_ptr[(tid+i)%L];		
				//laplaciano += ((up-u)+(um-u))/i; //funca
				laplaciano += -k1(fabs(up+i*a0-u))+k1(fabs(-i*a0+um-u)); 
				//laplaciano += k2(i)*(up+um-2.f*u);
			}
		#endif
		#ifdef ONLYANHARMONICCORRECTIONS
				//laplaciano += (up1-um1)*(up1-um1)*0.25; // completar
				laplaciano += ONLYANHARMONICCORRECTIONS*(powf(up1-u,ANHEXPO)+powf(um1-u,ANHEXPO)); // completar
		#endif	
		#ifdef ANHARMONICCORRECTIONS
				//laplaciano += (up1-um1)*(up1-um1)*0.25; // completar
				laplaciano += ANHARMONICCORRECTIONS*(powf(up1-u,ANHEXPO)+powf(um1-u,ANHEXPO)); // completar
		#endif	
	#endif
	#endif

	// DISORDER
	REAL quenched_noise=0.0; 
#ifndef NODISORDER
	quenched_noise += pinning_force(tid,u,countpin,upin);
	#ifdef DISORDERINX
	k[0]=tid; 	  //  KEY = {threadID} 
	REAL firstRN, secondRN;
	int U=int(u);
	c[0]=uint32_t(U); // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	firstRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	c[0]=uint32_t(U+1); // COUNTER={U+1,GLOBAL SEED}
	r = rng(c, k);
	secondRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	quenched_noise += 0.5*(up1-um1)*D0*(firstRN - (firstRN-secondRN)*(u-U));
	#endif	
#endif
#ifdef WASHBOARD
	quenched_noise += sinf(2.0f*M_PI*u);
#endif	
#ifdef WASHBOARDGAMMA
	REAL prefactor=sqrt(M_PI)*tgamma(WASHBOARDGAMMA*0.5+1)/(powf(2.0,WASHBOARDGAMMA*0.5)*tgamma(WASHBOARDGAMMA*0.5+0.5));
	//REAL prefactor=sqrt(M_PI)*tgamma(2.0*0.5+1)/(powf(2.0,2.0*0.5)*tgamma(2.0*0.5+0.5));
	quenched_noise += powf(1.0-cosf(2.0f*M_PI*u),WASHBOARDGAMMA*0.5)*prefactor-1.0;
#endif	
#ifdef WASHBOARDGAMMA2
	quenched_noise += cosf(u);
#endif	
#ifdef PERIODICTILT
	quenched_noise += PERIODICTILT*sinf(2.0f*M_PI*tid*1.0/L);
#endif

	// THERMAL NOISE
	REAL thermal_noise;

#ifdef FINITETEMPERATURE
	k[0]=tid; 	  //  KEY = {threadID} 
	c[0]=uint32_t(tiempo); // COUNTER={tiempo, GLOBAL SEED #2}

	c[1]=SEED2; // para evitar correlaciones entre el ruido térmico y el desorden congelado...
	r = rng(c, k);
	thermal_noise = noiseamp*box_muller(r);    			
#else
	thermal_noise=0.0;
#endif

#ifndef MM // force driven

	#ifdef DISORDERINX
		REAL slope1= (up1 - u);
		REAL slope2= (u - um1);
		return (laplaciano/(1+slope1*slope2)+
		quenched_noise+thermal_noise+
		f0*sqrt(1.0+slope1*slope2));
	#elif defined EDGEPINNING
		REAL amp=(tid==0)?(5.0):(1.0);
		return (laplaciano+amp*quenched_noise+thermal_noise+f0);
	#elif defined SLOPEDRIVE
		REAL slope= (um1 - up1)/2.0;
		return (laplaciano+quenched_noise+thermal_noise+f0/(1.0+slope*slope));	
	#else
		return (CELASTIC*laplaciano+quenched_noise+thermal_noise+f0);		
	#endif

#else // velocity driven (uparabola=f0*t)
	//return (laplaciano+quenched_noise+thermal_noise+MM*(f0*tiempo*Dt-u));
	//return (laplaciano+quenched_noise+thermal_noise+ MM*(uparabola-u));
	return (CELASTIC*laplaciano+quenched_noise+thermal_noise+ uparabola-MM*u);

#endif
  }
};


// Explicit forward Euler step: lo mas simple que hay 
// (pero ojo con el paso de tiempo que no sea muy grande!)
struct euler
{
    REAL dt;
    euler(REAL dt_):dt(dt_){};
		
    __device__
    REAL operator()(REAL u_old, REAL force)
    {	
	return (u_old + force*dt);
    }
};	


#ifdef OMP
#include <omp.h>
#endif
/////////////////////////////////////////////////////////////////////////////

class Cuerda
{
	public:
	#ifdef OMP
	omp_timer t;
	#else
	gpu_timer t;
	#endif

	double timer_fuerzas_elapsed;
	double timer_euler_elapsed;
	double timer_props_elapsed;
	
	std::ofstream fou;

	Cuerda(){};
	~Cuerda(){
	#ifdef FOURIER
	cufftDestroy(plan_d2z);
	cufftDestroy(plan_z2d);
	#endif
	};

	void restart(){
		char nom[50];sprintf(nom,"restart_%08lld.dat",tiempo);
		//char nom[50];sprintf(nom,"restart.tmp",tiempo);
		std::ofstream ofs(nom);
		restart(nom);
	}

	void restart(char *nom){

		//char nom[50];sprintf(nom,"restart_%08lld.dat",tiempo);
		//char nom[50];sprintf(nom,"restart.tmp",tiempo);
		std::ofstream ofs(nom);
		REAL u_;

		std::cout << "guardando restart..."; 
		ofs << mtprop << " " << tiempo << " " << count << std::endl;
		for(unsigned long i=0;i<L;i++)
		{
			u_ = u[i+1]; 
			ofs << u_ << "\n";
		}
		std::cout << "done" << std::endl; 

		//std::system("cp restart.tmp restart.dat");
	}

	void restart_flat_at(REAL here){
		fill(u_it0,u_it1,REAL(here)); 
	}

	void init(){
		#ifdef OMP
		std::cout << "#conociendo el host, OMP threads = " << omp_get_max_threads() << std::endl;
		#endif		

		u.resize(L+2);
		u_it0 = u.begin()+1;
		u_it1 = u.end()-1;
		u_raw_ptr = raw_pointer_cast(&u[1]);

		// container de fuerza total: 
		Ftot.resize(L); 
		// el rango de interés definido por los iteradores 
		Ftot_it0 = Ftot.begin();
		Ftot_it1 = Ftot.end();


		solitonindex.resize(L);

		// container para pinning especial
		countpin.resize(L); 
		countpin_it0 = countpin.begin();
		countpin_it1 = countpin.end();
		countpin_raw_ptr = raw_pointer_cast(&countpin[0]);
		upin.resize(L); 
		upin_it0 = upin.begin();
		upin_it1 = upin.end();
		upin_raw_ptr = raw_pointer_cast(&upin[0]);


		// alguna condición inicial chata (arbitraria)
		// TODO: incluir lectura de condicion inicial
		reset_log_monitoring();
		fill(u_it0,u_it1,REAL(10.0)); 
		fill(upin.begin(),upin.end(),(REAL)(9.5));
		fill(countpin.begin(),countpin.end(),(unsigned long)(SEED));

		std::ifstream ifs ("restart.dat", std::ifstream::in);
		if(ifs.good()){
			REAL u_;
			unsigned long mtprop_,tiempo_,count_; 
			ifs >> mtprop_ >> tiempo_ >> count_;
			reset_log_monitoring(mtprop_,tiempo_,count_);
			for(unsigned long i=0;i<L;i++)
			{
				ifs >> u_;
				u[i+1]=u_; 
			}
		}		

		// o una condicion inicial rugosa (random)
		//fourier_gen_path(1.5, L, u_it0, 123456);

		timer_fuerzas_elapsed=0.0;
		timer_euler_elapsed=0.0;
		timer_props_elapsed=0.0;
		#ifdef MM 
		uparabola=D0;
		#else
		uparabola=0.0;		
		#endif

		#ifndef OMP
		propsout.open("someprops.dat");
		propsout << "1tiempo 2velocity 3center_of_mass 4roughness 5posmax 6T0 7f0 8uparabola" << std::endl;
		zerosout.open("zeros.dat");
		nzerosout.open("nzeros.dat");
		#else
		propsout.open("someprops_omp.dat");
		propsout << "1tiempo 2velocity 3center_of_mass 4roughness 5posmax 6T0 7f0 8uparabola" << std::endl;
		zerosout.open("zeros_omp.dat");
		nzerosout.open("nzeros_omp.dat");
		#endif
		
		log.open("logfile.dat");

		f0=F0;
		f0ac=F0AC;
		tau0=TAU0;
		T0=TEMP;
		dt=Dt;
		smooth_velocity=0.0;

		#ifdef FOURIER

		#ifndef DOUBLEPRECISION
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_d2z,L,CUFFT_R2C,1));
		#else
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_d2z,L,CUFFT_D2Z,1));		
		#endif

		Ncomp=L/2+1;
		u_fourier.resize(Ncomp);
		cumu_fourier.resize(Ncomp);
		COMPLEX zero; zero.x=zero.y=0.0;
		thrust::fill(cumu_fourier.begin(),cumu_fourier.end(),zero);
		h_u_fourier.resize(Ncomp);
		u_fourier_raw = thrust::raw_pointer_cast(&u_fourier[0]); 

		//F_long_range_fourier.resize(Ncomp);

		fou.open("fourier.dat");
		//std::cout << u_fourier.size() << std::endl;

		// plan de antitransformada para interacciones de largo alcance
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_z2d,L,CUFFT_C2R,1));

		#ifdef LONGRANGEELASTICITY
		F_long_range.resize(L+2);
		fill(F_long_range.begin(),F_long_range.end(),REAL(0.0)); 

		frequencies_fft.resize(Ncomp);		
		thrust::host_vector<REAL> hfreq(Ncomp);		
		for(int i=0;i<Ncomp;i++) hfreq[i]=i*2.0*M_PI/L;
		//for(int i=L/2;i<L;i++) hfreq[i]=(-L/2+i)*2.0*M_PI/L;
		thrust::copy(hfreq.begin(),hfreq.end(),frequencies_fft.begin()); 	
		#endif

		#endif


		log << "L=" << L << std::endl;
		log << "f=" << f0 << std::endl;
		log << "T=" << T0 << std::endl;
		log << "Dt=" << dt << std::endl;

		#ifdef TPROP
		log << "TPROP =" << TPROP << std::endl;
		#else
		log << "log monitorinng" << std::endl;		
		#endif

		#ifdef FOURIER
		log << "fourier transforms activated"<< std:: endl;
		#endif	
		#ifdef LONGRANGEELASTICITY 
		log << "LONGRANGEELASTICITY  activated with amplitude = " << LONGRANGEELASTICITY << std:: endl;
		log << "SIGMA = "<< SIGMA << std:: endl;
		#endif	

		#ifdef ANHARMONICCORRECTIONS 
		log << "ANHARMONICCORRECTIONS activated" << std:: endl;
		#endif	

		#ifdef MEANFIELDINTERACTION
		log << "MEANFIELDINTERACTION  activated = "<< MEANFIELDINTERACTION << std:: endl;
		#endif	

		#ifdef FINITETEMPERATURE
			log << "FINITE TEMPERATURE" << std:: endl;
			log << "THERMAL SEED =" << SEED2 << std:: endl;
		#else
			log << "ZERO TEMPERATURE" << std:: endl;
		#endif	
		
		#ifdef RESTART
			log << "RESTART = " << RESTART << std::endl;
		#endif

		#ifdef DOUBLEPRECISION
			log << "DOUBLEPRECISION activated" << std::endl;
		#endif
		

		#ifdef NODISORDER
			log << "DISORDER desactivated" << std:: endl;
		#else	
			log << "DISORDER SEED =" << SEED << std:: endl;
			#ifdef COLUMNAR
			log << "COLUMNAR =" << SEED << std:: endl;			
			#endif		
			#ifdef PIECEWISELINEAR
				log << "PIECEWISELINEAR DISORDER" << std:: endl;
			#elif defined(PIECEWISEPARABOLIC)
				log << "PIECEWISEPARABOLIC DISORDER" << std:: endl;
			#elif defined(RANDOMPERIODICSIN)
				log << "RANDOM SIN DISORDER" << std:: endl;			
			#elif defined(RANDOMPERIODICSINFREQ)
				log << "RANDOM SIN DISORDER with RANDOM FREQS = " << RANDOMPERIODICSINFREQ << std:: endl;			
			#elif defined(RANDOMPERIODICPARABOLAS)
				log << "RANDOM PERIODIC PARABOLAS DISORDER" << std:: endl;			
			#elif defined(RANDOMFORCE)
				log << "RANDOM FORCE" << std:: endl;			
			#elif defined(VARIABLEPIECEWISECOSINES)
				log << "VARIABLEPIECEWISECOSINES = " << VARIABLEPIECEWISECOSINES << std:: endl;
			#elif defined(WASHBOARDGAMMA)
				log << "WASHBOARDGAMMA = " << WASHBOARDGAMMA << std:: endl;							
			#elif defined(WASHBOARD)
				log << "WASHBOARD = defined " << std:: endl;							
			#else
				log << "GAUSSIAN WELLS DISORDER:" << std:: endl;		
				log << "RINITPIN, RCUTPIN = " << RINTPIN << ", " << RCUTPIN << std:: endl;				
				log << "NPINSPERCELL = " << NPINSPERCELL << std:: endl;		
				#ifdef PINSATURATION
				log << "PINSATURATION, FLIM = " << FLIM << std:: endl;				
				#endif
			#endif
		#endif

		#ifdef PERIODICTILT
			log << "PERIODICTILT = " << PERIODICTILT << std:: endl;					
		#endif 	

		#ifdef FREEPARTICLES
			log << "FREEPARTICLES activated" << std:: endl;					
		#endif 	

		#ifdef MEANFIELDINTERACTION
			log << "MEANFIELDINTERACTION activated = " << MEANFIELDINTERACTION << std:: endl;					
		#endif 	

		#ifdef FIXPINSPERCELL
			log << "FIXPINSPERCELL activated " << std:: endl;					
		#endif 	

		#ifdef CHECKPINNINGFORCE
			log << "CHECKPINNINGFORCE activated, see pinningforce.dat " << std:: endl;					
		#endif 	



		#ifdef FINITERANGEELASTICITY
			log << "FINITERANGEELASTICITY" << std:: endl;	
			log << "a0 = " << a0 << std:: endl;		
		#endif

		#ifdef	VMINSTOP
			log << "VMINSTOP = " << VMINSTOP << std:: endl;	
			log << "SMALLVELOCITY = " << SMALLVELOCITY << std:: endl;	
			#ifdef MINAVERAGEVELOCITY
			log << "MINAVERAGEVELOCITY = " << MINAVERAGEVELOCITY << std::endl;
			#endif
		#endif	

		#ifndef OMP
		log << "CUDA" << std:: endl;				
		#else
		log << "OMP" << std:: endl;				
		#endif

		#ifdef MM 
		log << "VELOCITY DRIVEN" << std:: endl;				
		log << "MM = " << MM << std:: endl;				
		#else
		log << "FORCE DRIVEN" << std:: endl;				
		#endif


		#ifdef CHECKPINNINGFORCE
		int Nintervals=100000;
		REAL dx=0.01;
		thrust::device_vector<REAL> fpintest(Nintervals);
		thrust::device_vector<REAL> utest(Nintervals);
		using namespace thrust::placeholders;
		
		#ifndef VARIABLEPIECEWISECOSINES
		thrust::transform(thrust::make_counting_iterator(0), thrust::make_counting_iterator(Nintervals),utest.begin(),dx*_1);		
		thrust::transform(utest.begin(),utest.end(), fpintest.begin(), pinning_force_functor(10,countpin_raw_ptr,upin_raw_ptr));
		thrust::host_vector<REAL> hfpintest1(fpintest);
		thrust::host_vector<REAL> hutest(utest);

		thrust::transform(utest.begin(),utest.end(), fpintest.begin(), pinning_force_functor(20,countpin_raw_ptr,upin_raw_ptr));
		thrust::host_vector<REAL> hfpintest2(fpintest);

		std::ofstream pinout("pinningforce.dat");
		for(int i=0;i<Nintervals;i++){
			pinout << hutest[i] << " " << hfpintest1[i] << " " << hfpintest2[i] << std::endl;
		}
		#else //this is a special kind of disorder...

		std::ofstream pinout2("pinningforce2.dat");
		thrust::device_vector<REAL> utest2(1);

		REAL upinprevio=upin[10];
		unsigned long countpinprevio=countpin[10];

		REAL upinprevio20=upin[20];
		unsigned long countpinprevio20=countpin[20];
	
		utest2[0]=u[1];
		for(int i=0;i<Nintervals;i++){
			thrust::transform(utest2.begin(),utest2.begin()+1, fpintest.begin(), pinning_force_functor(10,countpin_raw_ptr,upin_raw_ptr));
			pinout2 << utest2[0] << " " << fpintest[0] << " ";

			thrust::transform(utest2.begin(),utest2.begin()+1, fpintest.begin(), pinning_force_functor(20,countpin_raw_ptr,upin_raw_ptr));
			pinout2 << fpintest[0] << std::endl;

			utest2[0]+=dx;
		}
		upin[10]=upinprevio;
		countpin[10]=countpinprevio;

		upin[20]=upinprevio20;
		countpin[20]=countpinprevio20;
		#endif

		#endif 

		log << "initialization done" << std::endl;

	};

	void reset_log_monitoring()
	{
		mtprop=1;
		tiempo=1;
		count=0;
	};
	void reset_mtprop()
	{
		mtprop=1;
		count=0;
	};

	void reset_log_monitoring(unsigned long _mtprop, unsigned long _tiempo,unsigned long _count)
	{
		mtprop=_mtprop;
		tiempo=_tiempo;
		count=_count;
	};
	
	void step()
	{
		//static unsigned long mtprop=1;
		//static unsigned long count=0;

		// Impone PBC en el "halo"		
		u[0]=u[L];u[L+1]=u[1];  // 0,[1,2...L],L+1 = *[x------*]x

		//ALGO ERRONEO EN ESTO
		/*#ifdef MANYSTRINGS
		int segment=int(L*1.0/MANYSTRINGS);
		for(int i=0;i<MANYSTRINGS;i++){
			u[i*segment]=u[(i+1)*segment];
			u[(i+1)*segment+1]=u[i*segment+1];
		}
		#endif
		*/

		unsigned long long n=tiempo; tiempo++;
		t.tic(); // para cronometrar el tiempo de la siguiente transformación
		
		// Fuerza en cada monómero calculada concurrentemente en la GPU: 
		// Ftot(X)= laplacian + disorder + thermal_noise + F0, X=0,..,L-1
		// mirar el functor "fuerza" mas arriba... 
		// Notar: los iteradores de interés estan agrupado con un "fancy" zip_iterator, 
		// ya que transform no soporta mas de dos secuencias como input.
		// Notar: make_counting_iterator es otro "fancy" iterator, 
		// que simula una secuencia que en realidad no existe en memoria (implicit sequences).
		// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
		// http://thrust.github.io/doc/group__fancyiterator.html
		//fuerza.set_time(n);
		#ifndef VMCDYNAMICS
			REAL drive=f0;
			#ifdef ACDRIVE
			drive=f0+f0ac*cos(2*M_PI*n/tau0);
			#endif
			transform(
				make_zip_iterator(make_tuple(
				make_counting_iterator<long>(0),u_it0-1,u_it0,u_it0+1
				)),
				make_zip_iterator(make_tuple(
				make_counting_iterator<long>(L),u_it1-1,u_it1,u_it1+1
				)),
				Ftot_it0,
				fuerza(n,drive,T0,dt,u_raw_ptr,uparabola,countpin_raw_ptr,upin_raw_ptr)
			);

			#ifdef LONGRANGEELASTICITY
				#ifdef FOURIER
				transform_fourier();
				anti_transform_fourier();
				using namespace thrust::placeholders;
				transform(Ftot_it0,Ftot_it1,F_long_range.begin()+1,Ftot_it0,_1+_2*LONGRANGEELASTICITY/REAL(L));
				#ifdef DEBUG			
				std::cout << "adding long range ok" << std::endl;
				#endif
			#else
				std::cout << "error: no se puede LONGRANGEELASTICITY sin FOURIER" << std::endl;
			#endif
		#endif

		#ifdef MEANFIELDINTERACTION // each monomer couples to center of mass, must use FREEPARTICLES
			center_of_mass=get_center_of_mass();
			using namespace thrust::placeholders;
			transform(Ftot_it0, Ftot_it1, u_it0, Ftot_it0, _1+MEANFIELDINTERACTION*(center_of_mass-_2));
		#endif

		timer_fuerzas_elapsed+=t.tac();

		// Explicit forward Euler step, implementado en paralelo en la GPU: 
		// u(X,n) += Ftot(X,n) Dt, X=0,...,L-1
		// Mirar el functor "euler" mas arriba...
		// Notar: no hace falta zip_iterator, ya que transform si soporta hasta dos secuencias de input
		t.tic();
		transform(
			u_it0,u_it1,Ftot_it0,u_it0, 
			euler(dt)
		);
		//uparabola+=MM*f0*dt;
		#ifdef MM
		uparabola=D0+MM*f0*(tiempo*dt);
		#endif
		timer_euler_elapsed+=t.tac();

		#ifdef DEBUG			
		std::cout << "euler ok" << std::endl;
		#endif

		#else
		// plan: avanzar pares, impares
		// para eso: declarar permutation-iterators pares e impares
/*		typedef thrust::device_vector<REAL>::iterator ElementIterator;
  		typedef parimpIterator IndexIterator;
	    thrust::permutation_iterator<ElementIterator,IndexIterator> upar0_left(u.begin(), genPAR(0));
	    thrust::permutation_iterator<ElementIterator,IndexIterator> upar0(u.begin(), genPAR(1));
	    thrust::permutation_iterator<ElementIterator,IndexIterator> upar0_right(u.begin(), genPAR(2));

	    thrust::permutation_iterator<ElementIterator,IndexIterator> uimpar0_left(u.begin(), genIMPAR(0));
	    thrust::permutation_iterator<ElementIterator,IndexIterator> uimpar0(u.begin(), genIMPAR(1));
	    thrust::permutation_iterator<ElementIterator,IndexIterator> uimpar0_right(u.begin(), genIMPAR(2));

		transform(
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(0),uimpar0_left,upar0,uimpar0
			)),
			make_zip_iterator(make_tuple(
			make_counting_iterator<long>(L),uimpar0_left+L,upar0+L,uimpar1
			)),
			upar0,
			vmctor(n,f0,T0,dt,u_raw_ptr)
		);
		timer_euler_elapsed+=t.tac();
*/
		#endif

		// algunas propiedades de interés, calculadas cada TPROP
		//#ifndef LOGPRINT
		//if(n%TPROP==0){	
		//#else

		if(n%TCHECKBIG==0){	
			//thrust::pair<REAL *, REAL *> result = thrust::minmax_element(u_it0,u_it1);
			REAL init = u[1];
			REAL min = thrust::reduce(u_it0,u_it1,init,thrust::minimum<REAL>());
			std::cout << "min displacement= " << min << std::endl;
			if(min>UMAX){
				using namespace thrust::placeholders;
				thrust::transform(u_it0,u_it1,u_it0,_1-UMAX);
				std::cout << "shifting u" << std::endl;
			};
		}
		#ifdef DEBUG			
		std::cout << "checkbig passed" << std::endl;
		#endif

		#ifndef TPROP
		if(n%mtprop==0){
			//adapt_time_step();
			count++;if(count==10){count=0;mtprop*=10;}
		#else
		if(n%TPROP==0){
		#endif	
		//#endif
			t.tic();
			
			/* TODO:
			   usando algoritmos REDUCE 
			   [ http://thrust.github.io/doc/group__reductions.html#gacf5a4b246454d2aa0d91cda1bb93d0c2 ]
			   calcule la velocidad media de la interface
			   y la posición del centro de masa de la interface 
			   HINT:
			   REAL velocity = reduce(....)/L; //center of mass velocity
			   REAL center_of_mass = reduce(....)/L; // center of mass position
			*/
			// SOLUCION:			
			REAL velocity = reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L); //center of mass velocity
			center_of_mass = reduce(u_it0,u_it1,REAL(0.0))/REAL(L); // center of mass position

			#ifdef DEBUG			
			std::cout << "velocity and center of mass..." << std::endl;	
			#endif


			/* TODO: 
			   usando el algoritmo TRANSFORM_REDUCE, 
	[ http://thrust.github.io/doc/group__transformed__reductions.html#ga087a5af8cb83647590c75ee5e990ef66 ]
			   el functor "roughtor" arriba definido, 
			   y la posición del centro de masa "ucm" calculada en el TODO anterior, 
			   calcule la rugosidad (mean squared width) de la interface:
			   roughness := Sum_X [u(X)-ucm]^2 /L
			   HINT:
			   REAL roughness = transform_reduce(...,...,roughtor(center_of_mass),0.0,thrust::plus<REAL>());
			*/
			// SOLUCION:	
			REAL roughness = transform_reduce
			(
				u_it0, u_it1,
                                roughtor(center_of_mass),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);
			// solución alternativa
  			/*REAL roughness = reduce
			(
			make_transform_iterator(u_it0, roughtor(center_of_mass)),
			make_transform_iterator(u_it1, roughtor(center_of_mass)),
			REAL(0.0)
			);
			*/
			//std::cout << "roughness done..." << std::endl;	

			timer_props_elapsed+=t.tac();
	
			/* TODO: calcule la posicion de la particula con maxima velocidad */
			//SOLUCION:
			//device_vector<REAL>::iterator result = 
			int posmax=int(thrust::max_element(Ftot_it0,Ftot_it1)-Ftot_it0);			
			//std::cout << "posmax done..." << std::endl;	

			/* TODO: descomentar para que imprima la velocidad media, centro de masa, y rugosidad 
			   calculadas en los otros "TODOes", en el file "someprops.dat" */	
			// SOLUCION:
			propsout << n << " " << velocity << " " << center_of_mass << " " 
					 << roughness << " " << posmax << " " << T0 << " " << f0 << " " << uparabola 
					 << std::endl;
			propsout.flush();
			//std::cout << "propiedades globales impresas" << std::endl;	
			
			/* TODO:
			 Descomente -DPRINTCONFS en el Makefile, y recompile, para que imprima la posición 
			 y velocidad de 128 (o lo que quiera) particulas de la interface de tamanio L (una de cada L/128) 
			 en pantalla (descomente con cuidado, que imprime mucho!. Solo para hacer un "intuitive debugging")
			*/
			#ifdef PRINTCONFS
			print_configuration(u,Ftot,velocity,MAXPOINTSDISPLAY,n);
			#endif

			#ifdef FOURIER
			transform_fourier();
			#ifdef DEBUG
			std::cout << "transform fourier" << std::endl;			
			#endif
			print_fourier(n);
			#ifdef DEBUG
			std::cout << "print fourier" << std::endl;			
			#endif
			#endif
		}
		
	};

	void print_timmings(){
		// resultados del timming
		double total_time = (timer_fuerzas_elapsed+timer_euler_elapsed+timer_props_elapsed); 

		std::cout << "L= " << L << " TRUN= " << TRUN << std::endl; 

		std::cout << "Forces calculation -> " << 1e3 * timer_fuerzas_elapsed << " miliseconds (" 
	  	<< int(timer_fuerzas_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Euler step -> " << 1e3 * timer_euler_elapsed << " miliseconds (" 
		  << int(timer_euler_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Properties -> " << 1e3 * timer_props_elapsed << " miliseconds (" 
		  << int(timer_props_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "Total -> " << 1e3 * total_time << " miliseconds (100%)" << std::endl;
	};

	void dynamics(long trun)
	{
		for(unsigned long n=0;n<trun ;n++)
		{

			#ifdef RESTART
			if(tiempo%RESTART==0){
				restart();
			}
			#endif
			step();		

			//if(n%TZEROS==0) nzerosout << get_zeros() << std::endl;
		}
		//print_timmings();
	};


	REAL dynamics(long trun, REAL drun)
	{
		center_of_mass  = reduce(u_it0,u_it1,REAL(0.0))/REAL(L); // center of mass position
		REAL cm0,cm1;

		#ifdef VMINSTOP
		REAL cm2,cm3;
		cm2 = center_of_mass;
		#endif

		cm1 = cm0 = center_of_mass;
		bool c1,c2, c3=1;	

		REAL averagevelocity=0.0;	
		averagevelocity = reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L);
		
		for(unsigned long n=0; (c1=(n<trun)) && (c2=(fabs(cm1-cm0)<drun));n++)
		{
			#ifdef KINKDETECTION			
			#endif
	
			#ifdef RESTART
			if(tiempo%RESTART==0){
				restart();
			}
			#endif

			step();	
			cm1=reduce(u_it0,u_it1,REAL(0.0))/REAL(L); // compares with the initial t=0

			#ifdef VMINSTOP
			if(n%VMINSTOP==0){
				averagevelocity = 
				reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L);

				REAL maxabsvelocity = 
				transform_reduce(Ftot_it0,Ftot_it1,absolute_value<REAL>(averagevelocity),
				REAL(0.0),thrust::maximum<REAL>());

				REAL minvelocity = 
				reduce(Ftot_it0,Ftot_it1, REAL(1.0),thrust::minimum<REAL>());

				cm3=reduce(u_it0,u_it1,REAL(0.0))/REAL(L); //compares with the t-VMINSTOP

				std::cout << "maximal absolute velocity " << maxabsvelocity << std::endl; 
				std::cout << "average velocity " << averagevelocity << std::endl; 
				std::cout << "minimal velocity " << minvelocity << std::endl; 
				std::cout << "cm relatice change % = " <<  (fabs(cm3 - cm2)/(cm2)) << std::endl; 

				cm2=cm3;

				if(maxabsvelocity<SMALLVELOCITY) {
					std::cout << "velocidad minima " << SMALLVELOCITY << " alcanzada" << std::endl; 
					break;
				}
			}
			#endif	
			#ifdef MINAVERAGEVELOCITY
			if(n%10000==0) c3=(averagevelocity>MINAVERAGEVELOCITY);
			#endif 
		}
		#ifdef DEBUG
		if(!c1) std::cout << "STOP: n>=trun" << std::endl;
		if(!c2) std::cout << "STOP: fabs(cm1-cm0)>=drun" 
		<< ", cm1=" << cm1 << ", cm0=" << cm0 << std::endl;			 
		if(!c3) std::cout << "STOP: averagevelocity<MINAVERAGEVELOCITY" << std::endl;			 
		#endif
		//print_timmings();
		return averagevelocity;
	};

	device_vector<REAL>::iterator posicion_begin(){
		return u_it0;
	};	
	device_vector<REAL>::iterator posicion_end(){
		return u_it1;
	};		
	REAL get_center_of_mass_vel(){ 
		return reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L); 
	};
	device_vector<REAL>::iterator velocidad_begin(){
		return Ftot_it0;
	};	
	device_vector<REAL>::iterator velocidad_end(){
		return Ftot_it1;
	};	
	
	REAL get_center_of_mass(){ 
		return reduce(u_it0,u_it1,REAL(0.0))/REAL(L); 
	};


	int get_zeros(){
		REAL cm=get_center_of_mass();
		thrust::device_vector<int> zeros(L);	
		thrust::device_vector<int>::iterator result= thrust::copy_if(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(L),
			zeros.begin(),is_zeros(cm,u_raw_ptr)
		);
		int n=int(result-zeros.begin());
		int signoinicial=(u[1+zeros[0]]>0)?(1):(-1);

		/*
		thrust::host_vector<int> densityzeros(129,0);	
		for(int i=0;i<n;i++){
			int j=int(zeros[i]*128.0/L);
			densityzeros[j]++; 
		}
		for(int i=0;i<128;i++)
		zerosout << densityzeros[i] << std::endl;
		if(n>0) zerosout << std::endl << std::endl;
		*/

		for(int i=0;i<n;i++){
			zerosout << zeros[i] << " " << signoinicial << std::endl;
			signoinicial*=-1;
		}
		if(n>0) zerosout << std::endl << std::endl;

		//return number_of_zeros;*/
		return n;
	};


	REAL get_roughness(REAL cm){
		return  transform_reduce
			(
				u_it0, u_it1,
                                roughtor(cm),
                                REAL(0.0),
                                thrust::plus<REAL>()
			)/REAL(L);
	};
	REAL get_maximum_velocity(){
		return  reduce
			(
				Ftot_it0, Ftot_it1,
                                REAL(0.0),
                                thrust::maximum<REAL>()
			)/REAL(L);
	};
	/*void adapt_time_step(){
		dt=0.1/get_maximum_velocity();
		if(dt>Dt) dt=Dt;
		std::cout << "dt=" << dt << std::endl;
	};*/

	void increment_f0(REAL df){
		f0+=df;
		//if(f0<0) f0=0.0;
	};
	void increment_T0(REAL dT0){
		T0+=dT0;
		if(T0<0.0f) T0=0.0f;
	};
	REAL get_f0(){
		return f0;
	}
	REAL get_T0(){
		return T0;
	}	
	void set_T0(REAL T1){
		T0=T1;
	}	
	void set_f0(REAL f1){
		f0=f1;
	}	
	void set_f0ac(REAL f1){
		f0ac=f1;
	}	
	void set_tau0(int tau){
		tau0=tau;
	}	
	void set_temp(REAL _T0){
		T0=_T0;
	}	
	REAL get_velocity(){ 
		REAL vel=reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L); //center of mass velocity
		//smooth_velocity+=vel;
		return vel;
	}

	REAL get_velocity_fluctuations(){ 
		REAL vel=reduce(Ftot_it0,Ftot_it1,REAL(0.0))/REAL(L); //center of mass velocity
		//smooth_velocity+=vel;

		using namespace thrust::placeholders;
		REAL velfluc=transform_reduce(Ftot_it0,Ftot_it1,(_1-vel)*(_1-vel),REAL(0.0),thrust::plus<REAL>())/REAL(L); //center of mass velocity
		//smooth_velocity+=vel;
		return velfluc;
	}
	
	#ifdef FOURIER
	void transform_fourier(){
		u[0]=u[L];u[L+1]=u[1];
		REAL *u_input=(REAL *) thrust::raw_pointer_cast(u.data());
		COMPLEX *u_output=(COMPLEX *) thrust::raw_pointer_cast(&u_fourier[0]);

		#ifndef DOUBLEPRECISION
		CUFFT_SAFE_CALL(cufftExecR2C(plan_d2z, u_input, u_output));
		#else
		CUFFT_SAFE_CALL(cufftExecD2Z(plan_d2z, u_input, u_output));
		#endif
		//cufftExecD2Z(plan_d2z, u_input, u_output);
		//thrust::transform(u_fourier.begin(),u_fourier.end(),cumu_fourier.begin(),cumu_fourier.begin(),suma());
	};
	void anti_transform_fourier(){
		COMPLEX *u_input=thrust::raw_pointer_cast(&u_fourier[0]);
		COMPLEX zero; zero.x=zero.y=0.0; u_fourier[0]=zero;
		thrust::transform(
			u_fourier.begin(),u_fourier.end(),
			frequencies_fft.begin(),u_fourier.begin(),
			longrange_functor()
		);


		REAL *u_output=thrust::raw_pointer_cast(&F_long_range[0]);

		#ifndef DOUBLEPRECISION
		CUFFT_SAFE_CALL(cufftExecC2R(plan_z2d, u_input, u_output));
		#else
		CUFFT_SAFE_CALL(cufftExecZ2D(plan_z2d, u_input, u_output));
		#endif

		F_long_range[L]=F_long_range[0];
	};

	void print_fourier(int tt){
		#ifdef DEBUG
		std::cout << "printing fourier" << std::endl;			
		#endif

		int numberofqs=(L>10000)?(10000):(L/2+1);
		thrust::copy(u_fourier.begin(),u_fourier.begin()+numberofqs,h_u_fourier.begin());
		for(int i=0;i<numberofqs;i++){
			COMPLEX c=h_u_fourier[i];
			fou << (2.f*M_PI*i/L) << " " << c.x*c.x+c.y*c.y << " " << tt << std::endl;
		}
		fou << "\n\n";
	};
	void print_fourier(double anim, std::ofstream &fouanim){
		int numberofqs=(L>10000)?(10000):(L/2+1);
		thrust::copy(u_fourier.begin(),u_fourier.begin()+numberofqs,h_u_fourier.begin());
		for(int i=0;i<numberofqs;i++){
			COMPLEX c=h_u_fourier[i];
			fouanim << (2.f*M_PI*i/L) << " " << c.x*c.x+c.y*c.y << " " << anim << " " << f0 << " " << T0 << std::endl;
		}
		fouanim << "\n\n";
	};
	#endif

	void print_displacements(double anim, std::ofstream &fouanim){
		int maxn=(L>10000)?(10000):(L);
		for(int i=0;i<maxn;i++){
			fouanim << i << " " << u[i] << " " << anim << " " << f0 << " " << T0 << std::endl;
		}
		fouanim << "\n\n";
	};

	void print_forces(std::ofstream &fout, double tt)
	{
		int NFtot=Ftot.size();
		thrust::host_vector<REAL> Ftot_h(Ftot);
		thrust::host_vector<int> seq(NFtot);
		thrust::sequence(seq.begin(), seq.end());
		thrust::sort_by_key(Ftot_h.begin(), Ftot_h.end(),seq.begin());
		//std::cout << "printing configuration" << std::endl;
		for(int i=0;i<100;i++)
		{
			fout << Ftot_h[i] << " " << seq[i] << " " << tt << std::endl;
		}
		fout << "\n\n";
		//std::cout << "done" << std::endl;
	}

	int print_solitons(double anim, std::ofstream &fouanim){

		auto res = thrust::copy_if(
		thrust::make_counting_iterator(0), thrust::make_counting_iterator(L), 
		solitonindex.begin(), is_soliton(u_raw_ptr));
		
		int Nres = res-solitonindex.begin(); 
		
		thrust::host_vector<int> posv(Nres);
		thrust::host_vector<int> sigv(Nres);
			
		bool signo;
		int pos;		
		for(int i=0;i<Nres;i++){
			signo=(solitonindex[i] > 0);
			pos=solitonindex[i]*(2*signo-1); 
			posv[i]=pos;
			sigv[i]=signo;
		}
		//thrust::sort_by_key(posv.begin(), posv.end(),sigv.begin());
		for(int i=0;i<Nres;i++){
			fouanim << posv[i] << " " << sigv[i] << " " << anim << std::endl;
		}
		if(Nres>0) fouanim << "\n\n";

		return Nres;//number of solitons
	};

	int count_solitons()
	{
		auto res = thrust::copy_if(
		thrust::make_counting_iterator(0), thrust::make_counting_iterator(L), 
		solitonindex.begin(), is_soliton(u_raw_ptr));
		
		int Nres = res-solitonindex.begin(); 	
		return Nres;
	}



    ///////////////////// PRIVATE MEMBERS ////////////////////////
	private:
	unsigned long long tiempo, mtprop, count;
	REAL f0,f0ac;	
	REAL T0;
	REAL dt;
	REAL uparabola;	
	REAL center_of_mass;
	unsigned int tau0;
	REAL smooth_velocity;
	REAL physical_time;

	// posiciones de los monómeros: 
	// Notar que alocamos dos elementos de mas para usarlos como "halo"
	// esto nos permite fijar las condiciones de borde facilmente, por ejemplo periódicas: u[0]=u[L]; u[L+1]=u[1];
	device_vector<REAL> u;
	device_vector<REAL>::iterator u_it0;
	device_vector<REAL>::iterator u_it1;
	REAL * u_raw_ptr;

	// container de fuerza total: 
	device_vector<REAL> Ftot; 
	// el rango de interés definido por los iteradores 
	device_vector<REAL>::iterator Ftot_it0;
	device_vector<REAL>::iterator Ftot_it1;

	device_vector<REAL> F_long_range;


	device_vector<unsigned long> countpin; 
	device_vector<unsigned long>::iterator countpin_it0;
	device_vector<unsigned long>::iterator countpin_it1;
	unsigned long * countpin_raw_ptr;
	device_vector<REAL> upin; 
	device_vector<REAL>::iterator upin_it0;
	device_vector<REAL>::iterator upin_it1;
	REAL * upin_raw_ptr;

	thrust::device_vector<int> solitonindex;

	// file para guardar algunas propiedades dependientes del tiempo
	#ifndef OMP
	std::ofstream propsout;
	std::ofstream zerosout;
	std::ofstream nzerosout;
	#else
	std::ofstream propsout;
	std::ofstream zerosout;
	std::ofstream nzerosout;
	#endif
	std::ofstream log; // logfile


	#ifdef FOURIER
	int Ncomp;	
	thrust::device_vector<COMPLEX> u_fourier;
	thrust::device_vector<COMPLEX> cumu_fourier;
	thrust::host_vector<COMPLEX> h_u_fourier;
	COMPLEX *u_fourier_raw; 
	cufftHandle plan_d2z;

	//thrust::device_vector<COMPLEX> F_long_range_fourier;
	thrust::device_vector<REAL> frequencies_fft;
	cufftHandle plan_z2d;
	#endif
};



/////////////////////////////////////////////////////////////////////////////
// sin opengl el main seria simplemente algo asi...

/*#include "qew_minimal_solucion.h"
int main(){
	Cuerda C;
	C.init();
	C.dynamics(TRUN);
	C.print_timmings();
	return 0;
}*/

