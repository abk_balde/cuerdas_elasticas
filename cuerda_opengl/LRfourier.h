// functor usado para calcular la rugosidad 
struct longrange_functor
{
    __device__
    COMPLEX operator()(COMPLEX c, REAL r)
    {	
    	COMPLEX res;
    	float f=-powf(fabs(r),SIGMA);
    	res.x = c.x*f;
    	res.y = c.y*f;
		return res;
    }
};	

class long_range{
	public:
	.... plan_d2z;
	

}


void transform_fourier(thrust::device<> REAL *u_input){
		u[0]=u[L];u[L+1]=u[1];
		REAL *u_input=(REAL *) thrust::raw_pointer_cast(u.data());
		COMPLEX *u_output=(COMPLEX *) thrust::raw_pointer_cast(&u_fourier[0]);

		#ifndef DOUBLEPRECISION
		CUFFT_SAFE_CALL(cufftExecR2C(plan_d2z, u_input, u_output));
		#else
		CUFFT_SAFE_CALL(cufftExecD2Z(plan_d2z, u_input, u_output));
		#endif
		//cufftExecD2Z(plan_d2z, u_input, u_output);
		//thrust::transform(u_fourier.begin(),u_fourier.end(),cumu_fourier.begin(),cumu_fourier.begin(),suma());
};

void anti_transform_fourier(){
		COMPLEX *u_input=thrust::raw_pointer_cast(&u_fourier[0]);
		COMPLEX zero; zero.x=zero.y=0.0; u_fourier[0]=zero;
		thrust::transform(
			u_fourier.begin(),u_fourier.end(),
			frequencies_fft.begin(),u_fourier.begin(),
			longrange_functor()
		);


		REAL *u_output=thrust::raw_pointer_cast(&F_long_range[0]);

		#ifndef DOUBLEPRECISION
		CUFFT_SAFE_CALL(cufftExecC2R(plan_z2d, u_input, u_output));
		#else
		CUFFT_SAFE_CALL(cufftExecZ2D(plan_z2d, u_input, u_output));
		#endif

		F_long_range[L]=F_long_range[0];
};
