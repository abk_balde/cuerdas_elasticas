res
set title '1DRP L=132768, error bars = sqrt[<v^2(x,f)>_x - v^2(f)]/sqrt(L)'
set xla 'f'
set yla 'v(f)'

set key left

set multi lay 1,2

set log y
plot [][0.005:] for [file in system("ls vvsf_L132768cont_T0.0*")] file u 1:2:(temp=column(4),sqrt($5-$2**2)/sqrt(132768)) w error t sprintf("T=%f",temp)
unset log y

set xla 'f'
set yla 'v(f)/T**0.35'
#set log y 
#set logs
fc=0.73;
#psi=0.2
beta=0.5
plot [0.725:0.735][0.005:] for [file in system("ls vvsf_L132768cont_T0.0*")] file u (temp=column(4),$1):($2/temp**psi):(sqrt($5-$2**2)/sqrt(132768)) w errorl t sprintf("T=%f",temp)
#unset logs


unset multi
