function histofirstkink(infile,outfile)

[infile, " -> ",outfile]

#infiles=["./first_soliton_L132766_T0.00000001_0.dat"; 
#"./first_soliton_L65536_T0.00000001_0.dat";
#"./first_soliton_L32768_T0.00000001_0.dat";
#"./first_soliton_L8192_T0.00000001_0.dat";
#"./first_soliton.dat"];

#outfiles=["histoL132766_T0.00000001_0.dat"; 
#"histoL65536_T0.00000001_0.dat";
#"histoL32768_T0.00000001_0.dat";
#"histoL8192_T0.00000001_0.dat";
#"histoL4096_T0.00000001_0.dat"];

#for i=1:5

#data=load("-ascii", infiles(i,1:end));

data=load("-ascii", infile);

size(data)

#infiles(i,1:end)

#first_soliton=load("-ascii", "first_soliton.dat");
#first_soliton=load("-ascii","first_soliton_L132766_T0.00000001_0.dat");
#first_soliton=load("-ascii","first_soliton_L65536_T0.00000001_0.dat");
#first_soliton=load("-ascii","first_soliton_L32768_T0.00000001_0.dat");
#first_soliton=load("-ascii","first_soliton_L8192_T0.00000001_0.dat");

[nn,xx]=hist(data,50);
H=transpose(nn)/(sum(nn)*(xx(2)-xx(1))); 
bins=transpose(xx);
A=[bins(1:end),H(1:end)];

#plot(bins,H,"-o")
semilogy(bins,H,"-o")

#save "histoL132768T0.00000001.dat" A; 
#save "histoL65536T0.00000001.dat" A; 
#save "histoL32768T0.00000001.dat" A; 
#save "histoL8192T0.00000001.dat" A; 
#save "histoL4096T0.00000001.dat" A; 

#save outfiles(i,1:end) A; 


save("-ascii", outfile(1:end),"A");

#save outfile A; 

#endfor

endfunction

#Ejemplos de uso
#histofirstkink("first_soliton.dat","histoL256T0.00000001.dat")






