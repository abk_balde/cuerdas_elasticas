#include "string_zeros.h"

void sampling(int n)
{

	elastic_string Cuerda;		

	// choose here the desired times to sample
    #ifdef STEADYSTATESAMPLING
    std::cout << "steady state sampling" << std::endl;
    int mytimes[] = {0,1};
    #else
    std::cout << "non steady state sampling" << std::endl;
    //int mytimes[] = {0,1,2,3,4,5,10,20,30,40,50,100,200,300,400,500,1000,2000,3000,4000,5000,10000, 20000, 30000, 40000, 50000, 100000,200000, 300000, 400000, 500000, 1000000};
    int mytimes[] = {0,1,2,3,4,5,10,20,30,40,50,100,200,300,400,500,1000,2000,3000,4000,5000,10000, 20000, 30000, 40000, 50000, 100000};
    #endif
    std::vector<int> tiempos (mytimes, mytimes + sizeof(mytimes) / sizeof(int) );
    std::cout << tiempos.size() << " tiempos" << std::endl;
     
    
    // list of properties we will calculate

    // average number of zeros vs time
    std::vector<int> numberZeros(tiempos.size(),0);

    // average width of interfaces vs time
    std::vector<float> width(tiempos.size(),0);

    // average structure factor vs time
    int stridesofq=(SIZE/2+1);
    thrust::host_vector<float> sofq(stridesofq*tiempos.size(),0.0);

    // average interval distribution vs time
    int stridehisto=SIZE;
    thrust::host_vector<int> intervalhisto(stridehisto*tiempos.size(),0.0);

    // average area distribution vs time
    //unsigned long stridehistoa=SIZE*SIZE;
    //thrust::host_vector<int> areahisto(stridehistoa*tiempos.size(),0.0);

    // average correlation vs time
    int stridecorr=SIZE;
	thrust::host_vector<REAL> intervalcorrel(stridecorr*tiempos.size(),0.0);;

	#ifdef IMPRIMIRCONFIGURACIONES
	std::ofstream configs("configuraciones.dat"); 
	#endif


	std::ofstream nzerosout("nzeros.dat"); 

    // loop over samples
    for(int nsamples=0;nsamples<n;nsamples++)
    {
		std::cout << "sample " << nsamples << std::endl;

    	// initial condition
    	Cuerda.make_flat();

		// loop over times (only "one time" for steady state sampling)
		int t=0;
		for(int j=1;j<tiempos.size();j++)
		{

			int nsteps=tiempos[j]-tiempos[j-1];
			//std::cout << "increment = " << tiempos[j]-tiempos[j-1] << " t=" << t << std::endl; // a check...

			#ifdef STEADYSTATESAMPLING
			Cuerda.steady_state_sampling(nsamples);
			#else
			// evolve nsteps
			Cuerda.dynamics(nsteps, t, nsamples);
			t+=nsteps;
			#endif

			// slow: involves D->H copy... 
			#ifdef GETSTRUCTUREFACTOR
			Cuerda.get_structure_factor(sofq, (j-1)*stridesofq);
			#endif

			// get string in real space
			Cuerda.antitransformar();

			#ifdef IMPRIMIRCONFIGURACIONES
			Cuerda.imprimir_real(configs);			
			#endif	


			// get interval histogram
			#ifdef COMPUTEINTERVALHISTOGRAM
			Cuerda.get_interval_histogram(intervalhisto, (j-1)*stridehisto);		
			#endif

			// get area histogram
			//Cuerda.get_areas_histogram(areahisto, (j-1)*stridehistoa);		

			// get interval histogram
			#ifdef COMPUTECORRELATIONINTERVALS
			Cuerda.get_correlacion_intervalos(intervalcorrel, (j-1)*stridecorr);		
			#endif

			// get all zeros
			int instnzeros=Cuerda.get_zerosN();
			numberZeros[j]+=instnzeros;

			// get Sum_y [u(y)]^2 
			float instwidth=Cuerda.roughness(); 
			width[j]+=instwidth;

			std::cout << "at t=" << t << " "; //check
			std::cout << "zeros=" << instnzeros << " width=" << instwidth << " " << std::endl; //check
			
			nzerosout << instnzeros << std::endl;

		}
		std::cout << "\n\n";	

		// print partial averaged results each 100 samples (just in case we stop the program before it finishes)
		if((nsamples+1)%100==0 || nsamples==n-1)
		{
		
		    // global quantitities vs time ---------------------------------------
			std::ofstream scalarsvstimeout("scalars_vs_time.dat"); 
		        scalarsvstimeout << "#time #zeros #width" << std::endl;
			for(int j=1;j<tiempos.size();j++){
					//std::cout << "printing partial averages" << tiempos[j] << std::endl;
					scalarsvstimeout << tiempos[j] << 
					" " << float(numberZeros[j]*1.0/nsamples) << 
					" " << float(width[j]*1.0/nsamples) <<
					std::endl;
			}

			// structure factor vs time --------------------------------------------
			std::ofstream sofqvstimeout("structure_factor_vs_time.dat"); 
		    sofqvstimeout 
		    << "#plot it like this:\n #plot for[i=1:" << tiempos.size()-1 
		    << "] 'structure_factor_vs_time.dat' u i w lp t columnhead" 
		    << "\n #first row are the times\n" << std::endl;
			for(int j=1;j<tiempos.size();j++){
			    sofqvstimeout << tiempos[j] << " ";
			} sofqvstimeout << std::endl;

			for(int k=0;k<stridesofq;k++){
				for(int j=1;j<tiempos.size();j++){
					sofqvstimeout << sofq[(j-1)*stridesofq+k]*1.0/nsamples << " " ;
				}
				sofqvstimeout << std::endl;
			}

			// interval histogram vs time --------------------------------------------
			std::ofstream inthistovstimeout("interval_histogram_vs_time.dat"); 
		    inthistovstimeout 
		    << "#plot it like this:\n #plot for[i=1:" << tiempos.size()-1 
		    << "] 'interval_histogram_vs_time.dat' u i w lp t columnhead" 
		    << "\n #first row are the times\n" << std::endl;
			for(int j=1;j<tiempos.size();j++){
			    inthistovstimeout << tiempos[j] << " ";
			} inthistovstimeout << std::endl;

			for(int k=0;k<stridehisto;k++){
				for(int j=1;j<tiempos.size();j++){
					inthistovstimeout << intervalhisto[(j-1)*stridehisto+k]*1.0/nsamples << " " ;
				}
				inthistovstimeout << std::endl;
			}

			// area histogram vs time --------------------------------------------
			/*std::ofstream inthistoavstimeout("area_histogram_vs_time.dat"); 
		    inthistoavstimeout 
		    << "#plot it like this:\n #plot for[i=1:" << tiempos.size()-1 
		    << "] 'area_histogram_vs_time.dat' u i w lp t columnhead" 
		    << "\n #first row are the times\n" << std::endl;
			for(int j=1;j<tiempos.size();j++){
			    inthistoavstimeout << tiempos[j] << " ";
			} inthistoavstimeout << std::endl;

			for(int k=0;k<stridehistoa;k++){
				for(int j=1;j<tiempos.size();j++){
					inthistoavstimeout << areahisto[(j-1)*stridehistoa+k]*1.0/nsamples << " " ;
				}
				inthistoavstimeout << std::endl;
			}*/


			// interval correlation vs time --------------------------------------------
			std::ofstream corrvstimeout("interval_correlation_vs_time.dat"); 
		    corrvstimeout 
		    << "#plot it like this:\n #plot for[i=1:" << tiempos.size()-1 
		    << "] 'interval_correlation_vs_time.dat' u i w lp t columnhead" 
		    << "\n #first row are the times\n" << std::endl;
			for(int j=1;j<tiempos.size();j++){
			    corrvstimeout << tiempos[j] << " ";
			} corrvstimeout << std::endl;

			for(int k=0;k<stridecorr;k++){
				for(int j=1;j<tiempos.size();j++){
					corrvstimeout << intervalcorrel[(j-1)*stridecorr+k]*1.0/nsamples << " " ;
				}
				corrvstimeout << std::endl;
			}
		}

    }


}


int main(int argc, char* argv[]){
	int GPUNUMBER;
	if(argc==2){
		GPUNUMBER=atoi(argv[1]);
	}
	else GPUNUMBER=0;

	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop,GPUNUMBER);
	printf("Device Number %d\n",GPUNUMBER);
	printf("Device Number %s\n",prop.name);
	cudaSetDevice(GPUNUMBER);


	cout << "# Tamanio del array = " << SIZE << endl;
	cout << "# Tiempo simulacion = " << TRUN << endl;
	cout << "# Temperatura = " << TEMP << endl;
	cout << "# Paso de tiempo = " << TIMESTEP << endl;
	cout << "# Zeta = " << ZETA << endl;
	cout << "# ACOEFFICIENT = " << ACOEFFICIENT << endl;
	cout << "# REALIZATION = " << REALIZATION << endl;
	cout << "# NQCUT = " << NQCUT << "= (SIZE/2) - " << (SIZE/2)-NQCUT << endl; 

	#ifdef EXACTQ
	cout << "# EXACTQ ON" << endl;
	#endif	
	#ifdef STEADYSTATESAMPLING
	cout << "# STEADYSTATESAMPLING ON" << endl;
	#endif
	#ifdef IMPRIMIR
	cout << "# IMPRIMIR ON" << endl;
	#endif	
	#ifdef INITSTEADYSTATE
	cout << "# INITSTEADYSTATE" << endl;
	#endif
	#ifdef CORRECTINTERVAL
	cout << "# CORRECTINTERVAL" << endl;
	#endif	
	cout << "# LARGEINTERVAL = " << LARGEINTERVAL << endl;
	cout << "# MASS2 = " << MASS2 << endl;
	#ifdef FLUCTUATINGCENTEROFMASS
	cout << "# FLUCTUATINGCENTEROFMASS" << endl;
	#endif
	#ifdef FORCESPARREANDERSEN
	cout << "# FORCESPARREANDERSEN" << endl;
	#endif


	sampling(REALIZATION);
}



