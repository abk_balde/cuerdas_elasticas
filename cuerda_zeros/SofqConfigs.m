1;

#################################################################
# lee un file de "Samples" configuraciones "infile" de tamaño "L"
# y le calcula el factor de estructura promedio

function Sofq(infile,Samples, L)

#A=load("-ascii","configuraciones.dat");
A=load("-ascii",infile);

S=zeros(L,1); 

for n=1:Samples 
	i1=L*n+1; 
	i2=i1+L-1; 
	#plot(A(i1:i2),"-o"); 
	#sleep(.1); 
	S=S+abs(fft(A(i1:i2))).^2/L; 
endfor; 

S=S/Samples;

q=0:L-1; 
loglog(q,S); 
#hold on; 
#loglog(q,q.^(-2)); 

save("-ascii","Sofq.dat", "S")

#hold off
endfunction

#################################################################
# Genera una señal gausiana de tamaño "L" con los primeros 
# con roughness "zeta" usando los primeros "Nmodos" modos   
# de Fourier

function signal=generate_random_paths(N,L,zeta)

#zero mode is zero
q=[1:N]; 
sigmaq=q.^(-(1.+2.*zeta)/2.);
aq=sigmaq.*stdnormal_rnd(1,N);
bq=sigmaq.*stdnormal_rnd(1,N);

signal=zeros(1,L);

# se puede vectorizar para mas eficiencia?
for j=1:L
	fac = (j-1)*2*pi/L;
	signal(j)=sum(aq.*cos(q.*fac)+bq.*sin(q.*fac));
endfor

endfunction

##############################################################
# genera "Samples" samples de configuraciones 
# de tamaño "L" y exponente de rugosidad "zeta", 
# usando los primeros "Nmodes" modos

function AverageSofq=Sofqtest(Samples, L, Nmodes, zeta)

	S=zeros(L,1); 

	for n=1:Samples
		u=generate_random_paths(Nmodes,L,zeta);
		#plot(u);  
		S=S+abs(fft(u')).^2/L; 
	endfor

	#q=[0:L-1]';
	#AverageSofq=[q,S/Samples];

	AverageSofq=S/Samples;

endfunction

##################################


# por ejemplo, generar y grabar
# S=Sofqtest(200,256,256,0.4); save "zeta0.4.dat" S
# etc...
# para despues plotearlo en gnuplot
# set logs; plot [:128] for[zeta in "0.2 0.4 0.6 0.8 1.0 1.2"] sprintf("zeta%s.dat",zeta) u 0:1 w p t 'zeta='.zeta, 
# for[zeta in "0.2 0.4 0.6 0.8 1.0 1.2"] 120./x**(1+2*zeta) t 'Zeta='.zeta


