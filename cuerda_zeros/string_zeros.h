/*
=============================================================================================
A. B. Kolton 26/11/2014
Ultima modificacion 24/03/2016
=============================================================================================
*/


/* algunos headers de la libreria thrust */
// https://github.com/thrust/thrust/wiki/Documentation
#include <thrust/transform.h>
#include <thrust/for_each.h>
#include <thrust/tuple.h>
#include <thrust/transform_reduce.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/copy.h>
#include <thrust/sort.h>
#include "histograma.h"

#include <thrust/device_ptr.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>


#include <cstdlib> // for rand() and srand()
/*
#include <ctime> // for time()
*/

#include <iostream>
#include <fstream>
#include "cutil.h"	// CUDA_SAFE_CALL, CUT_CHECK_ERROR
#include "gpu_timer.h"
// CUFFT include http://docs.nvidia.com/cuda/cufft/index.html
#include <cufft.h>

/*
#include <cmath>
#include <iostream>
#include <fstream>
#include "cpu_timer.h"
#include "openmp_timer.h"
#include <omp.h>
#include <complex>
#include<fftw3.h>
*/

using namespace std;

//#define DOUBLE_PRECISION

#ifdef DOUBLE_PRECISION
typedef cufftDoubleReal REAL;
typedef cufftDoubleComplex COMPLEX;
#else
typedef cufftReal REAL;
typedef cufftComplex COMPLEX;
#endif

/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox4x32 RNG; // un counter-based RNG

// esta funcion retorna un numero aleatorio complejo  
// las dos componentes son independientes y gausianas con media 0 y variancia 1, 
// Se usa solo un numero generado por philox4x32
__device__
COMPLEX box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	float u1x = u01_open_closed_32_53(r_philox[0]);
  	float u2x = u01_open_closed_32_53(r_philox[1]);
	float u1y = u01_open_closed_32_53(r_philox[2]);
  	float u2y = u01_open_closed_32_53(r_philox[3]);

  	float rx = sqrtf( -2.0*logf(u1x) );
  	float thetax = 2.0*M_PI*u2x;

  	float ry = sqrtf( -2.0*logf(u1y) );
  	float thetay = 2.0*M_PI*u2y;

	COMPLEX result;
	result.x = rx*sinf(thetax);
	result.y = ry*sinf(thetay);
	
	return result;    			
}



#ifndef SEED
#define SEED		123456789 // una semilla global 
#endif

#ifndef STEADYSTATESAMPLING
#define NSTEPS		1000
#else
#define NSTEPS 		1
#endif

#ifndef TRUN
#define TRUN		1000000  // IMPORTANT: If time is modified change the names of the files at the bottom, please !!!
#endif

#ifndef TIMESTEP
#define TIMESTEP	0.01
#endif

#ifndef REALIZATION
#define REALIZATION	1
#endif

#ifndef SIZE
#define SIZE		1024
#endif

#ifndef TEMP 
#define TEMP		0.1
#endif

#ifndef ZETA
#define ZETA	0.5
#endif

#ifndef NQCUT
#define NQCUT 	SIZE/2
#endif

#ifndef ACOEFFICIENT
#define ACOEFFICIENT 	0
#endif

#ifndef MASS2
#define MASS2	(powf(2*M_PI/SIZE, 1.0+2.0*ZETA))
#endif

#ifndef LARGEINTERVAL
#define LARGEINTERVAL SIZE
#endif

#ifndef SMALLINTERVAL
#define SMALLINTERVAL 0
#endif

#ifndef MININT
#define MININT SIZE
#endif

#ifndef MAXINT
#define MAXINT SIZE
#endif


// FUNCTOR: Semiimplicit steps of the dynamics in Fourier Space
struct semi_implicit_steps
{
	COMPLEX *S;
	int nsteps;
	int tglobal;
	int seed;
	REAL fac;
	
	semi_implicit_steps(COMPLEX *_S, int _nsteps, int _tglobal, int _seed):S(_S),nsteps(_nsteps), tglobal(_tglobal), seed(_seed)
	{
		fac=sqrt(TEMP*TIMESTEP);
	};

	__device__ 
	COMPLEX operator()(int tid)
	{

		// keys and counters 
    		RNG philox; 	
    		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;

		// TODO:Elija k[0] tal que se genere una secuencia random "unica" para cada particula	
    		k[0]=tid; 

    		// TODO:Elija c[1] tal que se generen secuencias random distintas para cada corrida cambiando SEED	
		//c[1]=rand(); // set initial seed value to system clock
		//c[1]=rand();	
    		c[1]=seed;

		COMPLEX newvalue;
		newvalue.x = S[tid].x;
		newvalue.y = S[tid].y;
		
		#ifndef EXACTQ
		const REAL q=M_PI*tid*2.0/SIZE;
		#else
		const REAL q=sqrt(2.0-2.0*cos(M_PI*tid*2.0/SIZE));
		#endif

		const REAL q2=powf(q,1+2.0*ZETA); //+MASS2; porque estaba esto?
	
		for(int i=0;i<nsteps;i++){
			c[0]=tglobal+i; 

		      	r = philox(c, k); 

		      	// convierte r en un numero gaussiano N[0,1], usando la funcion box_muller
		      	// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
		      	COMPLEX NroGausiano=box_muller(r);

			// explicit
		      	//newvalue.x += -TIMESTEP*newvalue.x*q2+NroGausiano.x*fac;
		      	//newvalue.y += -TIMESTEP*newvalue.y*q2+NroGausiano.y*fac;

			// implicit
		      	newvalue.x = ((1.0-(ACOEFFICIENT*TIMESTEP*q2))*newvalue.x+NroGausiano.x*fac)/(1.0+((1.0-ACOEFFICIENT)*TIMESTEP*q2));
		      	newvalue.y = ((1.0-(ACOEFFICIENT*TIMESTEP*q2))*newvalue.x+NroGausiano.y*fac)/(1.0+((1.0-ACOEFFICIENT)*TIMESTEP*q2));
		}
		if(tid==0 || tid>NQCUT) newvalue.x=newvalue.y=0.0;
		return newvalue;
	}
};


// FUNTOR: Steady state sampling in Fourier Space
struct ss_sampling
{
	COMPLEX *S;
	int nsteps;
	int tglobal;
	REAL fac;
	
	ss_sampling(COMPLEX *_S, int _tglobal):S(_S), tglobal(_tglobal)
	{
		fac=sqrt(TEMP*0.5);
		nsteps=1;	
	};

	__device__ 
	COMPLEX operator()(int tid)
	{

		// keys and counters 
    		RNG philox; 	
    		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;

		// TODO:Elija k[0] tal que se genere una secuencia random "unica" para cada particula	
    		k[0]=tid; 

    		// TODO:Elija c[1] tal que se generen secuencias random distintas para cada corrida cambiando SEED	
		//srand(time(0)); // set initial seed value to system clock
		//c[1]=rand();	
    		c[1]=SEED;

		COMPLEX newvalue;
		newvalue.x = S[tid].x;
		newvalue.y = S[tid].y;
		
		#ifndef EXACTQ
		const REAL q=M_PI*tid*2.0/SIZE;
		#else
		const REAL q=sqrt(2.0-2.0*cos(M_PI*tid*2.0/SIZE));
		#endif

		const REAL q2=powf(q,1+2.0*ZETA); //+MASS2; porque estaba esto?
	
		for(int i=0;i<nsteps;i++){
			    c[0]=tglobal+i; 

		      	r = philox(c, k); 

		      	// convierte r en un numero gaussiano N[0,1], usando la funcion box_muller
		      	// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
		      	COMPLEX NroGausiano=box_muller(r);

			// explicit
		      	//newvalue.x += -TIMESTEP*newvalue.x*q2+NroGausiano.x*fac;
		      	//newvalue.y += -TIMESTEP*newvalue.y*q2+NroGausiano.y*fac;

			// implicit
		      	//newvalue.x = (newvalue.x+NroGausiano.x*fac)/(1.0+TIMESTEP*q2);
		      	//newvalue.y = (newvalue.y+NroGausiano.y*fac)/(1.0+TIMESTEP*q2);

			#ifndef MASS2 
		      		newvalue.x = (tid>0)?( (NroGausiano.x*fac)/sqrt(q2*(1+(0.5*(1.0-2.0*ACOEFFICIENT)*TIMESTEP*q2))) ):(0.0f);

					// sin series
 		      		#ifdef SINBC 
		      		newvalue.y = -newvalue.x;

					// cos series
		      		#elif defined(COSBC)
		      		newvalue.y = newvalue.x;

		      		// free series (translationally invariant)
		      		#else 
		      		newvalue.y = (tid>0)?( (NroGausiano.y*fac)/sqrt(q2*(1+(0.5*(1.0-2.0*ACOEFFICIENT)*TIMESTEP*q2)))):(0.0f);
		      		#endif
		      	#else
		      		newvalue.x = ( (NroGausiano.x*fac)/sqrt(q2*(1+0.5*(1.0-2.0*ACOEFFICIENT)*TIMESTEP*q2)));
		      		#ifdef SINBC
		      		newvalue.y = -newvalue.x;
		      		#elif defined(COSBC)
		      		newvalue.y = newvalue.x;
		      		#else 
		      		newvalue.y = ( (NroGausiano.y*fac)/sqrt(q2*(1+0.5*(1.0-2.0*ACOEFFICIENT)*TIMESTEP*q2)));
		      		#endif
		      	#endif
		}
		if(tid>NQCUT) newvalue.x=newvalue.y=0.0; // filtra altas frecuencias 

		#ifndef FLUCTUATINGCENTEROFMASS
		if(tid==0) newvalue.x=newvalue.y=0.0; // center of mass is at rest
		#endif

		return newvalue;
	}
};


// predicate para detectar zeros en la cuerda, de cualquier signo
struct is_zero
{
	REAL *u_ptr;
	is_zero(REAL * _ptr):u_ptr(_ptr){};
	__device__
	bool operator()(const int i)
	{
		REAL first_point= u_ptr[i];
		REAL second_point= u_ptr[(i+1)%SIZE];
		//REAL third_point= u_ptr[(i-1+SIZE)%SIZE];

		bool positive_zero=(first_point < 0.0)&&(second_point > 0.0);
		bool negative_zero=(first_point > 0.0)&&(second_point < 0.0);

		// diferente asignacion del zero segun signo de pared
		//bool negative_zero=(third_point > 0.0)&&(first_point < 0.0);

		return (positive_zero || negative_zero);
	}
}; 

struct is_positive
{
	REAL *u_ptr;
	is_positive(REAL * _ptr):u_ptr(_ptr){};
	__device__
	bool operator()(const int i)
	{
		return (u_ptr[i]>0.0f);
	}
}; 



struct resta
{
	REAL *u_ptr;
	int x;
	resta(REAL * _ptr, int _x):u_ptr(_ptr),x(_x){};
	__device__
	REAL operator()(const int i)
	{
		REAL first_point= u_ptr[i];
		REAL second_point= u_ptr[(i+x)%SIZE];
		

		return (first_point-second_point)*(first_point-second_point);
	}
}; 


__device__
int corregir_intervalo(int *zeros, REAL *u_ptr, int i, size_t nzeros){
		int zero0, zero1; 
		zero0 = zeros[i];
		zero1 = zeros[(i+1)%nzeros];


		#ifdef CORRECTINTERVAL
		REAL u0, u1, u0n, u1n;

		u0 = u_ptr[zero0];
		u0n = u_ptr[(zero0+1)%SIZE];

		u1 = u_ptr[zero1];
		u1n = u_ptr[(zero1+1)%SIZE];

		//int first_interval=  (fabs(u1)<fabs(u1n))?(zero1):((zero1+1)%SIZE) - (fabs(u0)<fabs(u0n))?(zero0):((zero0+1)%SIZE);
		if(fabs(u0)>fabs(u0n)) zero0= (zero0+1)%SIZE;
		if(fabs(u1)>fabs(u1n)) zero1= (zero1+1)%SIZE;
		#endif

		#ifdef FORCESPARREANDERSEN // this is not the whole correction an it does not work, should be more complicated...
		REAL u0, u1;
		u0 = u_ptr[zero0];
		u1 = u_ptr[zero1];
		if(fabs(u0)>fabs(u1)) zero1++;
		#endif

		//int first_interval=  (1)?(zero1):((zero1+1)%SIZE) - (1)?(zero0):((zero0+1)%SIZE);

		// naive version
		int first_interval=  zero1 - zero0;

		first_interval += (first_interval<0)?(SIZE):(0.0);


		return first_interval;
}

struct multiplica
{
	int *zeros;
	size_t nzeros;
	int x;
	REAL *u_ptr;
	multiplica(int * _zeros, REAL *u_, int _x, size_t nzeros_):zeros(_zeros),x(_x),u_ptr(u_),nzeros(nzeros_){};
	__device__
	REAL operator()(const int i) // i is the i-th zero
	{
		//REAL first_interval= zeros[(i+1)%SIZE]-zeros[i];
		REAL first_interval = corregir_intervalo(zeros, u_ptr, i, nzeros);

		//REAL second_interval= zeros[(i+x+1)%SIZE]-zeros[(i+x)%SIZE];
		REAL second_interval = corregir_intervalo(zeros, u_ptr, (i+x)%nzeros, nzeros);
		
		//first_interval += (first_interval<0)?(SIZE):(0.0);
		//second_interval += (second_interval<0)?(SIZE):(0.0);

		//if(first_interval>LARGEINTERVAL || second_interval>LARGEINTERVAL) return 0;
		return first_interval*second_interval;
	}
}; 

struct interval
{
	int *zeros;
	REAL *u_ptr;
	size_t nzeros;

	interval(int * _zeros, REAL *u_, size_t nzeros_):zeros(_zeros),u_ptr(u_), nzeros(nzeros_){};
	__device__
	int operator()(const int i)
	{
		int intervalo_corregido;
/*		int zero0, zero1; 
		REAL u0, u1, u0n, u1n;

		zero0 = zeros[i];
		zero1 = zeros[(i+1)%SIZE];

		u0 = u_ptr[zero0];
		u0n = u_ptr[(zero0+1)%SIZE];

		u1 = u_ptr[zero1];
		u1n = u_ptr[(zero1+1)%SIZE];

		int first_interval=  (fabs(u1)<fabs(u1n))?(zero1):((zero1+1)%SIZE) - (fabs(u0)<fabs(u0n))?(zero0):((zero0+1)%SIZE);
		first_interval += (first_interval<0)?(SIZE):(0.0);
*/			
		intervalo_corregido = corregir_intervalo(zeros, u_ptr, i, nzeros);
		
		return intervalo_corregido;
	}
}; 


struct areator
{
	int *zeros; //arrays con los zeros
	REAL *u_ptr; // desplazamientos
	size_t nzeros; // numero de zeros

	areator(int * _zeros, REAL *u_, size_t nzeros_):zeros(_zeros),u_ptr(u_), nzeros(nzeros_){};
	__device__
	int operator()(const int i)
	{
		// area in the interval
		REAL area=0.0;
		int j0 = zeros[i];
		int j1 = zeros[(i+1)%SIZE];

		if(j1>j0){
			for(int j=j0;j<j1;j++){
				area+=(u_ptr[j]); 	
			}
		}	
		else{ 
			for(int j=j0;j<SIZE;j++){
				area+=(u_ptr[j]); 	
			}
			for(int j=0;j<j0;j++){
				area+=(u_ptr[j]); 	
			}
		}
		return int(area);
	}
}; 

/*
struct loginterval
{
	int *zeros;
	loginterval(int * _zeros):zeros(_zeros){};
	__device__
	int operator()(const int i)
	{
		int first_interval= zeros[(i+1)%SIZE] - zeros[i];
				
		first_interval += (first_interval<0)?(SIZE):(0.0);
		
		return int(logf(first_interval)/logf(2.0f));
	}
}; 
*/
struct modulo_cuadrado
{
	__device__
	REAL operator()(COMPLEX d)
	{
		return d.x*d.x+d.y*d.y;
	}
};

struct modulo_cuadrado_for_each
{
	template <typename Tuple>
    __host__ __device__
	void operator()(Tuple tup)
	{
		COMPLEX fou=thrust::get<0>(tup);
		REAL fou2 = fou.x*fou.x+fou.y*fou.y;

		thrust::get<2>(tup)=fou2; 

		thrust::get<1>(tup) = thrust::get<1>(tup) + fou2; 
	}
};

class Factor_de_estructura
{
	public:

	// container para acumular el factor de estructura
	thrust::device_vector<REAL> Sofq_inst;
	thrust::device_vector<REAL> Sofq_acum;

	//typedef thrust::device_ptr<REAL> dptr;
	//std::vector<dptr> Sofq_inst_aging();

	//contador
	int cont;

	void init(int Ncomp)
	{
		Sofq_acum.resize(Ncomp);
		Sofq_inst.resize(Ncomp);			
		thrust::fill(Sofq_acum.begin(), Sofq_acum.end(),0.0f);
		thrust::fill(Sofq_inst.begin(), Sofq_inst.end(),0.0f);
		cont=0;
	};

	void acum(thrust::device_vector<COMPLEX> &D)
	{
		cont++;
		thrust::for_each(
			thrust::make_zip_iterator(thrust::make_tuple(D.begin(),Sofq_acum.begin(),Sofq_inst.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(D.end(),Sofq_acum.end(),Sofq_inst.end())),
			modulo_cuadrado_for_each()
		);

		//thrust::transform(D.begin(),D.end(),Sofq_inst.begin(),modulo_cuadrado());
		//thrust::transform(Sofq_inst.begin(),Sofq_inst.end(),Sofq.begin(), Sofq.begin(),_1+_2); // Sofq+=Sofq_inst;
		//thrust::transform(Sofq.begin(),Sofq.end(),Sofq_inst.begin(), Sofq.begin(),thrust::plus<REAL>()); // Sofq+=Sofq_inst;
	}		

	void print_inst(ofstream &fout)
	{
		thrust::host_vector<REAL> H_Sofq_inst=Sofq_inst;
	
		for(int i=0;i < SIZE/2+1;i++)
		fout << H_Sofq_inst[i] << "\n"; 	
		fout << "\n\n";
	}		

	//void print(const char *nombre)
	void print(ofstream &fout)
	{
		//ofstream fout(nombre);
		thrust::host_vector<REAL> H_Sofq_acum=Sofq_acum;
		thrust::host_vector<REAL> H_Sofq_inst=Sofq_inst;

		for(int i=0;i < SIZE/2+1;i++)
		fout << H_Sofq_acum[i] << " " << H_Sofq_inst[i] << " " << cont << "\n";	
		fout << "\n\n";
	}		
};

class elastic_string
{
	private:
	// string length
	int L;
	
	// Un container de thrust para guardar la cuerda en espacio real en GPU 
	thrust::device_vector<REAL> D_real;
	REAL *d_real;

	// Un container de thrust para guardar la cuerda en espacio de Fourier en GPU  
	int Ncomp;	
	thrust::device_vector<COMPLEX> D_fourier;
	COMPLEX *d_fourier; 

	// crea plan de antitransformada de cuFFT
	#ifdef DOUBLE_PRECISION
	cufftHandle plan_z2d;
	#else
	cufftHandle plan_c2r;
	#endif


    //PROPIEDADES EN DEVICE ------------------------------------

	// container para acumular el histograma de intervalos
	thrust::device_vector<int> histograma_de_intervalos;

	// container para acumular el histograma de areas
	thrust::device_vector<float> histograma_de_areas;

	// container para guardar las posiciones de los zeros
	thrust::device_vector<int> zeros;	

	// container para guardar las areas entre zeros
	thrust::device_vector<int> areas;	


	public: 

	// container para acumular propiedades
	thrust::host_vector<REAL> correlacion_de_intervalos;
	thrust::host_vector<int> counts;
	thrust::host_vector<REAL> structure_factor;

	Factor_de_estructura SQ;

	thrust::host_vector<REAL> structure_factor_steps; //para que se usa este?

	// constructor-inicializador
	elastic_string()
	{
		L = SIZE;
		if(L%2!=0) 
		{
			std::cout << "Elegir L par, preferiblemente potencia de 2 \n"; 
			exit(1);
		}

		D_real.resize(L+1);
		d_real = thrust::raw_pointer_cast(&D_real[0]);

		Ncomp=L/2+1;
		D_fourier.resize(Ncomp);
		d_fourier = thrust::raw_pointer_cast(&D_fourier[0]); 

		SQ.init(Ncomp);

		zeros.resize(L);

		COMPLEX zero; zero.x=zero.y=0.0;
		thrust::fill(D_fourier.begin(), D_fourier.end(),zero);
		

		histograma_de_intervalos.resize(L,0);
		thrust::fill(histograma_de_intervalos.begin(), histograma_de_intervalos.end(),0);

		counts.resize(L,0);
		thrust::fill(counts.begin(), counts.end(),0);

		correlacion_de_intervalos.resize(L,0);
		thrust::fill(correlacion_de_intervalos.begin(), correlacion_de_intervalos.end(),0);

		structure_factor.resize(Ncomp,0);
		thrust::fill(structure_factor.begin(), structure_factor.end(),0);

		int newsize=10*(log10(TRUN))*Ncomp;
		structure_factor_steps.resize(newsize,0);
		thrust::fill(structure_factor_steps.begin(), structure_factor_steps.end(),0);

		//histograma_de_areas.resize(L,0);
		//thrust::fill(histograma_de_areas.begin(), histograma_de_areas.end(),0.0f);

		// una inicializacion particular
		//zero.x=0.0; zero.y=0.0;
		//D_fourier[3]=zero;
		//D_fourier[20]=zero;
		//D_fourier[50]=zero;
		

		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_z2d,L,CUFFT_Z2D,1));
		#else
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_c2r,L,CUFFT_C2R,1));
		#endif

		//std::cout << D_real.size() << std::endl; 	
		//std::cout << D_fourier.size() << std::endl; 	

	};

	void make_flat(){
		COMPLEX zero; zero.x=zero.y=0.0;
		thrust::fill(D_fourier.begin(), D_fourier.end(),zero);
	};

	// antitransforma la senial Fourier
	void antitransformar(){
		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftExecZ2D(plan_z2d, d_fourier, d_real));
		#else
		CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_fourier, d_real));
		#endif
	};

	// avanza nsteps pasos de la dinamica en espacio de fourier.
	// t se usa solo para meter fuerzas dependientes del tiempo
	// semilla es una semilla global para el CBRNG philox.
	void dynamics(int nsteps, int t, int semilla)
	{
		thrust::transform(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(L),D_fourier.begin(),
			semi_implicit_steps(d_fourier, nsteps, t, semilla)
		);
	};


	// genera una configuracion en Fourier con zeta=ZETA bien definido
	// t es opcional... 
	void steady_state_sampling(int t)
	{
		thrust::transform(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(L),D_fourier.begin(),
			ss_sampling(d_fourier,t)
		);
	};

	void imprimir_real(ofstream &fout){
		thrust::host_vector<REAL> H_real=D_real;
		for(int i=0;i<L;i++)
		fout << D_real[i] << std::endl;

		fout << "\n\n";
	};


	// calcula los zeros en el espacio real, y copia sus indices a zeros[] en forma ordenada.
	// retorna el numero de zeros. El criterio de zero es en el functor iszero()
	int get_zerosN(){
		thrust::device_vector<int>::iterator result= 
		thrust::copy_if(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(L),zeros.begin(),is_zero(d_real)
		);
		int n = int(result-zeros.begin());
		thrust::sort(zeros.begin(),zeros.begin()+n);				
		return n;
	};


	int imprimir_zeros(ofstream &zerosout){
		int n=get_zerosN();		
		int signoinicial=(D_real[1+zeros[0]]>0)?(1):(-1);

		for(int i=0;i<n;i++)
		{
			zerosout << zeros[i] << " " << signoinicial << std::endl;
			signoinicial*=-1;
		}
		if(n>0) zerosout << std::endl << std::endl;
		//return number of zeros
		return n;
	};

    // Given a configuration calculates its contribution to the interval correlation function.
	REAL interval_correlation_function(int x, int nzeros)
	{
		//int nzeros=get_zerosN();
		int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	


		int maxinterval = get_maxintervals();

		if(maxinterval<LARGEINTERVAL && x<nzeros){
			REAL correlation=
			thrust::transform_reduce(
				thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),
				multiplica(zeros_ptr,d_real,x, nzeros), REAL(0.0), thrust::plus<REAL>()	);
			return (correlation/(REAL)nzeros)-powf((REAL)SIZE/(REAL) nzeros,2.0); //-1.0;
		}
		else {
			return 0.0f;			
		} 
	};

	// acumula factor de estructura en el host. Hace copia del device cada vez.
	void acumular_structure_factor(){
		thrust::host_vector<COMPLEX> H_fourier=D_fourier;
	
		/*for(int i=0;i < SIZE/2+1;i++)
		structureDiscreteout << (double) time * TIMESTEP  << "\t"  << i << "\t" <<  H_fourier[i].x*H_fourier[i].x + H_fourier[i].y*H_fourier[i].y << "\t" << std::endl ;
		structureDiscreteout << "\n\n";*/
		for(int i=0;i < SIZE/2+1;i++)
		structure_factor[i] += H_fourier[i].x*H_fourier[i].x + H_fourier[i].y*H_fourier[i].y;
	};

	// suponfo que acumula
	void structure_factor_realizations(int step){
		thrust::host_vector<COMPLEX> H_fourier=D_fourier;
	
		for(int i=0;i < SIZE/2+1;i++)
		structure_factor_steps[(step*(SIZE/2+1))+i] += H_fourier[i].x*H_fourier[i].x + H_fourier[i].y*H_fourier[i].y;
	};


	void get_structure_factor(thrust::host_vector<REAL> &S, int start)
	{
		thrust::host_vector<COMPLEX> H_fourier=D_fourier;
		for(int i=0;i<L/2+1;i++)
		S[start+i] += H_fourier[i].x*H_fourier[i].x + H_fourier[i].y*H_fourier[i].y;
	};



	REAL acumular_correlacion_de_intervalos(){
		int nzeros=get_zerosN();
		if(nzeros==0){
			std::cout << "nzeros=0\n";
			return 0.0f;
		}
		else{
			for(int x=0;x<nzeros;x+=1){
				correlacion_de_intervalos[x] += interval_correlation_function(x, nzeros);
				counts[x]+=1;			
			}
			return 1.0f;
		}
	};

	REAL get_correlacion_intervalos(thrust::host_vector<REAL> &C, int start){
		int nzeros=get_zerosN();
		if(nzeros==0){
			std::cout << "nzeros=0\n";
			return 0.0f;
		}
		else{
			for(int x=0;x<nzeros;x+=1){
				C[start+x] += interval_correlation_function(x, nzeros);
			}
			return 1.0f;
		}
	};



    // solo un check
	int get_intervalsN()
	{
		int nzeros=get_zerosN();
		if(nzeros==0){
			std::cout << "nzeros=0\n";
			return 0;
		}
		else{		
			int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	
			REAL sumIntervals=
			thrust::transform_reduce(
				thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),
				interval(zeros_ptr,d_real,nzeros),
				0, thrust::plus<int>()
			);
			return (int) sumIntervals; //-1.0;
		}
	};

    // solo un check
	int get_maxintervals()
	{
		int nzeros=get_zerosN();
		if(nzeros==0){
			std::cout << "nzeros=0\n";
			return 0;
		}
		else{		
			int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	
			int maxIntervals=
			thrust::transform_reduce(
				thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),
				interval(zeros_ptr,d_real,nzeros),
				0, thrust::maximum<int>()
			);
			return maxIntervals; //-1.0;
		}
	};

	int check_intervals()
	{
		int SEECONF=0;
		int nzeros=get_zerosN();
		if(nzeros==0){
			std::cout << "nzeros=0\n";
			return 0;
		}
		else{		
			thrust::host_vector<int> h_zeros(zeros);
			for(int i=0;i<nzeros;i++){
				int inter = h_zeros[(i+1)%SIZE] - h_zeros[i] ;
				//intervalsout << inter << std::endl;
				if(inter>MININT && inter<MAXINT){
					SEECONF=1;
					//std::cout << "Interval wanted has been found\n";
				}
			}
			return SEECONF;
		}
	};


	// retorna Sum_y [u(y)]^2 de la interface
	REAL roughness(){
		using namespace thrust::placeholders;
		REAL roughness= thrust::transform_reduce(D_real.begin(),D_real.end(),_1*_1,REAL(0.0), thrust::plus<REAL>());
		return roughness;
	};
	
	
	// retorna Sum_y [u(y)-u(y+x)]^2/L de la interface
	REAL roughnessDiscrete(int x)
	{
		REAL roughness= thrust::transform_reduce(
				thrust::make_counting_iterator(0),thrust::make_counting_iterator(L), resta(d_real,x), REAL(0.0), thrust::plus<REAL>() 	);

		return roughness/(float)SIZE;
	};
	
	
	// retorna Sum_y u[y]. Deberia ser 0 si fijamos ucm=0. 
	REAL height(){
		using namespace thrust::placeholders;
		REAL sumheights= thrust::reduce(D_real.begin(),D_real.end(),REAL(0.0), thrust::plus<REAL>());

		return sumheights/(float) SIZE;
	};


	// retorna Sum_y (u[y]-ucm)^2. Deberia ser igual a roughness/L si fijamos ucm=0. 
	REAL width(){
		using namespace thrust::placeholders;
		REAL sumheights= thrust::reduce(D_real.begin(),D_real.end(),REAL(0.0), thrust::plus<REAL>());
		REAL width2= thrust::transform_reduce(D_real.begin(),D_real.end(),(_1-(sumheights/(double)SIZE))*(_1-(sumheights/(double)SIZE)),REAL(0.0), thrust::plus<REAL>());
		return width2/(double) SIZE;
	};


	// imprime el factor de estructura instantaneo.
	void structure_factorDiscrete(ofstream &structureDiscreteout, int time){
		thrust::host_vector<COMPLEX> H_fourier=D_fourier;
	
		for(int i=0;i < SIZE/2+1;i++)
			structureDiscreteout << (double) time * TIMESTEP  << "\t"  << i << "\t" <<  H_fourier[i].x*H_fourier[i].x + H_fourier[i].y*H_fourier[i].y  << std::endl ;

		structureDiscreteout << "\n\n";
		
	};
	
	REAL interval_Squared()
	{
		int nzeros=get_zerosN();
		int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	
		using namespace thrust::placeholders;
		REAL lSquared = thrust::transform_reduce(thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),_1*_1,REAL(0.0),
		thrust::plus<REAL>());
		return lSquared/(REAL) nzeros; //-1.0;
	};
	
/*

	REAL roughnessModified(){
		using namespace thrust::placeholders;
		REAL roughness= thrust::transform_reduce(D_real.begin(),D_real.end(),(_1-D_real.begin())*(_1-D_real.begin()),REAL(0.0), thrust::plus<REAL>());
		return roughness;
	}
*/
	void imprimir_fourier(ofstream &fout){
		thrust::host_vector<COMPLEX> H_fourier=D_fourier;
		for(int i=0;i<L;i++)
		fout << H_fourier[i].x << " " << H_fourier[i].y << "\n";
		fout << "\n\n";
	};


	~elastic_string(){};

	void print_SQ(ofstream &fout){
		SQ.print(fout);
	};
	void print_SQ_inst(ofstream &fout){
		SQ.print_inst(fout);
	};
	void acum_SQ(){
		SQ.acum(D_fourier);
	};

	void acumulate_interval_histogram()
	{
		//std::cout << "Sparse Histogram" << std::endl;

		int nzeros=get_zerosN();
		if(nzeros==0){
			std::cout << "nzeros = 0" << std::endl;
		}
		else{	
    		thrust::device_vector<int> input(nzeros);

	    	// uniform binning
			int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	
			thrust::transform(
				thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),
				input.begin(),
				interval(zeros_ptr,d_real, nzeros)
			);
		// logarithmic binning
		/*int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	
		thrust::transform(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),
			input.begin(),
			loginterval(zeros_ptr)
		);*/
		//std::cout << "suma=" << thrust::reduce(input.begin(), input.end()) << std::endl;
		/*
		int nzeros=zeros.size();
		int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	
		int sumIntervals=
		thrust::transform_reduce(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),
			interval(zeros_ptr),
			0, thrust::plus<int>()
		);
		std::cout << "suma=" << sumIntervals << std::endl;
		*/

    	//using namespace thrust::placeholders;
    	//thrust::transform(zeros.begin()+1, zeros.end(), zeros.begin(), input.begin(), _1-_2);
		/*int suma=0;
		for(int i=0;i<nzeros;i++){
			suma+=input[i];	
			std::cout << input[i] << " + ";
		}
		std::cout << " = " << suma << std::endl;
		*/
		    thrust::device_vector<int> histogram_values;
    		thrust::device_vector<int> histogram_counts;
	    	sparse_histogram(input, histogram_values, histogram_counts);

			typedef thrust::device_vector<int>::iterator   ElementIterator;
			typedef thrust::device_vector<int>::iterator   IndexIterator;
			thrust::permutation_iterator<ElementIterator,IndexIterator> iter(histograma_de_intervalos.begin(), histogram_values.begin());

			// ERROR! (no usamos iter, altera histograma acumulado a valores chicos...)
			//thrust::transform(histogram_counts.begin(), histogram_counts.end(), histograma_de_intervalos.begin(), histograma_de_intervalos.begin(), thrust::plus<int>());
			thrust::transform(histogram_counts.begin(), histogram_counts.end(), iter, iter, thrust::plus<int>());
		}

    	//histograma_de_intervalos
	}


	void get_interval_histogram(thrust::host_vector<int> &histo, int start)
	{
		int nzeros=get_zerosN();
		if(nzeros==0){
			std::cout << "nzeros = 0" << std::endl;
		}
		else{
			thrust::device_vector<int> Dhisto(histo.begin()+start,histo.begin()+start+SIZE);
			//std::cout << "instantaneous histogram of size " << Dhisto.size() << std::endl;	

    		thrust::device_vector<int> input(nzeros);

	    	// uniform binning
			int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	
			thrust::transform(
				thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),
				input.begin(),
				interval(zeros_ptr,d_real, nzeros)
			);
		    thrust::device_vector<int> histogram_values;
    		thrust::device_vector<int> histogram_counts;
	    	sparse_histogram(input, histogram_values, histogram_counts);

			typedef thrust::device_vector<int>::iterator   ElementIterator;
			typedef thrust::device_vector<int>::iterator   IndexIterator;
			thrust::permutation_iterator<ElementIterator,IndexIterator> 
			iter(Dhisto.begin(), histogram_values.begin());

			thrust::transform(
				histogram_counts.begin(), 
				histogram_counts.end(), 
				iter, 
				iter, 
				thrust::plus<int>());

			thrust::copy(Dhisto.begin(),Dhisto.end(),histo.begin()+start);
		}

    	//histograma_de_intervalos
	}


	void get_areas_histogram(thrust::host_vector<int> &histo, int start)
	{
		int nzeros=get_zerosN();
		if(nzeros==0){
			std::cout << "nzeros = 0" << std::endl;
		}
		else{
			thrust::device_vector<int> Dhisto(histo.begin()+start,histo.begin()+start+SIZE);
			//std::cout << "instantaneous histogram of size " << Dhisto.size() << std::endl;	

    		thrust::device_vector<int> input(nzeros);

	    	// uniform binning
			int *zeros_ptr = thrust::raw_pointer_cast(&zeros[0]);	
			thrust::transform(
				thrust::make_counting_iterator(0),thrust::make_counting_iterator(nzeros),
				input.begin(),
				areator(zeros_ptr,d_real, nzeros)
			);
		    thrust::device_vector<int> histogram_values;
    		thrust::device_vector<int> histogram_counts;
	    	sparse_histogram(input, histogram_values, histogram_counts);

			typedef thrust::device_vector<int>::iterator   ElementIterator;
			typedef thrust::device_vector<int>::iterator   IndexIterator;
			thrust::permutation_iterator<ElementIterator,IndexIterator> 
			iter(Dhisto.begin(), histogram_values.begin());

			thrust::transform(
				histogram_counts.begin(), 
				histogram_counts.end(), 
				iter, 
				iter, 
				thrust::plus<int>());

			thrust::copy(Dhisto.begin(),Dhisto.end(),histo.begin()+start);
		}

    	//histograma_de_intervalos
	}


	void imprimir_histograma_de_intervalos(ofstream &fout){
		thrust::host_vector<int> H_histograma_de_intervalos(histograma_de_intervalos);
		int normalizacion = thrust::reduce(histograma_de_intervalos.begin(),histograma_de_intervalos.end());	

		for(int i=0;i<H_histograma_de_intervalos.size();i++){
			if(H_histograma_de_intervalos[i]>0){
				/*
				float Pdex = H_histograma_de_intervalos[i]*1.0/normalizacion;
				float x = i;
				float y = expf(x*logf(SIZE)/SIZE);
				fout << y << " " << Pdex*SIZE/(logf(SIZE)*y) << " " << i << " " << Pdex << "\n";				
				*/
				fout << i << " " << H_histograma_de_intervalos[i]*1.0/normalizacion << "\n";				

			} 
		}		
		fout << "\n\n";
	}

/*
	void acumulate_area_histogram()
	{
		//std::cout << "Sparse Histogram" << std::endl;
	    thrust::device_vector<int> histogram_values;
    	thrust::device_vector<int> histogram_counts;

		int nzeros=get_zerosN();
    	thrust::device_vector<int> input(nzeros);

    	// cargar el input = areas
    	thrust::reduce_by_key();
		
    	sparse_histogram(input, histogram_values, histogram_counts);

		typedef thrust::device_vector<int>::iterator   ElementIterator;
		typedef thrust::device_vector<int>::iterator   IndexIterator;
		thrust::permutation_iterator<ElementIterator,IndexIterator> iter(histograma_de_areas.begin(), histogram_values.begin());

		// ERROR! (no usamos iter, altera histograma acumulado a valores chicos...)
		//thrust::transform(histogram_counts.begin(), histogram_counts.end(), histograma_de_intervalos.begin(), histograma_de_intervalos.begin(), thrust::plus<int>());

		thrust::transform(histogram_counts.begin(), histogram_counts.end(), iter, iter, thrust::plus<int>());

    	//histograma_de_intervalos
	}
*/

};

