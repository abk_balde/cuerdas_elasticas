set multi lay 2,2

set title "L=1024, samples=200"
unset logs
set ylabel 'interval correlation'
set xlabel 'distance'
plot [:200][-200:200] for[i=1:5] 'interval_correlation_vs_time.dat' u i w l t columnhead

set logs
set xlabel 'area'
set ylabel 'counts'
plot [:10000][] for[i=1:5] 'area_histogram_vs_time.dat' u i w l t columnhead

set xlabel 'length'
set ylabel 'counts'
plot [][:10000] for[i=1:5] 'interval_histogram_vs_time.dat' u i w l t columnhead

set xlabel 'time'
set ylabel ''
plot 'scalars_vs_time.dat' u 1:2 t 'number of zeros' w lp, '' u 1:3 t 'interface width' w lp

unset multi
