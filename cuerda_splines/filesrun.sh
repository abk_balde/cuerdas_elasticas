for i in 2 4 8 16 32 64 128 256 1024 2048 4096
do 
	forceexp="1.915*(1+1./$i)"
	force=$(echo $forceexp | bc -l)

	for temp in 0.01 0.005 0.002 0.001 0.0005 0.0002 0.0001
	do 
		echo "T=$temp, F=$force"
	done
done
