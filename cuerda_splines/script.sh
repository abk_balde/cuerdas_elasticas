# para extraer la temperatura
# echo 'V_L1024M2048S100Tr131072Td1024T0.00500b1.00e1.00h1.915dt0.03gpu0.tmp' | cut -d'T' -f 4 | cut -d'b' -f 1
# --> 0.00500

#para extraer el drive
#echo V_L1024M2048S100Tr131072Td1024T0.00500b1.00e1.00h1.975dt0.03gpu0.tmp | cut -d'h' -f 2 | cut -d'd' -f 1
#1.975

#extrae los dos ultimos puntos de v vs t para una fuerza dada 
#for i in $(ls V_L1024M2048S100Tr131072Td1024T0.0*tmp | grep "h1.915")
for i in $(ls V_L1024M2048S100Tr131072Td1024T0.0*tmp)
do 
	temp=$(echo $i | cut -d'T' -f 4 | cut -d'b' -f 1)
	force=$(echo $i | cut -d'h' -f 2 | cut -d'd' -f 1)
	gawk -v T=$temp -v f=$force '{if($1==131072) v1=$2; if($1==65536) v2=$2; if($1==32768) v3=$2}END{print f,T,v1,v2,v3}' $i
done
