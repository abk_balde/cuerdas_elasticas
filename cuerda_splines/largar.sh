for i in 1 2 4 8 32 64 128 256 1024 2048 4096 8192 16384 32768
do 
	forceexp="1.915*(1+1./$i)"
	force=$(echo $forceexp | bc -l)

	for temp in 0.0000
	do 
		echo "T=$temp, F=$force"
		DIR=run"_"$temp"_"$force
		make clean
		make TEMP=$temp H_START=$force

		mkdir $DIR
		cp submit*.sh main-QEW $DIR/
		cd $DIR
		qsub submit_gpu.sh
		cd ../
		#./main-QEW > zzz"_"$temp"_"$force
	done
done
