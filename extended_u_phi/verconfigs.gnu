unset multi
res

#v vs f
set term qt 0 size 600,600
plot [:] "zzz" u 1:2 w lp, "" u 1:3 w lp,  "" u 1:($4) w lp,x,0

#configs
set term qt 1 size 600,900
set xla "x"; set yla "u"; set multi lay 4,2; do for[i in "20 40 60 80"] {f=0.8+i*0.005; print f; set yla "u"; p [:256][-2.5:2.5] "conf".i.".dat" u ($1-$3) index 20 w lp t sprintf("f/f_W=%f",f),0; set yla "{/Symbol f}"; p [:256][-2.5:2.5] "conf".i.".dat" u ($2-$4) index 20 w lp t sprintf("f/f_W=%f",f),0 }
unset multi

#factores de estructura
set logs
set term qt 2 size 600,900
set xla "q"; set yla "u"; set multi lay 4,2; do for[i in "20 40 60 80"] {f=0.8+i*0.005; print f; set yla "S_u"; p [1:128][0.001:100000] "sofq".i.".dat" u 1 index 19 w lp t sprintf("f/f_W=%f",f),0; set yla "S_{/Symbol f}"; p [1:128][0.001:100000] "sofq".i.".dat" u 2 index 19 w lp t sprintf("f/f_W=%f",f),0,10/x**(1+2*0.5) }; 
unset multi

#hacer pelicula
set xla "x"; j=60; set tit sprintf("f=%f",0.8+j*0.005); do for[i=1:1000]{set multi lay 1,2; set yla "r-<r>"; plot [0:256][-2:2] "lastconf".j.".dat" index (i%1000) u ($1-$3) w l t "".(i%1000); set yla "{/Symbol f}-<{/Symbol f}>"; plot [0:256][-2:2] "lastconf".j.".dat" index (i%1000) u ($2-$4) t "".(i%1000)
w l; pause 0.1; }

#hacer peli gif from png
set xla "x"; set tit sprintf("f=%f",0.8+j*0.005); do for[i=1:1000]{set term png; set out sprintf("peli%05d.png",i);set multi lay 1,2; set yla "r-<r>"; plot [0:256][-2:2] "lastconf".j.".dat" index (i%1000) u ($1-$3) w l t "".(i%1000); set yla "{/Symbol f}-<{/Symbol f}>"; plot [0:256][-2:2] "lastconf".j.".dat" index (i%1000) u ($2-$4) t "".(i%1000) w l; pause 0.1; unset multi;}
ffmpeg -i peli%05d.png output.gif
