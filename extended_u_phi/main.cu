#include<thrust/device_vector.h>
#include<thrust/for_each.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include<thrust/reduce.h>
#include<fstream>
#include<cstdlib>
#include <thrust/transform_reduce.h>
#include <cufft.h>
#include "cutil.h"


class cuerda{

    public:
    cuerda(unsigned long _L, float _dt):L(_L),dt(_dt),fourierCount(0)
    {
        u.resize(L);
        phi.resize(L);

        force_u.resize(L);
        force_phi.resize(L);

        thrust::fill(u.begin(),u.end(),float(0.0));
        thrust::fill(phi.begin(),phi.end(),float(0.0));

        float epsilon=0.1;
        for(int i=0;i<L;i++){
            u[i]=epsilon*rand()/RAND_MAX;
            phi[i]=epsilon*rand()/RAND_MAX;
        }

        //thrust::sequence(u.begin(),u.end());

        f0=0;

        CUFFT_SAFE_CALL(cufftPlan1d(&plan_r2c,L,CUFFT_R2C,1));

	    int Lcomp=L/2+1;
	    Fou_u.resize(Lcomp);
	    Fou_phi.resize(Lcomp);

        acum_Sofq_u.resize(L);
        acum_Sofq_phi.resize(L);

        thrust::fill(acum_Sofq_u.begin(),acum_Sofq_u.end(),float(0.0));
        thrust::fill(acum_Sofq_phi.begin(),acum_Sofq_phi.end(),float(0.0));

        #ifdef DEBUG
        std::cout << "L=" << L << ", dt=" << dt << std::endl;
        #endif
    }

    void set_f0(float x){
        f0=x;
    };

    thrust::tuple<float, float> center_of_mass()
    {
        float cmu = thrust::reduce(u.begin(),u.end(),float(0.0))/L;
        float cmphi = thrust::reduce(phi.begin(),phi.end(),float(0.0))/L;
        return thrust::make_tuple(cmu,cmphi);
    }

    void fourier_transform(){

        float *raw_u = thrust::raw_pointer_cast(&u[0]); 
        float *raw_phi = thrust::raw_pointer_cast(&phi[0]); 
        cufftComplex *raw_fou_u = thrust::raw_pointer_cast(&Fou_u[0]); 
        cufftComplex *raw_fou_phi = thrust::raw_pointer_cast(&Fou_phi[0]); 

	    CUFFT_SAFE_CALL(cufftExecR2C(plan_r2c, raw_u, raw_fou_u));
	    CUFFT_SAFE_CALL(cufftExecR2C(plan_r2c, raw_phi, raw_fou_phi));

        thrust::for_each(
            thrust::make_zip_iterator(
                thrust::make_tuple(Fou_u.begin(),Fou_phi.begin(),acum_Sofq_u.begin(),acum_Sofq_phi.begin())
            ),
            thrust::make_zip_iterator(
                thrust::make_tuple(Fou_u.end(),Fou_phi.end(),acum_Sofq_u.end(),acum_Sofq_phi.end())
            ),
            [=] __device__ (thrust::tuple<cufftComplex,cufftComplex,float &,float &> t){
                cufftComplex fu=thrust::get<0>(t);
                cufftComplex fphi=thrust::get<1>(t);

                thrust::get<2>(t) += fu.x*fu.x + fu.y*fu.y; // S_u(q)
                thrust::get<3>(t) += fphi.x*fphi.x + fphi.y*fphi.y;; // S_phi(q)
            }
        );
        fourierCount++;
    }

    thrust::tuple<float, float, float, float> roughness()
    {
        float cmu = thrust::reduce(u.begin(),u.end(),float(0),thrust::plus<float>())/float(L);
        float cmphi = thrust::reduce(phi.begin(),phi.end(),float(0),thrust::plus<float>())/float(L);

        float cmu2 = 
        thrust::transform_reduce(
            u.begin(),u.end(),
            [=] __device__ (float x){
                return (x-cmu)*(x-cmu);
            },
            float(0),
            thrust::plus<float>()
        )/float(L);

        float cmphi2 = 
        thrust::transform_reduce(
            phi.begin(),phi.end(),
            [=] __device__ (float x){
                return (x-cmphi)*(x-cmphi);
            },
            float(0),
            thrust::plus<float>()
        )/float(L);

        return thrust::make_tuple(cmu,cmphi,cmu2,cmphi2);
    }


    void print_center_of_mass(std::ofstream &out)
    {
        thrust::tuple<float,float> cm=center_of_mass();
    
        float cmu=thrust::get<0>(cm);
        float cmphi=thrust::get<1>(cm);

        out << cmu << " " << cmphi << std::endl;
    }

    void print_roughness(std::ofstream &out, float t)
    {
        thrust::tuple<float,float,float,float> cm=roughness();
    
        float cmu=thrust::get<0>(cm);
        float cmphi=thrust::get<1>(cm);
        float cmu2=thrust::get<2>(cm);
        float cmphi2=thrust::get<3>(cm);

        out << t << " " << cmu << " " << cmphi << " " << cmu2 << " " << cmphi2 << std::endl;
    }

    void update(){

        float *raw_u = thrust::raw_pointer_cast(&u[0]); 
        float *raw_phi = thrust::raw_pointer_cast(&phi[0]); 
        
        // not elegant solution for lambda...
        float dt_=dt;
        float f0_=f0;
        unsigned long L_=L;

        // Forces
        thrust::for_each(
            thrust::make_zip_iterator(
                thrust::make_tuple(force_u.begin(),force_phi.begin(),thrust::make_counting_iterator((unsigned long)0))        
            ),
            thrust::make_zip_iterator(
                thrust::make_tuple(force_u.end(),force_phi.end(),thrust::make_counting_iterator((unsigned long)L))        
            ),
            [=] __device__ (thrust::tuple<float &,float &,unsigned long> t)
            {
                unsigned long i=thrust::get<2>(t);
                unsigned long ileft = (i-1+L_)%L_;
                unsigned long iright = (i+1)%L_;

                float lap_u=raw_u[iright]+raw_u[ileft]-2.0*raw_u[i];
                float lap_phi=raw_phi[iright]+raw_phi[ileft]-2.0*raw_phi[i];

                float force_u = f0_ + lap_u;
                float force_phi = -sinf(2.0*raw_phi[i])+ lap_phi;

                thrust::get<0>(t) = 0.5*(force_u-force_phi);
                thrust::get<1>(t) = 0.5*(force_u+force_phi);
                //thrust::get<0>(t) = F0 + lap_u;
                //thrust::get<1>(t) = lap_phi;
            } 
        );

        #ifdef DEBUG
        std::cout << "updating" << std::endl;
        #endif

        // Euler step
        thrust::for_each(
            thrust::make_zip_iterator(
                thrust::make_tuple(u.begin(),phi.begin(), force_u.begin(),force_phi.begin())        
            ),
            thrust::make_zip_iterator(
                thrust::make_tuple(u.end(),phi.end(), force_u.end(),force_phi.end())        
            ),
            [=] __device__ (thrust::tuple<float &,float &,float,float> t)
            {
                thrust::get<0>(t) = thrust::get<0>(t) + dt_*thrust::get<2>(t);
                thrust::get<1>(t) = thrust::get<1>(t) + dt_*thrust::get<3>(t);
            } 
        );
    };

    void print_config(std::ofstream &out){
        thrust::tuple<float,float> cm = center_of_mass();

        for(int i=0;i<L;i++){
            out << u[i] << " " << phi[i] << " " << thrust::get<0>(cm) << " " << thrust::get<1>(cm) << "\n";
        }
        out << "\n" << std::endl;
    };

    void print_sofq(std::ofstream &out){
        for(int i=0;i<L;i++){
            out << acum_Sofq_u[i]/fourierCount << " " << acum_Sofq_phi[i]/fourierCount << "\n";
        }
        out << "\n" << std::endl;
    };


    private:
        float dt;
        unsigned long L;
        float f0;
        thrust::device_vector<float> u;
        thrust::device_vector<float> phi;
        thrust::device_vector<float> force_u;
        thrust::device_vector<float> force_phi;

        int fourierCount;
        cufftHandle plan_r2c;
        thrust::device_vector<cufftComplex> Fou_u;
	    thrust::device_vector<cufftComplex> Fou_phi;
        thrust::device_vector<float> acum_Sofq_u;
	    thrust::device_vector<float> acum_Sofq_phi;
};

int main(int argc, char **argv){
    
    // ./a.out 4 0.0 1000

    std::ofstream confout("conf.dat");
    std::ofstream sofqout("sofq.dat");
    std::ofstream cmout("cm.dat");
    std::ofstream lastconfout("lastconf.dat");

    unsigned int L=atoi(argv[1]);
    float f0=atof(argv[2]);
    unsigned long Nrun = atoi(argv[3]);
    float dt=0.01;

    cuerda C(L,dt);
    C.set_f0(f0);

    //thrust::tuple<float,float> cm0 = C.center_of_mass();
    thrust::tuple<float,float,float,float> r0 = C.roughness();
    for(int i=0;i<Nrun;i++){

        //if(i%100==0) C.fourier_transform();

        if(i%(Nrun/100)==0){
            C.print_config(confout);
            C.print_roughness(cmout,i*dt);
            C.print_sofq(sofqout);
        } 
        
        //if(i%(Nrun/1000)==0) C.print_config(lastconfout); 

        C.update();
    }
    //thrust::tuple<float,float> cm1 = C.center_of_mass();
    thrust::tuple<float,float,float,float> r1 = C.roughness();

    C.print_config(confout);

    /*std::cout << f0 << " " 
    << (thrust::get<0>(cm1)-thrust::get<0>(cm0))/(Nrun*dt) << " " 
    << (thrust::get<1>(cm1)-thrust::get<1>(cm0))/(Nrun*dt) << std::endl;*/
    

    std::cout << f0 << " " 
    << (thrust::get<0>(r1)-thrust::get<0>(r0))/(Nrun*dt) << " " 
    << (thrust::get<1>(r1)-thrust::get<1>(r0))/(Nrun*dt) << " " 
    << thrust::get<2>(r1) << " " << thrust::get<3>(r1) << " "
    << thrust::get<2>(r0) << " " << thrust::get<3>(r0) << " "
    << std::endl;


    return 0;
}