/*
=============================================================================================
A. B. Kolton
=============================================================================================

Version paralelizada del codigo para simular el modelo de A. Dobrinevsky.

*/

#include "timer.h"
#include<cmath>
#include<fstream>
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include<cstdio>
#include <gsl/gsl_histogram.h>
#include "histogram_extra.h"

/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG


/* algunos headers de la libreria thrust */
// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
// https://github.com/thrust/thrust/wiki/Documentation
#include<thrust/transform.h>
#include <thrust/for_each.h>
#include<thrust/copy.h>
#include <thrust/replace.h>
#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include<thrust/functional.h>
#include<thrust/sequence.h>
#include <thrust/fill.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/reduce.h>
#include <thrust/count.h>
#include "histo.h"

/* parámetros default del problema */
#ifndef TAMANIO
#define L	4096   // numero de partículas/monomeros
#else
#define L	TAMANIO   // numero de partículas/monomeros
#endif
#ifndef TIEMPORUN
#define TRUN	100000       // numero de iteraciones temporales 
#else
#define TRUN	TIEMPORUN    // numero de iteraciones temporales 
#endif
#ifndef TPROP
#define TPROP	1000 // intervalo entre mediciones
#endif

#ifndef SEED
#define SEED 	12345678 // global seed RNG (quenched noise)
#endif

//#define SEED2 	12312313 // global seed#2 RNG (thermal noise)

//Alex "For my best simulations I typically used m^2 ~ 0.0001, a ~ 5, dt ~ 0.002,  dw*m^2 ~ 0.001"
#ifndef A
#define A 1.0
#endif

#ifndef B
#define B A
#endif

#ifndef M2
#define M2  0.00001
#endif

#ifndef TIMESTEP
#define TIMESTEP	 0.001
#endif

#ifndef dwM2
#define dwM2	0.001
#endif

#ifndef MAXNROAVALANCHES
#define MAXNROAVALANCHES 10000
#endif

#define VERYSMALLVELOCITY	0.00000001
#define MINAVALANCHESIZE	1.0000

#include "numrec.h"

// para evitar poner "thrust::" a cada rato all llamar sus funciones
using namespace thrust;

// precisón elegida para los números reales
typedef double 	REAL;

// para generar números aleatorios gausianos a partir de dos uniformes
// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
__device__
REAL box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	REAL u1 = u01_open_closed_32_53(r_philox[0]);
  	REAL u2 = u01_open_closed_32_53(r_philox[1]);

  	REAL r = sqrt( -2.0*log(u1) );
  	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);    			
}

#ifdef PRINTCONFS
// para imprimir configuraciones (realeadas y cada tanto...)
std::ofstream configs_out("configuraciones.dat");
void print_configuration(device_vector<REAL> &U, int size)
{
	long i=0;
	int every = int(L*1.0/size);
	if(every==0) every=1;
	for(i=0;i<U.size();i+=every)
	{
		configs_out << U[i] << std::endl;
	}
	configs_out << "\n\n";
}

// para imprimir configuraciones cada tanto...
std::ofstream velostop_out("velocity_near_stop.dat");
void print_velocities(device_vector<REAL> &x, int size, int t)
{
	long i=0;
	int every = int(L*1.0/size);
	if(every==0) every=1;
	for(i=1;i<x.size()-1;i+=every)
	{
		float xi=x[i];
		float xip1=x[(i+1)%L];
		
		bool c1=xi>VERYSMALLVELOCITY && xip1<VERYSMALLVELOCITY;
		bool c2=xi<VERYSMALLVELOCITY && xip1>VERYSMALLVELOCITY;
		
		if(c1 || c2) velostop_out << i << " " << t << " ";

		if(c1) velostop_out << 1 << std::endl;
		if(c2) velostop_out << -1 << std::endl;
	}
	//velostop_out << "\n\n";
}
#endif


/////////////////////////////////////////////////////////////////////////////
// FUNCTORS usados en los algoritmos TRANSFORM

struct fuerza_alexander
{
    RNG rng;       // random number generator
    long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL Dt;	

    fuerza_alexander(REAL _Dt, long _t):tiempo(_t),Dt(_Dt)
    {};

    __device__
    tuple<REAL,REAL,long> operator()(tuple<long,REAL,REAL,REAL, REAL, long> tt)
    {
	// thread/particle id
	uint32_t tid=get<0>(tt);

	// LAPLACIAN
	REAL um1=get<1>(tt);
	REAL u=get<2>(tt);
	REAL up1=get<3>(tt);
	REAL f=get<4>(tt);
	long tiempo_propio=get<5>(tt);

	// DYNAMICALLY PRODUCED DISORDER

	#ifndef EXACTSTOCHASTIC
	// keys and counters
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	k[0]=tid; 	  //  KEY = {threadID}
	c[1]=SEED; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}
	c[0]=uint32_t(tiempo_propio); // COUNTER={tiempo, GLOBAL SEED #2}
	//c[1]=SEED2; // para evitar correlaciones entre el ruido térmico y el desorden congelado...

	REAL noise=0.0f;
	r = rng(c, k);
	//noise = (u>0.0f)?(box_muller(r)*sqrt(2.0f*u*Dt*A)):(0.0f);
	noise = box_muller(r)*sqrt(2.*fabs(u)*Dt*A);
	tiempo_propio++;

	REAL delta_f = (u>=0.0)?(noise-A*u*f*Dt):(0.0f); 
	REAL delta_udot = (um1 + up1 -(2.0f+M2)*u)*Dt + delta_f; 

	#else

	REAL uu=0.0f;
	REAL ff=0.0f;
	
	// operator splitting
	// d(u,F)/dt = L o (u,F) = L1 o (u,F) + L2 o (u,F) 
	// (u',F') = U1 o (u,F)  
	// (u'',F'') = U2 o (u',F') -> (du, dF) = U2 o (u',F') - (u, f) = L2 o (u', F')  

	// First operator: d(u,F)/dt = sqrt(A u) \xi (1,1) = L1 o (u,f)
	REAL arg=(u)/(A*Dt);	
	int poisson=(arg>=0.0f)?(int(poidev(arg, tid, tiempo_propio))):(0);
	uu = (poisson==0)?(0.0):(A*Dt*gamdev(poisson,tid,tiempo_propio));
	ff = f+uu-u;	

	// Second operator: d(u,F)/dt =  = L2 (u,f)
	REAL delta_f = (uu-u)-A*uu*ff*Dt; 

	#ifndef NONINTERACTING
	REAL delta_udot = (um1 + up1 -(2.0f+M2)*uu)*Dt; 
	#else
	REAL delta_udot = -M2*uu*Dt; 	
	#endif

	#endif

	// Fuerza total en el monómero tid
	return make_tuple(delta_udot, delta_f,tiempo_propio);
  }
};

// this functor only calculate the deterministic forces
struct fuerza_deterministic_alexander
{
    REAL Dt;	
    REAL V0;	
    fuerza_deterministic_alexander(REAL _Dt, REAL _V0):Dt(_Dt),V0(_V0){};

    __device__
    tuple<REAL,REAL> operator()(tuple<REAL, REAL, REAL, REAL> tt)
    {
	// LAPLACIAN
	REAL udotm1=get<0>(tt);
	REAL udot=get<1>(tt);
	REAL udotp1=get<2>(tt);
	REAL f=get<3>(tt);

	REAL delta_f = -B*udot*f*Dt; 

#ifndef NONINTERACTING
	REAL delta_udot = (udotm1 + udotp1 -(2.0f+M2)*udot + M2*V0)*Dt + delta_f; 
#else
	// we ignore neighbord (simple but inefficient implementation)
	REAL delta_udot = -M2*udot*Dt + delta_f + M2*V0*Dt; 
#endif
	// Fuerza total en el monómero tid
	return make_tuple(delta_udot, delta_f);
  }


};

// This makes the stochastic update
struct stochastic_update_alexander
{
    RNG rng;       // random number generator
    long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL Dt;	

    stochastic_update_alexander(REAL _Dt, long _t):tiempo(_t),Dt(_Dt)
    {};

   //stochastic_update_alexander	
    __device__
    tuple<REAL,REAL,long> operator()(tuple<long,REAL,REAL,long> tt)
    {
	// thread/particle id
	uint32_t tid=get<0>(tt);

	// LAPLACIAN
	REAL u=get<1>(tt);
	REAL f=get<2>(tt);
	long tiempo_propio=get<3>(tt);

	// DYNAMICALLY PRODUCED DISORDER

	#ifndef EXACTSTOCHASTIC
	// keys and counters
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	k[0]=tid; 	  //  KEY = {threadID}
	c[1]=SEED; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}
	c[0]=uint32_t(tiempo_propio); // COUNTER={tiempo, GLOBAL SEED #2}

	REAL noise;
	r = rng(c, k);
	noise = box_muller(r)*sqrt(2.*fabs(u)*Dt*A);
	tiempo_propio++;

	REAL new_f = f + noise; 
	REAL new_udot = u + noise; 

	#else
	REAL arg=(u)/(A*Dt);	
	int poisson=(arg>=0.0f)?(int(poidev(arg, tid, tiempo_propio))):(0);
	REAL new_udot = (poisson==0)?(0.0):(A*Dt*gamdev(poisson,tid,tiempo_propio));
	REAL new_f = f + new_udot - u;
	#endif

	// Fuerza total en el monómero tid
	return make_tuple(new_udot, new_f,tiempo_propio);
  }
};



struct euler_alexander
{
    REAL Dt;	
    euler_alexander(REAL _Dt):Dt(_Dt){};
    __device__
    tuple<REAL,REAL,REAL> operator()(tuple<REAL, REAL, REAL, REAL, REAL> tt)
    {
    	REAL udot_old=get<0>(tt);
    	REAL delta_udot=get<1>(tt);
   
 	REAL fold=get<2>(tt);
    	REAL delta_f=get<3>(tt);

    	REAL Uold=get<4>(tt);

	REAL udot_new = udot_old + delta_udot; 
	//if(unew<0.0) unew=0.0f; esto produce un drift

	REAL fnew=fold + delta_f;
	REAL Unew=Uold + 0.5*(udot_new+udot_old)*Dt;

    	return make_tuple(udot_new, fnew, Unew);
    }
};

// only for testing
struct gammapoisson
{
    REAL Dt;	
    gammapoisson(REAL _Dt):Dt(_Dt){};
    //REAL operator()(int tid, long tiempo_propio){
    template <typename Tuple>
    __device__
    void operator()(Tuple tt)
    {
	int tid=get<0>(tt);
	long tiempo_propio=get<1>(tt);
	REAL arg=(get<2>(tt))/(A*Dt);	

	REAL noise=0.0f;
	int poisson=int(poidev(arg, tid, tiempo_propio));
	if(poisson>0) noise=A*Dt*gamdev(poisson,tid,tiempo_propio)-get<2>(tt);		
	
	get<1>(tt)=tiempo_propio;
	get<2>(tt)=get<2>(tt)+noise;
    }	
};

// for computing the roughness
struct roughtor
{
	REAL ucm;
    	roughtor(REAL _Ucm):ucm(_Ucm){};
    	__host__ __device__ 
	REAL operator()(REAL x) const
    	{
   	   return (x-ucm)*(x-ucm);
    	}
};


// for testing random mixture...
void test()
{
	const int LL=1000000;
	device_vector<REAL> D(LL); //, D2(LL);
	device_vector<long> tow(LL);  // tiempo propio para generar random numbers... 

	fill(tow.begin(),tow.end(),1);
	fill(D.begin(),D.end(),0.1f);
	
	for(int n=0;n<10;n++){
		for_each(
			make_zip_iterator(
			make_tuple(make_counting_iterator(0),tow.begin(), D.begin())
			),
			make_zip_iterator(
			make_tuple(make_counting_iterator(LL),tow.end(), D.end())
			),
			gammapoisson(0.01f)	
		);
	}

	/*for(int i=0;i<LL;i++){
		std::cout << D[i] << std::endl;
	}*/

	const int NBINS=100;

	device_vector<REAL> H(NBINS);
	dense_histogram_data_on_device(D,H, 0.0f, 4.0f);
	std::ofstream fout("histograma.dat");
	print_histograma(H, 0.0f, 4.0f, fout);
	exit(1);
}

/*
struct sumapositivos
{
	__device__
	REAL operator()(REAL x1, REAL x2)
	{
		return REAL((x1>0.0f)*x1+(x2>0.0f)*x2);
	}
};
*/

struct is_less_than_zero
{
  __host__ __device__
  bool operator()(REAL x)
  {
    return (x < 0.0);
  }
};

/*
struct divide_tupla: public thrust::unary_function< tuple<REAL, REAL>,REAL>
{
	REAL dt;
	divide_tupla(REAL _dt):dt(_dt){};
	__device__ 
	REAL operator()(tuple<REAL,REAL> tt){
		if(fabsf(get<0>(tt))>0)
		return REAL(fabsf(dt*get<1>(tt)/get<0>(tt)));
		else return 	 
	}
};
*/

#ifdef OMP
#include <omp.h>
#endif

class sistema{
	public:
	
	sistema::sistema(){
		u.resize(L+2);
		f.resize(L);
		U.resize(L);
		tow.resize(L);

		u_it0 = u.begin()+1;
		u_it1 = u.end()-1;

		f_it0 = f.begin();
		f_it1 = f.end();

		U_it0 = U.begin();
		U_it1 = U.end();

		// Si necesita el puntero "crudo" (y sin halo) al array para pasarselo a un kernel de CUDA C/C++:
		u_raw_ptr = raw_pointer_cast(&u[1]);
		f_raw_ptr = raw_pointer_cast(&f[0]);
	
		// container de fuerza total: 
		Ftot.resize(L); 
		Ftot2.resize(L);

		// el rango de interés definido por los iteradores 
		Ftot_it0 = Ftot.begin();
		Ftot_it1 = Ftot.end();
		Ftot2_it0 = Ftot2.begin();
		Ftot2_it1 = Ftot2.end();
		Dt=TIMESTEP;

		initialize();
	}

	void initialize();

	private:
	device_vector<REAL> u;  // velocidades udot(x,t)
	device_vector<REAL> f;    // desorden dinamico f(x,t) 
	device_vector<REAL> U;    // desplazamientos u(x,t)
	device_vector<long> tow;  // tiempo propio para generar random numbers... 

	device_vector<REAL>::iterator u_it0;
	device_vector<REAL>::iterator u_it1;

	device_vector<REAL>::iterator f_it0;
	device_vector<REAL>::iterator f_it1;

	device_vector<REAL>::iterator U_it0;
	device_vector<REAL>::iterator U_it1;

	// Si necesita el puntero "crudo" (y sin halo) al array para pasarselo a un kernel de CUDA C/C++:
	REAL * u_raw_ptr;
	REAL * f_raw_ptr;
	
	// container de fuerza total: 
	device_vector<REAL> Ftot; 
	device_vector<REAL> Ftot2;

	// el rango de interés definido por los iteradores 
	device_vector<REAL>::iterator Ftot_it0;
	device_vector<REAL>::iterator Ftot_it1;
	device_vector<REAL>::iterator Ftot2_it0;
	device_vector<REAL>::iterator Ftot2_it1;
	REAL Dt;



}


void sistema::initialize()
{
	//////////////////////// condición inicial 
	std::ifstream initin("initial_condition.dat");
	std::string msg; initin >> msg;
	if(!initin.good())
	{
		fill(U_it0,U_it1,0.0); //posicion
		fill(u_it0,u_it1,dwM2);//velocidad
		fill(f_it0,f_it1,0.0); //fuerza de pinning
	}
	else{
		REAL uu, UU, ff;
		for(int n=0;n<L;n++){
			initin >> uu >> UU >> ff;
			u[n+1]=uu;	
			U[n]=UU;
			f[n]=ff;
		}
	}
	fill(tow.begin(),tow.end(),1);
	//////////////////////////////////////////	
}

