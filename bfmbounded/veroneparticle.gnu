a=0.5
fx(x,y)=a*x+(1-a)*y
stats 'hist1part_parcial.dat' index 282 u ($3*fx($1,$2)) prefix "A"
stats 'hist1part_parcial.dat' index 282 u ($3*fx($1,$2)**2) prefix "B"
stats 'hist1part_parcial.dat' index 282 u ($3*fx($1,$2)**3) prefix "C"
stats 'hist1part_parcial.dat' index 282 u ($3*fx($1,$2)**4) prefix "D"

print "<S>=", A_sum
print "<S^2>=", B_sum
print "<S^3>=", C_sum
print "<S^4>=", D_sum


set title "dt=0.0001"
set label 1 sprintf("<S>=%f",A_sum) at graph 0.5,0.5
set label 2 sprintf("<S^2>=%f",B_sum) at graph 0.5,0.4
set label 3 sprintf("<S^3>=%f",C_sum) at graph 0.5,0.3
set label 4 sprintf("<S^4>=%f",D_sum) at graph 0.5,0.2

set logs
set xlabel 'S'
set ylabel 'P(S)'
f(x)=(1.0/(2.0*sqrt(pi)*x**1.5))*exp(-(1-x)**2/(4*x))
plot [:][1e-7:] \
'hist1part_parcial.dat' index 282 u 2:($3/($2-$1)) w p t '2820000 samples', \
f(x) t 'exact'
unset logs 