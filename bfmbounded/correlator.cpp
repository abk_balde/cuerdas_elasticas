#include<iostream>
#include<fstream>
#include<vector>
#include<cstdlib>
using namespace std;
int main(int argc, char **argv)
{

	if(argc!=7){ 
		cout << "usage: ./a.out nmax dn jss dw L file\n";
		exit(1);
	}	


	int nmax=atoi(argv[1]);
	int dn=atoi(argv[2]);
	int jss=atoi(argv[3]);
	double dw=atof(argv[4]);
	double L=atof(argv[5]);

	cout << "#maximun distance for correlation " << nmax << "\n";
	cout << "#distance step for correlation " << dn << "\n";
	cout << "#starting avalanche " << jss << "\n";
	cout << "#dw " << dw << "\n";
	cout << "#L " << L << "\n";

	string file_nom(argv[6]);

	ifstream file_in(file_nom.c_str());	
	vector<double> v;
	while(file_in.good())
	{
		double s;
		file_in >> s;
		v.push_back((dw-s/L));
	};	

	int N=v.size()-1; // number of avalanches
	cout << "#total number of avalanches" << N << "\n";

	vector<double> Correlator(nmax);

	for(int n=1;n<nmax;n+=dn){// correlator array
		Correlator[n]=0.0f;
		double sum=0.0f;
		int count=0;
		for(int j=jss;j<N-n;j++){ // j in [jss,N-n) punto de partida de la ventanita
			double sum2=0.0f;
			for(int i=j;i<j+n;i++){ // reduccion de v en [j,j+n), ventanitas de n
				sum2+=v[i];	
			}
			sum+=sum2*sum2; // sum = sum + (sum_n v[i])*(sum_n v[i])
			count++;	
		}
		Correlator[n]=sum/2.0/count; 
	}

	for(int n=1;n<nmax;n+=dn) 
	cout << n << " " << Correlator[n] << endl;		

	return 1;
}
