res

set multi lay 2,2

set xla 'v'; 
set yla 'P(v)'; 

file1d='runs/1d_m1e-5/histograma_velocidades_cm_last.dat'
file2d='runs/2d_m1e-4/histograma_velocidades_cm_last.dat'

stats file1d u 3 name 'file1d'
stats file2d u 3 name 'file2d'

set key bot left

set logs
plot [0.00005:0.5] \
file1d u ($1):($3/file1d_max) w lp t '1d, L=1024, m2=1e-5', \
file2d u ($1):($3/file2d_max) w lp t '2d L=32, m2=1e-4',\
0.9*x**0.45, 0.003/x**0.32




file1dS='runs/1d_m1e-5/histograma_avaS.dat'
file2dS='runs/2d_m1e-4/histograma_avaS.dat'
set xla 'S'; set yla 'P(S)'

p file1dS u 1:3 t '1d' , file2dS  u 1:3 t '2d',\
x**(-2+2/(1+1.25)), x**(-2+2/(2+0.75))

file1dL='runs/1d_m1e-5/histograma_avaL.dat'
file2dL='runs/2d_m1e-4/histograma_avaL.dat'
set xla 'L'; set yla 'P(L)'

p file1dL u 1:3 t '1d' , file2dL  u 1:3 t '2d', x**(-1.25), x**(-0.75)


unset logs

unset multi