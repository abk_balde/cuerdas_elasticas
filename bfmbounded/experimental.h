struct _fuerza_
{
    RNG rng;       // random number generator
    long tiempo;   // parámetro
    REAL noiseamp; // parámetro
		
    _fuerza_()
    {
	noiseamp=sqrt(TEMP/Dt);
	tiempo=13;
    }; 

    __device__
    REAL operator()(long tt)
    {	
	// thread/particle id
	uint32_t tid=(tt); 

	// keys and counters 
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	k[0]=tid; 	  //  KEY = {threadID} 
	c[1]=SEED; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}

	// LAPLACIAN
	REAL um1=1.0; 
	REAL u=1.1;
	REAL up1=1.2;
	REAL laplaciano = um1 + up1 - 2.*u;

	// DISORDER
#ifndef NODISORDER
	REAL firstRN, secondRN;
	int U=int(u);
	c[0]=uint32_t(U); // COUNTER={U,GLOBAL SEED}
	r = rng(c, k);
	firstRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	c[0]=uint32_t(U+1); // COUNTER={U+1,GLOBAL SEED}
	r = rng(c, k);
	secondRN=(u01_closed_closed_32_53(r[0])-0.5); // alternativa: box_muller(r);
	REAL quenched_noise = D0*(firstRN - (firstRN-secondRN)*(u-U));// linearly interpolated force
#endif
	// THERMAL NOISE
	REAL thermal_noise;

// TODO: agregar -DFINITETEMPERATURE en el Makefile para agregar 
// fluctuaciones termicas pero antes corregir el FIXME siguiente! 
#ifdef FINITETEMPERATURE
	// FIXME: Lo de abajo tiene un error grave!!. Corrijalo.
	// c[0]=tid; // COUNTER={tid, GLOBAL SEED #2} 
	// SOLUCION: lo correcto es usar el indice del tiempo como counter! (el ruido termico NO se repite).
	c[0]=uint32_t(tiempo); // COUNTER={tiempo, GLOBAL SEED #2}

	c[1]=SEED2; // para evitar correlaciones entre el ruido térmico y el desorden congelado...
	r = rng(c, k);
	thermal_noise = noiseamp*box_muller(r);    			
#else
	thermal_noise=0.0;
#endif
	// Fuerza total en el monómero tid
	return (laplaciano+quenched_noise+thermal_noise+F0);
  }
};

// Explicit forward Euler step: lo mas simple que hay 
// (pero ojo con el paso de tiempo que no sea muy grande!)
struct _euler_
{
    __device__
    REAL operator()(REAL u_old, REAL force)
    {	
	return (u_old + force*Dt);
	
    }
};	


