set multi lay 2,1

l=512

f(x)=ucm
fit f(x) 'configuraciones.dat' index i u 0:1 via ucm

m=50
plot [0:l][-500:500] \
'configuraciones.dat' index i u 0:($1-ucm):($2-ucm) w filledcu t '', \
'' index (i-m):(i+m) u 0:($1-ucm) w l t '' lt 0, \
'' index (i-m):(i+m) u 0:($2-ucm) w l t '' lt 0

#set log y 
plot 'last_avalanche.dat' index i u 0:1 w l t sprintf('avalanche #%d',i)
#unset log y 

unset multi

pause mouse key
#print MOUSE_KEY
if(MOUSE_KEY==1009) i=i+1; reread
if(MOUSE_KEY==1011) i=(i>1)?(i-1):(i); reread

