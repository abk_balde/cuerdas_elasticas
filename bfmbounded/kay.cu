#include <cmath>
#include <fstream>
#include <iostream>
#include <stdlib.h>     /* srand, rand */

/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG

#include <thrust/transform.h>
#include <thrust/device_vector.h>
#include <thrust/iterator/counting_iterator.h>

#include "cutil.h"
#include <cufft.h>
typedef cufftReal REAL;
typedef cufftComplex COMPLEX;

// Box muller transformation
// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
__device__
float box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	float u1 = u01_open_closed_32_53(r_philox[0]);
  	float u2 = u01_open_closed_32_53(r_philox[1]);

	if(u1<=float(0.0f)) printf("error in box muller: u1<=0\n");
  	float r = sqrt( -2.0*log(u1) );
  	float theta = 2.0*M_PI*u2;
	return r*sin(theta);    			
}


// Gaussian uncorrelated random number generator
// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
struct gaussian_RNS
{
    RNG rng;       // random number generator
    int global_seed;
    float mean, dispersion;

    gaussian_RNS(int global_seed_, float mean_, float dispersion_):
    global_seed(global_seed_),mean(mean_), dispersion(dispersion_)
    {}

    __device__
    float operator()(int tt)
    {
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
		RNG::ctr_type r;

		k[0]=tt; 	  
		c[1]=global_seed; 	  
		r = rng(c, k);

		return mean+float(box_muller(r))*dispersion;  	  	
    } 	
};


using namespace thrust;

void randnormal(int global_seed, float mean, float dispersion, device_vector<float> &X)
{
	// filling V with gaussian RN in parallel
	int N=X.size();
	transform
	(
		make_counting_iterator(0),make_counting_iterator(N),
		X.begin(),gaussian_RNS(global_seed, mean, dispersion)
	);
}


void print_array(device_vector<float> &X)
{
	for(int i=0;i<X.size();i++){
		std::cout << X[i] << std::endl;
	}	
}

void print_array(device_vector<COMPLEX> &X)
{
	for(int i=0;i<X.size();i++){
		COMPLEX aux=X[i];
		std::cout << aux.x << " " << aux.y << std::endl;
	}	
}


int main(int argc, char **argv)
{

	int N=50;
	int seed=0;

	// first command line argument
	if(argc==3){
		int N=atoi(argv[1]);
		int seed=atoi(argv[2]);
	}


	// declaration and memory allocation for a gpu float array of size 2N 
	device_vector<float> V(2*N);	

	randnormal(seed, 0.0, 1.0, V);	

	print_array(V);

	///////////////////////////////
	cufftHandle plan_fft;
	CUFFT_SAFE_CALL(cufftPlan1d(&plan_fft,int(V.size()),CUFFT_R2C,1));

	int Ncomp=int(V.size())/2+1;
	device_vector<COMPLEX> fourier_array(Ncomp);
	
	COMPLEX *output=(COMPLEX *) thrust::raw_pointer_cast(fourier_array.data());
	REAL *input=(REAL *) thrust::raw_pointer_cast(V.data());

	CUFFT_SAFE_CALL(cufftExecR2C(plan_fft, input, output));

	print_array(fourier_array);
	///////////////////////////////

	return 0;
}





