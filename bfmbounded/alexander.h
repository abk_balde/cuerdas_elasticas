/*
=============================================================================================
A. B. Kolton
=============================================================================================

Version paralelizada del codigo para simular el modelo de A. Dobrinevsky.
Version 3
*/

#include "timer.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <cstdio>
#include <gsl/gsl_histogram.h>
#include "histogram_extra.h"

#include <cuda.h>
#include <cuda_runtime.h>
//#include <helper_cuda.h>


/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG
#define MAXCOUNTER	2147483647	


/* algunos headers de la libreria thrust */
// https://github.com/thrust/thrust/wiki/Quick-Start-Guide
// https://github.com/thrust/thrust/wiki/Documentation
#include <thrust/transform.h>
#include <thrust/for_each.h>
#include <thrust/copy.h>
#include <thrust/replace.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <thrust/functional.h>
#include <thrust/sequence.h>
#include <thrust/fill.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/reduce.h>
#include <thrust/count.h>
#include <thrust/system_error.h>
#include <thrust/logical.h> 
#include <thrust/execution_policy.h>

#include "histo.h"

#include "LRfourier.h"

#ifdef USE_NVTX
#include <nvToolsExt.h>

const uint32_t colors[] = { 0x0000ff00, 0x000000ff, 0x00ffff00, 0x00ff00ff, 0x0000ffff, 0x00ff0000, 0x00ffffff };
const int num_colors = sizeof(colors)/sizeof(uint32_t);

#define PUSH_RANGE(name,cid) { \
	int color_id = cid; \
	color_id = color_id%num_colors;\
	nvtxEventAttributes_t eventAttrib = {0}; \
	eventAttrib.version = NVTX_VERSION; \
	eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE; \
	eventAttrib.colorType = NVTX_COLOR_ARGB; \
	eventAttrib.color = colors[color_id]; \
	eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII; \
	eventAttrib.message.ascii = name; \
	nvtxRangePushEx(&eventAttrib); \
}
#define POP_RANGE nvtxRangePop();
#else
#define PUSH_RANGE(name,cid)
#define POP_RANGE
#endif


/* parámetros default del problema */

#ifdef TWODIMENSIONAL
	#define TAMANIO (TAMANIOX*TAMANIOX)
#elif defined(THREEDIMENSIONAL)
	#define TAMANIO (TAMANIOX*TAMANIOX*TAMANIOX)
#else
	#define TAMANIO (TAMANIOX)
#endif

#ifndef TAMANIO
#define L	4096   // numero de partículas/monomeros
#else
#define L	TAMANIO   // numero de partículas/monomeros
#endif
#ifndef TIEMPORUN
#define TRUN	100000       // numero de iteraciones temporales 
#else
#define TRUN	TIEMPORUN    // numero de iteraciones temporales 
#endif
#ifndef TPROP
#define TPROP	1000 // intervalo entre mediciones
#endif
#ifndef TVELOCITY
#define TVELOCITY	100 // intervalo entre mediciones de velocidad
#endif
#ifndef SEED
#define SEED 	12345678 // global seed RNG (quenched noise)
#endif

//#define SEED2 	12312313 // global seed#2 RNG (thermal noise)

//Alex email: "For my best simulations I typically used m^2 ~ 0.0001, a ~ 5, dt ~ 0.002,  dw*m^2 ~ 0.001"
#ifndef AAA
#define AAA 1.0
#endif

#ifndef BBB
#define BBB AAA
#endif

#ifndef M2
#ifndef LONGRANGEELASTICITY
#define M2  (100.0/(TAMANIOX*TAMANIOX)) /* ojo!, solo valido para SR */
#else 
#define M2  (10.0/(TAMANIOX)) /* ojo!, solo valido para SR */
#endif
#endif

#ifndef TIMESTEP
#define TIMESTEP	 0.001
#endif

#ifndef TSAVESTATE
#define TSAVESTATE	 1000000
#endif

#ifndef dwM2
#define dwM2	0.001
#endif

#ifndef PARABOL_V
#define PARABOL_V 	0.0 // imposed mean velocity
#endif


#ifndef MAXNROAVALANCHES
#define MAXNROAVALANCHES 10000
#endif

#define VERYSMALLVELOCITY	0.0000001

#ifndef MINAVALANCHESIZE
#define MINAVALANCHESIZE	(dwM2*L*TIMESTEP)
#endif
//#define MINAVALANCHESIZE	0.01

#ifndef MINAVALANCHESIZEFORPRINTING
#define MINAVALANCHESIZEFORPRINTING	(MINAVALANCHESIZE*100)
#endif

#define MINAVALANCHESIZEFORVELOCITIES MINAVALANCHESIZE

#define TIMINGS	10000

#include "numrec.h"

// para evitar poner "thrust::" a cada rato al llamar sus funciones
using namespace thrust;

// precisón elegida para los números reales
typedef float 	REAL;

// para generar números aleatorios gausianos a partir de dos uniformes
// http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
__device__
REAL box_muller(RNG::ctr_type r_philox)
{
	// transforma el philox number a dos uniformes en (0,1]
 	REAL u1 = u01_open_closed_32_53(r_philox[0]);
  	REAL u2 = u01_open_closed_32_53(r_philox[1]);

	if(u1<=REAL(0.0f)) printf("error en box muller: u1<=0\n");
  	REAL r = sqrt( -2.0*log(u1) );
  	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);    			
}

#ifdef PRINTCONFS
// para imprimir configuraciones (raleadas y cada tanto...)
void print_configuration(device_vector<REAL> &U, device_vector<REAL> &Ubefore, int size, std::ofstream &configs_out)
{
	//std::ofstream configs_out("configuraciones.dat");
	long i=0;
	int every = int(L*1.0/size);
	if(every==0) every=1;
	for(i=0;i<U.size();i+=every)
	{
		configs_out << U[i] << " " << Ubefore[i] << std::endl;
	}
	configs_out << "\n\n";
}

// para imprimir configuraciones cada tanto...
std::ofstream velostop_out("velocity_near_stop.dat");
void print_velocities(device_vector<REAL> &x, int size, int t)
{
	long i=0;
	int every = int(L*1.0/size);
	if(every==0) every=1;
	for(i=1;i<x.size()-1;i+=every)
	{
		float xi=x[i];
		float xip1=x[(i+1)%L];
		
		bool c1=xi>VERYSMALLVELOCITY && xip1<VERYSMALLVELOCITY;
		bool c2=xi<VERYSMALLVELOCITY && xip1>VERYSMALLVELOCITY;
		
		if(c1 || c2) velostop_out << i << " " << t << " ";

		if(c1) velostop_out << 1 << std::endl;
		if(c2) velostop_out << -1 << std::endl;
	}
	//velostop_out << "\n\n";
}
#endif


/////////////////////////////////////////////////////////////////////////////
// FUNCTORS usados en los algoritmos TRANSFORM

struct genera_uniforme
{
    RNG rng;       // random number generator
    __device__
    REAL operator()(long tt)
    {
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	k[0]=tt; 	  
	c[1]=SEED; 	  
	r = rng(c, k);

	return REAL(u01_open_closed_32_53(r[0]));  	  	
    } 	
};


struct fuerza_alexander
{
    RNG rng;       // random number generator
    long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL Dt;	

    fuerza_alexander(REAL _Dt, unsigned long long _t):tiempo(_t),Dt(_Dt)
    {};

    __device__
    tuple<REAL,REAL,long,long> operator()(tuple<long,REAL,REAL,REAL, REAL, unsigned long,unsigned long> tt)
    {
	// thread/particle id
	uint32_t tid=get<0>(tt);

	// LAPLACIAN
	REAL um1=get<1>(tt);
	REAL u=get<2>(tt);
	REAL up1=get<3>(tt);
	REAL f=get<4>(tt);
	unsigned long tiempo_propio=get<5>(tt);
	unsigned long tiempo_propio2=get<6>(tt);

	// DYNAMICALLY PRODUCED DISORDER ///////////////////////

	#ifndef EXACTSTOCHASTIC
	// keys and counters
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	k[0]=tid; 	  //  KEY = {threadID}
	c[1]=SEED; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}
	c[0]=uint32_t(tiempo_propio); // COUNTER={tiempo, GLOBAL SEED #2}
	//c[1]=SEED2; // para evitar correlaciones entre el ruido térmico y el desorden congelado...

	REAL noise=0.0f;
	r = rng(c, k);
	//noise = (u>0.0f)?(box_muller(r)*sqrt(2.0f*u*Dt*AAA)):(0.0f);
	noise = box_muller(r)*sqrt(2.*fabs(u)*Dt*A);
	tiempo_propio++;

	REAL delta_f = (u>=0.0)?(noise-AAA*u*f*Dt):(0.0f); 
	REAL delta_udot = (um1 + up1 -(2.0f+M2)*u)*Dt + delta_f; 


	////////////////////////////////////////////////////////

	#else // EXACTSTOCHASTIC 

	REAL uu=0.0f;
	REAL ff=0.0f;
	
	// operator splitting
	// d(u,F)/dt = L o (u,F) = L1 o (u,F) + L2 o (u,F) 
	// (u',F') = U1 o (u,F)  
	// (u'',F'') = U2 o (u',F') -> (du, dF) = U2 o (u',F') - (u, f) = L2 o (u', F')  

	// First operator: d(u,F)/dt = sqrt(A u) \xi (1,1) = L1 o (u,f)
	REAL arg=(u)/(AAA*Dt);	
	int poisson=(arg>=0.0f)?(int(poidev(arg, tid, tiempo_propio, tiempo_propio2))):(0);
	uu = (poisson==0)?(0.0):(AAA*Dt*gamdev(poisson,tid,tiempo_propio, tiempo_propio2));
	ff = f+uu-u;	

	// Second operator: d(u,F)/dt =  = L2 (u,f)
	REAL delta_f = (uu-u)-AAA*uu*ff*Dt; 

	#ifndef NONINTERACTING
	REAL delta_udot = (um1 + up1 -(2.0f+M2)*uu)*Dt; 
	#else
	REAL delta_udot = -M2*uu*Dt; 	
	#endif

	#endif

	// Fuerza total en el monómero tid
	return make_tuple(delta_udot, delta_f,tiempo_propio, tiempo_propio2);
  }
};

// this functor only calculate the deterministic forces
struct fuerza_deterministic_alexander
{
    REAL Dt;	
    REAL V0;	
    fuerza_deterministic_alexander(REAL _Dt, REAL _V0):Dt(_Dt),V0(_V0){};

    __device__
    tuple<REAL,REAL> operator()(tuple<REAL, REAL, REAL, REAL> tt)
    {
	// LAPLACIAN
	REAL udotm1=get<0>(tt);
	REAL udot=get<1>(tt);
	REAL udotp1=get<2>(tt);
	REAL f=get<3>(tt);

	REAL delta_f = -BBB*udot*f*Dt; 

#ifndef NONINTERACTING
	REAL delta_udot = (udotm1 + udotp1 -(2.0f+M2)*udot + M2*V0)*Dt + delta_f; 
#else
	// we ignore neighbord (simple but inefficient implementation)
	REAL delta_udot = -M2*udot*Dt + delta_f + M2*V0*Dt; 
#endif
	// Fuerza total en el monómero tid
	return make_tuple(delta_udot, delta_f);
  }


};

// this functor only calculate the deterministic forces
struct fuerza_deterministic_alexander_2D
{
    REAL Dt;	
    REAL V0;	
    REAL *udot_mat;
    fuerza_deterministic_alexander_2D(REAL _Dt, REAL _V0, REAL *udot_mat_):
    Dt(_Dt),V0(_V0),udot_mat(udot_mat_){};

    __device__
    tuple<REAL,REAL> operator()(int index, REAL f)
    {
    int Lx=TAMANIOX;

    int x=index%Lx;
    int y=(index/Lx)%Lx;
    int z=(index/(Lx*Lx))%Lx;

    int xp1=(x+1)%Lx;
    int yp1=(y+1)%Lx;
    int zp1=(z+1)%Lx;
    int xm1=(x-1+Lx)%Lx;
    int ym1=(y-1+Lx)%Lx;
    int zm1=(z-1+Lx)%Lx;


    int cen= x + Lx*y + Lx*Lx*z;
    int der= xp1 + Lx*y + Lx*Lx*z;	
    int izq= xm1 + Lx*y + Lx*Lx*z;	
    int arr= x + Lx*ym1 + Lx*Lx*z;	
    int aba= x + Lx*yp1 + Lx*Lx*z;	
    int ade= x + Lx*y + Lx*Lx*zp1;	
    int atr= x + Lx*y + Lx*Lx*zm1;	

	REAL delta_f = -BBB*udot_mat[cen]*f*Dt; 

#ifndef NONINTERACTING

	#ifdef TWODIMENSIONAL	
	REAL delta_udot = 
	(
		udot_mat[arr] + udot_mat[aba] 
		+ udot_mat[der] + udot_mat[izq] 
		-(4.0f+M2)*udot_mat[cen] + M2*V0
	)*Dt + delta_f; 
	#elif defined(THREEDIMENSIONAL)
	REAL delta_udot = 
	(
		udot_mat[arr] + udot_mat[aba] 
		+ udot_mat[der] + udot_mat[izq] 
		+ udot_mat[atr] + udot_mat[ade]
		-(6.0f+M2)*udot_mat[cen] + M2*V0
	)*Dt + delta_f; 
	#else //1D	
	REAL delta_udot = (
		udot_mat[der] + udot_mat[izq] 
		-(2.0f+M2)*udot_mat[cen] + M2*V0)*Dt + delta_f; 
	#endif
#else
	// we ignore neighbord (simple but inefficient implementation)
	REAL delta_udot = -M2*udot_mat[cen]*Dt + delta_f + M2*V0*Dt; 
#endif
	// Fuerza total en el monómero tid
	return make_tuple(delta_udot, delta_f);
  }


};



// this functor only calculate the deterministic forces
struct fuerza_deterministic_alexander_long_range
{
    REAL Dt;	
    REAL V0;	
    REAL *udot_mat;
    REAL *fel;
    fuerza_deterministic_alexander_long_range(REAL _Dt, REAL _V0, REAL *udot_mat_, REAL *fel_):
    Dt(_Dt),V0(_V0),udot_mat(udot_mat_),fel(fel_){};

    __device__
    tuple<REAL,REAL> operator()(int index, REAL f)
    {
    int cen= index;

	REAL delta_f = -BBB*udot_mat[cen]*f*Dt; 

#ifndef NONINTERACTING
	REAL delta_udot = (fel[cen]-M2*udot_mat[cen] + M2*V0)*Dt + delta_f; 
#else
	// we ignore neighbord (simple but inefficient implementation)
	REAL delta_udot = -M2*udot_mat[cen]*Dt + delta_f + M2*V0*Dt; 
#endif
	// Fuerza total en el monómero tid
	return make_tuple(delta_udot, delta_f);
  }

};



// This makes the stochastic update
struct stochastic_update_alexander
{
    RNG rng;       // random number generator
    unsigned long long tiempo;   // parámetro
    REAL noiseamp; // parámetro
    REAL Dt;	

    stochastic_update_alexander(REAL _Dt, unsigned long long _t):tiempo(_t),Dt(_Dt)
    {};

   //stochastic_update_alexander	
    __device__
    tuple<REAL,REAL,unsigned long,unsigned long> operator()(tuple<long,REAL,REAL,unsigned long,unsigned long> tt)
    {
	// thread/particle id
	uint32_t tid=get<0>(tt);

	// LAPLACIAN
	REAL u=get<1>(tt);
	REAL f=get<2>(tt);
	unsigned long tiempo_propio=get<3>(tt);
	unsigned long tiempo_propio2=get<4>(tt);

	// DYNAMICALLY PRODUCED DISORDER

	#ifndef EXACTSTOCHASTIC
	// keys and counters
    RNG::ctr_type c={{}};
    RNG::key_type k={{}};
	RNG::ctr_type r;

	k[0]=tid; 	  //  KEY = {threadID}

	//c[1]=SEED; 	  // COUNTER[1] = {a fijar mas tarde, GLOBAL SEED}
	//c[0]=uint32_t(tiempo_propio); // COUNTER={tiempo, GLOBAL SEED #2}

	c[1]=uint32_t(tiempo_propio2); 	  // FIX 
	c[0]=uint32_t(tiempo_propio); // 

	REAL noise;
	r = rng(c, k);
	noise = box_muller(r)*sqrt(2.*fabs(u)*Dt*A);
	 // should be bounded
	if(tiempo_propio+1<MAXCOUNTER)
	tiempo_propio++;
	else //counter con dos agujas: horas y minutos...
	{tiempo_propio=1;tiempo_propio2++;}

	REAL new_f = f + noise; 
	REAL new_udot = u + noise; 

	#else
	REAL arg=(u)/(AAA*Dt);	
	int poisson=(arg>=0.0f)?(int(poidev(arg, tid, tiempo_propio, tiempo_propio2))):(0);
	REAL new_udot = (poisson<1)?(0.0):(AAA*Dt*gamdev(poisson,tid,tiempo_propio,tiempo_propio2));
	REAL new_f = f + new_udot - u;
	#endif

	// Fuerza total en el monómero tid
	return make_tuple(new_udot, new_f,tiempo_propio, tiempo_propio2);
  }
};



struct euler_alexander
{
    REAL Dt;	
    euler_alexander(REAL _Dt):Dt(_Dt){};
    __device__
    tuple<REAL,REAL,REAL> operator()(tuple<REAL, REAL, REAL, REAL, REAL> tt)
    {
    	REAL udot_old=get<0>(tt);
    	REAL delta_udot=get<1>(tt);
   
 	REAL fold=get<2>(tt);
    	REAL delta_f=get<3>(tt);

    	REAL Uold=get<4>(tt);

	REAL udot_new = udot_old + delta_udot; 
	//if(unew<0.0) unew=0.0f; esto produce un drift

	REAL fnew=fold + delta_f;
	REAL Unew=Uold + 0.5*(udot_new+udot_old)*Dt;

    	return make_tuple(udot_new, fnew, Unew);
    }
};

// only for testing
struct gammapoisson
{
    REAL Dt;	
    gammapoisson(REAL _Dt):Dt(_Dt){};
    //REAL operator()(int tid, long tiempo_propio){
    template <typename Tuple>
    __device__
    void operator()(Tuple tt)
    {
	int tid=get<0>(tt);
	unsigned long tiempo_propio=get<1>(tt);
	unsigned long tiempo_propio2=get<2>(tt);
	REAL arg=(get<3>(tt))/(AAA*Dt);	

	REAL noise=0.0f;
	int poisson=int(poidev(arg, tid, tiempo_propio,tiempo_propio2));
	if(poisson>0) noise=AAA*Dt*gamdev(poisson,tid,tiempo_propio,tiempo_propio2)-get<3>(tt);		
	
	get<1>(tt)=tiempo_propio;
	get<2>(tt)=tiempo_propio2;
	get<3>(tt)=get<3>(tt)+noise;
    }	
};

// for computing the roughness
struct roughtor
{
	REAL ucm;
    	roughtor(REAL _Ucm):ucm(_Ucm){};
    	__host__ __device__ 
	REAL operator()(REAL x) const
    	{
   	   return (x-ucm)*(x-ucm);
    	}
};


// for testing random mixture...
#include <thrust/iterator/counting_iterator.h>
/*
void test()
{

	const int LL=10000000;
	device_vector<REAL> D(LL); //, D2(LL);
	device_vector<long> tow(LL);  // tiempo propio para generar random numbers... 
	device_vector<long> tow2(LL);  // tiempo propio para generar random numbers... 

	fill(tow.begin(),tow.end(),1);
	fill(tow2.begin(),tow2.end(),1);
	
	for(int n=0;n<1000;n++){
		//fill(D.begin(),D.end(),0.001f);
		counting_iterator<long> first(n*LL);
		counting_iterator<long> last = first + LL;
		transform(first,last,D.begin(), genera_uniforme());

		for_each(
			make_zip_iterator(
			make_tuple(make_counting_iterator(0),tow.begin(),tow2.begin(), D.begin())
			),
			make_zip_iterator(
			make_tuple(make_counting_iterator(LL),tow.end(),tow2.begin(), D.end())
			),
			gammapoisson(0.01f)	
		);
		//for(int i=0;i<LL;i++){
		//std::cout << D[i] << std::endl;
		//}
	}


	const int NBINS=100;

	device_vector<REAL> H(NBINS);
	dense_histogram_data_on_device(D,H, 0.0f, 4.0f);
	std::ofstream fout("histograma.dat");
	print_histograma(H, 0.0f, 4.0f, fout);
	exit(1);

}
*/

/*
struct sumapositivos
{
	__device__
	REAL operator()(REAL x1, REAL x2)
	{
		return REAL((x1>0.0f)*x1+(x2>0.0f)*x2);
	}
};
*/

struct is_less_than_zero
{
  __host__ __device__
  bool operator()(REAL x)
  {
    return (x < 0.0);
  }
};

/*
struct divide_tupla: public thrust::unary_function< tuple<REAL, REAL>,REAL>
{
	REAL dt;
	divide_tupla(REAL _dt):dt(_dt){};
	__device__ 
	REAL operator()(tuple<REAL,REAL> tt){
		if(fabsf(get<0>(tt))>0)
		return REAL(fabsf(dt*get<1>(tt)/get<0>(tt)));
		else return 	 
	}
};
*/

#ifdef OMP
#include <omp.h>
#endif

bool steadystate;

void initialize(
device_vector<REAL> &u, device_vector<REAL> &f, device_vector<REAL> &U,
device_vector<unsigned long> &tow, device_vector<unsigned long> &tow2
)
{
	device_vector<REAL>::iterator u_it0 = u.begin();
	device_vector<REAL>::iterator u_it1 = u.end();
	device_vector<REAL>::iterator f_it0 = f.begin();
	device_vector<REAL>::iterator f_it1 = f.end();
	device_vector<REAL>::iterator U_it0 = U.begin();
	device_vector<REAL>::iterator U_it1 = U.end();
	steadystate=0;

	//////////////////////// condición inicial 
	std::ifstream initin("initial_condition.dat");
	std::string msg; initin >> msg;
	if(!initin.good())
	{
		fill(U_it0,U_it1,REAL(0.0)); //posicion
		fill(u_it0,u_it1,REAL(dwM2));//velocidad
		//fill(u_it0,u_it1,REAL(0.0));//velocidad
		fill(f_it0,f_it1,REAL(0.0)); //fuerza de pinning
		fill(tow.begin(),tow.end(),(unsigned long)(1));
		fill(tow2.begin(),tow2.end(),(unsigned long)(SEED));
		std::cout  << "new initial condition\n";
		std::cout.flush();
	}
	else{
		std::cout << "initial condition from previous run\n";
		std::cout.flush();
		REAL uu, UU, ff, t1, t2;
		std::string s1;
		std::getline(initin,s1);
		for(int n=0;n<L;n++){
			initin >> uu >> UU >> ff >> t1 >> t2;
			u[n]=uu;	
			U[n]=UU;
			f[n]=ff;
			tow[n]=t1;
			tow2[n]=t2;
		}
		steadystate=1;
	}
}

void save_state(device_vector<REAL> &u, device_vector<REAL> &f, device_vector<REAL> &U, 
device_vector<unsigned long>&tow, device_vector<unsigned long>&tow2, char*mensaje)
{
	device_vector<REAL>::iterator u_it0 = u.begin();
	device_vector<REAL>::iterator u_it1 = u.end();
	device_vector<REAL>::iterator f_it0 = f.begin();
	device_vector<REAL>::iterator f_it1 = f.end();
	device_vector<REAL>::iterator U_it0 = U.begin();
	device_vector<REAL>::iterator U_it1 = U.end();
	std::ofstream initout("restart.dat");
	REAL uu,UU,ff;
	unsigned long t1, t2;

	std::cout << "saving current state\n";
	initout << mensaje << "\n";
	for(int n=0;n<L;n++){
		uu=u[n];	
		UU=U[n];
		ff=f[n];
		t1=tow[n];
		t2=tow2[n];
		
		initout << uu << " " << UU << " " << ff << " " << t1 << " " << t2 << "\n";
	}
	initout.flush();
};

void save_state(device_vector<REAL> &u, device_vector<REAL> &f, device_vector<REAL> &U, 
device_vector<unsigned long>&tow, device_vector<unsigned long>&tow2)
{
	char * msg = new char[50];
	sprintf(msg,"#after error");
	save_state(u, f, U, tow, tow2, msg);
};

//: public thrust::binary_function<REAL,REAL,REAL>
struct difference_larger_than 
{
  REAL threshold;
  difference_larger_than(REAL _threshold):threshold(_threshold){};	
  __host__ __device__
  bool operator()(REAL x, REAL y) { return (x-y>threshold); }
};

/////////////////////////////////////////////////////////////////////////////

