reset
set logs 

#plot [0.001:] 'L8192/histograma_velocidades_cm_last.dat'  u 1:($3/$1) w lp t '8192'
#min_y = GPVAL_DATA_Y_MIN
#max_y = GPVAL_DATA_Y_MAX
#print min_y, max_y

#a8192=2.95; a4096=0.66; a2048=0.67; a1024=0.65; a512=0.5
a8192=1; a4096=1; a2048=1; a1024=1; a512=1; a16384=1

vm(x)=x**(-0.2)
vmy(x)=1.0; #/vm(x)**(-0.4)

set key left
set xlabel '$v/L^{z-\zeta}$'
set ylabel '$v P(v)$'


set pointsize 0.5

 plot [0.00001:5][:20] \
'L512/histograma_velocidades_local_last.dat' u ($1/vm(512)):($3*vmy(512)/a512) w lp t '$L=512$', \
'L1024/histograma_velocidades_local_last.dat'  u ($1/vm(1024)):($3*vmy(1024)/a1024) w lp t '$L=1024$',\
'L2048/histograma_velocidades_local_last.dat'  u ($1/vm(2048)):($3*vmy(2048)/a2048) w lp t '$L=2048$',\
'L4096/steady1/histograma_velocidades_local_last.dat'  u ($1/vm(4096)):($3*vmy(4096)/a4096) w lp t '$L=4096$',\
'L8192/histograma_velocidades_local_last.dat'  u ($1/vm(8192)):($3*vmy(8192)/a8192) w lp t '$L=8192$',\
0.3*x**1.4 w l linetype 0
#'L16384/steady1/histograma_velocidades_cm_last.dat'  u ($1/vm(16384)):($3*vmy(16384)/a16384) w lp t '$L=16384$',\
#'L32768/histograma_velocidades_cm_last.dat'  u ($1/vm(32768)):($3*vmy(32768)/a16384) w lp t '$L=32768$',\

