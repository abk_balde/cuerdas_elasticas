/*
=============================================================================================
A. B. Kolton
=============================================================================================

Version paralelizada del codigo para simular el modelo de A. Dobrinevsky, K. Wiese and P. Le Doussal.
Version 3
*/

#include "alexander.h"

/////////////////////////////////////////////////////////////////////////////

double timer_fuerzas_elapsed;
double timer_euler_elapsed;
double timer_props_elapsed;
double timer_stochastic;
double timer_deterministic;
double timer_props_velocity;
double timer_props_Ucm;
double timer_props_Fcm;
double timer_props_roughness;
double timer_props_roughnessF;
double timer_props_towmax;
double timer_props_activity;
double timer_props_minmaxvelocity;
double timer_props_histograms;
double timer_props_printconfs;
void print_timmings (int);

int main(){

	//test();

	std::ofstream logfileout("logfile.dat");
	logfileout << "L= " << " " << L << std::endl;
	logfileout << "A= " << " " << AAA << std::endl;
	logfileout << "B= " << " " << BBB << std::endl;
	logfileout << "M2= " << " " << M2 << std::endl;
	logfileout << "Dt= " << " " << TIMESTEP << std::endl;
	logfileout << "dwM2= " << " " << dwM2 << std::endl;
	logfileout << "PARABOL_V= " << " " << PARABOL_V << std::endl;
	logfileout << "TSAVESTATE= " << TSAVESTATE << "\n";
	logfileout << "TAMANIO= " << TAMANIO << "\n";
	logfileout << "TAMANIOX= " << TAMANIOX << "\n";
	#ifdef TWODIMENSIONAL
	logfileout << "TWODIMENSIONAL" << "\n";
	#elif defined (THREEDIMENSIONAL)
	logfileout << "THREEDIMENSIONAL" << "\n";
	#elif defined (LONGRANGEELASTICITY)
	logfileout << "LONGRANGEELASTICITY" << "\n";
	#else
	logfileout << "ONEDIMENSIONAL" << "\n";
	#endif
	#ifndef EXACTSTOCHASTIC
	logfileout << "#Stochastic Euler method\n";
	#else
	logfileout << "#Operator splitting method\n";
	#endif
	#ifdef OMP
	int numthreads=omp_get_max_threads();
	logfileout << "#conociendo el host, OMP threads = " << numthreads << std::endl;
	//omp_set_dynamic(0);
	//omp_set_num_threads(numthreads-2);
	#else
	cudaDeviceProp deviceProp;
    int dev; cudaGetDevice(&dev);
    cudaGetDeviceProperties(&deviceProp, dev);
	logfileout << "Device " << dev << ", " << deviceProp.name << "\n"; 
    //printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);
	#endif		
	logfileout.flush();


	// file para guardar algunas propiedades dependientes del tiempo
	std::ofstream propsout("someprops.dat");
	propsout << "time velocity roughness Ucm Umin towmax vmax vmin roughnessF" << std::endl; propsout.flush();

	std::ofstream avalanche_duration_out("avalanches_T.dat");
	std::ofstream avalanche_size_out("avalanches_S.dat");
	std::ofstream configs_out("configuraciones.dat");

    	//gsl_histogram_set_ranges_uniform (histovel, VERYSMALLVELOCITY, MAXVELOCITY);
	FILE *histovelout=fopen("histograma_velocidades_cm.dat","w");
	FILE *histovellocalout=fopen("histograma_velocidades_local.dat","w");
	FILE *histovellocallocalout=fopen("histograma_velocidades_locallocal.dat","w");
	std::ofstream lastavout("last_avalanche.dat");

	std::cout << "files initialized" << std::endl;

	/*============================ containers e iteradores ========================*/
	// velocidades de los monómeros:
	// Notar que alocamos dos elementos de mas para usarlos como "halo"
	// esto nos permite fijar las condiciones de borde facilmente, por ejemplo periódicas: u[0]=u[L]; u[L+1]=u[1];
	device_vector<REAL> u(L);  // velocidades udot(x,t)
	device_vector<REAL> f(L);    // desorden dinamico f(x,t) 
	device_vector<REAL> U(L);    // desplazamientos u(x,t)
	device_vector<unsigned long> tow(L);  // tiempo propio para generar random numbers... 
	device_vector<unsigned long> tow2(L);  // tiempo propio para generar random numbers... 
	device_vector<REAL> Uold(L);    // desplazamientos u(x,t)
	device_vector<REAL> Uava0(L);    // desplazamientos u(x,t)
	

	// dos iteradores para definir el rango de interes para aplicar algoritmos 
	// (el +-1 nos permite descartar el halo en los algoritmos) 
	device_vector<REAL>::iterator u_it0 = u.begin();
	device_vector<REAL>::iterator u_it1 = u.end();

	device_vector<REAL>::iterator f_it0 = f.begin();
	device_vector<REAL>::iterator f_it1 = f.end();

	device_vector<REAL>::iterator U_it0 = U.begin();
	device_vector<REAL>::iterator U_it1 = U.end();

	// Si necesita el puntero "crudo" (y sin halo) al array para pasarselo a un kernel de CUDA C/C++:
	REAL * u_raw_ptr = raw_pointer_cast(&u[0]);
	REAL * f_raw_ptr = raw_pointer_cast(&f[0]);

	long_range_class LongRangeCalculator(u_raw_ptr,L);

	// container de fuerza total: 
	device_vector<REAL> Ftot(L); 
	device_vector<REAL> Ftot2(L);

	// el rango de interés definido por los iteradores 
	device_vector<REAL>::iterator Ftot_it0 = Ftot.begin();
	device_vector<REAL>::iterator Ftot_it1 = Ftot.end();
	device_vector<REAL>::iterator Ftot2_it0 = Ftot2.begin();
	device_vector<REAL>::iterator Ftot2_it1 = Ftot2.end();
	REAL Dt=TIMESTEP;
	std::cout << "containers initialized" << std::endl;
	/*============================ END containers e iteradores ========================*/

	std::cout << "initializing...\n"; std::cout.flush();
	initialize(u,f,U,tow,tow2);
	copy(U.begin(),U.end(),Uold.begin());
	copy(U.begin(),U.end(),Uava0.begin());

	//test();
	
	const int NBINS=200;
	const double MAXVELOCITY=2;
	const double MINVELOCITY=VERYSMALLVELOCITY;
	gsl_histogram * histovel = gsl_histogram_alloc(NBINS);
	gsl_histogram * histovellocal = gsl_histogram_alloc(NBINS);
	gsl_histogram * histovellocallocal = gsl_histogram_alloc(NBINS);
	double *range=(double *)malloc((NBINS+1)*sizeof(double));
	for(int i=0;i<NBINS+1;i++) range[i]=MINVELOCITY*powf(MAXVELOCITY/MINVELOCITY,i*1.0/NBINS);
	gsl_histogram_set_ranges (histovel, range, NBINS+1);
	for(int i=0;i<NBINS+1;i++) range[i]=MINVELOCITY*powf(MAXVELOCITY*10/MINVELOCITY,i*1.0/NBINS);
	gsl_histogram_set_ranges (histovellocal, range, NBINS+1);
	for(int i=0;i<NBINS+1;i++) range[i]=MINVELOCITY*powf(MAXVELOCITY*10/MINVELOCITY,i*1.0/NBINS);
	gsl_histogram_set_ranges (histovellocallocal, range, NBINS+1);


	const int NBINS2=50;
	gsl_histogram * histoavaL = gsl_histogram_alloc(NBINS2);
	gsl_histogram * histoavaS = gsl_histogram_alloc(NBINS2);
	gsl_histogram * histoavaT = gsl_histogram_alloc(NBINS2);
	gsl_histogram * histoavalocalS = gsl_histogram_alloc(NBINS2);
	gsl_histogram * histoavalocalT = gsl_histogram_alloc(NBINS2);
	const double MAXAVAS=powf(L,1+1.25);
	const double MINAVAS=MINAVALANCHESIZE;
	const double MAXAVAL=powf(L,1.0);
	const double MINAVAL=5.0;
	const double MAXAVAT=TIMESTEP*powf(L,1.433)*100.0;
	const double MINAVAT=TVELOCITY*TIMESTEP*5.0;
	for(int i=0;i<NBINS2+1;i++) range[i]=MINAVAS*powf(MAXAVAS/MINAVAS,i*1.0/NBINS2);
	gsl_histogram_set_ranges (histoavaS, range, NBINS2+1);
	for(int i=0;i<NBINS2+1;i++) range[i]=MINAVAT*powf(MAXAVAT/MINAVAT,i*1.0/NBINS2);
	gsl_histogram_set_ranges (histoavaT, range, NBINS2+1);
	for(int i=0;i<NBINS2+1;i++) range[i]=MINAVAL*powf(MAXAVAL/MINAVAL,i*1.0/NBINS2);
	gsl_histogram_set_ranges (histoavaL, range, NBINS2+1);
	for(int i=0;i<NBINS2+1;i++) range[i]=(MINAVAS/L)*powf(MAXAVAS/(MINAVAS/L),i*1.0/NBINS2);
	gsl_histogram_set_ranges (histoavalocalS, range, NBINS2+1);
	for(int i=0;i<NBINS2+1;i++) range[i]=MINAVAT*powf(MAXAVAT/MINAVAT,i*1.0/NBINS2);
	gsl_histogram_set_ranges (histoavalocalT, range, NBINS2+1);
	std::cout << "histograms initialized" << std::endl;


	// simple (GPU/CPU) timer (curiosear el timer.h)
	timer t, totaltime, tparts;
	timer_fuerzas_elapsed=0.0;
	timer_euler_elapsed=0.0;
	timer_props_elapsed=0.0;
	timer_stochastic=0.0;
	timer_deterministic=0.0;
	timer_props_velocity=0.0;
	timer_props_Ucm=0.0;
	timer_props_Fcm=0.0;
	timer_props_roughness=0.0;
	timer_props_roughnessF=0.0;
	timer_props_towmax=0.0;
	timer_props_activity=0.0;
	timer_props_minmaxvelocity=0.0;
	timer_props_histograms=0.0;
	timer_props_printconfs=0.0;

	long nstart=0;
	long nstartlocal=0;
	long nbigavalanchas=0;
	long navalanchas=0;
	double Snew, Sold, Slocalnew, Slocalold; 
	Sold=Slocalold=0.0;
	unsigned long long n;
	std::vector<double> avalanche_vec;
	std::vector<double> local_avalanche_vec;
	std::vector<int> fraction_vec;
	REAL v0=PARABOL_V; 
	char msg[50]; 
	REAL velocity, vlocal;

	//std::cin.ignore();

	totaltime.restart();
	//for(n=0;n<TRUN && navalanchas<MAXNROAVALANCHES;n++)
	for(n=0;nbigavalanchas<MAXNROAVALANCHES && n<TIEMPORUN;n++)
	{
		t.restart(); 

		PUSH_RANGE("DYNAMICS",2)	

		if((n+1)%TSAVESTATE==0) 
		{ 
		std::cout << "saving state..." << totaltime.elapsed() << " s\n";std::cout.flush();
		sprintf(msg,"#tiempo=%llu",n);	
		save_state(u, f, U, tow, tow2, msg);
		}

		//////////////////////////////////////////////////////////////////////////////
		// FORCES CALCULATION
		#ifndef EXACTSTOCHASTIC

		// Calcula fuerzas, imponiendo PBC en el "halo" 		
		transform(
			make_zip_iterator
			(make_tuple(
			make_counting_iterator<long>(0),u_it0-1,u_it0,u_it0+1, f_it0, tow.begin(), tow2.begin()
			)),
			make_zip_iterator
			(make_tuple(
			make_counting_iterator<long>(L),u_it1-1,u_it1,u_it1+1, f_it1, tow.end(), tow2.end()
			)),
			make_zip_iterator(make_tuple(Ftot_it0, Ftot2_it0, tow.begin(), tow2.begin())),
			fuerza_alexander(Dt,n)
		);
		#else

		//////////////////////////////////////////////////////////////////////////////		
		//STOCHASTIC UPDATE
		//http://devblogs.nvidia.com/parallelforall/cuda-pro-tip-generate-custom-application-profile-timelines-nvtx/
		PUSH_RANGE("STOCHASTIC UPDATE",1)	
		tparts.restart();
		try{
		transform(
			make_zip_iterator
			(make_tuple(
			make_counting_iterator<long>(0),u_it0, f_it0, tow.begin(), tow2.begin()
			)),
			make_zip_iterator
			(make_tuple(
			make_counting_iterator<long>(L),u_it1, f_it1, tow.end(), tow2.end()
			)),
			make_zip_iterator(make_tuple(u_it0, f_it0, tow.begin(), tow2.begin())),
			stochastic_update_alexander(Dt,n)
		); // calculates intermediante u and F at t=n+1/2
		}
		catch(thrust::system_error &e)
		{
		 // output an error message and exit
				save_state(u, f, U, tow, tow2);
    			std::cerr << "Error doing stochastic update: " << e.what() << std::endl;
    			exit(-1);
		};
		timer_stochastic+=tparts.elapsed();
		//std::cout << "Stochastic part -> " << 1e3 * tparts.elapsed() << " miliseconds" << std::endl;
		POP_RANGE

		// Impone PBC en el "halo" 		
		//PUSH_RANGE("PBC",4)	
		//POP_RANGE

		// deterministic force calculation
		PUSH_RANGE("DETERMINISTIC FORCES",2)	
		tparts.restart();

/*		#ifndef TWODIMENSIONAL
		try{
		transform(
			make_zip_iterator
			(make_tuple(
			u_it0-1,u_it0,u_it0+1, f_it0
			)),
			make_zip_iterator
			(make_tuple(
			u_it1-1,u_it1,u_it1+1, f_it1
			)),
			make_zip_iterator(make_tuple(Ftot_it0, Ftot2_it0)),
			fuerza_deterministic_alexander(Dt,v0)
		); // calculates deterministic forces to add  
		}
		catch(thrust::system_error &e)
		{
 			// output an error message and exit
			save_state(u, f, U, tow, tow2);
    			std::cerr << "Error calculating deterministic force: " << e.what() << std::endl;
    			exit(-1);
		};

		#else
*/		
		REAL *u_it0_raw=thrust::raw_pointer_cast(&u_it0[0]);

		try{
		#ifndef LONGRANGEELASTICITY	
		transform(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(TAMANIO),f_it0,
			make_zip_iterator(make_tuple(Ftot_it0, Ftot2_it0)),
			fuerza_deterministic_alexander_2D(Dt,v0,u_it0_raw)
		); // calculates deterministic forces to add  
		#else
		REAL *fel_raw=LongRangeCalculator.get_fel();
		transform(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(TAMANIO),f_it0,
			make_zip_iterator(make_tuple(Ftot_it0, Ftot2_it0)),
			fuerza_deterministic_alexander_long_range(Dt,v0,u_it0_raw,fel_raw)
		); // calculates deterministic forces to add  
		#endif
		}
		catch(thrust::system_error &e)
		{
 			// output an error message and exit
			save_state(u, f, U, tow, tow2);
    			std::cerr << "Error calculating deterministic force: " << e.what() << std::endl;
    			exit(-1);
		};
		//#endif


		timer_deterministic+=tparts.elapsed();
		//std::cout << "Deterministic part -> " << 1e3 *tparts.elapsed()  << " miliseconds" << std::endl;
		#endif
		timer_fuerzas_elapsed+=t.elapsed();
		POP_RANGE
		//////////////////////////////////////////////////////////////////////////////


		PUSH_RANGE("EULER",3)	
		// Euler update
		t.restart();
		try{
		transform(
			make_zip_iterator(make_tuple(u_it0,Ftot_it0,f_it0, Ftot2_it0, U_it0)),
			make_zip_iterator(make_tuple(u_it1,Ftot_it1,f_it1, Ftot2_it1, U_it1)),
			make_zip_iterator(make_tuple(u_it0,f_it0,U_it0)),
			euler_alexander(Dt)
		);
		}
		catch(thrust::system_error &e)
		{
 		// output an error message and exit
				save_state(u, f, U, tow, tow2);
    			std::cerr << "Error doing Euler update: " << e.what() << std::endl;
    			exit(-1);
		};
		timer_euler_elapsed+=t.elapsed();
		//std::cout << "Euler step -> " << 1e3 *t.elapsed()  << " miliseconds" << std::endl;
		POP_RANGE
		POP_RANGE









		////////////////////////////////////////////////////////////////////////////////////
		PUSH_RANGE("PROPERTIES",4)	
		////////////////////////////////////////////////////////////////////////////////////
		// PROPERTIES CALCULATION OR MEASUREMENTS
		t.restart();	
		{
			nstart++;
			nstartlocal++;

			tparts.restart();
			////////////////////////////////////
			// instantaneous center of velocity
			if(n%TVELOCITY==0)
			{
				try{ velocity = thrust::reduce(u_it0,u_it1,REAL(0.0))/REAL(L);}//center of mass velocity (SYNC)			
				catch(thrust::system_error &e)
				{
			 		// output an error message and exit
					save_state(u, f, U, tow, tow2);
			    		std::cerr << "Error calculating mean velocity: " << e.what() << std::endl;
			    		exit(-1);
				}
				catch(std::bad_alloc &e)
  				{
					save_state(u, f, U, tow, tow2);
    					std::cerr << "Ran out of memory while calcuating mean velocity" << std::endl;
    					exit(-1);
  				}
				#ifdef MEASURE_AVALANCHE_SHAPE
				vlocal=u[1];
				avalanche_vec.push_back(velocity);
				local_avalanche_vec.push_back(vlocal);
				using namespace placeholders;
				/*tparts.restart();
				int fraction=count_if(u_it0,u_it1,_1>VERYSMALLVELOCITY);
				fraction_vec.push_back(fraction);
				timer_props_activity+=tparts.elapsed();*/
				#endif
				timer_props_velocity+=tparts.elapsed();
			}

			//using namespace placeholders;
			//bool negative_velocities=any_of(u_it0,u_it1,_1<-REAL(VERYSMALLVELOCITY)); 
			

			/*if(negative_velocities) 		
  			{
    				//std::cout << "Middleton catastrophe at " << n << " with " << minvelocity << "\n" << std::endl;
				//std::cout.flush();

				// PUT negative velocities to zero
				is_less_than_zero pred; replace_if(u_it0,u_it1, pred, REAL(0.0f));			
				
				// NEW mean velocity
				REAL velocity_positive = reduce(u_it0,u_it1,REAL(0.0f))/REAL(L); 			
	
				// Rescaling: vcm = v1 + v2 - v3 + v4 -> replace -> vcm'=v1 + v2 + v4
				// -> rescale -> vcm'' = (v1 + v2 + v4)*vcm/vcm' = vcm -> OK, conserved

				// Rescaled velocities
				using namespace placeholders;
				REAL alpha=velocity/velocity_positive;
				transform(u_it0,u_it1,u_it0,_1*alpha);
				
    				//exit(-1);
			
  			}*/

			

			if(n%TPROP==0) {

				////////////////////////////////////////////////////
				//center of mass position
				using namespace placeholders;
				tparts.restart();
				REAL Ucm;				
				try{
				Ucm = reduce(U_it0,U_it1,REAL(0.0))/REAL(L);
				} 
				catch(thrust::system_error &e)
				{
		 			// output an error message and exit
		    			std::cerr << "Error calculating center of mass: " << e.what() << std::endl;
						save_state(u, f, U, tow, tow2);
		    			exit(-1);
				};
				timer_props_Ucm+=tparts.elapsed();
				//////

				////////////////////////////////////////////////////
				// roughness
				tparts.restart();
				REAL roughness; 
				try{
				roughness = transform_reduce(U_it0,U_it1,roughtor(Ucm),REAL(0.0),thrust::plus<REAL>())/REAL(L);
				} 
				catch(thrust::system_error &e)
				{
		 			// output an error message and exit
		    			std::cerr << "Error calculating roughness: " << e.what() << std::endl;
						save_state(u, f, U, tow, tow2);
		    			exit(-1);
				};

				////////////////////////////////////////////////////
				// minimum displacement
				//tparts.restart();
				REAL Umin; try{Umin = reduce(U_it0,U_it1,REAL(Ucm), minimum<REAL>());} 
				catch(thrust::system_error &e)
				{
		 		// output an error message and exit
		    			std::cerr << "Error calculating minimum: " << e.what() << std::endl;
						save_state(u, f, U, tow, tow2);
		    			exit(-1);
				};
				timer_props_roughness+=tparts.elapsed();
				/////

				////////////////////////////////////////////////////
				// minimum velocity
				tparts.restart();
				REAL minvelocity = 
				reduce(u_it0,u_it1,REAL(1.0),thrust::minimum<REAL>()); //center of mass velocity
				//bool negative_velocities=minvelocity<-REAL(VERYSMALLVELOCITY);

				////////////////////////////////////////////////////
				// maximum velocity
				REAL maxvelocity = 
				reduce(u_it0,u_it1,REAL(-1.0),thrust::maximum<REAL>()); //center of mass velocity
				// uno deberia pedir delta t << min(u_it/Ftot_it) 
				// uno deberia pedir delta t << min(f_it/Ftot2_it) 
				/*REAL deltatmax = reduce(
					make_zip_iterator(make_tuple(u_it0, Ftot_it0)),
					divide_tupla(Dt)
					),
					make_transform_iterator(
					make_zip_iterator(make_tuple(u_it1, Ftot_it1)),
					divide_tupla(Dt)
					)
				);*/

				/*if(maxvel<maxvelocity) {
					maxvel=maxvelocity;
					//Dt=(0.001/maxvel);
					//Dt=(Dt>TIMESTEP)?(TIMESTEP):(Dt);				
					//REAL Dtmax=(0.000001/maxvelocity);				
					//Dt += ((Dtmax<TIMESTEP)?(Dtmax):(TIMESTEP)-Dt)/10000.0f; 
				}*/
				// dx/dt = f(t)/tau - x(t)/tau -> x(t)=int_0^t f(t')e^{-(t-t')/tau}/tau
				// Si f(t)=f0 -> x(t) = f0 [e^{-(t-t')/tau}tau]/tau
				// x(t) = f0 [1-e^{-t/tau}] -> f0 
				// dx = f(t)/tau' - x(t)/tau'				
				//propsout << velocity/REAL(L) << " " << maxvel << std::endl; propsout.flush();
				timer_props_minmaxvelocity+=tparts.elapsed();
				/////

				////////////////////////////////////////////////////
				// maximum tow...
				tparts.restart();
				int towmax; 
				try{towmax = reduce(tow.begin(),tow.end(),(long)(0), maximum<long>());} 
				catch(thrust::system_error &e)
				{
		 		// output an error message and exit
					save_state(u, f, U, tow, tow2);
		    			std::cerr << "Error calculating maximum particle time: " << e.what() << std::endl;
		    			exit(-1);
				};
				timer_props_towmax+=tparts.elapsed();
				//////

				////////////////////////////////////////////////////
				// force on center of mass...
				tparts.restart();
				REAL Fcm;				
				try{
				Fcm = reduce(f_it0,f_it1,REAL(0.0))/REAL(L);
				} 
				catch(thrust::system_error &e)
				{
		 			// output an error message and exit
		    			std::cerr << "Error calculating center of mass: " << e.what() << std::endl;
						save_state(u, f, U, tow, tow2);
		    			exit(-1);
				};
				timer_props_Fcm+=tparts.elapsed();
			
				////////////////////////////////////////////////////
				// f-roughness
				tparts.restart();
				REAL roughnessF; 
				try{
				roughnessF = transform_reduce(f_it0,f_it1,roughtor(Fcm),REAL(0.0),thrust::plus<REAL>())/REAL(L);
				} 
				catch(thrust::system_error &e)
				{
		 			// output an error message and exit
		    			std::cerr << "Error calculating roughnessF: " << e.what() << std::endl;
						save_state(u, f, U, tow, tow2);
		    			exit(-1);
				};
				timer_props_roughnessF+=tparts.elapsed();

				////////////////////////////////////////////////////
				// PRINTING ALL PROPERTIES
				propsout << n << " " << velocity << " "
					 << roughness << " " << Ucm << " " << Umin << " " << towmax << " "
					 << maxvelocity << " " << minvelocity << " " << roughnessF 		
					 << std::endl; propsout.flush();
				
				if(isnan(velocity)) 
				{
		    		std::cerr << "Error calculating velocity (isnan) " << std::endl;
					save_state(u, f, U, tow, tow2);
					exit(-1); 
				}



				////////////////////////////////////////////////////
				// GENERAL CRITERION FOR STEADYSTATE
				if(roughnessF>0.4 && !steadystate){
				// && n>TIEMPOEQ 	
				//if(roughness>powf((1.0)/sqrt(M2),1.25) && !steadystate){ 
				//if(roughness>1.0 && !steadystate){ 
				//if(!steadystate){
					//std::ifstream senial("senial.dat");
					//std::string msg; senial >> msg;
					//if(senial.good()){
					steadystate=1;
					v0=0.0;
					std::cout << "steady state reached at " << n << ", Umin=" << Umin << std::endl;
					std::cout.flush();
					sprintf(msg,"#steady state reached");
					save_state(u, f, U, tow, tow2,msg);
					//}
				}

			}


			////////////////////////////////////////////////////
			// PROPERTIES CALCULATED ONLY IN STEADY STATE 
			// histograma de velocidades
			//if(navalanchas>23000){
			//if(navalanchas>50000){
			tparts.restart();
			if(steadystate){
				/*for(int i=0;i<1;i++) {
					REAL veli=u[i+1];	
					gsl_histogram_increment (histovellocal,veli);
				}*/

				// saves partial histograms every once in a while
				if(n%(TPROP*10)==0){
					FILE *histovellastout=fopen("histograma_velocidades_cm_last.dat","w");
					FILE *histovellocallastout=fopen("histograma_velocidades_local_last.dat","w");
					FILE *histoavaSout=fopen("histograma_avaS.dat","w");
					FILE *histoavaTout=fopen("histograma_avaT.dat","w");
					FILE *histoavaLout=fopen("histograma_avaL.dat","w");
					FILE *histoavalocalSout=fopen("histograma_avalocalS.dat","w");
					FILE *histoavalocalTout=fopen("histograma_avalocalT.dat","w");
					//FILE *histovellocallocallastout=fopen("histograma_velocidades_locallocal_last.dat","w");
					//gsl_histogram_fprintf (histovelout, histovel , "%g", "%g");
					//fprintf(histovelout,"\n\n");
					//gsl_histogram_reset (histovel);
					//fclose(histovelout);
					
					if(histovellastout!=NULL && histovellocallastout!=NULL &&
					   histoavaSout!=NULL && histoavaTout!=NULL &&
					   histoavalocalSout!=NULL && histoavalocalTout!=NULL 
					){
					histogram_fprintf (histovellastout, histovel);
					histogram_fprintf (histovellocallastout, histovellocallocal);
					//std::cout << "events=" << gsl_histogram_sum (histoavaS) << std::endl;
					histogram_fprintf (histoavaSout, histoavaS);
					histogram_fprintf (histoavaLout, histoavaL);
					histogram_fprintf (histoavaTout, histoavaT);
					histogram_fprintf (histoavalocalSout, histoavalocalS);
					histogram_fprintf (histoavalocalTout, histoavalocalT);
					//histogram_fprintf (histovellocallocallastout, histovellocallocal);
					}
					else{
						save_state(u, f, U, tow, tow2);
		    			std::cerr << "Error saving partial histograms" << std::endl;
		    			exit(-1);
					}

					fclose(histovellastout);
					fclose(histovellocallastout);
					fclose(histoavaSout);
					fclose(histoavaTout);
					fclose(histoavalocalSout);
					fclose(histoavalocalTout);
					fclose(histoavaLout);
					//fclose(histovellocallocallastout);

					//histogram_fprintf (histovelout, histovel);
					//histogram_fprintf (histovellocalout, histovellocal);
				}
			}
			timer_props_histograms+=tparts.elapsed();
			timer_props_elapsed+=t.elapsed();
			POP_RANGE



			////////////////////////////////////////////////////////////////////////
			PUSH_RANGE("AVALANCHE DETECTION",5)	
			// separating avalanches: (VERYSMALLVELOCITY could be zero...)
			if(velocity<VERYSMALLVELOCITY) {
				//std::cout << "kick!" << std::endl;

				navalanchas++;
				//propsout << velocity << std::endl; propsout.flush();

				PUSH_RANGE("KICKING",6)	
				try{fill(u_it0,u_it1,dwM2);}
				catch(thrust::system_error &e)
				{
 				// output an error message and exit
    				std::cerr << "Error kicking velocities: " << e.what() << std::endl;
    				exit(-1);
				};
				POP_RANGE
				
				PUSH_RANGE("FINAL DISPLACEMENT",7)	
				try{Snew = reduce(U_it0,U_it1,REAL(0.0));} //center of mass velocity*L
				catch(thrust::system_error &e)
				{
 				// output an error message and exit
    				std::cerr << "Error computing final displacement: " << e.what() << std::endl;
    				exit(-1);
				};
				POP_RANGE

			
				PUSH_RANGE("PRINTING AVALANCHE",8)
				int lnew;	
				if(Snew-Sold > MINAVALANCHESIZE){
					using namespace placeholders;
					float lmin = 1.0/AAA; //powf(MINAVALANCHESIZE,10./(1.0+1.25));
					lnew=inner_product(U.begin(),U.end(),Uava0.begin(),0,plus<REAL>(),difference_larger_than(lmin));

					avalanche_duration_out << nstart*TIMESTEP << std::endl;
					avalanche_size_out << Snew-Sold << " " << lnew << " " << nstart*TIMESTEP << std::endl;

					gsl_histogram_increment (histoavaL,lnew);
					gsl_histogram_increment (histoavaS,Snew-Sold);
					gsl_histogram_increment (histoavaT,(nstart*TIMESTEP));
					//gsl_histogram_increment (histoavaS,Snew-Sold);

					tparts.restart();
					//std::cout << "big avalanche: " << Snew-Sold << " " << lnew << std::endl;
					nbigavalanchas++;

					#ifdef PRINTCONFS
					if(Snew-Sold> MINAVALANCHESIZEFORPRINTING) print_configuration(U,Uava0,1024,configs_out);
					#endif

					copy(U.begin(),U.end(),Uava0.begin());

					timer_props_printconfs+=tparts.elapsed();
				}			
				POP_RANGE

				PUSH_RANGE("MEASURING AVALANCHE",8)	
				#ifdef MEASURE_AVALANCHE_SHAPE
				//std::cout << "measuring avalanche shape..." << std::endl;
				if(Snew-Sold > MINAVALANCHESIZEFORVELOCITIES && lnew<(TAMANIO)){
					for(int k=0;k<avalanche_vec.size();k++){ 
						//lastavout << avalanche_vec[k] << " " << fraction_vec[k] << std::endl;
						//lastavout << avalanche_vec[k] << std::endl;
						//lastavout << "\n\n";
						gsl_histogram_increment (histovel,avalanche_vec[k]);
						gsl_histogram_increment(histovellocallocal,local_avalanche_vec[k]);
					}
					//lastavout << "\n\n";
				}
				avalanche_vec.clear();
				local_avalanche_vec.clear();
				avalanche_vec.push_back(dwM2);
				local_avalanche_vec.push_back(dwM2);
				//fraction_vec.clear();
				//fraction_vec.push_back(0);
				#endif
				Sold=Snew;
				nstart=0; 				
				POP_RANGE

			}
			// local avalanches
			if(vlocal<VERYSMALLVELOCITY){
				Slocalnew=U[1]; 
				if(Slocalnew-Slocalold > MINAVALANCHESIZE){				
					gsl_histogram_increment (histoavalocalS,Slocalnew-Slocalold);
					gsl_histogram_increment (histoavalocalT,(nstartlocal*TIMESTEP));				
				}			
				nstartlocal=0;
				Slocalold=Slocalnew;
			}	
			POP_RANGE
		}

#ifdef PARTIALTIMMINGS
		if(n%TIMINGS==0) print_timmings(n);
#endif
	}

    	gsl_histogram_free (histovel);
    	gsl_histogram_free (histovellocal);
    	gsl_histogram_free (histovellocallocal);

	fclose(histovelout);
	fclose(histovellocalout);
	fclose(histovellocallocalout);

	print_timmings(n);

	return 0;
}

// resultados del timming
void print_timmings(int m)
{	
		int n=m+1;
		double total_time = (timer_fuerzas_elapsed+timer_euler_elapsed+timer_props_elapsed);

		std::cout << "L= " << L << " TRUN= " << n << std::endl;

		std::cout << "Forces calculation -> " << 1e3 * timer_fuerzas_elapsed/n << " miliseconds ("
			  << int(timer_fuerzas_elapsed*100/total_time) << "%)" << std::endl;

		std::cout << "\tStochastic part -> " << 1e3 * timer_stochastic/n << " miliseconds ("
			  << int(timer_stochastic*100/total_time) << "%)" << std::endl;

		std::cout << "\tDeterministic part -> " << 1e3 * timer_deterministic/n << " miliseconds ("
			  << int(timer_deterministic*100/total_time) << "%)" << std::endl;

		std::cout << "Euler step -> " << 1e3 * timer_euler_elapsed/n << " miliseconds ("
			  << int(timer_euler_elapsed*100/total_time) << "%)" << std::endl;

		double timer_props_elapsed0 = timer_props_velocity+timer_props_minmaxvelocity+
				timer_props_activity + timer_props_roughness + timer_props_roughnessF +
				timer_props_Ucm + timer_props_Fcm + timer_props_towmax + timer_props_histograms
				+timer_props_printconfs;
		std::cout << "Properties -> " << 1e3 * timer_props_elapsed/n << " miliseconds ("
			  << int(timer_props_elapsed*100/total_time) << "%)" << std::endl;

			std::cout << "\t Properties (velocity) -> " << 1e3 * timer_props_velocity/n << " miliseconds ("
			  << int(timer_props_velocity*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (activity) -> " << 1e3 * timer_props_activity/n << " miliseconds ("
			  << int(timer_props_activity*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (roughness) -> " << 1e3 * timer_props_roughness/n << " miliseconds ("
			  << int(timer_props_roughness*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (roughnessF) -> " << 1e3 * timer_props_roughnessF/n << " miliseconds ("
			  << int(timer_props_roughnessF*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (Ucm) -> " << 1e3 * timer_props_Ucm/n << " miliseconds ("
			  << int(timer_props_Ucm*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (Fcm) -> " << 1e3 * timer_props_Fcm/n << " miliseconds ("
			  << int(timer_props_Fcm*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (towmax) -> " << 1e3 * timer_props_towmax/n << " miliseconds ("
			  << int(timer_props_towmax*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (minmaxvelocity) -> " << 1e3 * timer_props_minmaxvelocity/n << " miliseconds ("
			  << int( timer_props_minmaxvelocity*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (histograms) -> " << 1e3 * timer_props_histograms/n << " miliseconds ("
			  << int( timer_props_histograms*100/timer_props_elapsed0) << "%)" << std::endl;

			std::cout << "\t Properties (printconfs) -> " << 1e3 * timer_props_printconfs/n << " miliseconds ("
			  << int( timer_props_printconfs*100/timer_props_elapsed0) << "%)" << std::endl;


		std::cout << "Total -> " << 1e3 * total_time/n << " miliseconds (100%)" << std::endl << std::endl << std::endl;
}
