prefix="avalanches_"
for file in T.dat S.dat #someprops.dat
do
	discard=$1
	rawfile=$prefix$file
	archivo=$rawfile.sorted

	tail -n +$discard $rawfile > $file 

	#for Kay
	#tail -n +$discard $rawfile | tail -n 10000 | sort -g > $archivo
	tail -n +$discard $rawfile | sort -g > $archivo

	min=$(head -n 1 $archivo)
	max=$(tail -n 1 $archivo)
	sm=$(gawk '{s2+=$1*$1; s1+=$1}END{print s2*0.5/s1}' $archivo)
	events=$(wc -l $archivo | gawk '{print $1}')
	echo $archivo, $min, $max, $sm, "eventos="$events

gnuplot -persist << EOFGNU
		reset
		file='$archivo'
		min=abs($min)
		max=$max
		nroevents=$events
		load 'histograma.gnu'
EOFGNU

done

# calcuate Sm = <S^2> / 2<S>
# ABBM S^{-3/2} e^{-S/4Sm}
#normalization=???
