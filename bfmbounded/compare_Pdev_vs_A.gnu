set logs 

set multi lay 1,2
set title 'L=256'
set xlabel 'vcm'
set ylabel 'vcm P(vcm)'
plot [1e-8:][1e-06:1]\
'histograma_velocidades_cm_last.dat' u 1:($3/$4) w lp t 'A=100(run, small dt)',\
'A10_L256_dwM20.00001/histograma_velocidades_cm_last.dat' u 1:($3/$4) w lp t 'A=10 (large dt)',\
'A10_L256/histograma_velocidades_cm_last.dat' u 1:($3/$4) w lp t 'A=10  (large m2dw)',\
'A50_L256/histograma_velocidades_cm_last.dat' u 1:($3/$4) w lp t 'A=50',\
'A100_L256/histograma_velocidades_cm_last.dat' u 1:($3/$4) w lp t 'A=100',\
'A200_L256/histograma_velocidades_cm_last.dat' u 1:($3/$4) w lp t 'A=200',\
1*x**1.4

set xlabel 'vloc'
set ylabel 'vloc P(vloc)'
plot [1e-8:][1e-06:1]\
'histograma_velocidades_local_last.dat' u ($1/256.):($3/$4) w lp t 'A=100(run, small dt)',\
'A10_L256_dwM20.00001/histograma_velocidades_local_last.dat' u ($1/256.):($3/$4) w lp t 'A=10 (large dt)',\
'A10_L256/histograma_velocidades_local_last.dat' u ($1/256.):($3/$4) w lp t 'A=10 (large m2dw)',\
'A50_L256/histograma_velocidades_local_last.dat' u ($1/256.):($3/$4) w lp t 'A=50',\
'A100_L256/histograma_velocidades_local_last.dat' u ($1/256.):($3/$4) w lp t 'A=100',\
'A200_L256/histograma_velocidades_local_last.dat' u ($1/256.):($3/$4) w lp t 'A=200',\
x**1.0, x**2.0, x**3.0, 0.1*(x/1e-6)**(-12.)
unset multi
