void histogram_fprintf (FILE * histovelout, gsl_histogram *histovel)
{
	double suma=gsl_histogram_sum(histovel); 
	size_t nrobins=gsl_histogram_bins(histovel);	

	for(int i=0;i<nrobins;i++)
	{
		 double upper,lower,bin;
		 gsl_histogram_get_range(histovel, i, &lower, &upper);
		 bin=gsl_histogram_get(histovel,i);
		 fprintf(histovelout,"%g %g %g %g\n", lower, upper,bin, suma); 
	}
	fprintf(histovelout,"\n\n");
}
