/*
Dado un vector real me devuelve otro conteniendo 
la fuerza elastica de largo alcance, 
cada vez que llamo a la funcion....

u_input, u_output: memoria alocada afuera
frequencies, u_fouriers: estado interno

*/

#ifdef OMP

#include <omp.h>
#include <complex>
#include<fftw3.h>

#else

#include "cutil.h"
#include <cufft.h>
typedef cufftReal REAL;
typedef cufftComplex COMPLEX;

#ifndef SIGMA
#define SIGMA	1.0
#endif

#endif

// functor usado para calcular la rugosidad 
struct longrange_functor
{
    __device__
    COMPLEX operator()(COMPLEX c, REAL r)
    {	
    	COMPLEX res;

    	float f=(r>0)?(-powf(fabs(2.0*sinf(r*0.5)),SIGMA)):(0.0);
    	res.x = c.x*f;
    	res.y = c.y*f;
		return res;
    }
};	

class long_range_class{
	public:

	int LL; // size of the 1d transform
	REAL *u; // displacement field

	int Ncomp;	// number of complex components

	// containers for transform
	thrust::device_vector<COMPLEX> u_fourier;

	// containers for convolution
	thrust::device_vector<REAL> frequencies_fft;

	thrust::device_vector<REAL> fel;
	REAL *fel_raw;

	// transform handle
	cufftHandle plan_d2z;
	// antitransform handle
	cufftHandle plan_z2d;

	long_range_class(REAL *u_input_, int L_):
		u(u_input_),LL(L_){

		// plan de trasnformada
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_d2z,LL,CUFFT_R2C,1));

		// plan de antitransformada para interacciones de largo alcance
		CUFFT_SAFE_CALL(cufftPlan1d(&plan_z2d,LL,CUFFT_C2R,1));

		Ncomp=LL/2+1;

		u_fourier.resize(Ncomp);
		COMPLEX zero; zero.x=zero.y=0.0;
		thrust::fill(u_fourier.begin(),u_fourier.end(),zero);

		fel.resize(LL);
		fel_raw=(REAL *) thrust::raw_pointer_cast(&fel[0]);

		frequencies_fft.resize(Ncomp);		
		thrust::host_vector<REAL> hfreq(Ncomp);		
		for(int i=0;i<Ncomp;i++) hfreq[i]=i*2.0*M_PI/LL;
		//for(int i=L/2;i<L;i++) hfreq[i]=(-L/2+i)*2.0*M_PI/L;
		thrust::copy(hfreq.begin(),hfreq.end(),frequencies_fft.begin()); 

		self_test();
	};

	~long_range_class(){
		cufftDestroy(plan_d2z);
		cufftDestroy(plan_z2d);
	}

	void transform_fourier(thrust::device_vector<REAL> &vinp, thrust::device_vector<REAL> &vout)
	{
		REAL * vinp_raw= (REAL *) thrust::raw_pointer_cast(&vinp[0]);
		REAL * vout_raw= (REAL *) thrust::raw_pointer_cast(&vout[0]);
		transform_fourier(vinp_raw, vout_raw);		
	}


	void transform_fourier(REAL *u_input, REAL * u_output2)
	{
		//REAL *u_input=u;
		COMPLEX *u_output=(COMPLEX *)thrust::raw_pointer_cast(u_fourier.data());

		CUFFT_SAFE_CALL(cufftExecR2C(plan_d2z, u_input, u_output));

		// fix zero mode to zero
		//COMPLEX zero; zero.x=zero.y=0.0; u_fourier[0]=zero;

		// convolution product
		thrust::transform(
			u_fourier.begin(),u_fourier.end(),
			frequencies_fft.begin(),u_fourier.begin(),
			longrange_functor()
		);


		// long range elastic force --> u_output
		COMPLEX *u_input2=(COMPLEX *) thrust::raw_pointer_cast(&u_fourier[0]);
		//REAL * u_output2=(REAL *) thrust::raw_pointer_cast(&fel[0]);;

		CUFFT_SAFE_CALL(cufftExecC2R(plan_z2d, u_input2, u_output2));

		using namespace thrust::placeholders;
		thrust::device_ptr<REAL> u2(u_output2);
		//thrust::transform(thrust::device, u_output2, u_output2+LL, u_output2, _1/float(LL));
		thrust::transform(u2, u2+LL, u2, _1/float(LL));
	};

	REAL * get_fel(){
		transform_fourier(u,fel_raw);
		return (REAL *)(thrust::raw_pointer_cast(&fel[0]));
	}

	void self_test(){

		std::cout << "fourier transform self test...." << std::endl;	
	
		thrust::device_vector<REAL> aux_input(LL,0.0);
		for(int i=0;i<LL;i++) aux_input[i]=1.0+cos(2.0*M_PI*8*i/LL);
	
		thrust::device_vector<REAL> aux_output(LL,0.);

		transform_fourier(aux_input,aux_output);

		std::ofstream fout("testconv.dat");
		for(int i=0;i<LL;i++) fout << aux_input[i] << " " << aux_output[i]  << std::endl;

		std::cout << "LR initialized" << std::endl;	
		//exit(1);		
	}

};