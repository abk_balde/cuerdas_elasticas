#!/bin/bash
### Job settings
#SBATCH --job-name cuerda
#SBATCH --nodes 1
#SBATCH --ntasks 1
#SBATCH --gres=gpu:1

### Environment setup
. /etc/profile
module load cuda/5.0

### Run tasks
srun cat /proc/cpuinfo > cpuinfo.dat
srun deviceQuery > gpuinfo.dat

echo
echo "== CUDA performance"
srun --gres=gpu:1 -n1 --exclusive qew_CUDA 

echo
export OMP_NUM_THREADS=16
srun echo "== OMP performance:"
srun ./qew_OMP
echo

wait
