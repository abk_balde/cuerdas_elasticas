#!/bin/bash
### Job settings
#SBATCH --job-name cu(er)da
#SBATCH --nodes 1
#SBATCH --ntasks 1
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=1

### Environment setup
. /etc/profile
module load cuda/5.0

### Run tasks
srun cat /proc/cpuinfo > cpuinfo.dat
srun deviceQuery > gpuinfo.dat

#echo
#echo "== CUDA performance"
#time srun --gres=gpu:1 -n1 --exclusive qew_CUDA 
#echo "=="
#echo

wait
