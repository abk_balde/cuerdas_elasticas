#for A in  100.0 50.0 10.0 5.0 1.0 0.5 0.1 0.05 0.01
#for TIMESTEP in 0.005 0.001 0.0005 0.0001 0.00005 0.00001
for TIMESTEP in 0.005 0.001 0.0005 0.0001 0.00005 0.00001
do 
	make clean
	make EXTRA="-DTIMESTEP=$TIMESTEP -DA=5.0"
	optirun ./qew_CUDA
	./histogramador.sh 1000
	mkdir new_A5.0dt$TIMESTEP
	cp *.dat* new_A5.0dt$TIMESTEP/
done 
