#include <cmath>
//#include<stdio.h>
#define MAXCOUNTER	2147483647	

#define DEBUG

// ia=orden, tid=thread id, counter=counter privado del thread
__device__
float gamdev(int ia,int tid, unsigned long &counter,unsigned long &counter2)
{
	RNG rng;       // random number generator
	// keys and counters
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	k[0]=tid;

	//float ran1(long *idum);
	//void nrerror(char error_text[]);
	int j;
	float am,e,s,v1,v2,x,y;

#ifdef DEBUG
	if (ia < 1) {printf("Error in routine gamdev (ia<1)\n");}
#endif
	if (ia < 6) {
		x=1.0;
		for (j=1;j<=ia;j++) {

			c[1]=uint32_t(counter2);
			c[0]=uint32_t(counter); 
			if(counter+1<MAXCOUNTER) counter++; 
			else {counter=1;counter2++;};
			r = rng(c, k);

			x *= u01_open_open_32_53(r[0]);
		}
#ifdef DEBUG
		if (x<=0.0f) {printf("Error in routine gamdev (x<=0)\n");}
#endif
		x = -log(x); //--> x no puede ser <= 0.0!
	} else {
		do {
			do {
				do {
					c[1]=uint32_t(counter2);
					c[0]=uint32_t(counter); 
					if(counter+1<MAXCOUNTER) counter++; 
					else {counter=1;counter2++;};
					r = rng(c, k);

					v1=2.0*u01_open_open_32_53(r[0])-1.0;
					v2=2.0*u01_open_open_32_53(r[1])-1.0;
				} while (v1*v1+v2*v2 > 1.0);
				y=v2/v1;
				am=ia-1;
				s=sqrt(2.0*am+1.0);
				x=s*y+am;
			} while (x <= 0.0);
#ifdef DEBUG
			if (x<=0.0f) {printf("Error in routine gamdev (x<=0)\n");}
			if (am<=0.0f) {printf("Error in routine gamdev (am<=0)\n");}
#endif
			e=(1.0+y*y)*exp(am*log(x/am)-s*y);// --> Si x=am -> s*y>=0.0

			c[1]=uint32_t(counter2);
			c[0]=uint32_t(counter); 
			if(counter+1<MAXCOUNTER) counter++; 
			else {counter=1;counter2++;};
			r = rng(c, k);

		} while (u01_open_open_32_53(r[0]) > e);
	}
	return x;
}
/* (C) Copr. 1986-92 Numerical Recipes Software p,{2. */

__device__
float gammln(float xx)
{
	double x,y,tmp,ser;
	//static 
	double cof[6]={76.18009172947146,-86.50532032941677,
		24.01409824083091,-1.231739572450155,
		0.1208650973866179e-2,-0.5395239384953e-5};
	int j;

	y=x=xx;
	tmp=x+5.5;
#ifdef DEBUG
	if (tmp<=0.0f) {printf("Error in routine gammln (tmp<=0)\n");}
#endif
	tmp -= (x+0.5)*log(tmp);// tmp>0.0 -> x>-5.5 
	ser=1.000000000190015;
	for (j=0;j<=5;j++) ser += cof[j]/++y;

#ifdef DEBUG
	if (ser/x<=0.0f) {printf("Error in routine gammln (ser/x<=0)\n");}
#endif

	return -tmp+log(2.5066282746310005*ser/x); // x>0.0, ser>0.0
}
/* (C) Copr. 1986-92 Numerical Recipes Software p,{2. */
/*
typedef double Doub;
__device__
Doub gammln(const Doub xx) {
	Int j;
	Doub x,tmp,y,ser;
	static const Doub cof[14]={57.1562356658629235,-59.5979603554754912,
	14.1360979747417471,-0.491913816097620199,.339946499848118887e-4,
	.465236289270485756e-4,-.983744753048795646e-4,.158088703224912494e-3,
	-.210264441724104883e-3,.217439618115212643e-3,-.164318106536763890e-3,
	.844182239838527433e-4,-.261908384015814087e-4,.368991826595316234e-5};
	if (xx <= 0) printf("bad arg in gammln\n");
	y=x=xx;
	tmp = x+5.24218750000000000;
	tmp = (x+0.5)*log(tmp)-tmp;
	ser = 0.999999999999997092;
	for (j=0;j<14;j++) ser += cof[j]/++y;
	return tmp+log(2.5066282746310005*ser/x);
}
*/

//#define SIMPLEPOIDEV 
#ifdef SIMPLEPOIDEV //but not working...
// see  http://en.wikipedia.org/wiki/Poisson_distribution
__device__
float poidev(float xm, int tid, unsigned long &counter,unsigned long &counter2)
{
	RNG rng;      // random number generator
	// keys and counters
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	if(xm<12.0){// dudoso...
		k[0]=tid;
		c[1]=uint32_t(counter2);
		c[0]=uint32_t(counter); 
		if(counter+1<MAXCOUNTER) counter++; 
		else {counter=1;counter2++;};
		r = rng(c, k);

		float uu,xx,pp,ss;
		xx=0.f; pp=ss=exp(-xm);
		r = rng(c, k);
		uu=u01_open_closed_32_53(r[0]); 
		while(uu>ss){
			xx++;
			pp=pp*xm/xx;
			ss=ss+pp;
		}
		return xx;
	} 
	else{
		float ll,uu, pp; unsigned int kk;
		ll=exp(-xm);kk=0; pp=1.f;
		do{
			kk++;
			c[1]=uint32_t(counter2);
			c[0]=uint32_t(counter); 
			if(counter+1<MAXCOUNTER) counter++; 
			else {counter=1;counter2++;};
			r = rng(c, k);
			uu=u01_open_closed_32_53(r[0]); 
			pp=pp*uu;
		}while(pp>ll);
		return kk-1;
	}
/*
	float uu,xx,pp,ss;
	uu=u01_open_closed_32_53(r[0]); 
	xx=0.f; pp=ss=expf(-xm);
	while(uu>ss){
		xx++;
		pp=pp*xm/xx;
		ss=ss+pp;
	}
	return xx;
*/
}
#else

#define PI 3.141592654
__device__
float poidev(float xm, int tid, unsigned long &counter,unsigned long &counter2)
{

	//__device__ float gammln(float xx);
	//float ran1(long *idum);
	//static 
	float sq,alxm,g,oldm=(-1.0);
	float em,t,y;

	RNG rng;       // random number generator
	// keys and counters
    	RNG::ctr_type c={{}};
    	RNG::key_type k={{}};
	RNG::ctr_type r;

	k[0]=tid;

	if (xm < 12.0) 
	{
		if (xm != oldm) 
		{
			oldm=xm;
			g=exp(-xm);
		}
		em = -1;
		t=1.0;
		do {
			++em;

			c[1]=uint32_t(counter2);
			c[0]=uint32_t(counter); 
			if(counter+1<MAXCOUNTER) counter++; 
			else {counter=1;counter2++;};
			r = rng(c, k);

			t *= u01_open_open_32_53(r[0]);
		} while (t > g);
	} 
	else {
		if (xm != oldm) 
		{
			oldm=xm;
			sq=sqrt(2.0*xm);
			alxm=log(xm);
			g=xm*alxm-gammln(xm+1.0);
		}
		do {
			do {
				c[1]=uint32_t(counter2);
				c[0]=uint32_t(counter); 
				if(counter+1<MAXCOUNTER) counter++; 
				else {counter=1;counter2++;};
				r = rng(c, k);

				float rn=u01_open_open_32_53(r[0]);
#ifdef DEBUG
				//if (rn==0.5) {printf("Error in routine poidev (rn==0.5)\n");}
#endif
				y=tan(PI*rn); // si RN=0.5 da infinito!!
				em=sq*y+xm;

				/* BUG FIX in FORTRAN
				call ran2(harvest)
				y=tan(PI*harvest)
				em=sq*y+xm
				em=int(em)
				if ((em >= 0.0)) exit
				end do
				*/
				// -> FIX: 
				//em=int(em);

			} while (em < 0.0);

			em=floor(em);
			t=0.9*(1.0+y*y)*exp(em*alxm-gammln(em+1.0)-g);

			c[1]=uint32_t(counter2);
			c[0]=uint32_t(counter); 
			if(counter+1<MAXCOUNTER) counter++; 
			else {counter=1;counter2++;};
			r = rng(c, k);

		} while (u01_open_open_32_53(r[0]) > t);
	}
	return em;
}
#undef PI
#endif

/* (C) Copr. 1986-92 Numerical Recipes Software p,{2. */
