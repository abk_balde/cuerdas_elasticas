set title 'L=128, A=1'
set multi layout 1,2
set xlabel 'S'
set ylabel 'P(S)'
plot './histo.avalanches_S.dat.sorted' w p t '', \
500*x**(-3./2.)*exp(-x/(4*46.)) t 'ABBM', \
50*x**(-1.1) t 'tau=1.1'#,\
#55*x**(-1.5) t 'tau=1.3' lt -1

set ylabel 'T'
set ylabel 'P(T)'
plot './histo.avalanches_T.dat.sorted' w p t '',\
1900*x**(-1.5) t 'tau_T=1.5'#,\
#exp(-x/(4*5609))
unset multi
