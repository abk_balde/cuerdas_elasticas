set multi lay 1,3

plot [0.0001:][1000:] './L128/histo.someprops.dat.sorted' w his, \
'./L256/histo.someprops.dat.sorted' w his, \
'./L1024/histo.someprops.dat.sorted' w his, \
'./L2048/histo.someprops.dat.sorted' w his

plot [5:] './L128/histo.avalanches_S.dat.sorted' w his,\
'./L256/histo.avalanches_S.dat.sorted' w his,\
'./L1024/histo.avalanches_S.dat.sorted' w his,\
'./L2048/histo.avalanches_S.dat.sorted' w his

plot [5:] './L128/histo.avalanches_T.dat.sorted' w his,\
'./L256/histo.avalanches_T.dat.sorted' w his,\
'./L1024/histo.avalanches_T.dat.sorted' w his,\
'./L2048/histo.avalanches_T.dat.sorted' w his


unset multi

