\documentclass[a4paper,11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{amsmath}

\title{Bounded Brownian Force Interface Model}
\author{}

\begin{document}

\maketitle
\tableofcontents

%\begin{abstract}
%\end{abstract}

\section{Idea and Derivation}

\begin{eqnarray}
  \partial_t u = \partial_x^2 u + F(u,x) + m^2 (vt-u)
\end{eqnarray}

we propose a quenched force,
\begin{eqnarray}
  F(u,x) = \int_0^u du'\; \exp[-A(u-u')] \eta(u',x)
\end{eqnarray}

with 
\begin{equation}
  \overline{\eta(u_1,x)\eta(u_2,x')}=2 A \delta(x-x')\delta(u_1-u_2)
\end{equation}

such that, if $u'>u$: 

\begin{eqnarray}
  \overline{F(u,x)F(u',x')}  
  &=& \int_0^u du_1 \int_0^{u'} du_2\; \exp[-A(u+u'-u_1-u_2)] 
  \overline{\eta(u_1,x)\eta(u_2,x')} \nonumber \\
  &=& 2A \delta(x-x') \int_0^u du_1 \;\exp[-A(u+u'-2 u_1)] 
  \nonumber \\ 
  &=& \delta(x-x') \exp[-A(u+u')](\exp[2Au]-1)   
  \nonumber \\ 
  &=& 
   \delta(x-x') (\exp[-A(u'-u)]-\exp[-A(u+u')]) 
\end{eqnarray}
so in the stationary state
\begin{eqnarray}
  \overline{F(u,x)F(u',x')}  
  &=& 
   \delta(x-x') \exp[-A|u'-u|] = \delta(x-x') \Delta[u'-u]
\end{eqnarray}

The force thus satisfies the equation
\begin{eqnarray}
  \partial_u F = -A F + \eta(u,x) 
\end{eqnarray}

To write a time evolution for the pinning force we define the noise 
$\xi(x,t) \equiv \sqrt{|{\dot u}(x,t)|A^{-1}}\; \eta(x,u(x,t))$, 
so it is a normal Langevin noise
\begin{eqnarray}
\overline{\xi(x,t)\xi(x',t')} &=& A^{-1}|{\dot u}(x,t)|\; \overline{\eta(x,u(x,t))\eta(x',u(x',t'))}
\nonumber \\ &=&  2 \delta(x-x') |{\dot u}(x,t)|\delta(u(x,t')-u(x,t)) 
\nonumber \\ &=&
2 \delta(x-x') \delta(t-t')
\end{eqnarray}

where in the third equality we have used 
$|{\dot u}(x,t)|\delta(u(x,t')-u(x,t))=\delta(t-t')$, as the function 
$f(t)=u(x,t')-u(x,t)$ has only one root at $t=t'$ by virtue of 
the non-passing rule, and thus $\delta(f(t))=\delta(t-t')/|f'(t)|$.
Therefore
\begin{eqnarray}
  {\dot F} = {\dot u}\partial_u F = -A F {\dot u} + \sqrt{A|\dot u(x,t)|} \xi(x,t) 
\end{eqnarray}

We can thus finally write a closed system of equations for $F$ and ${\dot u}$:

\begin{eqnarray}
\partial_t \dot{u} &=& \partial_x^2 \dot{u} + {\dot F} + m^2 (v-\dot{u}) \\
  {\dot F} &=& -A F {\dot u} + \sqrt{A |\dot u|} \;\xi(x,t) 
\end{eqnarray}

\section{Stochastic Euler method}

Solve, iteratively, a finite difference system for $\dot{u}^n_i \equiv \dot{u}(x_i,t_n)$, 
and $\dot{F}^n_i \equiv \dot{F}(u(x_i,t_n),x_i)$:

\begin{eqnarray}
  {F}^{n+1}_i &=& {F}^n_i - 
  \delta t\;A F^n_i \dot{u}_i^n + \sqrt{2 A |\dot{u}_i^{n}| \delta t} \;R^n_i \\
\dot{u}_i^{n+1} &=& \dot{u}_i^{n} + \delta t \;[\delta_x^2 \dot{u}_i^{n} + 
 m^2 (v-\dot{u}_i^{n})] + {F}^{n+1}_{i}-{F}^n_{i}  
\end{eqnarray}

where $R^n_i$ are uncorrelated normally distributed random numbers, 
$\overline{R^n_i}=0$, $\overline{R^n_i R^m_j}=\delta_{ij}\delta_{nm}$, 
and $\delta_x^2$ is the discrete laplacian operator, 
$$\delta_x^2 u_i = u_{i+1}+u_{i-1}-2u_i$$

The system of equations can be easily solved in parallel (eg in a GPU), by assigning each thread 
the task of computing the force and update each particle.


% The above can be written
% \begin{eqnarray}
% \dot{u}_i^{n+1} &=& \dot{u}_i^{n} + \delta t \;[\delta_x^2 \dot{u}_i^{n} + 
%  m^2 (v-\dot{u}_i^{n})] + \delta t\;A F^n_i \dot{u}_i^n + \sqrt{2 A \dot{u}_i^{n} \delta t} \;R^n_i \\
%   {F}^{n+1}_i &=& {F}^n_i - 
%   \delta t\;A F^n_i \dot{u}_i^n + \sqrt{2 A \dot{u}_i^{n} \delta t} \;R^n_i 
% \end{eqnarray}

\section{More Accurate Solver}
The Fokker-Planck solution associated to the stochastic equation 
\begin{equation}
  \partial_t \rho = \alpha + \beta \rho + \sigma \sqrt{\rho} \xi
\end{equation}
is 
\begin{equation}
  P(\rho,t)=\lambda e^{-\lambda(\rho_0 e^{\beta t+\rho})} 
  \left[ \frac{\rho}{\rho_0 e^{\beta t}} \right]^{\mu/2} 
  I_{\mu} \left( 2\lambda \sqrt{\rho_0 \rho e^{\beta t}} \right)
\end{equation}
with $\lambda = \frac{2\beta}{\sigma^2 (e^{\beta t}-1)}$, $\mu=-1+\frac{2\alpha}{\sigma^2}$, 
and $P(\rho,t=0)=\delta(\rho-\rho_0)$.\\


Comparing with
\begin{eqnarray}
  {\dot F} = -A F {\dot u} + \sqrt{A|\dot u(x,t)|} \xi(x,t) 
\end{eqnarray}
we have $\alpha=0$, $\sigma^2=A$, $\beta=-A\dot{u}$.

\end{document}
