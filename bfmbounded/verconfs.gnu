if(i==0) \
istep=2;\
liminf=0.0;\
limsup=0.07;\
set cbrange [liminf:limsup];\
set palette model RGB;\
set term gif

tit=sprintf("tiempo=%d",i)
set title tit

#file=sprintf("file%d.gif",100000+i);
#set out file


plot [0:128][0:500] \
'./configuraciones.dat' index i u 0:1:3 w lp t ''  lc 0,\
'./configuraciones.dat' index 0:i:(istep*4) u 0:1:(($3)) w l t '' lc palette z

print i
if(i+istep<1000) i=i+istep; pause .1; reread

