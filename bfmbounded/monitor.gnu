#events=$(wc -l $archivo | gawk '{print $1}')

#gnuplot -persist << EOFGNU

set multi lay 3,2; 
#set tit sprintf("%d Events\n")

l=512

unset logs
#plot 'someprops.dat' u ($1*100):($2) w l t col; 
#p 'someprops.dat' u ($1*100):9 w l t col, 'someprops.dat' u ($1*100):($3*(100./l)**4) w l t col ; 


set logs
plot [][:] 'histograma_velocidades_cm_last.dat' u (($1)):(($3/$4)) w l t 'vcm P(vcm)', x**1.45, x**1.2,\
'histograma_velocidades_local_last.dat' u (($1)):(($3/$4)) w l t 'vi P(vi)'
unset logs

set logs
plot [][:] 'histograma_avaL.dat' u (($1)):(($3/$4)) w his t 'L P(L)', 0.02/x**(1.25-1) 
plot [][:] 'histograma_avaS.dat' u (($1)):(($3/$4)) w his t 'S P(S)', 0.02/x**(1.11-1) 
plot [][:] 'histograma_avaT.dat' u (($1)):(($3/$4)) w his t 'T P(T)', 0.02/x**(1.17-1)
#plot [][:] 'histograma_avalocalS.dat' u (($1)):(($3/$4)) w his t 'Sloc P(Sloc)', 0.02/x**(0.4-1) 
#plot [][:] 'histograma_avalocalT.dat' u (($1)):(($3/$4)) w his t 'Tloc P(Tloc)', 0.02/x**(0.476-1)
unset logs

#plot [0:127] for[i=1:1000] 'configuraciones.dat' index i u 0:1:2 w filledcu t ''

pause 5
#n=n+1
#print n
reread


