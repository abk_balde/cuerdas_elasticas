#include <fstream>
#include <iostream>
#include <cmath>
#include <gsl/gsl_histogram.h>

// numerical recipes in C 2nd edition
#include "numrecinc.h"

// some parameters
#define dt          0.0001
#define VMIN		0.00000001
#define NBINS		1000
#define NSAMPLES	100000000
#define MAXS		100.0
#define MINS		0.01
#define MAXT		100.0
#define MINT		0.01
#define MAXVEL		100.0
#define MINVEL		0.00001


// comment the followinf line if you do not have it!
#define HAVE_GSL_LIBRARY

// this is just a wrapper class for making histograms with GSL library
// it uses logarithmic binning
#ifdef HAVE_GSL_LIBRARY
class gsl_his{
	public:
	gsl_his(int nbins_, Real a, Real b, const char * nom_):nbins(nbins_),nom(nom_)
	{
		h=gsl_histogram_alloc(nbins);
		double *range=(double *)malloc((nbins+1)*sizeof(double));
		for(int i=0;i<nbins+1;i++) range[i]=a*powf(b/a,i*1.0/nbins);
		gsl_histogram_set_ranges(h, range, nbins+1);	
		samples=0;
	};	
	~gsl_his()
	{
		gsl_histogram_free (h);	
		free(range);
	};
	void increment(Real x){
		samples++;
		gsl_histogram_increment (h,x);		
	}	
	void print(){
		fout.open(nom);
		for(int i=0;i<NBINS-1;i++)
		fout << h->range[i] << " " << h->range[i+1] << " " << h->bin[i]/(samples) << " " << samples << std::endl; 		
		//fout << "\n\n";
		fout.flush();
		fout.close();
	}
	private:
	const char *nom;	
	gsl_histogram * h;
	Real *range;
	std::ofstream fout;
	int nbins;
	long samples;
};
#endif


int test1(int argc, char **argv)
{
	long idum=12345;

	Real u,udot;
	Real arg;
	int poisson;
	Real T;

	// for making histograms
	#ifdef HAVE_GSL_LIBRARY
	gsl_his hS(NBINS,MINS,MAXS,"hist1part_S_parcial.dat");
	gsl_his hT(NBINS,MINT,MAXT,"hist1part_T_parcial.dat");
	gsl_his hvel(NBINS,MINVEL,MAXVEL,"hist1part_vel_parcial.dat");
	#endif

	for(int s=0;s<NSAMPLES;s++)
	{	
		T=0;
		u=0.0;
		udot=1.0;
		while(udot>VMIN)
		{
			arg= udot/dt;
			poisson=(arg>0.0f)?(int(poidev(arg,&idum))):(0);
			udot = (poisson<1)?(0.0):(dt*gamdev(poisson,&idum));
			udot = udot*(1.0-dt);
			u+=udot*dt;
			T+=dt;

			// for the histograms
			#ifdef HAVE_GSL_LIBRARY
			hvel.increment(udot);
			#endif	
		}	

		// for the histograms
		#ifdef HAVE_GSL_LIBRARY
		hS.increment(u);
		hT.increment(T);
		if((s+1)%10000==0){
			std::cout << "printing..." << std::endl;
			hvel.print();hS.print();hT.print();
		}
		#endif	

	}

	// compare with
	// f(x)=(1.0/(2.0*sqrt(pi)*x**1.5))*exp(-(1-x)**2/(4*x))
	// plot [][1e-3:] 'hist1part.dat' u 1:($3/($2-$1)) w his, 'hist1part.dat' u 2:($3/($2-$1)) w his, f(x)
	return 0;
}


int main(int argc, char **argv)
{
	test1(argc, argv);
	return 0;
}
