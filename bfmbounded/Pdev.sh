### procesa un histograma de velocidades

cat histograma_velocidades_cm_last.dat | \
gawk '{if(NF==4){vP[$1]+=$3;vel[$1]=$1;vel2[$1]=$2;}}\
END{sum=0.0; for(v in vel){sum=sum+vP[v];}; for(v in vel){dv=vel2[v]-v; if(dv>0.0) print v,vP[v]/(dv*sum);}}' | \
LC_NUMERIC="en_US.UTF-8" sort -k 1 -g 

